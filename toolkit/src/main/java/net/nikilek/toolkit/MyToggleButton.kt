package net.nikilek.toolkit

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.ToggleButton
import net.nikilek.toolkit.constant.CodeConst
import net.nikilek.toolkit.utility.LogUtil

import net.nikilek.toolkit.utility.ResourceHolder

/**
 * Created by nikilek on 2016/01/27.
 * カスタムビュー
 */
open class MyToggleButton : ToggleButton {

    private var textStyle: Int = Typeface.NORMAL //文字の表示スタイル

    private var backgroundOn: Int = 0 //ON時の背景リソースID
    private var backgroundOff: Int = 0 //OFF時の背景リソースID

    private var onCheckedChangeListener: OnCheckedChangeListener? = null

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context)

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        getAttrs(attrs)
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        getAttrs(attrs)
    }

    /**
     * 属性の取得
     * @param attrs 属性セット
     */
    private fun getAttrs(attrs: AttributeSet) {
        textStyle = attrs.getAttributeIntValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "textStyle", textStyle)
        val attrsOriginal = context.obtainStyledAttributes(attrs, R.styleable.MyToggleButton)
        this.backgroundOn = attrsOriginal.getResourceId(R.styleable.MyToggleButton_backgroundOn, 0)
        this.backgroundOff = attrsOriginal.getResourceId(R.styleable.MyToggleButton_backgroundOff, 0)
        attrsOriginal.recycle()
    }

    public override fun onFinishInflate() {
        super.onFinishInflate()
        //if (this.isInEditMode) {
        //    return
        //}
        //フォントの設定
        setFont(context)
        //背景の設定
        refreshBackground()
        //チェック状態更新時のイベントを設定
        super.setOnCheckedChangeListener { buttonView, isChecked ->
            //外部から設定されたリスナに通知
            onCheckedChangeListener?.onCheckedChanged(buttonView, isChecked)
            //背景の更新
            refreshBackground()
        }
    }

    override fun setOnCheckedChangeListener(listener: OnCheckedChangeListener?) {
        onCheckedChangeListener = listener
    }

    /**
     * フォントを設定
     * @param context コンテキスト
     */
    private fun setFont(context: Context?) {
        try {
            //システム定義のフォントを設定
            if (textStyle == Typeface.BOLD) {
                this.typeface = ResourceHolder.Typefaces.getBold(context!!)
            } else {
                this.typeface = ResourceHolder.Typefaces.getDefault(context!!)
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * 背景のリソースを更新
     */
    private fun refreshBackground() {
        if (backgroundOn != 0 && backgroundOff != 0) {
            setBackgroundResource(if (isChecked) backgroundOn else backgroundOff)
        }
    }

    /**
     * 背景のリソースを設定
     * @param resIdOn check=ON時のbackgroundリソースID
     * @param resIdOff check=OFF時のbackgroundリソースID
     */
    fun setBackgroundResource(resIdOn: Int, resIdOff: Int) {
        backgroundOn = resIdOn
        backgroundOff = resIdOff
        refreshBackground()
    }
}
