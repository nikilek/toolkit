package net.nikilek.toolkit

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import net.nikilek.toolkit.component.InputNumberDialog
import net.nikilek.toolkit.constant.CodeConst
import net.nikilek.toolkit.extension.toIntOrDefault
import net.nikilek.toolkit.extension.validAdd
import net.nikilek.toolkit.utility.LogUtil

import net.nikilek.toolkit.utility.ResourceHolder
import net.nikilek.toolkit.utility.Toaster

/**
 * Created by nikilek on 2019/06/21.
 * カスタムビュー
 */
open class MyNumberPicker : LinearLayout {

    private var propWidth: Int = 0 //AttributeSetから取得した幅
    private var propHeight: Int = 0 //AttributeSetから取得した高さ

    //ボタン・入力エリア共通
    private var propTextSize: Float = 0f //文字のサイズ
    private var propTextColor: Int = 0 //文字色

    //ボタン
    private var propTextInc: String = "" //インクリメントボタンのテキスト
    private var propTextDec: String = "" //デクリメントボタンのテキスト
    private var propResIdInc: Int = 0 //インクリメントボタンのリソース
    private var propResIdDec: Int = 0 //デクリメントボタンのリソース

    //入力エリア
    private lateinit var form: TextView
    private var propValue: Int = 0 //初期値
    private var propMinValue: Int = 0 //最小値
    private var propMaxValue: Int = 0 //最大値
    private var dlg: InputNumberDialog? = null
    var onTextChangedListener: InputFormNumber.OnTextChangedListener? = null //テキスト変更時イベントのリスナ

    /**
     * 入力値の取得
     */
    val value: Int
        get() = form.text.toString().toIntOrDefault(propValue)

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context)

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        getAttrs(attrs)
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        getAttrs(attrs)
    }

    /**
     * 属性の取得
     * @param attrs 属性セット
     */
    private fun getAttrs(attrs: AttributeSet) {
        this.propWidth = attrs.getAttributeResourceValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "layout_width", 0)
        this.propHeight = attrs.getAttributeResourceValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "layout_height", 0)
        val attrsOriginal = context.obtainStyledAttributes(attrs, R.styleable.MyNumberPicker)
        this.propTextSize = attrsOriginal.getDimension(R.styleable.MyNumberPicker_textSize, 0f)
        this.propTextColor = attrsOriginal.getColor(R.styleable.MyNumberPicker_textColor,
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    @Suppress("DEPRECATION")
                    resources.getColor(R.color.font_default)
                } else {
                    ContextCompat.getColor(context, R.color.font_default)
                })
        this.propTextInc = attrsOriginal.getString(R.styleable.MyNumberPicker_textInc) ?: "＋"
        this.propTextDec = attrsOriginal.getString(R.styleable.MyNumberPicker_textDec) ?: "－"
        this.propResIdInc = attrsOriginal.getResourceId(R.styleable.MyNumberPicker_backgroundInc, R.drawable.bk_button_theme_base_oval)
        this.propResIdDec = attrsOriginal.getResourceId(R.styleable.MyNumberPicker_backgroundDec, R.drawable.bk_button_theme_base_oval)
        this.propValue = attrsOriginal.getInt(R.styleable.MyNumberPicker_value, 0)
        this.propMinValue = attrsOriginal.getInt(R.styleable.MyNumberPicker_minValue, 0)
        this.propMaxValue = attrsOriginal.getInt(R.styleable.MyNumberPicker_maxValue, Int.MAX_VALUE)
        attrsOriginal.recycle()
    }

    public override fun onFinishInflate() {
        super.onFinishInflate()
        //if (this.isInEditMode) {
        //    return
        //}
        //重ね合わせ用のレイアウトを追加
        val rel = RelativeLayout(context).apply {
            this.layoutParams = LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            this@MyNumberPicker.addView(this)
        }
        //入力エリアの追加
        form = createInputForm().apply {
            rel.addView(this)
        }
        //ボタンエリア用のレイアウトを追加
        val lin = LinearLayout(context).apply {
            this.layoutParams = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            this.orientation = this@MyNumberPicker.orientation
            rel.addView(this)
        }
        //デクリメントボタンの追加
        createButton(propTextDec, propResIdDec).let { btn ->
            if (!isInEditMode) {
                btn.setOnClickListener {
                    val currentValue = form.text.toString().toInt()
                    val newValue = currentValue.validAdd(-1, propMinValue, propMaxValue)
                    form.text = newValue.toString()
                    if(currentValue != newValue){
                        onTextChangedListener?.onTextChanged(newValue.toString())
                    }
                }
            }
            lin.addView(btn)
        }
        //余白の追加
        createMargin().let { margin ->
            lin.addView(margin)
        }
        //インクリメントボタンの追加
        createButton(propTextInc, propResIdInc).let { btn ->
            if (!isInEditMode) {
                btn.setOnClickListener {
                    val currentValue = form.text.toString().toInt()
                    val newValue = currentValue.validAdd(1, propMinValue, propMaxValue)
                    form.text = newValue.toString()
                    if(currentValue != newValue){
                        onTextChangedListener?.onTextChanged(newValue.toString())
                    }
                }
            }
            lin.addView(btn)
        }
    }

    /**
     * インクリメント／デクリメント用のボタンを生成
     * @param text ボタンのテキスト
     */
    private fun createButton(text: String, backgroundResId: Int): TextView {
        //サイズの取得
        val size = getButtonSize()
        //ボタンの生成
        val btn = TextView(context)
        btn.layoutParams = LayoutParams(size, size) //幅＝高さ
        btn.setBackgroundResource(backgroundResId)
        btn.text = text
        btn.textSize = if (propTextSize > 0f) propTextSize else (size / 6).toFloat()
        btn.setTextColor(propTextColor)
        btn.gravity = Gravity.CENTER
        btn.includeFontPadding = false
        setFont(btn)
        return btn
    }

    /**
     * ボタンサイズの取得
     */
    private fun getButtonSize(): Int {
        return when (orientation) {
            HORIZONTAL -> {
                if (propHeight > 0) {
                    context.resources.getDimension(propHeight).toInt()
                } else {
                    propHeight
                }
            }
            VERTICAL -> {
                if (propWidth > 0) {
                    context.resources.getDimension(propWidth).toInt()
                } else {
                    propWidth
                }
            }
            else -> 0
        }
    }

    /**
     * 余白を生成
     */
    private fun createMargin(): MyImageView {
        //ボタンサイズの取得
        val buttonSize = getButtonSize()
        //サイズの取得
        val layoutWidth = when (orientation) {
            VERTICAL -> buttonSize
            else -> 0
        }
        //余白を生成
        val margin = MyImageView(context)
        val layoutParams = if (layoutWidth == 0) {
            LayoutParams(layoutWidth, buttonSize, 1f)
        } else {
            LayoutParams(layoutWidth, buttonSize)
        }
        margin.layoutParams = layoutParams
        return margin
    }

    /**
     * 入力エリアを生成
     */
    private fun createInputForm(): TextView {
        //ボタンサイズの取得
        val buttonSize = getButtonSize()
        //サイズの取得
        val layoutWidth = when (orientation) {
            VERTICAL -> buttonSize
            else -> ViewGroup.LayoutParams.MATCH_PARENT
        }
        //入力エリアを生成
        val tx = TextView(context)
        val padding = context.resources.getDimension(R.dimen.margin_6).toInt()
        val layoutParams = LayoutParams(layoutWidth, buttonSize - padding)
        if (orientation == HORIZONTAL) {
            layoutParams.topMargin = padding / 2
            layoutParams.leftMargin = buttonSize / 2
            layoutParams.rightMargin = buttonSize / 2
        } else {
            layoutParams.topMargin = buttonSize + (padding / 2)
        }
        tx.layoutParams = layoutParams
        tx.text = propValue.toString()
        tx.textSize = if (propTextSize > 0f) propTextSize else (buttonSize / 6).toFloat()
        tx.setTextColor(
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    @Suppress("DEPRECATION")
                    resources.getColor(R.color.font_reverse)
                } else {
                    ContextCompat.getColor(context, R.color.font_reverse)
                })
        tx.setBackgroundResource(R.drawable.style_edittext_background_square)
        tx.gravity = Gravity.CENTER
        tx.includeFontPadding = false
        setFont(tx)
        //クリックイベントの付与
        if (!isInEditMode) {
            attachClickEventForInputNumber(tx)
        }
        return tx
    }

    /**
     * テキスト変更時イベントの設定
     * ※プロパティアクセスと同じ
     */
    fun attachOnTextChangedEvent(onTextChangedListener: InputFormNumber.OnTextChangedListener) {
        this.onTextChangedListener = onTextChangedListener
    }

    /**
     * クリックイベントの設定
     */
    private fun attachClickEventForInputNumber(textView: TextView) {
        this.setOnClickListener {
            val currentText = textView.text.toString()
            val onFinishListener = object : InputNumberDialog.OnFinishListener {
                override fun onFinish(text: String, textChanged: Boolean, canceled: Boolean) {
                    if (canceled) {
                        return
                    }
                    //入力されたテキストを反映
                    textView.text = text
                    //呼び出し元へ通知
                    if (textChanged) {
                        onTextChangedListener?.onTextChanged(text)
                    }
                }
            }
            //数値入力用ダイアログを表示
            dlg = InputNumberDialog(context as AbstractActivity, onFinishListener, "",
                    currentText, propMinValue, propMaxValue)
            dlg?.show()
        }
        this.setOnLongClickListener {
            //入力されているテキストをクリップボードにコピー
            val cm = context.getSystemService(Context.CLIPBOARD_SERVICE)
            if (cm is ClipboardManager) {
                cm.setPrimaryClip(ClipData.newPlainText("", textView.text.toString()))
                Toaster.showBottom(context, context.getString(R.string.toast_clipboard_copy))
            }
            return@setOnLongClickListener true
        }
    }

    /**
     * フォントを設定
     * @param textView 対象のテキストビュー
     */
    private fun setFont(textView: TextView) {
        try {
            //システム定義のフォントを設定
            textView.typeface = ResourceHolder.Typefaces.getDefault(context!!)
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * 入力可能範囲の設定
     * @param minValue 最小値
     * @param maxValue 最大値
     */
    fun setRange(minValue: Int, maxValue: Int) {
        //パラメタの格納
        propMinValue = minValue
        propMaxValue = maxValue
        //ダイアログに反映
        if (dlg != null && dlg!!.isShowing && context is Activity) {
            (context as Activity).runOnUiThread {
                dlg?.setRange(minValue, maxValue)
            }
        }
    }
}
