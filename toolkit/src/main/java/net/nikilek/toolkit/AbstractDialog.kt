package net.nikilek.toolkit

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager

import java.lang.ref.WeakReference

import net.nikilek.toolkit.interfaces.ISaveData
import net.nikilek.toolkit.utility.KeyboardUtil
import net.nikilek.toolkit.utility.LogUtil

/**
 * Created by nikilek on 2016/01/27.
 * 基底ダイアログ
 *
 * Fragmentを(あまり)使わず、Activityの数も極力増やさない思想となっています
 * Application, Activity, Dialog, Screen(独自), SubScreen(独自)は、それぞれAbstractクラスを継承して実装してください
 * ・{Root Activity}(MainActivity)は、{net.nikilek.toolkit._sample.MainActivity}を参考に実装してください
 * ・{Screen}は、Activityの上に重ねて起動する、小さなActivityのような役割です
 * ・{SubScreen}は、Screenの上に重ねて起動する、小さなDialogのような役割です
 * ・{Dialog}は、(同一Activity内の)全ての{Screen}や{SubScreen}の上位(表面)に表示されます
 * ・以下に、DialogとSubScreenの実装例を示します
 * 　- {net.nikilek.toolkit.component.MessageDialog}     ※Activityから起動します
 * 　- {net.nikilek.toolkit.component.MessageSubScreen}  ※Screenから起動します。ScreenはActivityから起動します
 *
 * {Application<AbstractApplication>}
 *    ├──{Root Activity<AbstractActivity>}
 *    │     ├──{Screen<AbstractScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ~
 *    │     ├──{Screen@AbstractScreen}
 *    │     ~
 *    │     ~
 *    │     └──{Dialog@AbstractDialog}
 *    │
 *    ├──{Activity<AbstractActivity>}
 *    │     ├──{Screen<AbstractScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ~
 *    │     ├──{Screen<AbstractScreen>}
 *    │     ~
 *    │     ~
 *    │     └──{Dialog<AbstractDialog>}
 *    ~
 */
abstract class AbstractDialog : AlertDialog {

    protected var activity: AbstractActivity //コンテキスト
    private var fullScreen: Boolean = false //フルスクリーンかのフラグ
    private var useKeyboard: Boolean = false //キーボードを使用するかのフラグ
    protected lateinit var handler: Handler // ハンドラ
    protected lateinit var v: View //親ビュー
    private var alert: AlertDialog? = null //このダイアログから呼び出したダイアログ

    /**
     * セーブデータの取得
     */
    @Suppress("UNCHECKED_CAST")
    protected fun <T : ISaveData> mSaveData(): T {
        return activity.mSaveData()
    }

    /**
     * コンストラクタ
     * @param activity 親Activity
     */
    protected constructor(activity: AbstractActivity) : super(activity) {
        //パラメータの格納
        this.activity = activity
    }

    /**
     * コンストラクタ
     * @param activity 親Activity
     * @param theme    テーマ
     */
    protected constructor(activity: AbstractActivity, theme: Int) : super(activity, theme) {
        //パラメータの格納
        this.activity = activity
    }

    /**
     * 初期化
     * @param layoutId レイアウトのリソースID
     * @param fullScreen フルスクリーン表示するかのフラグ
     * @param cancelable キャンセル可否
     * @param useKeyboard キーボードを使用するかのフラグ
     */
    protected fun prepare(layoutId: Int, fullScreen: Boolean, cancelable: Boolean, useKeyboard: Boolean) {
        //onCreate()で使用するパラメタを保管
        this.fullScreen = fullScreen
        this.useKeyboard = useKeyboard
        //オーナーの設定
        this.setOwnerActivity(activity)
        //ビューの取得
        v = View.inflate(activity, layoutId, null)
        //キャンセル可否の設定
        this.setCancelable(cancelable)
        //タイトル非表示
        this.window!!.requestFeature(Window.FEATURE_NO_TITLE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //ハンドラの生成
        handler = IncomingHandler(this)
        if (fullScreen) {
            this.setContentView(v)
            //this.show()
            //ウィンドウパラメタの設定
            this.window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN) //フルスクリーン
            this.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT) //幅と高さをMAXにする
            this.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //背景を透明にする
            this.window!!.attributes.dimAmount = 0.0f
            //色の設定
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //動的レイアウトの有効化
                //※一部端末で期待した挙動にならないため、コメントアウト
                //this.window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                //this.window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
                //this.window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                //this.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            }
        } else {
            this.setView(v)
            //this.show()
        }
        //キーボードの有効化
        if (useKeyboard) {
            this.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
            this.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        }
    }

    override fun setView(view: View?) {
        this.setView(v, getDimenInt(R.dimen.layout_12), getDimenInt(R.dimen.layout_16), getDimenInt(R.dimen.layout_12), getDimenInt(R.dimen.layout_24))
    }

    override fun dismiss() {
        //子ダイアログ非表示
        closeChildDialog()
        //自身を閉じる
        super.dismiss()
    }

    override fun <T : View?> findViewById(id: Int): T {
        return v.findViewById<T>(id)
    }

    /**
     * dismiss()の実行　※キーボード非表示処理付き
     * @param view フォーカスが当たっているビュー
     */
    fun dismiss(view: View?) {
        //キーボードを非表示にする
        hideKeyboard(view ?: v.findFocus())
        //自身を閉じる
        dismiss()
    }

    /**
     * 子ダイアログを表示する
     * @param dialog 子ダイアログ
     */
    fun showChildDialog(dialog: AlertDialog) {
        //子ダイアログ非表示
        closeChildDialog()
        //ダイアログ表示
        try {
            dialog.show()
            alert = dialog
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * 子ダイアログを非表示にする
     */
    fun closeChildDialog() {
        if (alert == null) {
            return
        }
        if (alert!!.isShowing) {
            alert!!.dismiss()
        }
        alert = null
    }

    /**
     * 指定されたビューにダイアログクローズイベントを付与する
     * @param v 対象のビュー
     */
    protected fun attachCloseEvent(v: View) {
        v.setOnClickListener { view ->
            //自身を閉じる
            dismiss(view)
        }
    }

    /**
     * 指定されたビューにActivity終了イベントを付与する
     * @param v 対象のビュー
     */
    protected fun attachFinishEvent(v: View) {
        v.setOnClickListener { view ->
            //自身を閉じる
            dismiss(view)
        }
    }

    /**
     * キーボードの消去
     * @param v フォーカスを受けているビュー
     */
    protected fun hideKeyboard(v: View) {
        try {
            KeyboardUtil.hide(activity, v)
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * オーナーのActivityにMessageオブジェクトを送信する
     * @param msg 送信するメッセージ
     */
    protected fun sendToOwnerActivity(msg: Message) {
        try {
            //親Activityにメッセージを送信する
            activity.handler.sendMessage(msg)
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * オーナーのActivity経由でリソースの文字列を取得する
     * @param resId リソースID
     */
    protected fun getString(resId: Int): String {
        return activity.getString(resId)
    }

    /**
     * オーナーのActivity経由でリソースの文字列の配列を取得する
     * @param resId リソースID
     */
    protected fun getStringArray(resId: Int): Array<String> {
        return activity.getStringArray(resId)
    }

    /**
     * オーナーのActivity経由でリソースの数値を取得する
     * @param resId リソースID
     */
    protected fun getDimen(resId: Int): Float {
        return activity.getDimen(resId)
    }

    /**
     * オーナーのActivity経由でリソースの数値を取得する
     * @param resId リソースID
     */
    protected fun getDimenInt(resId: Int): Int {
        return activity.getDimenInt(resId)
    }

    /**
     * オーナーのActivity経由でリソースの色を取得する
     * @param resId リソースID
     */
    protected fun getColor(resId: Int): Int {
        return activity.getResColor(resId)
    }

    /**
     * ハンドラ生成用の内部クラス
     * @param dialog 参照するダイアログ
     */
    internal class IncomingHandler(dialog: AbstractDialog) : Handler() {
        private val mDialog: WeakReference<AbstractDialog> = WeakReference(dialog) //ダイアログへの弱参照
        override fun handleMessage(msg: Message) {
            //ダイアログの取得
            val dialog = mDialog.get()
            if (dialog != null) {
                //メッセージ処理の実行
                try {
                    dialog.handleMessage(msg)
                } catch (e: Exception) {
                    LogUtil.debug(e)
                }
            }
        }
    }

    /**
     * メッセージ処理
     * @param msg メッセージ
     */
    protected open fun handleMessage(msg: Message) {
        try {
            when (msg.what) {
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }
}