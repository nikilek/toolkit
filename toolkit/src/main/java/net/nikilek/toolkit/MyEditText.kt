package net.nikilek.toolkit

import android.content.Context
import android.util.AttributeSet
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

/**
 * Created by nikilek on 2016/01/27.
 * カスタムビュー
 */
open class MyEditText : EditText {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    public override fun onFinishInflate() {
        super.onFinishInflate()
        if (this.isInEditMode) {
            return
        }
        //フォーカス変更時イベントの設定
        this.onFocusChangeListener = OnFocusChangeListener { view, b ->
            val inputMethodManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (b) { // フォーカスを受け取ったとき
                // ソフトキーボードを表示する
                inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_FORCED)
            } else { // フォーカスが外れたとき
                // ソフトキーボードを閉じる
                inputMethodManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            }
        }
    }
}
