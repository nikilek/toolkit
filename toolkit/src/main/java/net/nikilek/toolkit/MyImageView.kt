package net.nikilek.toolkit

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.util.AttributeSet
import android.widget.ImageView
import android.graphics.BitmapFactory
import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.utility.FileIO
import net.nikilek.toolkit.utility.LogUtil
import net.nikilek.toolkit.utility.ViewUtil
import kotlin.math.roundToInt

/**
 * Created by nikilek on 2018/01/07.
 * カスタムビュー
 * ※RecyclerView上での使用には非対応
 */
open class MyImageView : ImageView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    init {
        this.contentDescription = resources.getString(R.string.contentDescription)
    }

    var bitmap: Bitmap? = null

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        setImageDrawable(null)
        bitmap?.recycle()
    }

    override fun setImageBitmap(bitmap: Bitmap?) {
        this.setImageDrawable(null)
        this.bitmap?.recycle()
        super.setImageBitmap(bitmap)
        this.bitmap = bitmap
    }

    /**
     * 内部ストレージから画像を読み込んで表示
     * @param fileName ファイル名（パッケージのパスは自動付与のため不要）
     * @param inSampleSize 取得するBitmapの縮小倍率（例：2の場合：1/2倍、3の場合：1/3倍）　※既定値：1
     */
    @JvmOverloads
    fun loadBitmap(fileName: String, inSampleSize: Int = 1) {
        try {
            val fio = FileIO(context, fileName, true)
            if (!fio.isExistFile) {
                return
            }
            val buf = fio.readFile(Settings.File.BUFFER_SIZE)!!
            //指定倍率を用いて画像をメモリ上にデコード
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = false //メモリ上に展開する
            options.inSampleSize = inSampleSize
            this.setImageBitmap(BitmapFactory.decodeByteArray(buf, 0, buf.size, options))
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * 内部ストレージから画像を読み込んで表示
     * @param fileName ファイル名（パッケージのパスは自動付与のため不要）
     * @param reqWidth 取得するBitmapの幅（リサイズ用）
     * @param reqHeight 取得するBitmapの高さ（リサイズ用）
     */
    fun loadBitmap(fileName: String, reqWidth: Int, reqHeight: Int) {
        try {
            val fio = FileIO(context, fileName, true)
            if (!fio.isExistFile) {
                return
            }
            val buf = fio.readFile(Settings.File.BUFFER_SIZE)!!
            //サイズ取得用に画像をデコード
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true //メモリ上には展開しない
            BitmapFactory.decodeByteArray(buf, 0, buf.size, options)
            //指定サイズにするには何倍にすればいいかを算出
            val rawHeight = options.outHeight //元画像の幅
            val rawWidth = options.outWidth //元画像の高さ
            var inSampleSize = 1.0f //倍率
            if (reqHeight > 0 && rawWidth > rawHeight) {
                //高さで合わせる
                inSampleSize = rawHeight.toFloat() / reqHeight.toFloat()
            } else if (reqWidth > 0 && rawHeight > reqHeight) {
                //幅で合わせる
                inSampleSize = rawWidth.toFloat() / reqWidth.toFloat()
            } else if (reqHeight in 1 until rawHeight) {
                //高さで合わせる
                inSampleSize = rawHeight.toFloat() / reqHeight.toFloat()
            } else if (reqWidth in 1 until rawWidth) {
                //幅で合わせる
                inSampleSize = rawWidth.toFloat() / reqWidth.toFloat()
            }
            //算出した倍率を用いて画像をメモリ上にデコード
            options.inJustDecodeBounds = false //メモリ上に展開する
            options.inSampleSize = inSampleSize.roundToInt()
            this.setImageBitmap(BitmapFactory.decodeByteArray(buf, 0, buf.size, options))
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * リソースからファイルを読み込んで表示
     * @param activity 呼び出し元のActivity
     * @param resId リソースID
     * @param inSampleSize 取得するBitmapの縮小倍率（例：2の場合：1/2倍、3の場合：1/3倍）　※既定値：1
     */
    fun loadImageResource(activity: AbstractActivity, resId: Int, inSampleSize: Int = 1) {
        try {
            //指定倍率を用いて画像をメモリ上にデコード
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = false //メモリ上に展開する
            options.inSampleSize = inSampleSize
            //Bitmapを生成して読込
            this.setImageBitmap(BitmapFactory.decodeResource(activity.resources, resId, options))
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * リソースからファイルを読み込んで表示
     * @param activity 呼び出し元のActivity
     * @param fileName リソースファイル名
     * @param inSampleSize 取得するBitmapの縮小倍率（例：2の場合：1/2倍、3の場合：1/3倍）　※既定値：1
     */
    fun loadImageResource(activity: AbstractActivity, fileName: String, inSampleSize: Int = 1) {
        val resId = ViewUtil(activity).getImgResId(fileName)
        return loadImageResource(activity, resId, inSampleSize)
    }

    /**
     * リソースからファイルを読み込んで表示
     * @param activity 呼び出し元のActivity
     * @param resId リソースID
     * @param reqWidth 取得するBitmapの幅（リサイズ用）
     * @param reqHeight 取得するBitmapの高さ（リサイズ用）
     */
    fun loadImageResource(activity: AbstractActivity, resId: Int, reqWidth: Int, reqHeight: Int) {
        try {
            //サイズ取得用に画像をデコード
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true //メモリ上には展開しない
            BitmapFactory.decodeResource(activity.resources, resId, options)
            //指定サイズにするには何倍にすればいいかを算出
            val rawHeight = options.outHeight //元画像の幅
            val rawWidth = options.outWidth //元画像の高さ
            var inSampleSize = 1.0f //倍率
            if (reqHeight > 0 && rawWidth > rawHeight) {
                //高さで合わせる
                inSampleSize = rawHeight.toFloat() / reqHeight.toFloat()
            } else if (reqWidth > 0 && rawHeight > reqHeight) {
                //幅で合わせる
                inSampleSize = rawWidth.toFloat() / reqWidth.toFloat()
            } else if (reqHeight in 1 until rawHeight) {
                //高さで合わせる
                inSampleSize = rawHeight.toFloat() / reqHeight.toFloat()
            } else if (reqWidth in 1 until rawWidth) {
                //幅で合わせる
                inSampleSize = rawWidth.toFloat() / reqWidth.toFloat()
            }
            //算出した倍率を用いて画像をメモリ上にデコード
            options.inJustDecodeBounds = false //メモリ上に展開する
            options.inSampleSize = inSampleSize.roundToInt()
            //Bitmapを生成して読込
            this.setImageBitmap(BitmapFactory.decodeResource(activity.resources, resId, options))
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * リソースからファイルを読み込んで表示
     * @param activity 呼び出し元のActivity
     * @param fileName リソースファイル名
     * @param reqWidth 取得するBitmapの幅（リサイズ用）
     * @param reqHeight 取得するBitmapの高さ（リサイズ用）
     */
    fun loadImageResource(activity: AbstractActivity, fileName: String, reqWidth: Int, reqHeight: Int) {
        val resId = ViewUtil(activity).getImgResId(fileName)
        return loadImageResource(activity, resId, reqWidth, reqHeight)
    }

    /**
     * 指定されたマトリクス情報を適用する
     * @param matrix 適用するマトリクス情報
     */
    fun applyMatrix(matrix: Matrix) {
        //既存の画像を取得
        var bmp = (this.drawable as BitmapDrawable).bitmap
        //加工後の画像を取得して設定
        bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.width, bmp.height, matrix, false)
        this.setImageBitmap(bmp)
    }

    /**
     * 画像の回転
     * @param degrees 角度（時計回り方向）
     */
    fun rotate(degrees: Int) {
        val matrix = Matrix()
        matrix.postRotate(degrees.toFloat())
        applyMatrix(matrix)
    }

    /**
     * 画像の反転
     * @param horizontal 水平反転するか
     * @param vertical 垂直反転するか
     */
    fun turnOver(horizontal: Boolean, vertical: Boolean) {
        val matrix = Matrix()
        matrix.preScale(if (horizontal) -1f else 1f, if (vertical) -1f else 1f)
        applyMatrix(matrix)
    }
}
