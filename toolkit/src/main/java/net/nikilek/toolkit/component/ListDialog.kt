package net.nikilek.toolkit.component

import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import net.nikilek.toolkit.AbstractActivity
import net.nikilek.toolkit.AbstractDialog

import net.nikilek.toolkit.R

/**
 * Created by nikilek on 2016/01/27.
 * リスト表示用ダイアログ
 */
class ListDialog : AbstractDialog {

    private lateinit var lv: ListView

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param title 表示タイトル
     * @param adapter リスト表示用アダプタ
     * @param cancelable キャンセル可否　※既定値：true
     */
    @JvmOverloads
    constructor(activity: AbstractActivity, title: String, adapter: BaseAdapter,
                cancelable: Boolean = true) : super(activity) {
        this.activity = activity
        prepare(title, adapter, null, cancelable)
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param theme ダイアログテーマ
     * @param title 表示タイトル
     * @param adapter リスト表示用アダプタ
     * @param cancelable キャンセル可否　※既定値：true
     */
    @JvmOverloads
    constructor(activity: AbstractActivity, theme: Int, title: String, adapter: BaseAdapter,
                cancelable: Boolean = true) : super(activity, theme) {
        this.activity = activity
        prepare(title, adapter, null, cancelable)
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param title 表示タイトル
     * @param adapter リスト表示用アダプタ
     * @param onItemClickListener リスト選択時イベントのリスナ
     * @param cancelable キャンセル可否　※既定値：true
     */
    @JvmOverloads
    constructor(activity: AbstractActivity, title: String, adapter: BaseAdapter,
                onItemClickListener: AdapterView.OnItemClickListener, cancelable: Boolean = true) : super(activity) {
        this.activity = activity
        prepare(title, adapter, onItemClickListener, cancelable)
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param theme ダイアログテーマ
     * @param title 表示タイトル
     * @param adapter リスト表示用アダプタ
     * @param onItemClickListener リスト選択時イベントのリスナ
     * @param cancelable キャンセル可否　※既定値：true
     */
    @JvmOverloads
    constructor(activity: AbstractActivity, theme: Int, title: String, adapter: BaseAdapter,
                onItemClickListener: AdapterView.OnItemClickListener, cancelable: Boolean = true) : super(activity, theme) {
        this.activity = activity
        prepare(title, adapter, onItemClickListener, cancelable)
    }

    /**
     * 初期化処理
     * @param title 表示タイトル
     * @param adapter リスト表示用アダプタ
     * @param onItemClickListener リスト選択時イベントのリスナ
     * @param cancelable キャンセル可否
     */
    private fun prepare(title: String, adapter: BaseAdapter, onItemClickListener: AdapterView.OnItemClickListener?, cancelable: Boolean) {
        super.prepare(R.layout.dialog_list, false, cancelable, false)
        //タイトルの設定
        v.findViewById<TextView>(R.id.title).text = title
        //リストの設定
        lv = v.findViewById(R.id.list)
        lv.adapter = adapter
        if (onItemClickListener != null) {
            lv.onItemClickListener = onItemClickListener
        }
        //ビューの更新（コンテンツの設定により高さが変化したため）
        setView(v)
    }

    /**
     * リスト選択時イベントの設定
     * @param onItemClickListener アイテムクリック時イベントのリスナ
     */
    fun attachItemClickEvent(onItemClickListener: AdapterView.OnItemClickListener) {
        lv.onItemClickListener = onItemClickListener
    }
}
