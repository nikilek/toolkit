package net.nikilek.toolkit.component

import net.nikilek.toolkit.AbstractActivity
import net.nikilek.toolkit.AbstractDialog

/**
 * Created by nikilek on 2016/01/27.
 * 指定レイアウト表示用ダイアログ
 */
class SimpleDialog : AbstractDialog {

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param layoutId レイアウトのリソースID
     * @param cancelable キャンセル可否　※既定値：true
     */
    @JvmOverloads
    constructor(activity: AbstractActivity, layoutId: Int,
                cancelable: Boolean = true) : super(activity) {
        this.activity = activity
        prepare(layoutId, cancelable)
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param theme ダイアログテーマ
     * @param layoutId レイアウトのリソースID
     * @param cancelable キャンセル可否　※既定値：true
     */
    @JvmOverloads
    constructor(activity: AbstractActivity, theme: Int, layoutId: Int,
                cancelable: Boolean = true) : super(activity, theme) {
        this.activity = activity
        prepare(layoutId, cancelable)
    }

    /**
     * 初期化処理
     * @param layoutId レイアウトのリソースID
     * @param cancelable キャンセル可否
     */
    private fun prepare(layoutId: Int, cancelable: Boolean) {
        super.prepare(layoutId, true, cancelable, false)
    }
}
