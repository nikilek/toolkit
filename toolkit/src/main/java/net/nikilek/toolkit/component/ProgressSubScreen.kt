package net.nikilek.toolkit.component

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.widget.ProgressBar
import net.nikilek.toolkit.AbstractSubScreen
import net.nikilek.toolkit.R

/**
 * Created by nikilek on 2018/08/13.
 * プログレス表示用サブスクリーン
 */
class ProgressSubScreen(context: Context, attrs: AttributeSet) : AbstractSubScreen(context, attrs) {

    private var progressBar: ProgressBar? = null

    override fun onAttach() {
        progressBar = v.findViewById(R.id.progress)
    }

    override fun onDetach() {
        progressBar = null
    }

    @TargetApi(Build.VERSION_CODES.O)
    fun setRange(min: Int, max: Int) {
        progressBar?.post {
            progressBar?.min = min
            progressBar?.max = max
        }
    }

    fun setMax(max: Int) {
        progressBar?.post {
            progressBar?.max = max
        }
    }

    fun setProgress(progress: Int) {
        progressBar?.post {
            progressBar?.progress = progress
        }
    }
}