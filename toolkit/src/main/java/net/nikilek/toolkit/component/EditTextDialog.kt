package net.nikilek.toolkit.component

import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import net.nikilek.toolkit.AbstractActivity

import net.nikilek.toolkit.R

import net.nikilek.toolkit.AbstractDialog
import net.nikilek.toolkit.extension.substringByEms
import net.nikilek.toolkit.extension.validMin
import net.nikilek.toolkit.utility.KeyboardUtil

/**
 * Created by nikilek on 2016/01/27.
 * テキスト入力用ダイアログ
 */
class EditTextDialog : AbstractDialog {

    private var onFinishListener: OnFinishListener //終了時イベント発火用
    private var oldText = "" //ダイアログ起動時の入力内容
    private var maxEms = -1 //最大幅　※最大幅を超えた文字はダイアログ終了時に削除

    /**
     * ダイアログ終了時イベントのリスナ
     */
    interface OnFinishListener {
        /**
         * @param text 入力内容
         * @param textChanged 入力内容が変化したか否か
         * @param canceled キャンセルボタンを押されたか否か
         */
        fun onFinish(text: String, textChanged: Boolean, canceled: Boolean)
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param onFinishListener 終了時イベントのリスナ
     * @param maxLines 最大行数（1行またはそれ以上）
     * @param title 入力項目のタイトル
     * @param currentText 現在の入力内容
     * @param inputType キーボードタイプ
     */
    @JvmOverloads
    constructor(activity: AbstractActivity, onFinishListener: EditTextDialog.OnFinishListener, maxLines: Int,
                title: String, currentText: String, inputType: Int = InputType.TYPE_NULL) : super(activity) {
        this.onFinishListener = onFinishListener
        this.oldText = currentText
        prepare(maxLines, title, currentText, -1, inputType)
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param theme ダイアログのテーマ
     * @param onFinishListener 終了時イベントのリスナ
     * @param maxLines 最大行数（1行またはそれ以上）
     * @param title 入力項目のタイトル
     * @param currentText 現在の入力内容
     * @param inputType キーボードタイプ
     */
    @JvmOverloads
    constructor(activity: AbstractActivity, theme: Int, onFinishListener: EditTextDialog.OnFinishListener, maxLines: Int,
                title: String, currentText: String, inputType: Int = InputType.TYPE_NULL) : super(activity, theme) {
        this.onFinishListener = onFinishListener
        this.oldText = currentText
        prepare(maxLines, title, currentText, -1, inputType)
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param onFinishListener 終了時イベントのリスナ
     * @param maxLines 最大行数（1行またはそれ以上）
     * @param title 入力項目のタイトル
     * @param currentText 現在の入力内容
     * @param maxLength 最大文字数（半角／全角考慮なし）
     * @param maxEms 最大幅（全角＝２、半角＝１としてカウント）※最大幅を超えた文字はダイアログ終了時に削除
     * @param inputType キーボードタイプ
     */
    @JvmOverloads
    constructor(activity: AbstractActivity, onFinishListener: EditTextDialog.OnFinishListener, maxLines: Int,
                title: String, currentText: String, maxLength: Int, maxEms: Int, inputType: Int = InputType.TYPE_NULL) : super(activity) {
        this.onFinishListener = onFinishListener
        this.oldText = currentText
        this.maxEms = maxEms
        prepare(maxLines, title, currentText, maxLength, inputType)
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param theme ダイアログのテーマ
     * @param onFinishListener 終了時イベントのリスナ
     * @param maxLines 最大行数（1行またはそれ以上）
     * @param title 入力項目のタイトル
     * @param currentText 現在の入力内容
     * @param maxLength 最大文字数（半角／全角考慮なし）
     * @param maxEms 最大幅（全角＝２、半角＝１としてカウント）※最大幅を超えた文字はダイアログ終了時に削除
     * @param inputType キーボードタイプ
     */
    @JvmOverloads
    constructor(activity: AbstractActivity, theme: Int, onFinishListener: EditTextDialog.OnFinishListener, maxLines: Int,
                title: String, currentText: String, maxLength: Int, maxEms: Int, inputType: Int = InputType.TYPE_NULL) : super(activity, theme) {
        this.onFinishListener = onFinishListener
        this.oldText = currentText
        this.maxEms = maxEms
        prepare(maxLines, title, currentText, maxLength, inputType)
    }

    /**
     * 初期化
     * @param maxLines 最大行数（1行またはそれ以上）
     * @param title 入力項目のタイトル
     * @param currentText 現在の入力内容
     * @param maxLength 最大文字数（半角／全角考慮なし）
     * @param inputType キーボードタイプ
     */
    private fun prepare(maxLines: Int, title: String, currentText: String, maxLength: Int, inputType: Int) {
        super.prepare(R.layout.dialog_edittext, fullScreen = true, cancelable = false, useKeyboard = true)
        //入力受付用のビューを取得
        val edit = v.findViewById<EditText>(R.id.value)
        //マージンの設定
        v.findViewById<ViewGroup>(R.id.parent).apply {
            var parentHeightDefault = 0
            var parentHeightWithKeyboard = 0
            viewTreeObserver.addOnGlobalLayoutListener {
                //処理済チェック
                if (parentHeightDefault != 0 && parentHeightWithKeyboard != 0) {
                    return@addOnGlobalLayoutListener
                }
                //キーボード表示前後の高さを取得
                if (parentHeightDefault == 0) {
                    parentHeightDefault = this.height
                } else if (this.height != parentHeightDefault) { //高さが初期状態から変化した場合
                    parentHeightWithKeyboard = this.height
                }
                //設定すべき高さを取得
                val marginHeight = parentHeightWithKeyboard - activity.getDimenInt(R.dimen.layout_400).validMin(0)
                val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, marginHeight)
                //取得した高さをマージンとして設定
                v.findViewById<View>(R.id.margin).layoutParams = layoutParams
            }
        }
        //キーボードを表示
        edit.requestFocus()
        //タイトルの設定
        val txtTitle = v.findViewById<TextView>(R.id.title)
        if (title.isEmpty()) {
            txtTitle.visibility = View.INVISIBLE
        } else {
            txtTitle.text = title
        }
        //入力済みの値を書き戻す
        edit.setText(currentText)
        //入力タイプの設定
        edit.inputType = inputType
        if (inputType == InputType.TYPE_NULL) {
            if (maxLines == 1) {
                edit.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_NORMAL
            } else {
                edit.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE
            }
        }
        //最大長の設定
        if (maxEms >= 0) {
            edit.maxEms = maxEms
        }
        //フィルターの設定
        if (maxLength >= 0) {
            val filters = arrayOfNulls<InputFilter>(1)
            filters[0] = InputFilter.LengthFilter(maxLength)
            edit.filters = filters
        }
        //ラインサイズの設定
        if (maxLines > 0) {
            edit.maxLines = maxLines
            edit.setSingleLine(maxLines == 1)
        }
        //カーソル位置の設定
        edit.setSelection(edit.text.length)
        //キーボード表示
        KeyboardUtil.show(activity, edit)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //OK押下時
        val btnOk = v.findViewById<View>(R.id.btn_positive)
        btnOk.setOnClickListener { view ->
            //入力された値を取得
            val edit = v.findViewById<EditText>(R.id.value)
            var str = edit.text.toString()
            //最大長が指定されている場合
            if (maxEms >= 0) {
                //指定幅で区切る
                str = str.substringByEms(maxEms)
            }
            //コールバック
            onFinishListener.onFinish(str, (oldText != str), false)
            //キーボードを閉じる
            hideKeyboard(view)
            //自身を閉じる
            dismiss()
        }
        //キャンセル押下時
        val btnCancel = v.findViewById<View>(R.id.btn_negative)
        btnCancel.setOnClickListener { view ->
            //コールバック
            onFinishListener.onFinish(oldText, textChanged = false, canceled = true)
            //キーボードを閉じる
            hideKeyboard(view)
            //自身を閉じる
            dismiss()
        }
        //クリア押下時
        val btnClear = v.findViewById<View>(R.id.btn_clear)
        btnClear.setOnClickListener {
            //入力を空にする
            val edit = v.findViewById<EditText>(R.id.value)
            edit.setText("")
        }
    }
}
