package net.nikilek.toolkit.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.TextView

import net.nikilek.toolkit.AbstractSubScreen
import net.nikilek.toolkit.MyImageView
import net.nikilek.toolkit.R

/**
 * Created by nikilek on 2016/01/27.
 * メッセージ表示用ダイアログ（サブスクリーン版）
 */
class MessageSubScreen(context: Context, attrs: AttributeSet) : AbstractSubScreen(context, attrs) {

    /**
     * 起動パラメータ
     */
    data class InitData(
            var dialogType: MessageSubScreen.Type, //ダイアログの種類　※enum定義を参照
            var iconResId: Int = 0, //アイコンのリソースID　※自動設定の場合は0を指定。非表示の場合は-1を指定
            var title: String, //ダイアログのタイトル
            var message: String, //ダイアログに表示するメッセージ
            var positiveButtonText: String = "", //肯定ボタンのテキスト表記　※自動設定の場合は空白を指定
            var negativeButtonText: String = "", //否定ボタンのテキスト表記　※自動設定の場合は空白を指定
            var onFinishListener: OnFinishListener? = null //終了時イベントのリスナ
    )

    /**
     * ダイアログの種類
     */
    enum class Type {
        INFO,    //「OK」ボタンのみ（情報メッセージ用）
        WARNING, //「OK／キャンセル」ボタン（警告メッセージ用）
        ERROR,   //「OK」ボタンのみ（エラーメッセージ用）
        QUESTION //「はい／いいえ」ボタン（質問メッセージ用）
    }

    /**
     * ダイアログ終了時イベントのリスナ
     */
    interface OnFinishListener {
        /**
         * @param canceled 「キャンセル」or「いいえ」ボタンを押されたか否か
         */
        fun onFinish(canceled: Boolean)
    }

    override fun onAttach() {
        //初期化
        prepare(initData as MessageSubScreen.InitData)
    }

    override fun onDetach() {
        //何もしない
    }

    /**
     * 初期化処理
     * @param initData 初期化データ
     */
    private fun prepare(initData: MessageSubScreen.InitData) {
        //初期化データの取得
        val dialogType = initData.dialogType
        val iconResId = initData.iconResId
        val title = initData.title
        val message = initData.message
        val positiveButtonText = initData.positiveButtonText
        val negativeButtonText = initData.negativeButtonText
        val onFinishListener = initData.onFinishListener
        //アイコンの設定
        val imgIcon = v.findViewById<MyImageView>(R.id.icon)
        when (iconResId) {
            0 -> {
                val tmpResId = when (dialogType) {
                    Type.INFO -> R.drawable.ic_info
                    Type.WARNING -> R.drawable.ic_warning
                    Type.ERROR -> R.drawable.ic_error
                    Type.QUESTION -> R.drawable.ic_question
                }
                imgIcon.setImageResource(tmpResId)
            }
            -1 -> imgIcon.visibility = View.GONE
            else -> imgIcon.setImageResource(iconResId)
        }
        //タイトルとメッセージの設定
        v.findViewById<TextView>(R.id.title).text = title
        v.findViewById<TextView>(R.id.message).text = message

        //ボタンの表示情報を取得
        var resIdPositiveBtnText = R.string.dummy_empty
        var resIdNegativeBtnText = R.string.dummy_empty
        var negativeBtnVisibility = View.GONE
        when (dialogType) {
            Type.INFO, Type.ERROR -> {
                resIdPositiveBtnText = R.string.button_ok
                resIdNegativeBtnText = R.string.dummy_empty
                negativeBtnVisibility = View.GONE
            }
            Type.WARNING -> {
                resIdPositiveBtnText = R.string.button_ok
                resIdNegativeBtnText = R.string.button_cancel
                negativeBtnVisibility = View.VISIBLE
            }
            Type.QUESTION -> {
                resIdPositiveBtnText = R.string.button_yes
                resIdNegativeBtnText = R.string.button_no
                negativeBtnVisibility = View.VISIBLE
            }
        }

        //ボタンエリアの参照を取得
        val btnPositive = v.findViewById<Button>(R.id.btn_positive)
        val btnNegative = v.findViewById<Button>(R.id.btn_negative)
        val btnBorder = v.findViewById<View>(R.id.btn_border)

        //ボタンのテキストを設定
        btnPositive.text =
                if (positiveButtonText.isEmpty()) {
                    activity.getString(resIdPositiveBtnText)
                } else {
                    positiveButtonText
                }
        btnNegative.text =
                if (negativeButtonText.isEmpty()) {
                    activity.getString(resIdNegativeBtnText)
                } else {
                    negativeButtonText
                }

        //ボタンの表示有無を設定
        btnNegative.visibility = negativeBtnVisibility
        btnBorder.visibility = negativeBtnVisibility

        //ボタンのイベントを設定
        attachOnClickEvent(btnPositive, onFinishListener, false)
        attachOnClickEvent(btnNegative, onFinishListener, true)
    }

    /**
     * クリックイベントの設定
     * @param target 対象のビュー
     * @param onFinishListener 終了時イベントのリスナ
     * @param cancelButton 対象が「キャンセル」or「いいえ」ボタンか否か
     */
    private fun attachOnClickEvent(target: View, onFinishListener: OnFinishListener?, cancelButton: Boolean) {
        target.setOnClickListener {
            //終了時イベントの発火
            onFinishListener?.onFinish(cancelButton)
            //自身を閉じる
            hideOwn()
        }
    }
}
