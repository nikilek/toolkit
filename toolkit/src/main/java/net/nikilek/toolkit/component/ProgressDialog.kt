package net.nikilek.toolkit.component

import android.annotation.TargetApi
import android.os.Build
import android.widget.ProgressBar
import net.nikilek.toolkit.AbstractActivity
import net.nikilek.toolkit.AbstractDialog
import net.nikilek.toolkit.R

/**
 * Created by nikilek on 2018/08/13.
 * プログレス表示用ダイアログ
 */
class ProgressDialog(activity: AbstractActivity) : AbstractDialog(activity, R.style.Theme_MessageDialog) {

    private var progressBar: ProgressBar

    init {
        super.prepare(R.layout.dialog_progress, fullScreen = true, cancelable = false, useKeyboard = false)
        progressBar = v.findViewById(R.id.progress)
    }

    @TargetApi(Build.VERSION_CODES.O)
    fun setRange(min: Int, max: Int) {
        progressBar.post {
            progressBar.min = min
            progressBar.max = max
        }
    }

    fun setMax(max: Int) {
        progressBar.post {
            progressBar.max = max
        }
    }

    fun setProgress(progress: Int) {
        progressBar.post {
            progressBar.progress = progress
        }
    }
}