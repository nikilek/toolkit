package net.nikilek.toolkit.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import net.nikilek.toolkit.AbstractSubScreen
import net.nikilek.toolkit.MyRecyclerView

import net.nikilek.toolkit.R
import net.nikilek.toolkit.adapter.AbstractRecyclerAdapter

/**
 * Created by nikilek on 2016/01/27.
 * リスト表示用ダイアログ
 */
class RecyclerSubScreen(context: Context, attrs: AttributeSet) : AbstractSubScreen(context, attrs) {

    /**
     * 起動パラメータ
     */
    data class InitData(
            var title: String, //表示タイトル
            var adapter: AbstractRecyclerAdapter<*>, //リスト表示用アダプタ
            var onItemClickListener: OnItemClickListener?, //リスト選択時イベントのリスナ
            var autoClose: Boolean, //リスト選択時に自動でダイアログを閉じるかのフラグ
            var showCloseBtn: Boolean, //閉じるボタン表示フラグ
            var layoutWidth: Int = 0 //リストの幅（未指定時は@layout/subscreen_recyclerで指定された幅）※既定値：0（未指定）
    )

    /**
     * アイテム選択時イベントのリスナ
     */
    interface OnItemClickListener {
        /**
         * アイテム選択時処理
         * @param position アイテム位置
         */
        fun onItemClick(position: Int)
    }

    private lateinit var lv: MyRecyclerView

    override fun onAttach() {
        //初期化
        prepare(initData as RecyclerSubScreen.InitData)
    }

    override fun onDetach() {
        //何もしない
    }

    /**
     * 初期化処理
     * @param initData 初期化データ
     */
    private fun prepare(initData: InitData) {
        val title = initData.title
        val adapter = initData.adapter
        val onItemClickListener = initData.onItemClickListener
        val autoClose = initData.autoClose
        val showCloseBtn = initData.showCloseBtn
        val layoutWidth = initData.layoutWidth
        //幅の設定
        if (layoutWidth > 0) {
            val area = v.findViewById<ViewGroup>(R.id.area_list)
            area.removeView(v.findViewById(R.id.list))
            area.addView(View.inflate(activity, R.layout.subscreen_recycler_part, null),
                    layoutWidth, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
        //タイトルの設定
        v.findViewById<TextView>(R.id.title).text = title
        //リストの設定
        lv = v.findViewById(R.id.list)
        lv.adapter = adapter
        if (onItemClickListener != null) {
            adapter.onItemClickListener = OnClickListener {
                onItemClickListener.onItemClick(adapter.getItemPosition(it))
                if (autoClose) {
                    hideOwn()
                }
            }
        }
        //閉じるボタンの設定
        val closeBtn = v.findViewById<View>(R.id.btn_close)
        if (showCloseBtn) {
            closeBtn.visibility = View.VISIBLE
            closeBtn.setOnClickListener {
                hideOwn()
            }
        } else {
            closeBtn.visibility = View.GONE
        }
    }
}
