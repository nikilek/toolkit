package net.nikilek.toolkit.component

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import net.nikilek.toolkit.AbstractActivity

import net.nikilek.toolkit.R

import net.nikilek.toolkit.AbstractDialog
import net.nikilek.toolkit.extension.*

/**
 * Created by nikilek on 2018/02/23.
 * 数値入力用ダイアログ
 * ※現時点では自然数のみ入力可能
 */
class InputNumberDialog : AbstractDialog {

    private var onFinishListener: OnFinishListener //終了時イベント発火用
    private var oldText = "" //ダイアログ起動時の入力内容
    private var minValue = 0 //最小値
    private var maxValue = Int.MAX_VALUE //最大値

    private val numberPickers = mutableListOf<View>() //ナンバーピッカーの参照
    private val numbers = mutableListOf<TextView>() //ナンバーピッカーのvalue要素の参照

    /**
     * ダイアログ終了時イベントのリスナ
     */
    interface OnFinishListener {
        /**
         * @param text 入力内容
         * @param textChanged 入力内容が変化したか否か
         * @param canceled キャンセルボタンを押されたか否か
         */
        fun onFinish(text: String, textChanged: Boolean, canceled: Boolean)
    }

    /**
     * 入力可能範囲の設定
     * @param minValue 最小値
     * @param maxValue 最大値
     */
    fun setRange(minValue: Int, maxValue: Int) {
        //最小値の取得
        if (minValue >= 0) {
            this.minValue = minValue
        }
        if (maxValue > minValue) {
            this.maxValue = maxValue
        }
        //入力ガイドの設定
        val txtHint = v.findViewById<TextView>(R.id.hint)
        if (maxValue != 0) {
            txtHint.text = getString(R.string.hint_input_range).putParams(minValue.toString(), maxValue.toString())
        } else {
            txtHint.text = getString(R.string.hint_input_range_min).putParams(minValue.toString())
        }
        //上下限のバリデーションのために入力済みの値を書き戻す（prepareからのコール時は空振り）
        setNumberPicker(getNumberPicker())
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param onFinishListener 終了時イベントのリスナ
     * @param title 入力項目のタイトル
     * @param currentText 現在の入力内容
     * @param maxLength 最大桁数（1以上）
     */
    constructor(activity: AbstractActivity, onFinishListener: InputNumberDialog.OnFinishListener,
                title: String, currentText: String, maxLength: Int) : super(activity) {
        this.onFinishListener = onFinishListener
        this.oldText = currentText
        prepare(title, 0, (10).pow(maxLength) - 1)
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param theme ダイアログのテーマ
     * @param onFinishListener 終了時イベントのリスナ
     * @param title 入力項目のタイトル
     * @param currentText 現在の入力内容
     * @param maxLength 最大桁数（1以上）
     */
    constructor(activity: AbstractActivity, theme: Int, onFinishListener: InputNumberDialog.OnFinishListener,
                title: String, currentText: String, maxLength: Int) : super(activity, theme) {
        this.onFinishListener = onFinishListener
        this.oldText = currentText
        prepare(title, 0, (10).pow(maxLength) - 1)
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param onFinishListener 終了時イベントのリスナ
     * @param title 入力項目のタイトル
     * @param currentText 現在の入力内容
     * @param minValue 最小値（0以上）
     * @param maxValue 最大値（0以上）
     */
    constructor(activity: AbstractActivity, onFinishListener: InputNumberDialog.OnFinishListener,
                title: String, currentText: String, minValue: Int, maxValue: Int) : super(activity) {
        this.onFinishListener = onFinishListener
        this.oldText = currentText
        prepare(title, minValue, maxValue)
    }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のActivity
     * @param theme ダイアログのテーマ
     * @param onFinishListener 終了時イベントのリスナ
     * @param title 入力項目のタイトル
     * @param currentText 現在の入力内容
     * @param minValue 最小値（0以上）
     * @param maxValue 最大値（0以上）
     */
    constructor(activity: AbstractActivity, theme: Int, onFinishListener: InputNumberDialog.OnFinishListener,
                title: String, currentText: String, minValue: Int, maxValue: Int) : super(activity, theme) {
        this.onFinishListener = onFinishListener
        this.oldText = currentText
        prepare(title, minValue, maxValue)
    }


    /**
     * 初期化
     * @param title 入力項目のタイトル
     * @param minValue 最小値
     * @param maxValue 最大値
     */
    private fun prepare(title: String, minValue: Int, maxValue: Int) {
        super.prepare(R.layout.dialog_input_number, fullScreen = true, cancelable = true, useKeyboard = false)
        //タイトルの設定
        val txtTitle = v.findViewById<TextView>(R.id.title)
        if (title.isEmpty()) {
            txtTitle.visibility = View.INVISIBLE
        } else {
            txtTitle.text = title
        }
        //入力範囲の設定
        setRange(minValue, maxValue)
    }

    /**
     * ナンバーピッカーに値を設定
     * @param num 新しい値
     */
    private fun setNumberPicker(num: Int) {
        setNumberPicker(num, false)
    }

    /**
     * ナンバーピッカーに値を設定
     * @param num 新しい値
     * @param addDigit 桁追加（最小桁に数値が追加された）かのフラグ
     * @param force 強制（上下限無視するか）のフラグ
     */
    private fun setNumberPicker(num: Int, addDigit: Boolean, force: Boolean = false) {
        var value = num
        //桁追加（最小桁に数値が追加された）の場合かつ、最大値を超える場合
        if (addDigit && value > maxValue) {
            //桁追加ではなく、元の最小桁への上書きに変更する
            value = (value / 100) * 10 + value.pick(1)
        }
        //範囲チェック
        if(!force){
            if (value < minValue) {
                value = minValue
            }
            if (value > maxValue) {
                value = maxValue
            }
        }
        //ナンバーピッカーに設定
        for (i in 0 until numberPickers.size) {
            //対象の桁に数値があるかを判定
            if (value.length() < i + 1) {
                //0を設定して非表示
                numberPickers[i].visibility = View.INVISIBLE
                numbers[i].text = "0"
            } else {
                //対象の桁を表示する
                numberPickers[i].visibility = View.VISIBLE
                numbers[i].text = value.pick(i + 1).toString()
            }
        }
    }

    /**
     * ナンバーピッカーから値を取得
     * @return ナンバーピッカーに設定済の値
     */
    private fun getNumberPicker(): Int {
        //各桁の数値を取得して合算
        return (0 until numbers.size).sumBy { numbers[it].text.toString().toIntOrDefault(0) * (10).pow(it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //ナンバーピッカーの参照を取得
        val numberPickerArea = findViewById<ViewGroup>(R.id.area_number_picker)
        (numberPickerArea.childCount - 1 downTo 0).mapTo(numberPickers) { numberPickerArea.getChildAt(it) }
        //各ナンバーピッカーについて処理
        for (i in 0 until numberPickers.size) {
            val picker = numberPickers[i]
            //value要素の参照を格納
            numbers.add(picker.findViewById(R.id.value))
            //増減ボタン押下時処理を設定
            picker.findViewById<View>(R.id.btn_up).setOnClickListener {
                setNumberPicker(getNumberPicker() + (1).scale(i + 1))
            }
            picker.findViewById<View>(R.id.btn_down).setOnClickListener {
                setNumberPicker(getNumberPicker() - (1).scale(i + 1))
            }
        }
        //入力済みの値を書き戻す
        setNumberPicker(oldText.toIntOrDefault(0))
        //数値ボタンの設定
        findViewById<View>(R.id.btn_0).setOnClickListener { setNumberPicker(getNumberPicker() * 10 + 0, true) }
        findViewById<View>(R.id.btn_1).setOnClickListener { setNumberPicker(getNumberPicker() * 10 + 1, true) }
        findViewById<View>(R.id.btn_2).setOnClickListener { setNumberPicker(getNumberPicker() * 10 + 2, true) }
        findViewById<View>(R.id.btn_3).setOnClickListener { setNumberPicker(getNumberPicker() * 10 + 3, true) }
        findViewById<View>(R.id.btn_4).setOnClickListener { setNumberPicker(getNumberPicker() * 10 + 4, true) }
        findViewById<View>(R.id.btn_5).setOnClickListener { setNumberPicker(getNumberPicker() * 10 + 5, true) }
        findViewById<View>(R.id.btn_6).setOnClickListener { setNumberPicker(getNumberPicker() * 10 + 6, true) }
        findViewById<View>(R.id.btn_7).setOnClickListener { setNumberPicker(getNumberPicker() * 10 + 7, true) }
        findViewById<View>(R.id.btn_8).setOnClickListener { setNumberPicker(getNumberPicker() * 10 + 8, true) }
        findViewById<View>(R.id.btn_9).setOnClickListener { setNumberPicker(getNumberPicker() * 10 + 9, true) }
        findViewById<View>(R.id.btn_00).setOnClickListener { setNumberPicker(getNumberPicker() * 100) }
        findViewById<View>(R.id.btn_000).setOnClickListener { setNumberPicker(getNumberPicker() * 1000) }
        //DEL押下時
        val btnDel = v.findViewById<View>(R.id.btn_del)
        btnDel.setOnClickListener {
            //最小桁を消す
            setNumberPicker(getNumberPicker() / 10)
        }
        //クリア押下時
        val btnClear = v.findViewById<View>(R.id.btn_clear)
        btnClear.setOnClickListener {
            //0にする
            setNumberPicker(0, addDigit = false, force = true)
        }
        //OK押下時
        val btnOk = v.findViewById<View>(R.id.btn_ok)
        btnOk.setOnClickListener {
            //入力値の取得
            val str = getNumberPicker().toString()
            //範囲チェック
            if(!str.toIntOrDefault(Int.MIN_VALUE).isInRange(minValue, maxValue)){
                val title = activity.getString(R.string.title_error)
                val message = activity.getString(R.string.hint_input_range).putParams(minValue, maxValue)
                activity.showErrorMessage(title, message)
                return@setOnClickListener
            }
            //コールバック
            onFinishListener.onFinish(str, (oldText != str), false)
            //自身を閉じる
            dismiss()
        }
    }
}