package net.nikilek.toolkit.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import net.nikilek.toolkit.AbstractSubScreen

import net.nikilek.toolkit.R

/**
 * Created by nikilek on 2016/01/27.
 * リスト表示用ダイアログ
 */
class ListSubScreen(context: Context, attrs: AttributeSet) : AbstractSubScreen(context, attrs) {

    /**
     * 起動パラメータ
     */
    data class InitData(
            var title: String, //表示タイトル
            var adapter: BaseAdapter, //リスト表示用アダプタ
            var onItemClickListener: AdapterView.OnItemClickListener?, //リスト選択時イベントのリスナ
            var showCloseBtn: Boolean, //閉じるボタン表示フラグ
            var layoutWidth: Int = 0 //リストの幅（未指定時は@layout/subscreen_listで指定された幅）※既定値：0（未指定）
    )

    private lateinit var lv: ListView

    override fun onAttach() {
        //初期化
        prepare(initData as ListSubScreen.InitData)
    }

    override fun onDetach() {
        //何もしない
    }

    /**
     * 初期化処理
     * @param initData 初期化データ
     */
    private fun prepare(initData: InitData) {
        val title = initData.title
        val adapter = initData.adapter
        val onItemClickListener = initData.onItemClickListener
        val showCloseBtn = initData.showCloseBtn
        val layoutWidth = initData.layoutWidth
        //幅の設定
        if (layoutWidth > 0) {
            val area = v.findViewById<ViewGroup>(R.id.area_list)
            area.removeView(v.findViewById(R.id.list))
            area.addView(View.inflate(activity, R.layout.subscreen_list_part, null),
                    layoutWidth, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
        //タイトルの設定
        v.findViewById<TextView>(R.id.title).text = title
        //リストの設定
        lv = v.findViewById(R.id.list)
        lv.adapter = adapter
        if (onItemClickListener != null) {
            lv.onItemClickListener = onItemClickListener
        }
        //閉じるボタンの設定
        val closeBtn = v.findViewById<View>(R.id.btn_close)
        if (showCloseBtn) {
            closeBtn.visibility = View.VISIBLE
            closeBtn.setOnClickListener {
                hideOwn()
            }
        } else {
            closeBtn.visibility = View.GONE
        }
    }

    /**
     * リスト選択時イベントの設定
     * @param onItemClickListener アイテムクリック時イベントのリスナ
     */
    fun attachItemClickEvent(onItemClickListener: AdapterView.OnItemClickListener) {
        lv.onItemClickListener = onItemClickListener
    }
}
