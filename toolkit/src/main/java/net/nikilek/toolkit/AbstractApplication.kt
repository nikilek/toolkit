package net.nikilek.toolkit

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex

import net.nikilek.toolkit.utility.FileUtil
import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.interfaces.ISaveData
import net.nikilek.toolkit.model.EmptySaveData
import net.nikilek.toolkit.utility.ToastUtil
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Created by nikilek on 2016/01/27.
 * 基底アプリケーション
 *
 * Fragmentを(あまり)使わず、Activityの数も極力増やさない思想となっています
 * Application, Activity, Dialog, Screen(独自), SubScreen(独自)は、それぞれAbstractクラスを継承して実装してください
 * ・{Root Activity}(MainActivity)は、{net.nikilek.toolkit._sample.MainActivity}を参考に実装してください
 * ・{Screen}は、Activityの上に重ねて起動する、小さなActivityのような役割です
 * ・{SubScreen}は、Screenの上に重ねて起動する、小さなDialogのような役割です
 * ・{Dialog}は、(同一Activity内の)全ての{Screen}や{SubScreen}の上位(表面)に表示されます
 * ・以下に、DialogとSubScreenの実装例を示します
 * 　- {net.nikilek.toolkit.component.MessageDialog}     ※Activityから起動します
 * 　- {net.nikilek.toolkit.component.MessageSubScreen}  ※Screenから起動します。ScreenはActivityから起動します
 *
 * {Application<AbstractApplication>}
 *    ├──{Root Activity<AbstractActivity>}
 *    │     ├──{Screen<AbstractScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ~
 *    │     ├──{Screen@AbstractScreen}
 *    │     ~
 *    │     ~
 *    │     └──{Dialog@AbstractDialog}
 *    │
 *    ├──{Activity<AbstractActivity>}
 *    │     ├──{Screen<AbstractScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ~
 *    │     ├──{Screen<AbstractScreen>}
 *    │     ~
 *    │     ~
 *    │     └──{Dialog<AbstractDialog>}
 *    ~
 */
abstract class AbstractApplication : Application() {

    private var savedata: ISaveData? = null //セーブデータ
    private val lockSavedata = ReentrantLock() //セーブデータ操作用ロック

    /**
     * セーブデータの取得
     * @param force    参照中のデータを破棄して読み込み直すかのフラグ
     * @return 取得したセーブデータ
     */
    fun getSaveData(force: Boolean): ISaveData {
        lockSavedata.withLock {
            //読込済の場合はそのまま返す
            if (savedata is ISaveData && !force) {
                return savedata!!
            }
            //最新版の読込
            var fileUtil = FileUtil(this, Settings.File.PATH_SAVEDATA, Settings.File.TO_SYSTEM)
            savedata = fileUtil.load(Settings.File.SECRET_SAVEDATA, Settings.Key.AES)
            //最新版読込成功時は正常復帰
            if (savedata is ISaveData) {
                return savedata!!
            }
            //バックアップを最新版にリストア
            fileUtil = FileUtil(this, Settings.File.PATH_SAVEDATA_BACKUP, Settings.File.TO_SYSTEM)
            fileUtil.rename(Settings.File.PATH_SAVEDATA_BACKUP)
            //バックアップの読込
            savedata = fileUtil.load(Settings.File.SECRET_SAVEDATA, Settings.Key.AES)
            //バックアップ読込成功時は正常復帰
            if (savedata is ISaveData) {
                //トースト表示
                ToastUtil.showLoadBackup(this)
                return savedata!!
            }
            //それでも読込に失敗した場合は、空のセーブデータを返す
            savedata = EmptySaveData()
            if (force) {
                //トースト表示
                ToastUtil.showLoadError(this)
            }
            return savedata!!
        }
    }

    /**
     * セーブデータの保存
     * @param savedata 新たなセーブデータ（参照を更新しない場合はnullを指定）　※既定値：null
     * @return 成否
     */
    fun setSaveData(savedata: ISaveData? = null): Boolean {
        lockSavedata.withLock {
            //新たなデータが指定された場合は受け取る
            if (savedata != null) {
                this.savedata = savedata
            }
            //参照が無く、新たなデータも渡されなかった場合
            if (this.savedata == null) {
                return false
            }
            //セーブデータが空の場合
            if (this.savedata?.isEmpty != false) {
                return false
            }
            //ファイル出力
            var result = false
            for (i in 1..3) {
                //テンポラリに出力
                val fileUtilTemp = FileUtil(this, Settings.File.PATH_SAVEDATA_TEMPORARY, Settings.File.TO_SYSTEM)
                if (!fileUtilTemp.save(this.savedata, Settings.File.SECRET_SAVEDATA, Settings.Key.AES)) {
                    continue
                }
                //最新版をバックアップに移行
                val fileUtilCurrent = FileUtil(this, Settings.File.PATH_SAVEDATA, Settings.File.TO_SYSTEM)
                if (fileUtilCurrent.isExistFile && !fileUtilCurrent.rename(Settings.File.PATH_SAVEDATA_BACKUP,
                                autoAddEnvironmentDirectory = true, autoAddExtension = false)) {
                    //ファイルが存在しない場合はスルー
                    continue
                }
                //テンポラリを最新版に移行
                if (!fileUtilTemp.rename(Settings.File.PATH_SAVEDATA,
                                autoAddEnvironmentDirectory = true, autoAddExtension = false)) {
                    continue
                }
                //成功フラグON
                result = true
                break
            }
            if (!result) {
                //トースト表示
                ToastUtil.showSaveError(this)
            }
            return result
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}