package net.nikilek.toolkit

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.FrameLayout

/**
 * Created by nikilek on 2016/01/27.
 * タッチイベントを子ビューに伝播させないレイアウト
 */
open class InterceptFrameLayout : FrameLayout {

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context) {
        this.isClickable = true
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.isClickable = true
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        this.isClickable = true
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return true
    }

}
