package net.nikilek.toolkit

import java.lang.Thread.UncaughtExceptionHandler

import android.content.Context

import net.nikilek.toolkit.utility.LogUtil

/**
 * Created by nikilek on 2016/01/27.
 * カスタムハンドラ
 * キャッチされない例外を処理する　※Thread#setDefaultUncaughtExceptionHandlerにて設定
 */
open class MyUncaughtExceptionHandler(context: Context) : UncaughtExceptionHandler {
    private val defaultHandler: UncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()!! //デフォルトハンドラ
    private val sContext: Context = context //コンテキスト

    override fun uncaughtException(th: Thread, t: Throwable) {
        try {
            //エラー内容の出力
            LogUtil.outputErrorLog(sContext, t)
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
        //デフォルトの処理
        defaultHandler.uncaughtException(th, t)
    }
}