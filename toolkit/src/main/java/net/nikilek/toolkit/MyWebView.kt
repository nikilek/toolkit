package net.nikilek.toolkit

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.util.AttributeSet
import android.webkit.CookieManager
import android.webkit.HttpAuthHandler
import android.webkit.SslErrorHandler
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.webkit.WebViewDatabase
import net.nikilek.toolkit.utility.AssetReader
import net.nikilek.toolkit.utility.LogUtil

import java.util.ArrayList

/**
 * Created by nikilek on 2016/01/27.
 * カスタムビュー
 *
 * 下記条件の場合、別アプリに委譲
 * ・URLの末尾が「pdf」：別ブラウザ起動
 * ・URLの先頭が「market://」：別ブラウザ起動
 * ・URLの先頭が「mailto:」：メールアプリ起動
 * ・URLの先頭が「tel:」：電話アプリ起動
 * ・URLの先頭が「except:」：別ブラウザ起動　※オリジナルの識別子。上記以外のURLで、別ブラウザで起動したい場合用
 */
open class MyWebView : WebView {

    //ページ情報
    var currentURL: String = "" //現在表示しているページのURL
    var currentPageTitle: String = "" //現在表示しているページのタイトル

    //履歴情報
    private val history = ArrayList<String>() //ページの表示履歴
    val existPrevious: Boolean //前のページが存在するか
        get() = history.size > 1

    //処理情報
    var loading: Boolean = false //ロード中フラグ
    private val scripts = ArrayList<String>() //ロードするスクリプトのキュー（this#loadScript経由でスクリプト実行の場合に使用）

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    @SuppressLint("SetJavaScriptEnabled", "ObsoleteSdkInt")
    public override fun onFinishInflate() {
        super.onFinishInflate()
        if (this.isInEditMode) {
            return
        }
        //WebViewClientの初期化
        this.webViewClient = object : WebViewClient() {
            private var loginCookie: String? = null //サイトのログイン用Cookie

            //リソースの読込時処理
            override fun onLoadResource(view: WebView, url: String) {
                val cookieManager = CookieManager.getInstance()
                loginCookie = cookieManager.getCookie(url)
            }

            //ページ読込終了時処理
            override fun onPageFinished(view: WebView, url: String) {
                currentURL = getUrl() // 現在ページのURLを取得
                currentPageTitle = title // 現在ページのタイトルを取得
                if (loginCookie != null) {
                    val cookieManager = CookieManager.getInstance()
                    cookieManager.setCookie(url, loginCookie)
                    cookieManager.setAcceptCookie(true)
                }
                history.add(currentURL) //履歴保存
                //待機状態にしていたスクリプトを実行
                scripts.forEach {
                    loadUrl("javascript:$it") //※再度ロード中フラグがONになる事に注意。なお、onPageFinishedは発生しないので考慮不要。
                }
                scripts.clear() //実行済のスクリプトを破棄（排他考慮しない）
                loading = false //ロード中フラグOFF
            }

            //ページ読込時処理
            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                @Suppress("DEPRECATION")
                return shouldOverrideUrlLoading(view, request.url.toString())
            }

            //ページ読込時処理
            @Suppress("OverridingDeprecatedMember")
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                try {
                    //if(view.getHitTestResult().getType() != 0 || url == "about:blank" || getUrl() == url){ //リダイレクト無効化の場合はこちらの条件
                    if (url == "about:blank" || getUrl() == url) {
                        return false
                    }
                    //自分でどうしようもなかったら他に投げる
                    if (context is Activity) {
                        var intent: Intent? = null
                        if (url.substring(url.length - 4) == ".pdf" || url.substring(0, 9) == "market://") {
                            intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                        } else if (url.substring(0, 7) == "mailto:") {
                            intent = Intent(Intent.ACTION_SENDTO, Uri.parse(url))
                        } else if (url.substring(0, 4) == "tel:") {
                            intent = Intent(Intent.ACTION_CALL, Uri.parse(url))
                        } else if (url.substring(0, 7) == "except:") {
                            intent = Intent(Intent.ACTION_VIEW, Uri.parse(url.substring(7, url.length - 1)))
                        }
                        // 別アプリ起動
                        if (intent != null) {
                            (context as Activity).startActivity(intent)
                            return true
                        }
                    }
                    //ページのロード
                    if (!loading) {
                        //明示的な読み込みでない（ページ内リンクでの遷移など）場合はロードを実行
                        loadUrl(url)
                    }
                    return true
                } catch (e: Exception) {
                    LogUtil.debug(e)
                    return false
                }
            }

            //SSL認証エラー回避
            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
                //handler.proceed(); //継続
                handler.cancel() //強制キャンセル
            }

            //Basic認証　※自動認証には非対応（別途コーディングが必要）
            override fun onReceivedHttpAuthRequest(view: WebView, handler: HttpAuthHandler, host: String, realm: String) {
                CookieManager.getInstance().setAcceptCookie(false)
                //以下、必要なら入力ダイアログの表示処理を挟む
                val up = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    WebViewDatabase.getInstance(context).getHttpAuthUsernamePassword(host, realm) as Array<String>
                } else {
                    @Suppress("DEPRECATION")
                    view.getHttpAuthUsernamePassword(host, realm) as Array<String>
                }
                val id = arrayOf("test", "test")
                handler.proceed(id[0], id[1])
                handler.proceed(up[0], up[1])
            }
        }
        //WebChromeClientの初期化
        this.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                //プログレス表示したい場合はここに処理を書く
            }
        }
        //Basic認証の設定
        WebViewDatabase.getInstance(context).clearHttpAuthUsernamePassword()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            WebViewDatabase.getInstance(context).setHttpAuthUsernamePassword("host", "realm", "username", "password")
        } else {
            @Suppress("DEPRECATION")
            this.setHttpAuthUsernamePassword("host", "realm", "username", "password")
        }
        //ズームの設定
        this.settings.setSupportZoom(true)
        this.settings.builtInZoomControls = true
        //this.settings.setUseWideViewPort(true); // ダブルタップによるズームを許可
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            this.settings.displayZoomControls = false //ズームコントロールを非表示
        }
        //その他の設定
        this.settings.setSupportMultipleWindows(false) // マルチウィンドウ許可
        this.settings.loadsImagesAutomatically = true //画像の自動読込を許可
        this.settings.javaScriptEnabled = true // JavaScript有効
        this.settings.javaScriptCanOpenWindowsAutomatically = false // JavaScriptからのウィンドウオープンを許可
        this.settings.domStorageEnabled = true //HTML5のWebStorage有効化
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            @Suppress("DEPRECATION")
            this.settings.saveFormData = true // フォームデータの保存
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        try {
            this.destroy()
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    override fun loadUrl(url: String?, additionalHttpHeaders: MutableMap<String, String>?) {
        loading = true //ロード中フラグON
        super.loadUrl(url, additionalHttpHeaders)
    }

    override fun loadUrl(url: String?) {
        loading = true //ロード中フラグON
        super.loadUrl(url)
    }

    /**
     * Assetディレクトリ配下（直下）のファイルを読み込んで表示
     * @param fileName 読み込むファイルのファイル名
     */
    fun loadFile(fileName: String) {
        loadFile("", fileName)
    }

    /**
     * Assetディレクトリ配下のファイルを読み込んで表示
     * @param directory Assetディレクトリからの、対象ディレクトリの相対パス
     * @param fileName 読み込むファイルのファイル名
     */
    fun loadFile(directory: String, fileName: String) {
        var dir = directory
        if (dir.isNotEmpty()) {
            //プレフィックスの修正
            if (dir.startsWith("/")) {
                dir = dir.substring(1)
            }
            //サフィックスの修正
            if (!dir.endsWith("/")) {
                dir += "/"
            }
        }
        loadUrl("file:///android_asset/$dir$fileName")
    }

    /**
     * スクリプトの実行
     * @param script 実行するスクリプトの文字列
     */
    fun loadScript(script: String) {
        if (loading) {
            //ロード中の場合はロード終了時まで保管
            scripts.add(script)
        } else {
            //即時実行
            loadUrl("javascript:$script")
        }
    }

    /**
     * 部分ビューの読み込み
     * @param parentId 部分ビューを読み込む先のタグのID
     * @param htmlFilePath 読み込む部分ビューのパス（「file:///android_asset/xxx/yyy.html」の「xxx/yyy.html」部分）
     * @param insertType 挿入位置のタイプ（beforebegin/afterbegin/beforeend/afterend）※既定値：beforeend
     */
    fun loadHtml(parentId: String, htmlFilePath: String, insertType: String = "beforeend") {
        val html = AssetReader(context).readRawText(htmlFilePath)
        loadScript("document.getElementById('$parentId').insertAdjacentHTML('$insertType','$html')")
    }

    /**
     * 前のページに戻る
     */
    fun previous() {
        if (history.size < 2) {
            return
        }
        val url = history[history.size - 2]
        for (i in 0..1) {
            history.removeAt(0)
        }
        loadUrl(url)
    }
}
