package net.nikilek.toolkit

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.ScrollView

/**
 * Created by nikilek on 2016/01/27.
 * カスタムビュー
 */
open class MyScrollView : ScrollView {

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context) {
        prepare()
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        prepare()
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        prepare()
    }

    /**
     * 初期化
     */
    @SuppressLint("ObsoleteSdkInt")
    private fun prepare() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD_MR1) {
            //オーバースクロールの設定
            this.overScrollMode = View.OVER_SCROLL_IF_CONTENT_SCROLLS
        }
        this.isFillViewport = true //ビューの高さの設定
    }

    @Suppress("RedundantOverride")
    override fun overScrollBy(deltaX: Int, deltaY: Int, scrollX: Int, scrollY: Int, scrollRangeX: Int, scrollRangeY: Int, maxOverScrollX: Int, maxOverScrollY: Int, isTouchEvent: Boolean): Boolean {
        //オーバースクロール時の挙動を設定
        return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent)
        //return super.overScrollBy(0, deltaY, 0, scrollY, 0, scrollRangeY, 0, 50, isTouchEvent);
    }

    /**
     * 一番上までスクロール
     */
    fun scrollTop() {
        this.post { fullScroll(ScrollView.FOCUS_UP) }
    }

    /**
     * 一番下までスクロール
     */
    fun scrollBottom() {
        this.post { fullScroll(ScrollView.FOCUS_DOWN) }
    }
}
