package net.nikilek.toolkit

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.*
import android.view.WindowManager.LayoutParams
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import net.nikilek.toolkit.component.MessageDialog
import net.nikilek.toolkit.component.MessageSubScreen
import net.nikilek.toolkit.constant.AnimationConst

import java.lang.ref.WeakReference

import net.nikilek.toolkit.constant.CodeConst
import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.extension.setGone
import net.nikilek.toolkit.extension.setInvisible
import net.nikilek.toolkit.extension.setVisible
import net.nikilek.toolkit.model.SubScreenData
import net.nikilek.toolkit.interfaces.ISaveData
import net.nikilek.toolkit.model.ScreenData
import net.nikilek.toolkit.utility.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Created by nikilek on 2016/01/27.
 * 基底アクティビティ
 *
 * Fragmentを(あまり)使わず、Activityの数も極力増やさない思想となっています
 * Application, Activity, Dialog, Screen(独自), SubScreen(独自)は、それぞれAbstractクラスを継承して実装してください
 * ・{Root Activity}(MainActivity)は、{net.nikilek.toolkit._sample.MainActivity}を参考に実装してください
 * ・{Screen}は、Activityの上に重ねて起動する、小さなActivityのような役割です
 * ・{SubScreen}は、Screenの上に重ねて起動する、小さなDialogのような役割です
 * ・{Dialog}は、(同一Activity内の)全ての{Screen}や{SubScreen}の上位(表面)に表示されます
 * ・以下に、DialogとSubScreenの実装例を示します
 * 　- {net.nikilek.toolkit.component.MessageDialog}     ※Activityから起動します
 * 　- {net.nikilek.toolkit.component.MessageSubScreen}  ※Screenから起動します。ScreenはActivityから起動します
 *
 * {Application<AbstractApplication>}
 *    ├──{Root Activity<AbstractActivity>}
 *    │     ├──{Screen<AbstractScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ~
 *    │     ├──{Screen@AbstractScreen}
 *    │     ~
 *    │     ~
 *    │     └──{Dialog@AbstractDialog}
 *    │
 *    ├──{Activity<AbstractActivity>}
 *    │     ├──{Screen<AbstractScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ~
 *    │     ├──{Screen<AbstractScreen>}
 *    │     ~
 *    │     ~
 *    │     └──{Dialog<AbstractDialog>}
 *    ~
 */
abstract class AbstractActivity : Activity() {
    private lateinit var savedata: ISaveData //セーブデータ //onCreateで初期化
    lateinit var handler: Handler //ハンドラ //onCreateで初期化
    lateinit var view: View //ビュー //onCreateで初期化

    //ActivityResultのコールバックのマップ { requestCode, コールバック関数(結果コード, Intentデータ) }
    private var activityResultCallback: MutableMap<Int, ((Int, Intent?) -> Unit)> = mutableMapOf()
    private var activityResultCallbackLock = ReentrantLock() //ロックオブジェクト
    fun addActivityResultCallback(requestCode: Int, callback: ((Int, Intent?) -> Unit)) {
        activityResultCallbackLock.withLock {
            activityResultCallback[requestCode] = callback
        }
    }

    //RequestPermissionsResultのコールバックのマップ { requestCode, コールバック関数(全て許可されたかのフラグ, リクエストしたパーミッションの配列, 結果の配列) }
    private var requestPermissionResultCallback: MutableMap<Int, ((Boolean, Array<String>, IntArray) -> Unit)> = mutableMapOf()
    private var requestPermissionResultCallbackLock = ReentrantLock() //ロックオブジェクト
    fun addRequestPermissionResultCallback(requestCode: Int, callback: ((Boolean, Array<String>, IntArray) -> Unit)) {
        requestPermissionResultCallbackLock.withLock {
            requestPermissionResultCallback[requestCode] = callback
        }
    }

    //スクリーン操作関連
    protected val screens: MutableList<ScreenData> = mutableListOf() //子スクリーン
    private val screenLock = ReentrantLock() //スクリーン追加／削除ロックオブジェクト

    @Volatile
    private var screenLocked: Boolean = false //スクリーン追加／削除ロック中フラグ

    //メッセージスクリ－ンのレイアウト
    var messageSubScreenLayoutId = R.layout.subscreen_message
        protected set

    //ダイアログ関連
    private var dialog: Dialog? = null //ダイアログ（1つのActivityに対して1つのダイアログのみ　※ダイアログから子ダイアログを表示する事は可能）
    val dialogShowing: Boolean //ダイアログ表示中フラグ
        get() = dialog != null && dialog!!.isShowing

    //その他
    protected var lastClickTime: Long = 0 //最後にボタンを押した時刻（連打防止チェック用）

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE) // タイトルバー無し
        this.window.setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN or LayoutParams.SOFT_INPUT_ADJUST_PAN) // キーボード関係
        //色の設定
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //動的レイアウトの有効化
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            //色の設定
            this.window.statusBarColor = getResColor(R.color.theme_base_darker) //ステータスバー
            this.window.navigationBarColor = getResColor(R.color.theme_base_darker) //ナビゲーションバー
        }
        //クラスメンバの初期化
        load() //セーブデータのロード
        this.handler = IncomingHandler(this) //ハンドラの取得
        this.view = findViewById(android.R.id.content)
    }

    override fun onResume() {
        super.onResume()
        (handler as? BufferedHandler)?.resume()
        (screens.lastOrNull()?.createdView as? AbstractScreen)?.onActive()
    }

    override fun onPause() {
        super.onPause()
        (screens.lastOrNull()?.createdView as? AbstractScreen)?.onInActive()
        (handler as? BufferedHandler)?.pause()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        activityResultCallbackLock.withLock {
            if (activityResultCallback.containsKey(requestCode)) {
                activityResultCallback[requestCode]?.invoke(resultCode, data)
                activityResultCallback.remove(requestCode)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        requestPermissionResultCallbackLock.withLock {
            if (requestPermissionResultCallback.containsKey(requestCode)) {
                val result = permissions.size == grantResults.filter { it == PackageManager.PERMISSION_GRANTED }.size
                requestPermissionResultCallback[requestCode]?.invoke(result, permissions, grantResults)
                requestPermissionResultCallback.remove(requestCode)
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    /*
     * ↓セーブデータ関連↓
     */

    /**
     * セーブデータの取得
     */
    @Suppress("UNCHECKED_CAST")
    open fun <T : ISaveData> mSaveData(): T {
        return savedata as T
    }

    /**
     * セーブデータのロード
     * @param force 参照中のデータを破棄してファイルから読み込み直すかのフラグ　※既定値：false
     */
    fun load(force: Boolean = false) {
        savedata = (this.application as AbstractApplication).getSaveData(force)
        savedata.valid()
    }

    /**
     * セーブデータのセーブ
     * @param savedata 参照中のデータを破棄して新たなデータを使用する場合に指定　※既定値：null
     */
    fun save(savedata: ISaveData? = null) {
        (this.application as AbstractApplication).setSaveData(savedata)
        if (savedata != null) {
            //新たなデータが設定された場合は再ロード
            load()
        }
    }

    /*
     * ↓スクリーン関連↓
     */

    /**
     * スクリーンの表示
     * @param layoutId スクリーンのレイアウトID
     * @param initData 起動時に受け渡すデータ
     * @param enableBack バックキー有効／無効　※既定値：true
     * @return 生成したスクリーン情報
     */
    @JvmOverloads
    fun showScreen(layoutId: Int, initData: Any?, enableBack: Boolean = true): ScreenData {
        screenLock.withLock {
            screenLocked = true //ロック開始
            val vg = view.findViewById<ViewGroup>(R.id.content_main) //ルートビュー
            val v = View.inflate(this, layoutId, null)
            val s = ScreenData(newScreenId(), layoutId, v, System.currentTimeMillis(), enableBack)
            (v as? AbstractScreen)?.setup(this, s, screens.lastOrNull(), initData) //スクリーン情報の関連付け
            //参照の保管
            val frontScreen: ScreenData? = screens.lastOrNull()
            val frontView: View? = frontScreen?.createdView
            //生成したスクリーンを保管
            screens.add(s)
            //対象のスクリーンを表示
            vg.apply {
                post {
                    v.setInvisible(true)
                    v.apply {
                        post {
                            v.setVisible(true)
                            startAnimation(AnimationUtils.loadAnimation(this@AbstractActivity, R.anim.activity_start))
                        }
                    }
                    addView(v)
                }
            }
            //１つ下の（最前面だった）スクリーン配下のサブスクリーンを非表示にする
            frontScreen?.subScreens?.indices?.reversed()?.forEach { idx ->
                val frontSubScreen = frontScreen.subScreens[idx]
                frontSubScreen.createdView.apply { post { setInvisible(true) } }
            }
            //１つ下の（最前面だった）スクリーンを非表示にする
            (frontView as? AbstractScreen)?.apply { post { onInActive() } } //非表示時処理
            GlobalScope.launch {
                delay(250)
                frontView?.apply { post { setInvisible(true) } }
            }
            screenLocked = false //ロック終了
            return s
        }
    }

    /**
     * スクリーンの非表示
     * @param s 対象のスクリーン情報の参照
     * @param byBackKey バックキー起点か　※既定値：false
     */
    @JvmOverloads
    fun hideScreen(s: ScreenData, byBackKey: Boolean = false) {
        screenLock.withLock {
            if (byBackKey && !s.enableBack) {
                //スクリーンに通知してリターン
                (s.createdView as? AbstractScreen)?.processMessage(CodeConst.MSG.ToIndividualScreen.BACK_KEY_PRESSED, 0)
                return
            }
            screenLocked = true //ロック開始
            val vg = view.findViewById<ViewGroup>(R.id.content_main) //ルートビュー
            //対象のスクリーンを削除
            screens.remove(s)
            //１つ下の（最前面に来た）スクリーンを可視化
            val frontScreen: ScreenData? = screens.lastOrNull()
            val frontView: View? = frontScreen?.createdView
            frontView?.post {
                (frontView as? AbstractScreen)?.onActive() //表示時処理
                frontView.setVisible(true)
            }
            //１つ下の（最前面に来た）スクリーン配下のサブスクリーンを可視化
            frontScreen?.subScreens?.indices?.forEach { idx ->
                val frontSubScreen = frontScreen.subScreens[idx]
                frontSubScreen.createdView.apply { post { setVisible(true) } }
            }
            //対象のスクリーン配下のサブスクリーンを全て削除
            if (s.subScreens.isNotEmpty()) {
                vg.post {
                    s.subScreens.forEach { ss ->
                        //サブスクリーン配下のサブスクリーンを全て削除
                        ss.getAllChildSubScreen().let { list ->
                            for (i in list.size - 1 downTo 0) {
                                val sss = list[i]
                                vg.removeViewInLayout(sss.createdView)
                            }
                        }
                        //サブスクリーンを削除
                        vg.removeViewInLayout(ss.createdView)
                    }
                }
            }
            //対象のスクリーンを非表示にする
            s.createdView.let { v ->
                v.post {
                    v.startAnimation(AnimationUtils.loadAnimation(this@AbstractActivity, R.anim.activity_end))
                    GlobalScope.launch {
                        delay(250)
                        vg.apply {
                            post {
                                v.setGone(true)
                                removeViewInLayout(v)
                            }
                        }
                    }
                }
            }
            screenLocked = false //ロック終了
        }
    }

    /**
     * サブスクリーンの表示
     * @param s サブスクリーンの親となるスクリーンの情報
     * @param layoutId サブスクリーンのレイアウトID
     * @param initData 起動時に受け渡すデータ
     * @param enableBack バックキー有効／無効　※既定値：true
     * @return 生成したサブスクリーン情報の参照
     */
    @JvmOverloads
    fun showSubScreen(s: ScreenData, layoutId: Int, initData: Any?, enableBack: Boolean = true): SubScreenData {
        return showSubScreen(s, layoutId, 0, initData, enableBack)
    }

    /**
     * サブスクリーンの表示
     * @param parent サブスクリーンの親となるスクリーンの情報（nullの場合は、Activityが管理しないスクリーンを自動生成）
     * @param layoutId サブスクリーンのレイアウトID
     * @param windowAnimationTarget ウィンドウアニメーションの対象とするビューのID（0：ウィンドウ全体、-1：アニメーション無し）
     * @param animationPivot ウィンドウアニメーションの中心（AnimationConst.PIVOT_xxx）
     * @param initData 起動時に受け渡すデータ
     * @param enableBack バックキー有効／無効　※既定値：true
     * @return 生成したサブスクリーン情報の参照
     */
    @JvmOverloads
    fun showSubScreen(
            parent: ScreenData?, layoutId: Int,
            windowAnimationTarget: Int, animationPivot: Int,
            initData: Any?, enableBack: Boolean = true
    ): SubScreenData {
        screenLock.withLock {
            screenLocked = true //ロック開始
            val vg = findViewById<ViewGroup>(R.id.content_sub) //ルートビュー
            val v = View.inflate(this, layoutId, null)
            val s = parent ?: ScreenData(createdView = View(this))
            val ss = SubScreenData(s, layoutId, windowAnimationTarget, animationPivot, v, System.currentTimeMillis(), enableBack)
            (v as? AbstractSubScreen)?.setup(this, ss, initData) //サブスクリーン情報の関連付け
            //削除済のスクリーン上でサブスクリーンが生成された場合、処理を中断する
            if (parent != null && !screens.contains(parent)) {
                screenLocked = false //ロック終了
                return ss
            }
            //生成したサブスクリーンを保管
            s.subScreens.add(ss)
            //対象のサブスクリーンを表示
            val animationTarget =
                    when (windowAnimationTarget) {
                        -1 -> null
                        0 -> v
                        else -> v.findViewById(windowAnimationTarget)
                    }
            val animId =
                    when (animationPivot) {
                        AnimationConst.PIVOT_LEFT -> R.anim.compress_in_left
                        AnimationConst.PIVOT_TOP -> R.anim.compress_in_top
                        AnimationConst.PIVOT_RIGHT -> R.anim.compress_in_right
                        AnimationConst.PIVOT_BOTTOM -> R.anim.compress_in_bottom
                        else -> R.anim.dialog_compress_y_start
                    }
            vg.apply {
                post {
                    animationTarget?.apply {
                        setInvisible(true)
                        post {
                            setVisible(true)
                            startAnimation(AnimationUtils.loadAnimation(this@AbstractActivity, animId))
                        }
                    }
                    addView(v)
                }
            }
            screenLocked = false //ロック終了
            return ss
        }
    }

    /**
     * サブスクリーンの表示
     * @param parent サブスクリーンの親となるスクリーンの情報（nullの場合は、Activityが管理しないスクリーンを自動生成）
     * @param layoutId サブスクリーンのレイアウトID
     * @param windowAnimationTarget ウィンドウアニメーションの対象とするビューのID（0：ウィンドウ全体、-1：アニメーション無し）
     * @param initData 起動時に受け渡すデータ
     * @param enableBack バックキー有効／無効　※既定値：true
     * @return 生成したサブスクリーン情報の参照
     */
    @JvmOverloads
    fun showSubScreen(
            parent: ScreenData?, layoutId: Int, windowAnimationTarget: Int,
            initData: Any?, enableBack: Boolean = true
    ): SubScreenData {
        return showSubScreen(parent, layoutId, windowAnimationTarget, AnimationConst.PIVOT_CENTER, initData, enableBack)
    }

    /**
     * サブスクリーンの表示（サブスクリーンからサブスクリーンを呼び出す場合に使用）
     * @param ss サブスクリーンの親となるサブスクリーンの情報
     * @param layoutId サブスクリーンのレイアウトID
     * @param initData 起動時に受け渡すデータ
     * @param enableBack バックキー有効／無効　※既定値：true
     * @return 生成したサブスクリーン情報の参照
     */
    @JvmOverloads
    fun showSubScreen(ss: SubScreenData, layoutId: Int, initData: Any?, enableBack: Boolean = true): SubScreenData {
        return showSubScreen(ss, layoutId, 0, initData, enableBack)
    }

    /**
     * サブスクリーンの表示（サブスクリーンからサブスクリーンを呼び出す場合に使用）
     * @param ss サブスクリーンの親となるサブスクリーンの情報
     * @param layoutId サブスクリーンのレイアウトID
     * @param windowAnimationTarget ウィンドウアニメーションの対象とするビューのID（0：ウィンドウ全体、-1：アニメーション無し）
     * @param initData 起動時に受け渡すデータ
     * @param enableBack バックキー有効／無効　※既定値：true
     * @return 生成したサブスクリーン情報の参照
     */
    @JvmOverloads
    fun showSubScreen(ss: SubScreenData, layoutId: Int, windowAnimationTarget: Int,
                      initData: Any?, enableBack: Boolean = true): SubScreenData {
        return showSubScreen(ss, layoutId, windowAnimationTarget, AnimationConst.PIVOT_CENTER, initData, enableBack)
    }

    /**
     * サブスクリーンの表示（サブスクリーンからサブスクリーンを呼び出す場合に使用）
     * @param ss サブスクリーンの親となるサブスクリーンの情報
     * @param layoutId サブスクリーンのレイアウトID
     * @param windowAnimationTarget ウィンドウアニメーションの対象とするビューのID（0：ウィンドウ全体、-1：アニメーション無し）
     * @param animationPivot ウィンドウアニメーションの中心（AnimationConst.PIVOT_xxx）
     * @param initData 起動時に受け渡すデータ
     * @param enableBack バックキー有効／無効　※既定値：true
     * @return 生成したサブスクリーン情報の参照
     */
    @JvmOverloads
    fun showSubScreen(ss: SubScreenData, layoutId: Int, windowAnimationTarget: Int, animationPivot: Int,
                      initData: Any?, enableBack: Boolean = true): SubScreenData {
        val createdSubScreen = showSubScreen(ss.parentScreen, layoutId, windowAnimationTarget, animationPivot, initData, enableBack)
        ss.childSubScreens.add(createdSubScreen)
        return createdSubScreen
    }

    /**
     * サブスクリーンの非表示
     * @param ssList 対象のサブスクリーン情報の参照のリスト
     * @param byBackKey バックキー起点か　※既定値：false
     */
    @JvmOverloads
    fun hideSubScreen(ssList: List<SubScreenData>, byBackKey: Boolean = false) {
        screenLock.withLock {
            for (i in ssList.size - 1 downTo 0) {
                val ss = ssList[i]
                hideSubScreen(ss, byBackKey)
            }
        }
    }

    /**
     * サブスクリーンの非表示
     * @param ss 対象のサブスクリーン情報の参照
     * @param byBackKey バックキー起点か　※既定値：false
     */
    @JvmOverloads
    fun hideSubScreen(ss: SubScreenData, byBackKey: Boolean = false) {
        screenLock.withLock {
            if (byBackKey && !ss.enableBack) return
            screenLocked = true //ロック開始
            val vg = findViewById<ViewGroup>(R.id.content_sub) //ルートビュー
            //対象のスクリーン配下のサブスクリーンを全て削除
            if (ss.childSubScreens.isNotEmpty()) {
                vg.post {
                    ss.getAllChildSubScreen().let { list ->
                        for (i in list.size - 1 downTo 0) {
                            val sss = list[i]
                            vg.removeViewInLayout(sss.createdView)
                        }
                    }
                }
            }
            //対象のサブスクリーンを非表示
            val s = ss.parentScreen
            val v = ss.createdView
            val animationTarget =
                    when (ss.windowAnimationTarget) {
                        -1 -> null
                        0 -> v
                        else -> v.findViewById(ss.windowAnimationTarget)
                    }
            val animId =
                    when (ss.animationPivot) {
                        AnimationConst.PIVOT_LEFT -> R.anim.compress_out_left
                        AnimationConst.PIVOT_TOP -> R.anim.compress_out_top
                        AnimationConst.PIVOT_RIGHT -> R.anim.compress_out_right
                        AnimationConst.PIVOT_BOTTOM -> R.anim.compress_out_bottom
                        else -> R.anim.dialog_compress_y_end
                    }
            animationTarget?.apply { post { startAnimation(AnimationUtils.loadAnimation(this@AbstractActivity, animId)) } }
            GlobalScope.launch {
                if (animationTarget != null) {
                    delay(250)
                }
                vg.apply {
                    post {
                        v.setGone(true)
                        removeViewInLayout(v)
                    }
                }
            }
            //対象のサブスクリーンを削除
            s.subScreens.remove(ss)
            screenLocked = false //ロック終了
        }
    }

    /**
     * スクリーンIDの採番
     */
    private fun newScreenId(): Int {
        val max: Int? = screens.maxBy { it.screenId }?.screenId
        if (max is Int) {
            return max.plus(1)
        }
        return 1
    }

    /**
     * スクリーンIDをもとにスクリーンデータを取得
     */
    private fun screen(screenId: Int): ScreenData? {
        return screens.firstOrNull { it.screenId == screenId }
    }

    /*
     * ↓メッセージ表示関連（スクリーン版）↓
     */

    /**
     * メッセージの表示（アイコン要指定、肯定ボタンのみ）
     * @param iconResId アイコンのリソースID
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showMessage(
            iconResId: Int, titleResId: Int, messageResId: Int, positiveButtonTextResId: Int,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showMessage(
                iconResId, getString(titleResId), getString(messageResId),
                getString(positiveButtonTextResId), onFinishListener, enableBack
        )
    }

    /**
     * メッセージの表示（アイコン要指定、肯定ボタンのみ）
     * @param iconResId アイコンのリソースID
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showMessage(
            iconResId: Int, title: String, message: String, positiveButtonText: String,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showMessage(iconResId, title, message, positiveButtonText, onFinishListener, enableBack)
    }

    /**
     * メッセージの表示（アイコン要指定、肯定・否定ボタン）
     * @param iconResId アイコンのリソースID
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param negativeButtonTextResId 否定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showMessage(
            iconResId: Int, titleResId: Int, messageResId: Int, positiveButtonTextResId: Int, negativeButtonTextResId: Int,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showMessage(
                iconResId, getString(titleResId), getString(messageResId),
                getString(positiveButtonTextResId), getString(negativeButtonTextResId), onFinishListener, enableBack
        )
    }

    /**
     * メッセージの表示（アイコン要指定、肯定・否定ボタン）
     * @param iconResId アイコンのリソースID
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param negativeButtonText 否定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showMessage(
            iconResId: Int, title: String, message: String, positiveButtonText: String, negativeButtonText: String,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        val initData = MessageSubScreen.InitData(
                MessageSubScreen.Type.QUESTION, iconResId, title, message,
                positiveButtonText, negativeButtonText, onFinishListener
        )
        return showSubScreen(screens.lastOrNull(), messageSubScreenLayoutId, R.id.content_root, initData, enableBack)
    }

    /**
     * INFOメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showInfoMessage(
            titleResId: Int, messageResId: Int,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showInfoMessage(getString(titleResId), getString(messageResId), onFinishListener, enableBack)
    }

    /**
     * INFOメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showInfoMessage(
            title: String, message: String,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showInfoMessage(title, message, "", onFinishListener, enableBack)
    }

    /**
     * INFOメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showInfoMessage(
            titleResId: Int, messageResId: Int, positiveButtonTextResId: Int,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showInfoMessage(
                getString(titleResId), getString(messageResId),
                getString(positiveButtonTextResId), onFinishListener, enableBack
        )
    }

    /**
     * INFOメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showInfoMessage(
            title: String, message: String, positiveButtonText: String,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        val initData = MessageSubScreen.InitData(
                MessageSubScreen.Type.INFO, 0, title, message,
                positiveButtonText, "", onFinishListener
        )
        return showSubScreen(screens.lastOrNull(), messageSubScreenLayoutId, R.id.content_root, initData, enableBack)
    }

    /**
     * WARNINGメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showWarnMessage(
            titleResId: Int, messageResId: Int,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showWarnMessage(getString(titleResId), getString(messageResId), onFinishListener, enableBack)
    }

    /**
     * WARNINGメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showWarnMessage(
            title: String, message: String,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showWarnMessage(title, message, "", "", onFinishListener, enableBack)
    }

    /**
     * WARNINGメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param negativeButtonTextResId 否定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showWarnMessage(
            titleResId: Int, messageResId: Int, positiveButtonTextResId: Int, negativeButtonTextResId: Int,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showWarnMessage(
                getString(titleResId), getString(messageResId),
                getString(positiveButtonTextResId), getString(negativeButtonTextResId), onFinishListener, enableBack
        )
    }

    /**
     * WARNINGメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param negativeButtonText 否定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showWarnMessage(
            title: String, message: String, positiveButtonText: String, negativeButtonText: String,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        val initData = MessageSubScreen.InitData(
                MessageSubScreen.Type.WARNING, 0, title, message,
                positiveButtonText, negativeButtonText, onFinishListener
        )
        return showSubScreen(screens.lastOrNull(), messageSubScreenLayoutId, R.id.content_root, initData, enableBack)
    }

    /**
     * ERRORメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showErrorMessage(
            titleResId: Int, messageResId: Int,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showErrorMessage(getString(titleResId), getString(messageResId), onFinishListener, enableBack)
    }

    /**
     * ERRORメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showErrorMessage(
            title: String, message: String,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showErrorMessage(title, message, "", onFinishListener, enableBack)
    }

    /**
     * ERRORメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showErrorMessage(
            titleResId: Int, messageResId: Int, positiveButtonTextResId: Int,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showErrorMessage(
                getString(titleResId), getString(messageResId),
                getString(positiveButtonTextResId), onFinishListener, enableBack
        )
    }

    /**
     * ERRORメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showErrorMessage(
            title: String, message: String, positiveButtonText: String,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        val initData = MessageSubScreen.InitData(
                MessageSubScreen.Type.ERROR, 0, title, message,
                positiveButtonText, "", onFinishListener
        )
        return showSubScreen(screens.lastOrNull(), messageSubScreenLayoutId, R.id.content_root, initData, enableBack)
    }

    /**
     * QUESTIONメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showQuestionMessage(
            titleResId: Int, messageResId: Int,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showQuestionMessage(getString(titleResId), getString(messageResId), onFinishListener, enableBack)
    }

    /**
     * QUESTIONメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showQuestionMessage(
            title: String, message: String,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showQuestionMessage(title, message, "", "", onFinishListener, enableBack)
    }

    /**
     * QUESTIONメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param negativeButtonTextResId 否定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showQuestionMessage(
            titleResId: Int, messageResId: Int, positiveButtonTextResId: Int, negativeButtonTextResId: Int,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        return showQuestionMessage(
                getString(titleResId), getString(messageResId),
                getString(positiveButtonTextResId), getString(negativeButtonTextResId), onFinishListener, enableBack
        )
    }

    /**
     * QUESTIONメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param negativeButtonText 否定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showQuestionMessage(
            title: String, message: String, positiveButtonText: String, negativeButtonText: String,
            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true
    ): SubScreenData {
        val initData = MessageSubScreen.InitData(
                MessageSubScreen.Type.QUESTION, 0, title, message,
                positiveButtonText, negativeButtonText, onFinishListener
        )
        return showSubScreen(screens.lastOrNull(), messageSubScreenLayoutId, R.id.content_root, initData, enableBack)
    }

    /*
     * ↓メッセージ表示関連（ダイアログ版）↓
     */

    /**
     * INFOメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※規定値：null
     */
    @JvmOverloads
    fun showInfoMessageDialog(titleResId: Int, messageResId: Int, onFinishListener: MessageDialog.OnFinishListener? = null) {
        MessageDialog(this, R.style.Theme_MessageDialog, MessageDialog.Type.INFO, R.drawable.ic_info,
                getString(titleResId), getString(messageResId), onFinishListener).show()
    }

    /**
     * INFOメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※規定値：null
     */
    @JvmOverloads
    fun showInfoMessageDialog(title: String, message: String, onFinishListener: MessageDialog.OnFinishListener? = null) {
        MessageDialog(this, R.style.Theme_MessageDialog, MessageDialog.Type.INFO, R.drawable.ic_info,
                title, message, onFinishListener).show()
    }

    /**
     * WARNINGメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※規定値：null
     */
    @JvmOverloads
    fun showWarnMessageDialog(titleResId: Int, messageResId: Int, onFinishListener: MessageDialog.OnFinishListener? = null) {
        MessageDialog(this, R.style.Theme_MessageDialog, MessageDialog.Type.WARNING, R.drawable.ic_warning,
                getString(titleResId), getString(messageResId), onFinishListener).show()
    }

    /**
     * WARNINGメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※規定値：null
     */
    @JvmOverloads
    fun showWarnMessageDialog(title: String, message: String, onFinishListener: MessageDialog.OnFinishListener? = null) {
        MessageDialog(this, R.style.Theme_MessageDialog, MessageDialog.Type.WARNING, R.drawable.ic_warning,
                title, message, onFinishListener).show()
    }

    /**
     * ERRORメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※規定値：null
     */
    @JvmOverloads
    fun showErrorMessageDialog(titleResId: Int, messageResId: Int, onFinishListener: MessageDialog.OnFinishListener? = null) {
        MessageDialog(this, R.style.Theme_MessageDialog, MessageDialog.Type.ERROR, R.drawable.ic_error,
                getString(titleResId), getString(messageResId), onFinishListener).show()
    }

    /**
     * ERRORメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※規定値：null
     */
    @JvmOverloads
    fun showErrorMessage(title: String, message: String, onFinishListener: MessageDialog.OnFinishListener? = null) {
        MessageDialog(this, R.style.Theme_MessageDialog, MessageDialog.Type.ERROR, R.drawable.ic_error,
                title, message, onFinishListener).show()
    }

    /**
     * QUESTIONメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※規定値：null
     */
    @JvmOverloads
    fun showQuestionMessage(titleResId: Int, messageResId: Int, onFinishListener: MessageDialog.OnFinishListener? = null) {
        MessageDialog(this, R.style.Theme_MessageDialog, MessageDialog.Type.QUESTION, R.drawable.ic_question,
                getString(titleResId), getString(messageResId), onFinishListener).show()
    }

    /**
     * QUESTIONメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※規定値：null
     */
    @JvmOverloads
    fun showQuestionMessage(title: String, message: String, onFinishListener: MessageDialog.OnFinishListener? = null) {
        MessageDialog(this, R.style.Theme_MessageDialog, MessageDialog.Type.QUESTION, R.drawable.ic_question,
                title, message, onFinishListener).show()
    }

    /*
     * ↓UI制御関連↓
     */

    /**
     * ローディングの表示
     * @param progress true:プログレス表示 false:ローディング画面表示　※既定値：true
     */
    @JvmOverloads
    fun showLoading(progress: Boolean = true) {
        if (progress) {
            showLoading(R.layout.partial_loading_progress, true)
        } else {
            showLoading(R.layout.partial_loading_default, false)
        }
    }

    /**
     * ローディングの表示
     * @param layoutId ローディング画面のレイアウトID
     * @param topMost 最前面に表示するかのフラグ　※既定値：true
     */
    fun showLoading(layoutId: Int, topMost: Boolean = true) {
        val parent: ViewGroup =
                if (topMost) {
                    view.findViewById(R.id.parent_foreground)
                } else {
                    view.findViewById(R.id.parent_middle)
                }
        parent.apply {
            post {
                removeAllViewsInLayout()
                addView(View.inflate(this@AbstractActivity, layoutId, null))
            }
        }
    }

    /**
     * ローディングの非表示
     */
    fun hideLoading() {
        arrayOf(R.id.parent_middle, R.id.parent_foreground).forEach { viewId ->
            view.findViewById<ViewGroup>(viewId).apply {
                post {
                    removeAllViewsInLayout()
                }
            }
        }
    }

    /**
     * キーボードの消去
     */
    fun hideKeyboard() {
        try {
            KeyboardUtil.hide(this)
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * 全画面表示にする（ステータスバー、ナビゲーションバー非表示）
     */
    fun fullScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        }
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ->
                window.decorView.systemUiVisibility = (
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                or View.SYSTEM_UI_FLAG_FULLSCREEN)
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN ->
                window.decorView.systemUiVisibility = (
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                or View.SYSTEM_UI_FLAG_FULLSCREEN)
            else -> window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
    }

    /*
     * ↓Dialog・Activity関連↓
     */

    /**
     * Activityに直属する単一のダイアログを表示
     * @param dialog 表示するダイアログ
     */
    fun openDialog(dialog: Dialog?) {
        //既に開いているダイアログを非表示
        closeDialog()
        //ダイアログ表示
        this.dialog = dialog
        try {
            if (this.dialog != null && !this.dialog!!.isShowing) {
                this.dialog!!.show()
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
            closeDialog()
        }
    }

    /**
     * Activityに直属する単一のダイアログを非表示
     */
    fun closeDialog() {
        dialog?.dismiss()
        dialog = null
    }

    /**
     * アクティビティの終了
     */
    fun closeActivity(save: Boolean) {
        if (save) {
            save()
        }
        finish()
    }

    /*
     * ↓リソース関連↓
     */

    /**
     * リソースから文字列の配列を取得する
     * @param resId リソースID
     * @return リソースから取得した文字列の配列
     */
    fun getStringArray(resId: Int): Array<String> {
        return resources.getStringArray(resId)
    }

    /**
     * リソースから数値を取得する
     * @param resId リソースID
     * @return リソースから取得した数値
     */
    fun getDimen(resId: Int): Float {
        return resources.getDimension(resId)
    }

    /**
     * リソースから数値を取得する
     * @param resId リソースID
     * @return リソースから取得した数値
     */
    fun getDimenInt(resId: Int): Int {
        return resources.getDimension(resId).toInt()
    }

    /**
     * リソースから色を取得する
     * @param resId リソースID
     * @return リソースから取得した値
     */
    fun getResColor(resId: Int): Int {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            @Suppress("DEPRECATION")
            return resources.getColor(resId)
        }
        return ContextCompat.getColor(this, resId)
    }

    /*
     * ↓ハンドラ関連↓
     */

    /**
     * ハンドラ生成用の内部クラス
     * @param activity 参照するアクティビティ
     */
    internal class IncomingHandler(activity: AbstractActivity) : BufferedHandler() {
        private val mActivity: WeakReference<AbstractActivity> = WeakReference(activity) //アクティビティへの弱参照
        override fun onHandleMessage(msg: Message) {
            //アクティビティの取得
            val activity = mActivity.get()
            if (activity != null) {
                //メッセージ処理の実行
                try {
                    activity.handleMessage(msg)
                } catch (e: Exception) {
                    LogUtil.debug(e)
                }
            }
        }
    }

    /**
     * メッセージ処理
     * overrideする場合、個別に処理しないメッセージについては、super.handleMessage()を呼び出して処理すること
     * @param msg メッセージオブジェクト
     */
    protected open fun handleMessage(msg: Message) {
        try {
            when (msg.what) {
                CodeConst.MSG.ToAbstractActivity.SHOW_TOAST -> { //トースト表示
                    val title = getString(msg.arg1)
                    val hint = getString(msg.arg2)
                    val error = msg.obj as Boolean
                    Toaster.show(this, title, hint, error)
                }
                CodeConst.MSG.ToAbstractActivity.OPEN_DIALOG -> { //Dialog表示
                    openDialog(msg.obj as? Dialog)
                }
                CodeConst.MSG.ToAbstractActivity.CLOSE_DIALOG -> { //Dialog非表示
                    closeDialog()
                }
                CodeConst.MSG.ToAbstractActivity.CLOSE_ACTIVITY -> { //Activity終了
                    closeActivity(true)
                }
                CodeConst.MSG.ToAbstractActivity.SAVE -> { //セーブ
                    save()
                }
                CodeConst.MSG.ToAbstractActivity.LOAD -> { //ロード
                    load()
                }
                else -> { //スクリーンへ委譲
                    //送信先の取得
                    var targetScreen: ScreenData? = screens.lastOrNull()
                    if (msg.arg1 > 0) {
                        targetScreen = screen(msg.arg1)
                    }
                    //メッセージの委譲
                    val targetView: Any? = targetScreen?.createdView
                    if (targetView is AbstractScreen) {
                        targetView.processMessage(msg.what, msg.obj)
                    }
                }
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /*
     * ↓排他関連↓
     */

    /**
     * 連打チェック
     */
    fun checkClick(): Boolean {
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastClickTime < Settings.Interval.PERMIT_CLICK) {
            return false
        }
        lastClickTime = currentTime
        return true
    }

    /*
     * ↓ハード関連↓
     */

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                //最前面がスクリーン／ポップップである場合、閉じる（スクリーンロック中以外で、バックキーにより閉じる事が許可されている場合）
                if (!screenLocked && screens.size > 0) {
                    val s = screens.last()
                    if (s.subScreens.size > 0) {
                        //サブスクリーン表示中であれば、最前面のサブスクリーンを閉じる（バックキーにより閉じる事が許可されている場合）
                        hideSubScreen(s.subScreens.last(), true)
                    } else {
                        //スクリーンのバックキー押下時処理を先に実行する
                        if (s.createdView is AbstractScreen) {
                            if (s.createdView.onActivityKeyDown(keyCode, event)) {
                                //スクリーンの処理結果がtrueの場合は、以降の処理を行わない
                                return true
                            }
                        }
                        //スクリーンを閉じる（バックキーにより閉じる事が許可されている場合）
                        hideScreen(s, true)
                    }
                }
                //全てのスクリーンが閉じられた場合、アクティビティ終了
                if (screens.size == 0) {
                    closeActivity(true)
                }
                true
            }
            else -> {
                var result = false
                if (screens.size > 0 && screens.last().createdView is AbstractScreen) {
                    //スクリーンが表示されている場合は、スクリーンに処理を委ねる
                    result = (screens.last().createdView as AbstractScreen).onActivityKeyDown(keyCode, event)
                }
                if (!result) {
                    result = super.onKeyDown(keyCode, event)
                }
                result
            }
        }
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        var result = false
        if (screens.size > 0 && screens.last().createdView is AbstractScreen) {
            //スクリーンが表示されている場合は、スクリーンに処理を委ねる
            result = (screens.last().createdView as AbstractScreen).onActivityKeyUp(keyCode, event)
        }
        if (!result) {
            result = super.onKeyUp(keyCode, event)
        }
        return result
    }

    override fun onKeyLongPress(keyCode: Int, event: KeyEvent?): Boolean {
        var result = false
        if (screens.size > 0 && screens.last().createdView is AbstractScreen) {
            //スクリーンが表示されている場合は、スクリーンに処理を委ねる
            result = (screens.last().createdView as AbstractScreen).onActivityKeyLongPress(keyCode, event)
        }
        if (!result) {
            result = super.onKeyLongPress(keyCode, event)
        }
        return result
    }

    override fun onKeyShortcut(keyCode: Int, event: KeyEvent?): Boolean {
        var result = false
        if (screens.size > 0 && screens.last().createdView is AbstractScreen) {
            //スクリーンが表示されている場合は、スクリーンに処理を委ねる
            result = (screens.last().createdView as AbstractScreen).onActivityKeyShortcut(keyCode, event)
        }
        if (!result) {
            result = super.onKeyShortcut(keyCode, event)
        }
        return result
    }

    override fun onKeyMultiple(keyCode: Int, repeatCount: Int, event: KeyEvent?): Boolean {
        var result = false
        if (screens.size > 0 && screens.last().createdView is AbstractScreen) {
            //スクリーンが表示されている場合は、スクリーンに処理を委ねる
            (screens.last().createdView as AbstractScreen).onActivityKeyMultiple(keyCode, repeatCount, event)
        }
        if (!result) {
            result = super.onKeyMultiple(keyCode, repeatCount, event)
        }
        return result
    }
}