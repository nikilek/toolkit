package net.nikilek.toolkit

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View

/**
 * Created by nikilek on 2016/01/27.
 * タッチイベントを子ビューに伝播させないビュー
 */
open class InterceptView : View {

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context)

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs   アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    /**
     * コンストラクタ
     * @param context  コンテキスト
     * @param attrs    アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    init {
        this.isClickable = true
        this.isLongClickable = true
        this.isFocusable = true
        this.setOnTouchListener { _, _ -> false }
        this.setOnClickListener { }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun performClick(): Boolean {
        return true
    }
}
