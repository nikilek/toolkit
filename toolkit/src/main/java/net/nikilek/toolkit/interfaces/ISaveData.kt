package net.nikilek.toolkit.interfaces

import java.io.Serializable

/**
 * Created by nikilek on 2017/10/16.
 *
 * このinterfaceを実装したセーブデータクラスを別途作成してください
 * ※参考：net.nikilek.toolkit._sample.SaveData
 */
interface ISaveData : Serializable {

    /**
     * 空か否か
     */
    val isEmpty: Boolean

    /**
     * 上下限値のFIX
     */
    fun valid()

    /**
     * インスタンスの複製
     */
    fun clone(): ISaveData
}
