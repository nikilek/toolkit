package net.nikilek.toolkit.interfaces

/**
 * Created by nikilek on 2018/02/15.
 * メッセージコールバックのリスナ
 */
interface OnMessageListener{
    /**
     * メッセージコールバック処理
     * @param message メッセージ
     */
    fun onMessage(message: android.os.Message)
}