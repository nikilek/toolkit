package net.nikilek.toolkit

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.CheckBox
import net.nikilek.toolkit.constant.CodeConst
import net.nikilek.toolkit.utility.LogUtil

import net.nikilek.toolkit.utility.ResourceHolder

/**
 * Created by nikilek on 2016/01/27.
 * カスタムビュー
 */
open class MyCheckBox : CheckBox {

    private var textStyle: Int = Typeface.NORMAL //文字の表示スタイル

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context)

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        getAttrs(attrs)
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        getAttrs(attrs)
    }

    /**
     * 属性の取得
     * @param attrs 属性セット
     */
    private fun getAttrs(attrs: AttributeSet) {
        textStyle = attrs.getAttributeIntValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "textStyle", textStyle)
    }

    public override fun onFinishInflate() {
        super.onFinishInflate()
        //if (this.isInEditMode) {
        //    return
        //}
        //フォントの設定
        setFont(context)
    }

    /**
     * フォントを設定
     * @param context コンテキスト
     */
    private fun setFont(context: Context?) {
        try {
            //システム定義のフォントを設定
            if (textStyle == Typeface.BOLD) {
                this.typeface = ResourceHolder.Typefaces.getBold(context!!)
            } else {
                this.typeface = ResourceHolder.Typefaces.getDefault(context!!)
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }
}
