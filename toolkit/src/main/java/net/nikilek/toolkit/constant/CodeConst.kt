package net.nikilek.toolkit.constant

/**
 * Created by nikilek on 2017/10/16.
 * 各種コード
 */
class CodeConst {

    /**
     * Messageのwhat属性に設定するコード
     */
    object MSG {

        /**
         * Activityへの通知用（各画面で処理するもの）
         */
        object ToIndividualActivity {
            const val DRAW = 600001 //再描画依頼
        }

        /**
         * Activityへの通知用（基底クラスで処理するもの）
         */
        object ToAbstractActivity {
            const val SHOW_TOAST = 700001 //トースト表示
            const val OPEN_DIALOG = 700011 //ダイアログオープン
            const val CLOSE_DIALOG = 700012 //ダイアログクローズ
            const val CLOSE_ACTIVITY = 700021 //Activity終了
            const val SAVE = 700031 //セーブ
            const val LOAD = 700032 //ロード
            const val REQUEST_PERMISSION = 700041 //権限リクエスト
            const val REQUEST_OPEN_DOCUMENT = 700051 //ファイルオープン
            const val REQUEST_CREATE_DOCUMENT = 700052 //ファイル作成
            const val REQUEST_BLUETOOTH_ON = 700061 //Bluetooth有効化
        }

        /**
         * Screenへの通知用（各画面で処理するもの）
         */
        object ToIndividualScreen {
            const val BACK_KEY_PRESSED = 800001 //バックキー押下の通知
        }

        /**
         * Screenへの通知用（基底クラスで処理するもの）
         */
        object ToAbstractScreen {
            const val DRAW = 900001 //再描画依頼
        }
    }

    /**
     * Intentデータに設定するコード
     */
    object Intent {
        const val EXTRA_MAIN_FROM_NOTIFY = "MAIN_FROM_NOTIFY" //通知バー経由かのフラグ
    }

    /**
     * 名前空間
     */
    object NameSpace {
        const val ANDROID_SCHEMAS = "http://schemas.android.com/apk/res/android"
    }
}