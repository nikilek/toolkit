package net.nikilek.toolkit.constant

/**
 * Created by nikilek on 2017/10/16.
 * アプリケーション設定の定義
 */
class Settings {

    /**
     * アプリケーションモード
     */
    object Mode {
        /** デバッグモード */
        var DEBUG = true
    }

    /**
     * フォーマット
     */
    object Format {
        /** 日付フォーマット（年月日） */
        var DATE = "yyyy/MM/dd"
        /** 日付フォーマット（時分秒） */
        var TIME = "HH:mm:ss"
        /** 日付フォーマット（年月日時分） */
        var DATETIME = "yyyy/MM/dd HH:mm"
        /** 日付フォーマット（年月日時分秒） */
        var TIMESTAMP = "yyyy/MM/dd HH:mm:ss"
        /** 日付フォーマット（分秒） */
        var CLOCK = "mm:ss"
    }

    /**
     * 鍵
     */
    object Key {
        /** 暗号化鍵(AES用)  */
        var AES = "1234567890123456"
    }

    /**
     * 通知用
     */
    object Notification{
        /** ID */
        var ID = 0x7f000000
        /** チャネルID */
        var CHANNEL_ID = "toolkit_notification_channel_id"
        /** チャネル名 */
        var CHANNEL_NAME = "toolkit_notification_channel_name"
        /** バイブレーションパターン */
        var VIBRATION_PATTERN = longArrayOf(100, 400, 100, 100)
    }

    /**
     * TCP通信用
     */
    object TCP {
        /** サーバのIPアドレス  */
        var IP_SERVER = "127.0.0.1"
        /** サーバのURL  */
        var DOMAIN_SERVER = "www.xxx.yyy.zzz"
        /** サーバのポート  */
        var PORT_PRIMARY = 65535
        /** TCP通信のソケット接続タイムアウト時間（ミリ秒）  */
        var CONNECT_TIMEOUT = 5000
        /** TCP通信のソケット読込タイムアウト時間（ミリ秒）  */
        var READ_TIMEOUT = 5000
        /** TCP通信のリトライ回数（初回を含んだ回数）  */
        var RETRY_COUNT = 3
        /** TCP接続のリトライ時の待機時間（ミリ秒）  */
        var RETRY_INTERVAL: Long = 1000
        /** TCP受信時のバッファサイズ（byte）  */
        var READ_BUFFER_SIZE = 1024
        /** 文字エンコード */
        var CHARSET = "UTF-8"
        /** 送受信電文のSTX（未使用時はnull） */
        var STX: Byte? = null
        /** 電文ヘッダのうち、データ部のサイズを表す箇所のバイト長（送受信ともに同定義）
         *  この値は、送信電文作成時に次のように使用する。『String.format("%1$0Xd", データ部のサイズ)』※Xの部分
         *  また、受信電文処理時も、上記のように使用されている事を前提とする。
         *  例１：「STXなし」かつ「2」⇒1byte目～2byte目をデータ部のサイズの定義と見なす
         *  例２：「STXあり」かつ「4」⇒2byte目～5byte目をデータ部のサイズの定義と見なす */
        var BYTE_LENGTH_OF_DECLARER_DATA_SIZE: Int = 0
    }

    /**
     * HTTP通信用
     */
    object HTTP {
        /** HTTP通信のソケット接続タイムアウト時間（ミリ秒）  */
        var CONNECT_TIMEOUT = 5000
        /** HTTP通信のソケット読込タイムアウト時間（ミリ秒）  */
        var READ_TIMEOUT = 5000
        /** HTTP通信のソケット読込タイムアウト時間（ミリ秒）  */
        var READ_TIMEOUT_DOWNLOAD = 60000 * 30
        /** HTTP受信時のバッファサイズ（byte）  */
        var READ_BUFFER_SIZE = 1024
        /** HTTPファイルダウンロードの一時ファイル  */
        var PATH_TEMPORARY = "download/tmp.download"
    }

    /**
     * ファイル入出力用
     */
    object File {
        /** ファイルの文字コード  */
        var ENCODE = "UTF-8"
        /** すべて内部ストレージを使用するかのフラグ（SDカード使用時はfalse）  */
        var TO_SYSTEM = true
        /** システムログのパス  */
        var PATH_SYSTEM_LOG = "system.log"
        /** エラーログのパス  */
        var PATH_ERROR_LOG = "error.log"
        /** プリファレンスの格納先フォルダ */
        var PATH_PREFERENCE_FOLDER = "pref/"
        /** セーブデータを暗号化するかのフラグ */
        var SECRET_SAVEDATA = true
        /** セーブデータのファイル名 */
        var PATH_SAVEDATA = "savedata.dat"
        var PATH_SAVEDATA_BACKUP = "savedata.bak.dat" //バックアップ
        var PATH_SAVEDATA_TEMPORARY = "savedata.tmp.dat" //一時ファイル
        /** デフォルトのフォントファイル  */
        var PATH_FONT_FILE = "font/kazesawa/Kazesawa-Regular.ttf"
        var PATH_FONT_FILE_BOLD = "font/kazesawa/Kazesawa-Extrabold.ttf" //太字
        /** ファイル出力時のバッファサイズ（byte）  */
        var BUFFER_SIZE = 16384
    }

    /**
     * 文字列処理用
     */
    object String {
        /** 改行文字 */
        var SEPARATOR = "\r\n"
        /** 区切り文字（行） */
        var DELIMITER_LINE = "\n"
        /** 区切り文字（単語） */
        var DELIMITER_WORD = "\t"
        /** 区切り文字（単語内） */
        var DELIMITER_SUB = "~"
        /** プレフィックス（パラパラアニメーション対応の画像名） */
        var PREFIX_ANIMATION_FILE = "ef_"
        /** ユーザ入力可否（使用フォントの対応有無に応じて設定） */
        // 右記は対応している前提：半角英数字・全角かな・全角カナ・JIS第1水準漢字・JIS第2水準漢字
        var PERMIT_INPUT_ALL = false //下記設定によらず全て許可
        var PERMIT_INPUT_HALFWIDTH_KANA = true //半角カナ
        var PERMIT_INPUT_HALFWIDTH_SYMBOL = true //半角記号（ASCII準拠）
        var PERMIT_INPUT_FULLWIDTH_NUMERIC_AND_ALPHABET = true //全角英数字
        var PERMIT_INPUT_FULLWIDTH_SYMBOL1 = true //記号（JIS 1区）
        var PERMIT_INPUT_FULLWIDTH_SYMBOL2 = true //記号（JIS 2区）
        var PERMIT_INPUT_FULLWIDTH_GREEK = true //ギリシャ文字（JIS 6区）
        var PERMIT_INPUT_FULLWIDTH_CYRILLIC = true //キリル文字（JIS 7区）
        var PERMIT_INPUT_FULLWIDTH_BORDER = true //罫線（JIS 8区）
    }

    /**
     * パフォーマンス制御用
     */
    object Interval {
        /** ボタン連打の許容間隔（ミリ秒）  */
        var PERMIT_CLICK: Long = 500
        /** アニメーションのフレームスキップ間隔（ミリ秒）  */
        var FRAME_SKIP_ANIMATION: Long = 100
    }

    /**
     * サウンド制御用
     */
    object Sound {
        /** サウンドプールの予約サイズ */
        var POOL_SIZE = 50
    }
}