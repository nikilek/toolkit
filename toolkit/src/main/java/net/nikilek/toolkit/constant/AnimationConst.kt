package net.nikilek.toolkit.constant

/**
 * Created by nikilek on 2016/01/27.
 * アニメーション処理用定数
 */
object AnimationConst {
    /*
     * エフェクト
     */
    /** 上下に１回揺らす  */
    const val EFFECT_APPEAL = 1
    /** 左右に２回揺らす  */
    const val EFFECT_SHAKE = 2
    /** フェードイン  */
    const val EFFECT_FADE_IN = 3
    /** フェードアウト  */
    const val EFFECT_FADE_OUT = 4
    /** 点滅 */
    const val EFFECT_FLASH = 5
    /** サイズ小→元のサイズ  */
    const val EFFECT_SMALL_TO_MIDDLE = 6
    /** サイズ大→元のサイズ  */
    const val EFFECT_BIG_TO_MIDDLE = 7
    /** 元のサイズ→サイズ大  */
    const val EFFECT_MIDDLE_TO_BIG = 8
    /** 元のサイズ→サイズ小  */
    const val EFFECT_MIDDLE_TO_SMALL = 9
    /** 回転  */
    const val EFFECT_ROTATE = 10
    /** 回転（左右繰り返し）  */
    const val EFFECT_ROTATE_SHAKE = 11
    /*
     * ピボット
     */
    /** 中心 */
    const val PIVOT_CENTER = 0
    /** 左 */
    const val PIVOT_LEFT = 1
    /** 上 */
    const val PIVOT_TOP = 2
    /** 右 */
    const val PIVOT_RIGHT = 3
    /** 下 */
    const val PIVOT_BOTTOM = 4
}