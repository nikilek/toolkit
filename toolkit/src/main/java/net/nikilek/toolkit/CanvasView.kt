package net.nikilek.toolkit

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.model.AnimationData
import net.nikilek.toolkit.utility.LogUtil
import net.nikilek.toolkit.utility.ThreadUtil
import net.nikilek.toolkit.utility.ViewUtil
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * 指定パラメタをもとにキャンバスへの描画を行うビュー
 * ※CanvasView#start()をコールする事で描画を開始
 */
open class CanvasView : View {
    private var thread: Thread? = null //描画スレッド
    private var threadTarget: DrawThread? = null //描画スレッドの実装クラス
    private val threadLock = ReentrantLock() //スレッド操作用ロックオブジェクト
    private var offScreenBitmap: Bitmap? = null //ダブルバッファ用ビットマップ

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context)

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs   アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    /**
     * コンストラクタ
     * @param context  コンテキスト
     * @param attrs    アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    init {
        this.setWillNotDraw(false)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        //オフスクリーンバッファの生成
        if (changed || offScreenBitmap == null) {
            offScreenBitmap?.recycle()
            offScreenBitmap = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888)
        }
    }

    /**
     * デタッチ時処理
     */
    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        //描画スレッド停止
        stop()
        //オフスクリーンバッファの破棄
        offScreenBitmap?.recycle()
        offScreenBitmap = null
    }

    /**
     * 描画時処理
     */
    override fun onDraw(canvas: Canvas?) {
        if (offScreenBitmap != null) {
            canvas?.drawBitmap(offScreenBitmap!!, 0f, 0f, null) //オフスクリーンバッファからコピー
        }
    }

    /* ↓スレッド操作↓ */

    /**
     * 描画の開始
     * @param viewUtil 描画処理用汎用クラス
     * @return 成否
     */
    fun start(viewUtil: ViewUtil): Boolean {
        threadLock.withLock {
            threadTarget = DrawThread(this)
            threadTarget?.viewUtil = viewUtil
            thread?.interrupt()
            thread = Thread(threadTarget).apply { isDaemon = true; start() }
        }
        return true
    }

    /**
     * 描画の一時停止
     */
    fun pause() {
        threadLock.withLock {
            threadTarget?.paused = true
        }
    }

    /**
     * 描画の再開
     */
    fun resume() {
        threadLock.withLock {
            threadTarget?.reset()
            threadTarget?.paused = false
        }
    }

    /**
     * 描画の停止
     */
    fun stop() {
        threadLock.withLock {
            threadTarget?.attached = false
            thread?.interrupt()
            thread = null
            threadTarget = null
        }
    }

    /* ↓リスト操作↓ */

    /**
     * 描画情報リストの追加
     * @param animationObjectList 描画情報を保持するオブジェクトのリスト
     * @return 追加後のリストのサイズ
     */
    fun addAnimationList(animationObjectList: List<AnimationData>): Int {
        if (animationObjectList.isEmpty()) {
            return threadTarget?.addAnimationList(animationObjectList) ?: 0 //スレッドをロックしない
        }
        threadLock.withLock {
            return threadTarget?.addAnimationList(animationObjectList) ?: 0
        }
    }

    /**
     * 描画情報リストの追加
     * @param animationObject 描画情報を保持するオブジェクト
     * @return 追加後のリストのサイズ
     */
    fun addAnimationList(animationObject: AnimationData): Int {
        threadLock.withLock {
            return threadTarget?.addAnimationList(animationObject) ?: 0
        }
    }

    /**
     * 描画情報リストから指定インデックスの要素を削除
     * @param index 削除対象のインデックス
     * @return 削除後のリストのサイズ
     */
    fun removeAnimationList(index: Int): Int {
        threadLock.withLock {
            return threadTarget?.removeAnimationList(index) ?: 0
        }
    }

    /**
     * 描画情報のリストのクリア
     */
    fun clearAnimationList() {
        threadLock.withLock {
            threadTarget?.clearAnimationList()
        }
    }

    /* ↓描画関連↓ */

    /**
     * 描画スレッド
     */
    internal class DrawThread(private val canvasView: CanvasView) : Runnable {
        var viewUtil: ViewUtil? = null //描画処理用汎用クラス

        var attached: Boolean = true //アタッチ済みかのフラグ
        var paused: Boolean = true //停止中フラグ

        private var startTime: Long = 0 //描画開始時刻
        private var frameCnt: Long = 0 //現在のフレーム数

        private val animationList = mutableListOf<AnimationData>() //描画情報オブジェクトのリスト
        private val listLock = ReentrantLock() //リスト追加／削除ロックオブジェクト
        private var animationUpdated: Boolean = false //描画情報が更新されたかのフラグ

        private var offScreen: Canvas? = null //キャンバス

        /* ↓リスト操作↓ */

        /**
         * 描画情報リストの追加
         * @param animationObjectList 描画情報を保持するオブジェクトのリスト
         * @return 追加後のリストのサイズ
         */
        fun addAnimationList(animationObjectList: List<AnimationData>): Int {
            if (animationObjectList.isEmpty()) {
                return animationList.size
            }
            listLock.withLock {
                animationList.addAll(animationObjectList)
                animationUpdated = true //更新フラグON
                return animationList.size
            }
        }

        /**
         * 描画情報リストの追加
         * @param animationObject 描画情報を保持するオブジェクト
         * @return 追加後のリストのサイズ
         */
        fun addAnimationList(animationObject: AnimationData): Int {
            listLock.withLock {
                animationList.add(animationObject)
                animationUpdated = true //更新フラグON
                return animationList.size
            }
        }

        /**
         * 描画情報リストから指定インデックスの要素を削除
         * @param index 削除対象のインデックス
         * @return 削除後のリストのサイズ
         */
        fun removeAnimationList(index: Int): Int {
            listLock.withLock {
                if (0 <= index && index < animationList.size) {
                    animationList.removeAt(index)
                    animationUpdated = true //更新フラグON
                }
                return animationList.size
            }
        }

        /**
         * 描画情報のリストのクリア
         */
        fun clearAnimationList() {
            listLock.withLock {
                if (animationList.isNotEmpty()) {
                    animationList.clear()
                    animationUpdated = true //更新フラグON
                }
            }
        }

        /**
         * 描画情報リストの定期更新　※描画期間を過ぎた要素は削除する
         * ※紙芝居に使用する画像名の命名規則：{プレフィックス}{任意文字列}{２桁の連番}
         * 　↑プレフィックス＝Settings.String.PREFIX_ANIMATION_FILE
         * 　↑連番の開始番号は任意。最初にAnimationDataに設定したファイル名(連番込)を元に次の番号を探索する。
         */
        private fun updateAnimationList() {
            listLock.withLock {
                val delList = ArrayList<AnimationData>() // 削除対象のリスト
                //リストの各オブジェクトについて処理
                for (obj in animationList) {
                    //繰り返し回数の更新
                    obj.nowCount = obj.nowCount + 1
                    //繰り返し回数の上限判定
                    if (obj.nowCount >= obj.maxCount) {
                        //削除対象に追加
                        delList.add(obj)
                        continue
                    }
                    //画像ファイル名の更新（複数画像で紙芝居をする場合に使用）
                    if (obj.type == AnimationData.TYPE_IMAGE || obj.type == AnimationData.TYPE_ICON) {
                        val oldFileName = obj.value // 現在のファイル名
                        val nowCount = obj.nowCount // 現在の繰り返し回数
                        //連番の更新
                        val lenPrefix = Settings.String.PREFIX_ANIMATION_FILE.length //プレフィックスの文字長
                        if (oldFileName.length > (lenPrefix + 1)
                                && oldFileName.substring(0, lenPrefix) == Settings.String.PREFIX_ANIMATION_FILE) {
                            val rawFileName = oldFileName.substring(0, oldFileName.length - 2) // 連番部分を除いたファイル名
                            val newFileName = if (nowCount + 1 < 10) {
                                rawFileName + "0" + (nowCount + 1)
                            } else {
                                rawFileName + (nowCount + 1)
                            }
                            obj.value = newFileName //連番を更新したファイル名を設定
                            animationUpdated = true //更新フラグON
                        }
                    }
                }
                // 削除対象のオブジェクトをすべて削除する
                if (delList.isNotEmpty()) {
                    animationList.removeAll(delList)
                    animationUpdated = true //更新フラグON
                }
            }
        }

        /* ↓周期処理↓ */

        /**
         * 現在フレームのリセット（開始時・再開時）
         */
        fun reset() {
            if (!paused) {
                return
            }
            startTime = System.currentTimeMillis()
            frameCnt = 0
        }

        override fun run() {
            reset()
            paused = false
            while (attached) {
                if (paused || canvasView.offScreenBitmap == null) {
                    if (!ThreadUtil.sleep(Settings.Interval.FRAME_SKIP_ANIMATION)) break
                    continue
                }
                //フレーム数の更新
                frameCnt++
                if (System.currentTimeMillis() - startTime > frameCnt * Settings.Interval.FRAME_SKIP_ANIMATION) {
                    //フレームスキップ
                    if (!ThreadUtil.sleep(Settings.Interval.FRAME_SKIP_ANIMATION / 2)) break
                    continue
                }
                //描画実行
                if (animationUpdated) {
                    draw()
                    animationUpdated = false //更新フラグOFF
                }
                //描画リストの更新
                updateAnimationList()
                //次のフレームまで待機
                val wait = frameCnt * Settings.Interval.FRAME_SKIP_ANIMATION - (System.currentTimeMillis() - startTime) //スリープ時間の取得
                if (wait > 0 && !ThreadUtil.sleep(wait)) {
                    break
                }
            }
        }

        /**
         * 描画の実行
         */
        private fun draw() {
            try {
                if (canvasView.offScreenBitmap == null) {
                    return
                }
                //キャンバスの生成
                if (offScreen == null) {
                    offScreen = Canvas(canvasView.offScreenBitmap!!)
                }
                //各描画情報について、オフスクリーンバッファへの描画を実行
                offScreen!!.drawColor(0, PorterDuff.Mode.CLEAR) //一度クリア
                for (obj in animationList.toList()) {
                    drawBmp(offScreen!!, obj)
                }
                //キャンバスに描画
                canvasView.post {
                    canvasView.invalidate()
                }
            } catch (e: Exception) {
                LogUtil.debug(e)
            }
        }

        /**
         * 描画情報に従って描画を実行
         * @param canvas 描画先
         * @param data 描画情報
         */
        private fun drawBmp(canvas: Canvas, data: AnimationData) {
            if (data.type == AnimationData.TYPE_IMAGE || data.type == AnimationData.TYPE_ICON) {
                //画像の描画
                val bmp = viewUtil?.getBitmap(data.value, data.imgWidth, data.imgHeight)
                        ?: return //画像の取得
                val src = Rect(0, 0, bmp.width, bmp.height) //元画像の領域
                //リサイズ画像の領域を取得
                val dst = Rect(0, 0, data.imgWidth, data.imgHeight)
                dst.offset(data.positionX.toInt(), data.positionY.toInt())
                //画像をリサイズしてCanvasに描画
                canvas.drawBitmap(bmp, src, dst, data.paint)
            } else {
                //文字列の描画
                data.paint.textSize = data.fontSize.toFloat() //文字サイズの設定
                canvas.drawText(data.value, data.positionX, data.positionY, data.paint)
            }
        }
    }
}