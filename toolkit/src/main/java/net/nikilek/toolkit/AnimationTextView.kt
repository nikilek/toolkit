package net.nikilek.toolkit

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import net.nikilek.toolkit.constant.CodeConst
import net.nikilek.toolkit.utility.LogUtil
import net.nikilek.toolkit.utility.ResourceHolder
import net.nikilek.toolkit.utility.ThreadUtil

/**
 * Created by nikilek on 2017/10/16.
 * テキストをアニメーション表示するTextView
 * ※画面に表示された時点でアニメーション開始。１文字ずつ表示していく
 */
open class AnimationTextView : TextView {

    private var textStyle: Int = Typeface.NORMAL //文字の表示スタイル

    private var animationJob: Job? = null //アニメーション実行JOB

    var waitMillis: Int = 500 //アニメーション開始までの待機時間（ミリ秒）
    var intervalMillis: Int = 50 //文字表示の時間間隔（ミリ秒）

    /**
     * コンストラクタ
     * @param context
     */
    constructor(context: Context) : super(context)

    /**
     * コンストラクタ
     * @param context
     * @param attrs
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        getAttrs(attrs)
    }

    /**
     * コンストラクタ
     * @param context
     * @param attrs
     * @param defStyle
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        getAttrs(attrs)
    }

    public override fun onFinishInflate() {
        super.onFinishInflate()
        //if (this.isInEditMode) {
        //    return
        //}
        //フォントの設定
        setFont(context)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (this.isInEditMode) {
            return
        }
        //表示済のテキストをアニメーション表示
        animateText()
    }

    /**
     * テキストをアニメーション表示する　※表示済の文字列をアニメーション付きで再表示
     */
    fun animateText() {
        setTextWithAnimation(text)
    }

    /**
     * アニメーション付きでsetText()する
     * @param text 設定する文字列
     */
    fun setTextWithAnimation(text: CharSequence) {
        //表示済の文字列を一度クリアする
        stopAnimation()
        this@AnimationTextView.post { this.text = "" }
        //アニメーションの開始
        appendWithAnimation(text)
    }

    /**
     * アニメーション付きでappend()する
     * @param text 設定する文字列（内部で１文字ずつappendする）
     */
    open fun appendWithAnimation(text: CharSequence) {
        stopAnimation()
        animationJob = GlobalScope.launch {
            if (this@AnimationTextView.text.isEmpty()) {
                delay(waitMillis.toLong())
            }
            //１文字ずつappendする
            text.indices.forEach {
                delay(intervalMillis.toLong())
                this@AnimationTextView.post { super.append(text[it].toString()) }
            }
        }
    }

    /**
     * アニメーションの停止
     */
    fun stopAnimation() {
        ThreadUtil.cancel(animationJob)
        animationJob = null
    }

    /**
     * 属性の取得
     * @param attrs 属性セット
     */
    private fun getAttrs(attrs: AttributeSet) {
        textStyle = attrs.getAttributeIntValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "textStyle", textStyle)
        //オリジナル属性の取得
        val attrsOriginal = context.obtainStyledAttributes(attrs, R.styleable.AnimationTextView)
        waitMillis = attrsOriginal.getInt(R.styleable.AnimationTextView_waitMillis, waitMillis)
        intervalMillis = attrsOriginal.getInt(R.styleable.AnimationTextView_intervalMillis, intervalMillis)
        attrsOriginal.recycle()
    }

    /**
     * フォントを設定
     * @param context コンテキスト
     */
    private fun setFont(context: Context?) {
        try {
            //システム定義のフォントを設定
            if (textStyle == Typeface.BOLD) {
                this.typeface = ResourceHolder.Typefaces.getBold(context!!)
            } else {
                this.typeface = ResourceHolder.Typefaces.getDefault(context!!)
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }
}