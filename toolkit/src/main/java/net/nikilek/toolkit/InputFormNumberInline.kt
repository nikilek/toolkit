package net.nikilek.toolkit

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Typeface
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import android.widget.TextView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

import net.nikilek.toolkit.constant.CodeConst
import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.extension.*
import net.nikilek.toolkit.utility.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Created by nikilek on 2018/02/23.
 * 数値入力ができるテキストボックス（キーパッドは指定レイアウト配下に表示）
 * ※現時点では自然数限定
 * ※キーパッドの表示先レイアウトは、Activity/Fragment/Dialog/Screen等のコード側から指定する必要あり
 *
 * 【独自XML属性】※「res/values/attrs.xml」参照
 * ・minValue：入力可能な数値の下限（未指定時は下限チェックなし）
 * ・maxValue：入力可能な数値の上限（未指定時は上限チェックなし）
 * ・maxLength：入力可能な最大桁数（未指定時は上限チェックなし。maxValue指定時は無視される）
 */
open class InputFormNumberInline : TextView {

    /**
     * テキスト変更時時イベントのリスナ
     */
    interface OnTextChangedListener {
        /**
         * @param text 新たなテキスト
         */
        fun onTextChanged(text: String)
    }

    private var propMinValue: Int = -1 //最小値（無効値：-1）
    private var propMaxValue: Int = -1 //最大値（無効値：-1）
    private var propMaxLength: Int = 0 //最大文字長（無効値：0）
    private var textStyle: Int = Typeface.NORMAL //文字の表示スタイル
    var targetLayout: RelativeLayout? = null //キーパッドの表示先（未指定の場合、正常動作しない）
    var onTextChangedListener: InputFormNumberInline.OnTextChangedListener? = null //テキスト変更時イベントのリスナ
    private var dialPad: View? = null //ダイヤルパッドのビュー
    private var minValue = 0 //最小値（propMinValueまたはsetRangeにより決定）
    private var maxValue = 0 //最大値（propMaxValue／propMaxLengthまたはsetRangeにより決定）
    private val lockDialPad = ReentrantLock() //ダイヤルパッド表示制御用ロックオブジェクト

    /**
     * 文字列の取得
     */
    val textStr: String
        get() = text.toString()

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context)

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        getAttrs(attrs)
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        getAttrs(attrs)
    }

    /**
     * 属性の取得
     * @param attrs 属性セット
     */
    private fun getAttrs(attrs: AttributeSet) {
        //オリジナル属性の取得
        val attrsOriginal = context.obtainStyledAttributes(attrs, R.styleable.InputFormNumberInline)
        propMinValue = attrsOriginal.getInt(R.styleable.InputFormNumberInline_minValue, -1)
        propMaxValue = attrsOriginal.getInt(R.styleable.InputFormNumberInline_maxValue, -1)
        propMaxLength = attrsOriginal.getInt(R.styleable.InputFormNumberInline_maxLength, 0)
        attrsOriginal.recycle()
        //基本属性の取得
        textStyle = attrs.getAttributeIntValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "textStyle", textStyle)
    }

    public override fun onFinishInflate() {
        super.onFinishInflate()
        if (this.isInEditMode) {
            return
        }
        if (this.transformationMethod !is PasswordTransformationMethod) { //パスワード入力時は標準のフォントを使用する
            //フォントの設定
            setFont(context)
        }
        //クリック時イベントの設定
        attachClickEvent()
    }

    /**
     * ダイヤルパッドの表示先の設定
     * ※プロパティアクセスと同じ
     */
    fun attachTargetLayout(targetLayout: RelativeLayout) {
        this.targetLayout = targetLayout
    }

    /**
     * テキスト変更時イベントの設定
     * ※プロパティアクセスと同じ
     */
    fun attachOnTextChangedEvent(onTextChangedListener: InputFormNumberInline.OnTextChangedListener) {
        this.onTextChangedListener = onTextChangedListener
    }

    /**
     * クリックイベントの設定
     */
    private fun attachClickEvent() {
        this.setOnClickListener {
            if(!checkClick()) return@setOnClickListener //連打防止
            //既にダイヤルパッド表示済の場合
            if (dialPad != null) {
                //ダイヤルパッドを閉じて終了
                hideDialPad()
                return@setOnClickListener
            }
            //入力可能範囲の取得
            if (propMaxValue >= 0) {
                minValue = propMinValue
                maxValue = propMaxValue
            } else {
                minValue = 0
                maxValue = (10).pow(propMaxLength) - 1
            }
            //ダイヤルパッドの取得
            dialPad = View.inflate(context, R.layout.partial_dialpad, null)
            //数値ボタンの設定
            dialPad?.findViewById<View>(R.id.btn_0)?.setOnClickListener { setNumberPicker(getNumber() * 10 + 0, true) }
            dialPad?.findViewById<View>(R.id.btn_1)?.setOnClickListener { setNumberPicker(getNumber() * 10 + 1, true) }
            dialPad?.findViewById<View>(R.id.btn_2)?.setOnClickListener { setNumberPicker(getNumber() * 10 + 2, true) }
            dialPad?.findViewById<View>(R.id.btn_3)?.setOnClickListener { setNumberPicker(getNumber() * 10 + 3, true) }
            dialPad?.findViewById<View>(R.id.btn_4)?.setOnClickListener { setNumberPicker(getNumber() * 10 + 4, true) }
            dialPad?.findViewById<View>(R.id.btn_5)?.setOnClickListener { setNumberPicker(getNumber() * 10 + 5, true) }
            dialPad?.findViewById<View>(R.id.btn_6)?.setOnClickListener { setNumberPicker(getNumber() * 10 + 6, true) }
            dialPad?.findViewById<View>(R.id.btn_7)?.setOnClickListener { setNumberPicker(getNumber() * 10 + 7, true) }
            dialPad?.findViewById<View>(R.id.btn_8)?.setOnClickListener { setNumberPicker(getNumber() * 10 + 8, true) }
            dialPad?.findViewById<View>(R.id.btn_9)?.setOnClickListener { setNumberPicker(getNumber() * 10 + 9, true) }
            dialPad?.findViewById<View>(R.id.btn_00)?.setOnClickListener { setNumberPicker(getNumber() * 100) }
            dialPad?.findViewById<View>(R.id.btn_000)?.setOnClickListener { setNumberPicker(getNumber() * 1000) }
            //DEL押下時
            val btnDel = dialPad?.findViewById<View>(R.id.btn_del)
            btnDel?.setOnClickListener {
                //最小桁を消す
                setNumberPicker(getNumber() / 10)
            }
            //クリア押下時
            val btnClear = dialPad?.findViewById<View>(R.id.btn_clear)
            btnClear?.setOnClickListener {
                //0にする
                setNumberPicker(0, addDigit = false, force = true)
            }
            //OK押下時
            val btnOk = dialPad?.findViewById<View>(R.id.btn_ok)
            btnOk?.setOnClickListener {
                //範囲チェック
                if(!textStr.toIntOrDefault(Int.MIN_VALUE).isInRange(minValue, maxValue)){
                    val title = context.getString(R.string.title_error)
                    val message = context.getString(R.string.hint_input_range).putParams(minValue, maxValue)
                    Toaster.show(context, title, message)
                } else{
                    //呼び出し元へ通知
                    onTextChangedListener?.onTextChanged(textStr)
                    //ダイヤルパッドを閉じる
                    hideDialPad()
                }
            }
            //ダイヤルパッドを表示
            showDialPad()
        }
        this.setOnLongClickListener {
            //入力されているテキストをクリップボードにコピー
            val cm = context.getSystemService(Context.CLIPBOARD_SERVICE)
            if (cm is ClipboardManager) {
                cm.setPrimaryClip(ClipData.newPlainText("", this@InputFormNumberInline.text))
                Toaster.showBottom(context, context.getString(R.string.toast_clipboard_copy))
            }
            return@setOnLongClickListener true
        }
    }

    /**
     * 値を取得
     */
    private fun getNumber(): Int {
        if (CheckUtil.isNumber(textStr)) {
            return textStr.replaceAll(",", "").toInt()
        }
        return 0
    }

    /**
     * 値を設定
     * @param num 新しい値
     */
    private fun setNumberPicker(num: Int) {
        setNumberPicker(num, false)
    }

    /**
     * 値を設定
     * @param num 新しい値
     * @param addDigit 桁追加（最小桁に数値が追加された）かのフラグ
     * @param force 強制（上下限無視するか）のフラグ
     */
    private fun setNumberPicker(num: Int, addDigit: Boolean, force: Boolean = false) {
        var value = num
        //桁追加（最小桁に数値が追加された）の場合かつ、最大値を超える場合
        if (addDigit && value > maxValue) {
            //桁追加ではなく、元の最小桁への上書きに変更する
            value = (value / 100) * 10 + value.pick(1)
        }
        //範囲チェック
        if(!force){
            if (value < minValue) {
                value = minValue
            }
            if (value > maxValue) {
                value = maxValue
            }
        }
        //テキストボックスに設定
        this.text = "%,d".format(value)
    }

    /**
     * 連打チェック
     */
    private var lastClickTime: Long = 0 //最後にボタンを押した時刻（連打防止チェック用）
    private fun checkClick(): Boolean {
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastClickTime < Settings.Interval.PERMIT_CLICK) {
            return false
        }
        lastClickTime = currentTime
        return true
    }

    /**
     * フォントを設定
     * @param context コンテキスト
     */
    private fun setFont(context: Context?) {
        try {
            //システム定義のフォントを設定
            if (textStyle == Typeface.BOLD) {
                this.typeface = ResourceHolder.Typefaces.getBold(context!!)
            } else {
                this.typeface = ResourceHolder.Typefaces.getDefault(context!!)
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * 入力可能範囲の設定
     * @param minValue 最小値
     * @param maxValue 最大値
     */
    fun setRange(minValue: Int, maxValue: Int) {
        //パラメタの格納
        this.propMinValue = minValue
        this.propMaxValue = maxValue
        //表示済のダイヤルパッドがある場合に対応
        this.minValue = minValue
        this.maxValue = maxValue
    }

    /**
     * ダイヤルパッドを表示
     */
    fun showDialPad() {
        lockDialPad.withLock {
            if (targetLayout == null || dialPad == null) {
                return
            }
            targetLayout?.addView(dialPad)
            dialPad?.clearAnimation()
            dialPad?.startAnimation(AnimationUtils.loadAnimation(context, R.anim.compress_in_bottom))
        }
    }

    /**
     * ダイヤルパッドを非表示
     */
    fun hideDialPad() {
        GlobalScope.launch {
            lockDialPad.withLock {
                runBlocking {
                    if (targetLayout == null || dialPad == null) {
                        return@runBlocking
                    }
                    dialPad?.post {
                        dialPad?.clearAnimation()
                        dialPad?.startAnimation(AnimationUtils.loadAnimation(context, R.anim.compress_out_bottom))
                    }
                    delay(200)
                    targetLayout?.post {
                        if (dialPad != null) {
                            targetLayout?.removeView(dialPad)
                            dialPad = null
                        }
                    }
                }
            }
        }
    }
}
