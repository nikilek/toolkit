package net.nikilek.toolkit

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Typeface
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.widget.TextView

import net.nikilek.toolkit.component.EditTextDialog
import net.nikilek.toolkit.constant.CodeConst
import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.extension.replaceAllNGChar
import net.nikilek.toolkit.utility.LogUtil
import net.nikilek.toolkit.utility.ResourceHolder
import net.nikilek.toolkit.utility.Toaster

/**
 * Created by nikilek on 2016/01/27.
 * 別窓で文字入力ができるテキストボックス
 * 【独自XML属性】※「res/values/attrs.xml」参照
 * ・permitBlank：空白のみの入力を許可するかのフラグ
 * 　※permitBlank=falseで空白のみの入力がされた場合、入力内容の更新を行わず、元の入力内容のままとする。
 */
open class InputFormText : TextView {

    /**
     * テキスト変更時時イベントのリスナ
     */
    interface OnTextChangedListener {
        /**
         * @param text 新たなテキスト
         */
        fun onTextChanged(text: String)
    }

    companion object {
        // 入力を許可する記号
        // ※全半角英数字、ひらがな、全角カナ、JIS第一,二水準漢字、マルチバイトの英数字 以外を許可する場合はこの配列に指定
        var permitSymbol: Array<String> = arrayOf()
    }

    private var textStyle: Int = Typeface.NORMAL //文字の表示スタイル
    private var propSingleLine: Boolean = true //１行表示かのフラグ
    private var propMaxLines: Int = 0 //最大行数　※singleLineよりも優先
    private var propInputType: Int = 0 //文字の入力タイプ
    private var propMaxEms: Int = 0 //最大文字幅　※maxLengthよりも優先
    private var propMaxLength: Int = 0 //最大文字長
    private var propPermitBlank: Boolean = true //空白文字のみの入力を許可するかのフラグ
    private var title: String = "" //入力ダイアログのタイトル
    var onTextChangedListener: InputFormText.OnTextChangedListener? = null //テキスト変更時イベントのリスナ

    /**
     * 文字列の取得
     */
    val textStr: String
        get() = text.toString()

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context)

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        getAttrs(attrs)
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        getAttrs(attrs)
    }

    /**
     * 属性の取得
     * @param attrs 属性セット
     */
    private fun getAttrs(attrs: AttributeSet) {
        this.textStyle = attrs.getAttributeIntValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "textStyle", textStyle)
        this.propSingleLine = attrs.getAttributeBooleanValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "singleLine", true)
        this.propInputType = attrs.getAttributeIntValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "inputType", InputType.TYPE_CLASS_TEXT)
        this.propMaxEms = attrs.getAttributeIntValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "maxEms", -1) //maxLengthよりも優先
        this.propMaxLength = attrs.getAttributeIntValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "maxLength", -1)
        this.propMaxLines = attrs.getAttributeIntValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "maxLines", -1)
        if (context is Context) {
            val resIdEmpty = R.string.dummy_empty
            this.title = context.getString(attrs.getAttributeResourceValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "hint", resIdEmpty))
            if (context.getString(resIdEmpty) == this.title) {
                this.title = attrs.getAttributeValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "hint") ?: ""
            }
        }
        val attrsOriginal = context.obtainStyledAttributes(attrs, R.styleable.InputFormText)
        this.propPermitBlank = attrsOriginal.getBoolean(R.styleable.InputFormText_permitBlank, true)
        attrsOriginal.recycle()
    }

    public override fun onFinishInflate() {
        super.onFinishInflate()
        if (this.isInEditMode) {
            return
        }
        if (this.transformationMethod !is PasswordTransformationMethod) { //パスワード入力時は標準のフォントを使用する
            //フォントの設定
            setFont(context)
        }
        //クリック時イベントの設定
        attachClickEvent()
    }

    /**
     * テキスト変更時イベントの設定
     * ※プロパティアクセスと同じ
     */
    fun attachOnTextChangedEvent(onTextChangedListener: InputFormText.OnTextChangedListener) {
        this.onTextChangedListener = onTextChangedListener
    }

    /**
     * クリックイベントの設定
     */
    private fun attachClickEvent() {
        this.setOnClickListener {
            if(!checkClick()) return@setOnClickListener //連打防止
            val currentText = textStr
            val maxLines =
                    when {
                        propMaxLines > 0 -> propMaxLines
                        propSingleLine -> 1
                        else -> Int.MAX_VALUE
                    }
            val onFinishListener = object : EditTextDialog.OnFinishListener {
                override fun onFinish(text: String, textChanged: Boolean, canceled: Boolean) {
                    if (canceled) {
                        return
                    }
                    //空白文字のみの入力かをチェック
                    if (!propPermitBlank && text.isBlank()) {
                        return
                    }
                    //禁止文字の置換
                    var checkedStr = text
                    if (!Settings.String.PERMIT_INPUT_ALL) {
                        //「全て許可」以外の場合にチェック
                        checkedStr = text.replaceAllNGChar("?",
                                Settings.String.PERMIT_INPUT_HALFWIDTH_KANA,
                                Settings.String.PERMIT_INPUT_HALFWIDTH_SYMBOL,
                                Settings.String.PERMIT_INPUT_FULLWIDTH_NUMERIC_AND_ALPHABET,
                                Settings.String.PERMIT_INPUT_FULLWIDTH_SYMBOL1,
                                Settings.String.PERMIT_INPUT_FULLWIDTH_SYMBOL2,
                                Settings.String.PERMIT_INPUT_FULLWIDTH_GREEK,
                                Settings.String.PERMIT_INPUT_FULLWIDTH_CYRILLIC,
                                Settings.String.PERMIT_INPUT_FULLWIDTH_BORDER,
                                permitSymbol)!!
                    }
                    //入力されたテキストを反映
                    this@InputFormText.text = checkedStr
                    //呼び出し元へ通知
                    if (textChanged) {
                        onTextChangedListener?.onTextChanged(textStr)
                    }
                }
            }
            //テキスト入力用ダイアログを表示
            EditTextDialog(context as AbstractActivity, onFinishListener, maxLines,
                    title, currentText, propMaxLength, propMaxEms, propInputType).show()
        }
        this.setOnLongClickListener {
            //入力されているテキストをクリップボードにコピー
            val cm = context.getSystemService(Context.CLIPBOARD_SERVICE)
            if (cm is ClipboardManager) {
                cm.setPrimaryClip(ClipData.newPlainText("", this@InputFormText.text))
                Toaster.showBottom(context, context.getString(R.string.toast_clipboard_copy))
            }
            return@setOnLongClickListener true
        }
    }

    /**
     * 連打チェック
     */
    private var lastClickTime: Long = 0 //最後にボタンを押した時刻（連打防止チェック用）
    private fun checkClick(): Boolean {
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastClickTime < Settings.Interval.PERMIT_CLICK) {
            return false
        }
        lastClickTime = currentTime
        return true
    }

    /**
     * フォントを設定
     * @param context コンテキスト
     */
    private fun setFont(context: Context?) {
        try {
            //システム定義のフォントを設定
            if (textStyle == Typeface.BOLD) {
                this.typeface = ResourceHolder.Typefaces.getBold(context!!)
            } else {
                this.typeface = ResourceHolder.Typefaces.getDefault(context!!)
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }
}
