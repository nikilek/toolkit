package net.nikilek.toolkit

import android.os.Handler
import android.os.Message
import net.nikilek.toolkit.utility.AndroidUtil
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

abstract class BufferedHandler : Handler() {
    private var paused: Boolean = false //ポーズ中フラグ
    private val buffer = mutableListOf<Message>() //メッセージバッファ
    private val lock: ReentrantLock = ReentrantLock() //バッファ操作用ロック

    /**
     * メッセージ処理
     * @param msg メッセージ
     */
    abstract fun onHandleMessage(msg: Message)

    /**
     * メッセージ処理の中断
     */
    fun pause() {
        paused = true
    }

    /**
     * メッセージ処理の再開
     */
    fun resume() {
        paused = false
        lock.withLock {
            for (i in 0 until buffer.size) {
                val msg = buffer[i]
                sendMessage(msg)
            }
            buffer.clear()
        }
    }

    override fun handleMessage(msg: Message) {
        if (paused) {
            lock.withLock {
                buffer.add(AndroidUtil.createMessage(msg.what, msg.arg1, msg.arg2, msg.obj))
            }
        } else {
            onHandleMessage(msg)
        }
    }
}