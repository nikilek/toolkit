package net.nikilek.toolkit.adapter

/**
 * Created by nikilek on 2018/01/29.
 * 文字列リスト表示用アダプタ（RecyclerView用）
 */
import android.view.View
import android.widget.TextView
import net.nikilek.toolkit.R

class RecyclerAdapterString(layoutId: Int = R.layout.item_list_string_middle) : AbstractRecyclerAdapter<String>(layoutId) {

    override fun drawItem(v: View, position: Int, data: String) {
        v.findViewById<TextView>(R.id.value).text = data
    }
}