package net.nikilek.toolkit.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import net.nikilek.toolkit.extension.validMax
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.Comparator
import kotlin.concurrent.withLock

/**
 * Created by nikilek on 2016/01/27.
 * 文字列リスト表示用アダプタ（ListView用）
 * @param layoutId １行ごとのレイアウトのリソースID
 * @param listView 設定先のリストビュー　※既定値：null
 */
abstract class AbstractListAdapter<T>(private val layoutId: Int,
                                      private var listView: ListView? = null) : BaseAdapter() {

    private var dataList = mutableListOf<T>() //データリスト
    private val lockDataList = ReentrantLock() //データリスト操作用ロック

    init {
        //リストビューに設定
        attach(listView)
    }

    /*
     * ↓継承先で実装が必要↓
     */

    /**
     * アイテムごとの描画処理
     * @param v 描画先のビュー（レイアウトのルート）
     * @param position 対象のアイテムのインデックス
     * @param data 対象のアイテムに関連するデータ（AbstractRecyclerAdapter#getItem(position)）
     */
    abstract fun drawItem(v: View, position: Int, data: T)

    /*
     * ↓リストビュー関連↓
     */

    /**
     * リストビューに設定
     * @param listView 設定先のリストビュー
     */
    fun attach(listView: ListView?) {
        this.listView = listView
        this.listView?.apply {
            post {
                adapter = this@AbstractListAdapter
                notifyDataSetChanged()
            }
        }
    }

    /*
     * ↓データリストの操作↓
     */

    /**
     * データリストの設定
     * @param dataList データリスト
     */
    fun setItems(dataList: MutableList<T>) {
        val set = fun() {
            lockDataList.withLock {
                this@AbstractListAdapter.dataList = dataList
                notifyDataSetChanged()
            }
        }
        if (listView == null) {
            GlobalScope.launch(Dispatchers.Main) {
                set()
            }
        } else {
            listView?.post {
                set()
            }
        }
    }

    /**
     * データリストのソート
     * @param comparator コンパレーター
     */
    fun sortItems(comparator: Comparator<T>) {
        val sort = fun() {
            lockDataList.withLock {
                Collections.sort(this@AbstractListAdapter.dataList, comparator)
                notifyDataSetChanged()
            }
        }
        if (listView == null) {
            GlobalScope.launch(Dispatchers.Main) {
                sort()
            }
        } else {
            listView?.post {
                sort()
            }
        }
    }

    /**
     * データリストの逆転
     */
    fun reverseItems() {
        val reverse = fun() {
            lockDataList.withLock {
                this@AbstractListAdapter.dataList.reverse()
                notifyDataSetChanged()
            }
        }
        if (listView == null) {
            GlobalScope.launch(Dispatchers.Main) {
                reverse()
            }
        } else {
            listView?.post {
                reverse()
            }
        }
    }

    /**
     * データリストからアイテムを全て削除
     */
    fun clearItems() {
        val clear = fun() {
            lockDataList.withLock {
                dataList.clear()
                notifyDataSetChanged()
            }
        }
        if (listView == null) {
            GlobalScope.launch(Dispatchers.Main) {
                clear()
            }
        } else {
            listView?.post {
                clear()
            }
        }
    }

    /**
     * データリストにアイテムを追加
     * @param data 追加するアイテム
     */
    fun addItem(data: T) {
        addItem(-1, data)
    }

    /**
     * データリストにアイテムを追加
     * @param data 追加するアイテム
     */
    fun addItems(vararg data: T) {
        addItems(-1, data.toList())
    }

    /**
     * データリストにアイテムを追加
     * @param data 追加するアイテム
     */
    fun addItems(data: List<T>) {
        addItems(-1, data)
    }

    /**
     * データリストにアイテムを追加
     * @param position 追加先のインデックス
     * @param data 追加するアイテム
     */
    fun addItem(position: Int, data: T) {
        addItems(position, data)
    }

    /**
     * データリストにアイテムを追加
     * @param position 追加先のインデックス
     * @param data 追加するアイテム
     */
    fun addItems(position: Int, vararg data: T) {
        addItems(position, data.toList())
    }

    /**
     * データリストにアイテムを追加
     * @param position 追加先のインデックス
     * @param data 追加するアイテム
     */
    fun addItems(position: Int, data: List<T>) {
        val add = fun() {
            lockDataList.withLock {
                val index = if (position < 0) dataList.size else position.validMax(dataList.size)
                dataList.addAll(index, data)
                notifyDataSetChanged()
            }
        }
        if (listView == null) {
            GlobalScope.launch(Dispatchers.Main) {
                add()
            }
        } else {
            listView?.post {
                add()
            }
        }
    }

    /**
     * データリストからアイテムを削除
     * @param position 削除対象のインデックス
     */
    fun removeItem(position: Int) {
        removeItems(position)
    }

    /**
     * データリストからアイテムを削除
     * @param position 削除対象のインデックス
     */
    fun removeItems(vararg position: Int) {
        removeItems(position.toTypedArray())
    }

    /**
     * データリストからアイテムを削除
     * @param positions 削除対象のインデックス
     */
    fun removeItems(positions: Array<Int>) {
        val remove = fun() {
            lockDataList.withLock {
                positions.sortedArray().reversedArray().forEach { position ->
                    dataList.removeAt(position)
                }
                notifyDataSetChanged()
            }
        }
        if (listView == null) {
            GlobalScope.launch(Dispatchers.Main) {
                remove()
            }
        } else {
            listView?.post {
                remove()
            }
        }
    }

    /**
     * データリストからアイテムを削除
     * @param positions 削除対象のインデックス
     */
    fun removeItems(positions: List<Int>) {
        val remove = fun() {
            lockDataList.withLock {
                positions.sorted().reversed().forEach { position ->
                    dataList.removeAt(position)
                }
                notifyDataSetChanged()
            }
        }
        if (listView == null) {
            GlobalScope.launch(Dispatchers.Main) {
                remove()
            }
        } else {
            listView?.post {
                remove()
            }
        }
    }

    /**
     * データリストのコピーを取得
     * @return データリストのコピー
     */
    fun getCopyOfItems(): MutableList<T> {
        lockDataList.withLock {
            return dataList.toMutableList()
        }
    }

    /*
     * ↓アイテムの情報を取得↓
     */

    /**
     * アイテムの取得
     * @param position アイテムのインデックス
     * @return アイテムの参照
     */
    override fun getItem(position: Int): T {
        lockDataList.withLock {
            return this.dataList[position]
        }
    }

    /**
     * アイテムのIDを取得
     * @param position 対象のアイテムのインデックス
     * @return アイテムのID
     */
    override fun getItemId(position: Int): Long {
        lockDataList.withLock {
            return position.toLong()
        }
    }

    /**
     * アイテムの数を取得
     * @return アイテムの数
     */
    override fun getCount(): Int {
        lockDataList.withLock {
            return this.dataList.size
        }
    }

    /*
     * ↓ライフサイクル関連↓
     */

    /**
     * 各行の描画
     * @param position 対象の行
     * @param convertView 対象のビュー
     * @param parent 対象ビューの親要素
     * @return 対象行のビュー
     */
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        lockDataList.withLock {
            //ビューの取得
            val v: View = convertView ?: View.inflate(parent.context, layoutId, null)
            //データの取得
            val data = getItem(position)
            //アイテムごとの描画処理
            drawItem(v, position, data)
            return v
        }
    }
}
