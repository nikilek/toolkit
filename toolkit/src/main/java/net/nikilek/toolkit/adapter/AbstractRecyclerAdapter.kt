package net.nikilek.toolkit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.Comparator
import kotlin.concurrent.withLock

abstract class AbstractRecyclerAdapter<T>(private val layoutId: Int) : RecyclerView.Adapter<AbstractRecyclerAdapter<T>.ViewHolder>() {

    var onItemClickListener: View.OnClickListener? = null //アイテム選択時イベント

    private var dataList = mutableListOf<T>() //データリスト
    private val lockDataList = ReentrantLock() //データリスト操作用ロック

    /*
     * ↓継承先で実装が必要↓
     */

    /**
     * アイテムごとの描画処理
     * @param v 描画先のビュー（レイアウトのルート）
     * @param position 対象のアイテムのインデックス
     * @param data 対象のアイテムに関連するデータ（AbstractRecyclerAdapter#getItem(position)）
     */
    abstract fun drawItem(v: View, position: Int, data: T)

    /*
     * ↓データリストの操作↓
     */

    /**
     * データリストの設定
     * @param dataList データリスト
     * @param notifyChanged ビューに反映するかのフラグ　※既定値：true
     */
    @JvmOverloads
    fun setItems(dataList: MutableList<T>, notifyChanged: Boolean = true) {
        lockDataList.withLock {
            this.dataList = dataList
            //変更通知
            if (notifyChanged) {
                GlobalScope.launch(Dispatchers.Main) {
                    lockDataList.withLock {
                        notifyDataSetChanged()
                    }
                }
            }
        }
    }

    /**
     * データリストのソート
     * @param comparator コンパレーター
     * @param notifyChanged ビューに反映するかのフラグ　※既定値：true
     */
    fun sortItems(comparator: Comparator<T>, notifyChanged: Boolean = true) {
        lockDataList.withLock {
            Collections.sort(this.dataList, comparator)
            //変更通知
            if (notifyChanged) {
                GlobalScope.launch(Dispatchers.Main) {
                    lockDataList.withLock {
                        notifyDataSetChanged()
                    }
                }
            }
        }
    }

    /**
     * データリストの逆転
     * @param notifyChanged ビューに反映するかのフラグ　※既定値：true
     */
    fun reverseItems(notifyChanged: Boolean = true) {
        lockDataList.withLock {
            this.dataList.reverse()
            //変更通知
            if (notifyChanged) {
                GlobalScope.launch(Dispatchers.Main) {
                    lockDataList.withLock {
                        notifyDataSetChanged()
                    }
                }
            }
        }
    }

    /**
     * データリストからアイテムを全て削除
     * @param notifyChanged ビューに反映するかのフラグ　※既定値：true
     */
    @JvmOverloads
    fun clearItems(notifyChanged: Boolean = true) {
        lockDataList.withLock {
            this.dataList.clear()
            //変更通知
            if (notifyChanged) {
                GlobalScope.launch(Dispatchers.Main) {
                    lockDataList.withLock {
                        notifyDataSetChanged()
                    }
                }
            }
        }
    }

    /**
     * データリストにアイテムを追加
     * @param data 追加するアイテム
     * @param notifyChanged ビューに反映するかのフラグ　※既定値：true
     */
    @JvmOverloads
    fun addItem(data: T, notifyChanged: Boolean = true) {
        addItem(this.dataList.size, data, notifyChanged)
    }

    /**
     * データリストにアイテムを追加
     * @param position 追加先のインデックス
     * @param data 追加するアイテム
     * @param notifyChanged ビューに反映するかのフラグ　※既定値：true
     */
    @JvmOverloads
    fun addItem(position: Int, data: T, notifyChanged: Boolean = true) {
        lockDataList.withLock {
            this.dataList.add(position, data)
            //変更通知
            if (notifyChanged) {
                GlobalScope.launch(Dispatchers.Main) {
                    lockDataList.withLock {
                        notifyItemInserted(position)
                    }
                }
            }
        }
    }

    /**
     * データリストからアイテムを削除
     * @param position 削除対象のインデックス
     * @param notifyChanged ビューに反映するかのフラグ　※既定値：true
     */
    @JvmOverloads
    fun removeItem(position: Int, notifyChanged: Boolean = true) {
        lockDataList.withLock {
            this.dataList.removeAt(position)
            //変更通知
            if (notifyChanged) {
                GlobalScope.launch(Dispatchers.Main) {
                    lockDataList.withLock {
                        notifyItemRemoved(position)
                    }
                }
            }
        }
    }

    /**
     * データリストからアイテムを削除
     * @param v 対象アイテムにバインドされたビューホルダーにおける、ルートビュー（アイテムのクリックイベント等から取得）
     * @param notifyChanged ビューに反映するかのフラグ　※既定値：true
     */
    @JvmOverloads
    fun removeItem(v: View, notifyChanged: Boolean = true) {
        lockDataList.withLock {
            val position = getItemPosition(v)
            removeItem(position, notifyChanged)
        }
    }

    /**
     * データリストのコピーを取得
     * @return データリストのコピー
     */
    fun getCopyOfItems(): MutableList<T> {
        lockDataList.withLock {
            return this.dataList.toMutableList()
        }
    }

    /*
     * ↓アイテムの情報を取得↓
     */

    /**
     * アイテムの取得
     * @param position アイテムのインデックス
     * @return アイテムの参照
     */
    open fun getItem(position: Int): T? {
        lockDataList.withLock {
            return if (position < 0 || this.dataList.size <= position) {
                return null
            } else {
                this.dataList[position]
            }
        }
    }

    /**
     * アイテムの取得
     * @param v 対象アイテムにバインドされたビューホルダーにおける、ルートビュー（アイテムのクリックイベント等から取得）
     * @return アイテムの参照
     */
    open fun getItem(v: View): T? {
        lockDataList.withLock {
            val position = getItemPosition(v)
            return getItem(position)
        }
    }

    /**
     * アイテムのインデックスを取得
     * @param v 対象アイテムにバインドされたビューホルダーにおける、ルートビュー（アイテムのクリックイベント等から取得）
     * @return アイテムのインデックス
     */
    open fun getItemPosition(v: View): Int {
        lockDataList.withLock {
            return v.id
        }
    }

    /**
     * アイテムのIDを取得
     * @param position 対象のアイテムのインデックス
     * @return アイテムのID
     */
    override fun getItemId(position: Int): Long {
        lockDataList.withLock {
            return position.toLong()
        }
    }

    /**
     * アイテムのIDを取得
     * @param v 対象アイテムにバインドされたビューホルダーにおける、ルートビュー（アイテムのクリックイベント等から取得）
     * @return アイテムのID
     */
    open fun getItemId(v: View): Long {
        lockDataList.withLock {
            val position = getItemPosition(v)
            return getItemId(position)
        }
    }

    /**
     * アイテムの数を取得
     * @return アイテムの数
     */
    override fun getItemCount(): Int {
        lockDataList.withLock {
            return this.dataList.size
        }
    }

    /*
     * ↓ライフサイクル関連↓
     */

    /**
     * ビューホルダークラス（行ごとのレイアウト情報を保持）
     * @param rootView ルートビュー
     */
    inner class ViewHolder(val rootView: View) : RecyclerView.ViewHolder(rootView)

    /**
     * ビューホルダーの生成
     * @param viewGroup 親レイアウト
     * @param viewType ビューの種類
     * @return 生成したビューホルダー
     */
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(layoutId, viewGroup, false)
        return ViewHolder(v)
    }

    /**
     * ビューホルダーのバインド
     * @param viewHolder ビューホルダー
     * @param position 対象のアイテムのインデックス
     */
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        //IDの設定
        viewHolder.rootView.id = position
        //クリックイベントの設定
        viewHolder.rootView.setOnClickListener { v ->
            onItemClickListener?.onClick(v)
        }
        //アイテムごとの描画処理
        drawItem(viewHolder.rootView, position, getItem(position)!!)
    }
}
