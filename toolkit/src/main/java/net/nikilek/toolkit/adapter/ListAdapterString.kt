package net.nikilek.toolkit.adapter

import android.view.View
import android.widget.TextView

import net.nikilek.toolkit.R

/**
 * Created by nikilek on 2016/01/27.
 * 文字列リスト表示用アダプタ（ListView用）
 */
class ListAdapterString : AbstractListAdapter<String> {

    override fun drawItem(v: View, position: Int, data: String) {
        v.findViewById<TextView>(R.id.value).text = data
    }

    /**
     * コンストラクタ
     */
    constructor() : super(R.layout.item_list_string_middle)

    /**
     * コンストラクタ
     * @param layoutId レイアウトのリソースID
     */
    constructor(layoutId: Int) : super(layoutId)

    /**
     * コンストラクタ
     * @param dataList データリスト
     */
    constructor(dataList: MutableList<String>) : super(R.layout.item_list_string_middle) {
        setItems(dataList)
    }

    /**
     * コンストラクタ
     * @param dataList データリスト
     * @param layoutId レイアウトのリソースID
     */
    constructor(layoutId: Int, dataList: MutableList<String>) : super(layoutId) {
        setItems(dataList)
    }
}
