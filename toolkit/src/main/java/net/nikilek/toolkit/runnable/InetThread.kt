package net.nikilek.toolkit.runnable

import android.os.Handler
import net.nikilek.toolkit.interfaces.OnMessageListener

import net.nikilek.toolkit.utility.AndroidUtil
import net.nikilek.toolkit.utility.JsonConnector
import net.nikilek.toolkit.utility.LogUtil
import java.io.Serializable

/**
 * Created by nikilek on 2016/01/27.
 * サーバ通信用スレッド
 *
 * 返却の方法は下記のいずれか
 * ・android.os.Handler#sendMessage（呼び出し元から引数でHandlerを渡した場合）
 * ・OnMessageListener#onMessage（呼び出し元から引数でOnMessageListenerを渡した場合）
 *
 * 返却されるデータの内容は下記のとおり
 * ・what：Handler経由で返却の場合は、呼び出し元から引数で指定した識別番号。
 * 　      OnMessageListener経由で返却の場合は未使用（-1）
 * ・arg1：未使用（-1）
 * ・arg2：未使用（-1）
 * ・obj：サーバから受信したデータ（データの型は、呼び出し元からの引数で指定）※通信エラー時はnull
 */
class InetThread <T> : Runnable {

    private var jc: JsonConnector //通信クラス
    private lateinit var sndData: Serializable //送信データ
    private lateinit var rcvDataType: Class<T> //サーバから返却されるデータの型
    private var handler: Handler? = null //ハンドラ
    private var what: Int = -1 //メッセージの識別番号
    private var onMessageListener: OnMessageListener? = null //メッセージコールバック用リスナ

    /**
     * コンストラクタ
     * @param jc 通信クラス
     * @param sndData 送信データ
     * @param rcvDataType サーバから返却されるデータの型
     * @param handler ハンドラ
     * @param what メッセージの識別番号
     */
    constructor(jc: JsonConnector, sndData: Serializable, rcvDataType: Class<T>,
                handler: Handler, what: Int) {
        this.jc = jc
        setProp(sndData, rcvDataType, handler, what)
    }

    /**
     * コンストラクタ
     * @param jc 通信クラス
     * @param sndData 送信データ
     * @param rcvDataType サーバから返却されるデータの型
     * @param onMessageListener メッセージコールバック用リスナ
     */
    constructor(jc: JsonConnector, sndData: Serializable, rcvDataType: Class<T>,
                onMessageListener: OnMessageListener) {
        this.jc = jc
        setProp(sndData, rcvDataType, onMessageListener)
    }

    /**
     * プロパティの設定
     * @param sndData 送信データ
     * @param rcvDataType サーバから返却されるデータの型
     * @param handler ハンドラ
     * @param what メッセージの識別番号
     */
    fun setProp(sndData: Serializable, rcvDataType: Class<T>, handler: Handler, what: Int) {
        this.sndData = sndData
        this.rcvDataType = rcvDataType
        this.handler = handler
        this.what = what
        this.onMessageListener = null
    }

    /**
     * プロパティの設定
     * @param sndData 送信データ
     * @param rcvDataType サーバから返却されるデータの型
     * @param onMessageListener メッセージコールバック用リスナ
     */
    fun setProp(sndData: Serializable, rcvDataType: Class<T>, onMessageListener: OnMessageListener) {
        this.sndData = sndData
        this.rcvDataType = rcvDataType
        this.handler = null
        this.what = -1
        this.onMessageListener = onMessageListener
    }

    override fun run() {
        //通信実行
        var rcvData: T? = null
        try {
            rcvData = jc.send(true, sndData, rcvDataType)
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
        //結果をメッセージで返却
        val msg = AndroidUtil.createMessage(what, -1, -1, rcvData)
        if (handler != null) {
            handler?.sendMessage(msg)
        } else if (onMessageListener != null) {
            onMessageListener?.onMessage(msg)
        }
    }
}