package net.nikilek.toolkit.runnable

import kotlinx.coroutines.*
import net.nikilek.toolkit.utility.ThreadUtil
import java.lang.Runnable

/**
 * Created by nikilek on 2018/08/12.
 * 一定周期ごとに指定された処理を呼び出すスレッド
 * ※スレッド開始後、指定周期分待機してから１回目の処理呼び出しを実行
 * ※１回目の処理を即時行いたい場合は、呼び出し元にて対応すること
 */
class TimerThread
/**
 * コンストラクタ
 * @param interval 周期（ミリ秒）
 * @param callback 周期事に呼び出す関数
 * @param highPrecision 高精度モードフラグ　※既定値：false
 * @param callbackOnUiThread UIスレッド上でコールバックするかのフラグ　※既定値：false
 */
(private val interval: Long, private val callback: ((coroutineScope: CoroutineScope) -> Unit),
 private val highPrecision: Boolean = false,
 private val callbackOnUiThread: Boolean = false) : Runnable {
    private var lastActTime = 0L //最終処理時刻
    override fun run() {
        //最終処理時刻を初期化
        lastActTime = System.currentTimeMillis()
        //周期処理
        while (!Thread.currentThread().isInterrupted) {
            try {
                //一定時間待機（高精度モードの場合は10ミリ秒固定）
                val sleepTime = if (highPrecision) 10 else interval
                if (!ThreadUtil.sleep(sleepTime)) break
                //周期に達するまでスキップ（高精度モードの場合のみチェックを行う）
                if (highPrecision && (System.currentTimeMillis() - lastActTime) < interval) {
                    continue
                }
                //最終処理時刻を更新
                lastActTime = System.currentTimeMillis()
                //コールバック処理実行
                val dispatcher = if (callbackOnUiThread) Dispatchers.Main else Dispatchers.Default
                GlobalScope.launch(dispatcher) {
                    callback(this)
                }
            } catch (e: Exception) {
                break
            }
        }
    }
}