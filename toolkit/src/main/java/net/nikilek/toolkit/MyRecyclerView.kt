package net.nikilek.toolkit

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.nikilek.toolkit.adapter.AbstractRecyclerAdapter
import net.nikilek.toolkit.constant.CodeConst
import net.nikilek.toolkit.utility.LogUtil
import java.lang.Exception

/**
 * Created by nikilek on 2018/01/29.
 * カスタムビュー
 */
@SuppressLint("NewApi")
open class MyRecyclerView : RecyclerView {

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context) {
        prepare(null)
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        prepare(attrs)
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        prepare(attrs)
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param adapter リストアダプタ
     */
    constructor(context: Context, adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) : super(context) {
        prepare(null)
        this.adapter = adapter
        this.post { adapter.notifyDataSetChanged() }
    }

    /**
     * 初期設定
     */
    @SuppressLint("ObsoleteSdkInt")
    private fun prepare(attrs: AttributeSet?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD_MR1) {
            //オーバースクロールの設定
            this.overScrollMode = View.OVER_SCROLL_IF_CONTENT_SCROLLS
        }
        //レイアウトマネージャの設定
        val orientation = attrs?.getAttributeIntValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "orientation", LinearLayoutManager.VERTICAL)
                ?: LinearLayoutManager.VERTICAL
        this.layoutManager = MyLinearLayoutManager(context, orientation)
    }

    /**
     * リストアダプタのセット
     * @param adapter リストアダプタ
     */
    fun setListAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
        this.adapter = adapter
        this.post { adapter.notifyDataSetChanged() }
    }

    /**
     * リストアダプタのセット
     * @param adapter リストアダプタ
     */
    fun <T> setListAdapter(adapter: AbstractRecyclerAdapter<T>) {
        try {
            adapter.setHasStableIds(true) //AbstractRecyclerAdapterはgetItemId(int)を実装済
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
        this.adapter = adapter
        this.post { adapter.notifyDataSetChanged() }
    }

    /**
     * スクロール方向を垂直方向に設定
     */
    fun setOrientationVertical() {
        this.layoutManager = MyLinearLayoutManager(context, LinearLayoutManager.VERTICAL)
    }

    /**
     * スクロール方向を水平方向に設定
     */
    fun setOrientationHorizontal() {
        this.layoutManager = MyLinearLayoutManager(context, LinearLayoutManager.HORIZONTAL)
    }

    override fun overScrollBy(deltaX: Int, deltaY: Int, scrollX: Int, scrollY: Int, scrollRangeX: Int, scrollRangeY: Int, maxOverScrollX: Int, maxOverScrollY: Int, isTouchEvent: Boolean): Boolean {
        //オーバースクロール時の挙動を設定
        return false
        //return super.overScrollBy(0, deltaY, 0, scrollY, 0, scrollRangeY, 0, 50, isTouchEvent);
    }

    /**
     * 指定位置までスクロール
     * @param position 位置
     */
    fun scrollTo(position: Int) {
        this.post { super.scrollToPosition(position) }
    }

    /**
     * 指定位置までスクロール（スムーズなスクロール）
     * @param position 位置
     */
    fun smoothScrollTo(position: Int) {
        this.post { super.smoothScrollToPosition(position) }
    }

    /**
     * レイアウトマネージャクラス
     * @param context コンテキスト
     */
    inner class MyLinearLayoutManager(context: Context, orientation: Int) : LinearLayoutManager(context, orientation, false) {
        override fun onLayoutChildren(recycler: Recycler?, state: State) {
            try {
                super.onLayoutChildren(recycler, state)
            } catch (e: IndexOutOfBoundsException) {
                LogUtil.debug(e)
            }
        }
    }
}
