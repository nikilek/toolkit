package net.nikilek.toolkit

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import net.nikilek.toolkit.component.MessageSubScreen
import net.nikilek.toolkit.interfaces.ISaveData
import net.nikilek.toolkit.model.ScreenData
import net.nikilek.toolkit.model.SubScreenData
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Created by nikilek on 2017/10/16.
 * 基底サブスクリーン
 *
 * ◆使用方法
 * 　１．このクラスを継承したビューをルートに持つレイアウトxmlを定義
 * 　２．AbstractActivity#showSubScreenにレイアウトIDを渡す事で画面起動
 * 　３．AbstractActivity#hideSubScreenにより画面終了
 *
 * ◆ライフサイクル
 * 【親クラス依存】
 * ・super#onAttachedToWindow > this#onAttach > this#onActive
 * ・super#onDetachedFromWindow > this#onHide > this#onDetach
 * 【本画面の明示的な起動／終了】
 * ・AbstractActivity#showScreen > super#onAttachedToWindow > this#onAttach
 * ・AbstractActivity#hideScreen > super#onDetachedFromWindow > this#onDetach
 *
 * ◆設計思想
 * Fragmentを(あまり)使わず、Activityの数も極力増やさない思想となっています
 * Application, Activity, Dialog, Screen(独自), SubScreen(独自)は、それぞれAbstractクラスを継承して実装してください
 * ・{Root Activity}(MainActivity)は、{net.nikilek.toolkit._sample.MainActivity}を参考に実装してください
 * ・{Screen}は、Activityの上に重ねて起動する、小さなActivityのような役割です
 * ・{SubScreen}は、Screenの上に重ねて起動する、小さなDialogのような役割です
 * ・{Dialog}は、(同一Activity内の)全ての{Screen}や{SubScreen}の上位(表面)に表示されます
 * ・以下に、DialogとSubScreenの実装例を示します
 * 　- {net.nikilek.toolkit.component.MessageDialog}     ※Activityから起動します
 * 　- {net.nikilek.toolkit.component.MessageSubScreen}  ※Screenから起動します。ScreenはActivityから起動します
 *
 * {Application<AbstractApplication>}
 *    ├──{Root Activity<AbstractActivity>}
 *    │     ├──{Screen<AbstractScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ~
 *    │     ├──{Screen@AbstractScreen}
 *    │     ~
 *    │     ~
 *    │     └──{Dialog@AbstractDialog}
 *    │
 *    ├──{Activity<AbstractActivity>}
 *    │     ├──{Screen<AbstractScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ├──{SubScreen<AbstractSubScreen>}
 *    │     │     ~
 *    │     ├──{Screen<AbstractScreen>}
 *    │     ~
 *    │     ~
 *    │     └──{Dialog<AbstractDialog>}
 *    ~
 */
abstract class AbstractSubScreen : FrameLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    lateinit var activity: AbstractActivity //呼び出し元のActivity
    lateinit var subScreen: SubScreenData //自身の情報
    var initData: Any? = null //起動時に受け取ったデータ

    val screen: ScreenData
        get() = subScreen.parentScreen

    val v: View
        get() = this //subScreen.createdView

    /**
     * セーブデータの取得
     */
    @Suppress("UNCHECKED_CAST")
    protected fun <T : ISaveData> mSaveData(): T {
        return activity.mSaveData()
    }

    /**
     * 起動データの取得
     */
    @Suppress("UNCHECKED_CAST")
    protected fun <T> mInitData(): T {
        return this.initData as T
    }

    /*
     * 排他関連
     */

    private var processing = false
    private val lockProcess = ReentrantLock()

    /**
     * 排他ロックの取得
     * @return true：ロック取得成功、false：ロック取得失敗（既にロック済）
     */
    protected fun acquireLock(): Boolean {
        lockProcess.withLock {
            return if (processing) {
                false
            } else {
                processing = true
                true
            }
        }
    }

    /**
     * 排他ロックの解除
     */
    protected fun releaseLock() {
        lockProcess.withLock {
            processing = false
        }
    }

    /*
     * ↓ライフサイクル関連↓
     */

    public override fun onFinishInflate() {
        super.onFinishInflate()
        if (isInEditMode) return
        //枠外のイベントを背後のスクリーンに伝播しないように設定
        setInterceptTouchEvent(true)
    }

    public override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (!isInEditMode) {
            onAttach()
        }
    }

    public override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        if (!isInEditMode) {
            onDetach()
        }
    }

    /**
     * 初期処理（AbstractActivityから呼び出し）
     * @param activity 呼び出し元のActivity
     * @param subScreen 自身の情報
     * @param initData 起動時に受け取るデータ
     */
    fun setup(activity: AbstractActivity, subScreen: SubScreenData, initData: Any?) {
        this.activity = activity
        this.subScreen = subScreen
        this.initData = initData
    }

    /**
     * 起動時処理（初回表示時）
     */
    abstract fun onAttach()

    /**
     * 終了時処理
     */
    abstract fun onDetach()

    /**
     * サブスクリーンの表示（当該サブスクリーンの配下として生成）
     * @param layoutId サブスクリーンのレイアウトID
     * @param windowAnimationTarget ウィンドウアニメーションの対象とするビューのID（0：ウィンドウ全体、-1：アニメーション無し）
     * @param animationPivot ウィンドウアニメーションの中心（AnimationConst.PIVOT_xxx）
     * @param initData 起動時に受け渡すデータ
     * @param enableBack バックキー有効／無効　※既定値：true
     * @return 生成したサブスクリーン情報の参照
     */
    @JvmOverloads
    fun showSubScreen(layoutId: Int, windowAnimationTarget: Int, animationPivot: Int, initData: Any?, enableBack: Boolean = true): SubScreenData {
        return activity.showSubScreen(subScreen, layoutId, windowAnimationTarget, animationPivot, initData, enableBack)
    }

    /**
     * サブスクリーンの表示（当該サブスクリーンの配下として生成）
     * @param layoutId サブスクリーンのレイアウトID
     * @param windowAnimationTarget ウィンドウアニメーションの対象とするビューのID（0：ウィンドウ全体、-1：アニメーション無し）
     * @param initData 起動時に受け渡すデータ
     * @param enableBack バックキー有効／無効　※既定値：true
     * @return 生成したサブスクリーン情報の参照
     */
    @JvmOverloads
    fun showSubScreen(layoutId: Int, windowAnimationTarget: Int, initData: Any?, enableBack: Boolean = true): SubScreenData {
        return activity.showSubScreen(subScreen, layoutId, windowAnimationTarget, initData, enableBack)
    }

    /**
     * サブスクリーンの表示（当該サブスクリーンの配下として生成）
     * @param layoutId サブスクリーンのレイアウトID
     * @param initData 起動時に受け渡すデータ
     * @param enableBack バックキー有効／無効　※既定値：true
     * @return 生成したサブスクリーン情報の参照
     */
    @JvmOverloads
    fun showSubScreen(layoutId: Int, initData: Any?, enableBack: Boolean = true): SubScreenData {
        return activity.showSubScreen(subScreen, layoutId, initData, enableBack)
    }

    /**
     * サブスクリーンの非表示（当該サブスクリーン配下のサブスクリーンに限る）
     * @param childSubScreen 対象のサブスクリーン
     */
    fun hideSubScreen(childSubScreen: SubScreenData) {
        if (!subScreen.childSubScreens.contains(subScreen)) {
            return
        }
        activity.hideSubScreen(childSubScreen, false)
    }

    /**
     * 最上位のサブスクリーンの非表示（当該サブスクリーン配下のサブスクリーンに限る）
     * @param byBackKey バックキー起点か　※既定値：false
     */
    @JvmOverloads
    fun hideSubScreen(byBackKey: Boolean = false) {
        if (subScreen.childSubScreens.size == 0) {
            return
        }
        activity.hideSubScreen(subScreen.childSubScreens.last(), byBackKey)
    }

    /**
     * サブスクリーンを全て非表示（当該サブスクリーン配下のサブスクリーンに限る）
     * @param byBackKey バックキー起点か　※既定値：false
     */
    fun hideSubScreenAll(byBackKey: Boolean = false) {
        activity.hideSubScreen(subScreen.childSubScreens, byBackKey)
    }

    /**
     * 自身を閉じる
     */
    fun hideOwn() {
        if (screen.createdView is AbstractScreen) {
            (screen.createdView as AbstractScreen).hideSubScreen(subScreen)
        } else {
            activity.hideSubScreen(subScreen, false)
        }
    }

    /*
     * ↓メッセージ表示関連↓
     */

    /**
     * メッセージの表示（アイコン要指定、肯定ボタンのみ）
     * @param iconResId アイコンのリソースID
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showMessage(iconResId: Int, titleResId: Int, messageResId: Int, positiveButtonTextResId: Int,
                    onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showMessage(iconResId, activity.getString(titleResId), activity.getString(messageResId),
                activity.getString(positiveButtonTextResId), onFinishListener, enableBack)
    }

    /**
     * メッセージの表示（アイコン要指定、肯定ボタンのみ）
     * @param iconResId アイコンのリソースID
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showMessage(iconResId: Int, title: String, message: String, positiveButtonText: String,
                    onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showMessage(iconResId, title, message, positiveButtonText, onFinishListener, enableBack)
    }

    /**
     * メッセージの表示（アイコン要指定、肯定・否定ボタン）
     * @param iconResId アイコンのリソースID
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param negativeButtonTextResId 否定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showMessage(iconResId: Int, titleResId: Int, messageResId: Int, positiveButtonTextResId: Int, negativeButtonTextResId: Int,
                    onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showMessage(iconResId, activity.getString(titleResId), activity.getString(messageResId),
                activity.getString(positiveButtonTextResId), activity.getString(negativeButtonTextResId), onFinishListener, enableBack)
    }

    /**
     * メッセージの表示（アイコン要指定、肯定・否定ボタン）
     * @param iconResId アイコンのリソースID
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param negativeButtonText 否定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showMessage(iconResId: Int, title: String, message: String, positiveButtonText: String, negativeButtonText: String,
                    onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        val initData = MessageSubScreen.InitData(MessageSubScreen.Type.QUESTION, iconResId, title, message,
                positiveButtonText, negativeButtonText, onFinishListener)
        return showSubScreen(R.layout.subscreen_message, R.id.content_root, initData, enableBack)
    }

    /**
     * INFOメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showInfoMessage(titleResId: Int, messageResId: Int,
                        onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showInfoMessage(activity.getString(titleResId), activity.getString(messageResId), onFinishListener, enableBack)
    }

    /**
     * INFOメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showInfoMessage(title: String, message: String,
                        onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showInfoMessage(title, message, "", onFinishListener, enableBack)
    }

    /**
     * INFOメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showInfoMessage(titleResId: Int, messageResId: Int, positiveButtonTextResId: Int,
                        onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showInfoMessage(activity.getString(titleResId), activity.getString(messageResId),
                activity.getString(positiveButtonTextResId), onFinishListener, enableBack)
    }

    /**
     * INFOメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showInfoMessage(title: String, message: String, positiveButtonText: String,
                        onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        val initData = MessageSubScreen.InitData(MessageSubScreen.Type.INFO, 0, title, message,
                positiveButtonText, "", onFinishListener)
        return showSubScreen(R.layout.subscreen_message, R.id.content_root, initData, enableBack)
    }

    /**
     * WARNINGメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showWarnMessage(titleResId: Int, messageResId: Int,
                        onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showWarnMessage(activity.getString(titleResId), activity.getString(messageResId), onFinishListener, enableBack)
    }

    /**
     * WARNINGメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showWarnMessage(title: String, message: String,
                        onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showWarnMessage(title, message, "", "", onFinishListener, enableBack)
    }

    /**
     * WARNINGメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param negativeButtonTextResId 否定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showWarnMessage(titleResId: Int, messageResId: Int, positiveButtonTextResId: Int, negativeButtonTextResId: Int,
                        onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showWarnMessage(activity.getString(titleResId), activity.getString(messageResId),
                activity.getString(positiveButtonTextResId), activity.getString(negativeButtonTextResId), onFinishListener, enableBack)
    }

    /**
     * WARNINGメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param negativeButtonText 否定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showWarnMessage(title: String, message: String, positiveButtonText: String, negativeButtonText: String,
                        onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        val initData = MessageSubScreen.InitData(MessageSubScreen.Type.WARNING, 0, title, message,
                positiveButtonText, negativeButtonText, onFinishListener)
        return showSubScreen(R.layout.subscreen_message, R.id.content_root, initData, enableBack)
    }

    /**
     * ERRORメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showErrorMessage(titleResId: Int, messageResId: Int,
                         onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showErrorMessage(activity.getString(titleResId), activity.getString(messageResId), onFinishListener, enableBack)
    }

    /**
     * ERRORメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showErrorMessage(title: String, message: String,
                         onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showErrorMessage(title, message, "", onFinishListener, enableBack)
    }

    /**
     * ERRORメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showErrorMessage(titleResId: Int, messageResId: Int, positiveButtonTextResId: Int,
                         onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showErrorMessage(activity.getString(titleResId), activity.getString(messageResId),
                activity.getString(positiveButtonTextResId), onFinishListener, enableBack)
    }

    /**
     * ERRORメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showErrorMessage(title: String, message: String, positiveButtonText: String,
                         onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        val initData = MessageSubScreen.InitData(MessageSubScreen.Type.ERROR, 0, title, message,
                positiveButtonText, "", onFinishListener)
        return showSubScreen(R.layout.subscreen_message, R.id.content_root, initData, enableBack)
    }

    /**
     * QUESTIONメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showQuestionMessage(titleResId: Int, messageResId: Int,
                            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showQuestionMessage(activity.getString(titleResId), activity.getString(messageResId), onFinishListener, enableBack)
    }

    /**
     * QUESTIONメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showQuestionMessage(title: String, message: String,
                            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showQuestionMessage(title, message, "", "", onFinishListener, enableBack)
    }

    /**
     * QUESTIONメッセージの表示
     * @param titleResId タイトル文字列のリソースID
     * @param messageResId メッセージ文字列のリソースID
     * @param positiveButtonTextResId 肯定ボタンのテキストのリソースID
     * @param negativeButtonTextResId 否定ボタンのテキストのリソースID
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showQuestionMessage(titleResId: Int, messageResId: Int, positiveButtonTextResId: Int, negativeButtonTextResId: Int,
                            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        return showQuestionMessage(activity.getString(titleResId), activity.getString(messageResId),
                activity.getString(positiveButtonTextResId), activity.getString(negativeButtonTextResId), onFinishListener, enableBack)
    }

    /**
     * QUESTIONメッセージの表示
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param positiveButtonText 肯定ボタンのテキスト表記
     * @param negativeButtonText 否定ボタンのテキスト表記
     * @param onFinishListener 終了時イベント　※既定値：null
     */
    @JvmOverloads
    fun showQuestionMessage(title: String, message: String, positiveButtonText: String, negativeButtonText: String,
                            onFinishListener: MessageSubScreen.OnFinishListener? = null, enableBack: Boolean = true): SubScreenData {
        val initData = MessageSubScreen.InitData(MessageSubScreen.Type.QUESTION, 0, title, message,
                positiveButtonText, negativeButtonText, onFinishListener)
        return showSubScreen(R.layout.subscreen_message, R.id.content_root, initData, enableBack)
    }

    /*
     * ↓イベント関連↓
     */

    /**
     * 枠外のタッチイベントを背後のスクリーンに伝播"しない"かの設定を行う
     * @param interceptTouchEvent true：伝播しない、false：伝播する
     */
    fun setInterceptTouchEvent(interceptTouchEvent: Boolean) {
        //インターセプトビューを一度取り除く
        if (getChildAt(0) is InterceptView) {
            this.removeViewAt(0)
        }
        //インターセプトが必要であれば、インターセプトビューを生成して追加する
        if (interceptTouchEvent) {
            val child = InterceptView(context)
            val layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            this.addView(child, 0, layoutParams)
        }
    }

    /*
     * ↓プロセス内通信関連↓
     */

    /**
     * メッセージ処理（親スクリーン等からのメッセージの受取）
     * overrideする場合、個別に処理しないメッセージについては、super.processMessage()を呼び出して処理すること
     * ※ケース１：(other screen calls) android.os.Handler#sendMessage > AbstractActivity#handleMessage > AbstractScreen#processMessage > this method
     * ※ケース２：(other screen calls) this method
     * @param what メッセージ種別
     * @param obj 任意のオブジェクト
     */
    open fun processMessage(what: Int, obj: Any) {
        //please override this method.
    }

    /**
     * 親スクリーンにメッセージを送信
     * @param what メッセージ種別
     * @param obj 任意のオブジェクト
     */
    fun sendToParentScreen(what: Int, obj: Any) {
        (screen.createdView as? AbstractScreen)?.processMessage(what, obj)
    }

    /**
     * 呼び出し元のActivityへのメッセージ送信
     * @param msg メッセージオブジェクト
     */
    fun sendToActivity(msg: android.os.Message) {
        activity.handler.sendMessage(msg)
    }
}