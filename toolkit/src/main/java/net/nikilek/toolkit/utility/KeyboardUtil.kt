package net.nikilek.toolkit.utility

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.os.Handler
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.WindowManager.LayoutParams
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

/**
 * Created by nikilek on 2016/01/27.
 * キーボード関連の汎用クラス
 */
object KeyboardUtil {

    /**
     * キーボードを非表示にする
     * @param context コンテキスト
     * @param view フォーカス対象のビュー
     */
    fun hide(context: Context, view: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
        view.clearFocus()
    }

    /**
     * キーボードを非表示にする
     * @param activity 対象のActivity
     */
    fun hide(activity: Activity) {
        val view = activity.currentFocus
        if (view != null) {
            hide(activity, view)
        }
    }

    /**
     * キーボードを常に非表示にする
     * @param activity 対象のActivity
     */
    fun initHidden(activity: Activity) {
        activity.window.setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    /**
     * キーボードを表示する
     * @param context コンテキスト
     * @param edit フォーカス対象のテキストボックス
     * @param delayTime 表示までの遅延時間　※既定値：0
     */
    @JvmOverloads
    fun show(context: Context, edit: EditText, delayTime: Int = 0) {
        val showKeyboardDelay = Runnable {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.showSoftInput(edit, InputMethodManager.SHOW_IMPLICIT)
        }
        Handler().postDelayed(showKeyboardDelay, delayTime.toLong())
    }

    /**
     * キーボード表示／非表示時のイベントを定義
     * @param activity 対象のActivity
     * @param onKeyboardVisibilityListener イベントリスナ
     */
    fun setKeyboardListener(activity: Activity, onKeyboardVisibilityListener: OnKeyboardVisibilityListener) {
        val activityRootView = (activity.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0)
        activityRootView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            private var defaultHeightOfWindowVisibleDisplayFrame = -1
            private var wasOpened: Boolean = false
            private val r = Rect()

            override fun onGlobalLayout() {
                activityRootView.getWindowVisibleDisplayFrame(r)

                // デフォルトの高さを取得（未取得の場合）
                if (defaultHeightOfWindowVisibleDisplayFrame < 0) {
                    defaultHeightOfWindowVisibleDisplayFrame = r.height()
                }

                // 高さが低くなっていればキーボード表示状態
                val isOpen = defaultHeightOfWindowVisibleDisplayFrame - r.height() > 100

                if (isOpen == wasOpened) {
                    // キーボードの表示状態は変わっていないはずなので何もしない
                    return
                }

                // 状態の更新
                wasOpened = isOpen

                // イベントの発行
                onKeyboardVisibilityListener.onVisibilityChanged(isOpen)
            }
        })
    }

    /**
     * キーボード表示／非表示時のイベントリスナ
     */
    interface OnKeyboardVisibilityListener {
        /**
         * @param isVisible 表示状態になったか否か
         */
        fun onVisibilityChanged(isVisible: Boolean)
    }
}