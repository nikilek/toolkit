package net.nikilek.toolkit.utility

import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.SoundPool
import android.os.Build
import net.nikilek.toolkit.AbstractActivity
import net.nikilek.toolkit.constant.Settings

open class SoundPlayer(private val activity: AbstractActivity) {
    private var mediaPlayer: MediaPlayer? = null //メディアプレイヤー
    private var mediaFilePath: String = "" //メディアプレイヤーの再生対象のファイルパス
    private var audioAttributes: AudioAttributes? = null //サウンドプールの設定
    private var soundPool: SoundPool? = null //サウンドプール
    private val sounds = mutableMapOf<Int, Int>() //ロードしたサウンド<リソースID, サウンドID>

    /**
     * BGMの再生
     * @param filePath 再生ファイルの相対パス
     * @param volume 音量（0.0～1.0）※音量マイナス＝再生停止
     */
    fun startBGM(filePath: String, volume: Float) {
        mediaFilePath = filePath //ファイル名を保管（セーブデータ変更によるBGM開始に対応）
        if (mediaFilePath.isEmpty()) {
            return
        }
        if (volume < 0.0f) {
            return
        }
        try {
            //新しいメディアプレイヤーを生成
            val descriptor = activity.assets.openFd(mediaFilePath)
            val newMediaPlayer = MediaPlayer()
            newMediaPlayer.setDataSource(descriptor.fileDescriptor, descriptor.startOffset, descriptor.length)
            newMediaPlayer.isLooping = true
            newMediaPlayer.setVolume(volume, volume)
            newMediaPlayer.setOnPreparedListener { mp ->
                try {
                    //新しいメディアプレイヤーの再生を開始
                    mp.start()
                    //元のメディアプレイヤーの再生を停止
                    stopBGM()
                    //新しいメディアプレイヤーを保管
                    mediaPlayer = mp
                } catch (e: Exception) {
                    LogUtil.debug(e)
                }
            }
            newMediaPlayer.prepare()
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * BGMの音量を再設定
     * @param volume 音量（0.0～1.0）※音量マイナス＝再生停止
     */
    fun resetVolumeBGM(volume: Float) {
        if (mediaPlayer == null) {
            //BGM無効から有効に変わった場合はBGM開始
            startBGM(mediaFilePath, volume) //元々開始されていない（ファイル名が空）場合は呼び出し先で処理中断
            return
        }
        if (volume < 0.0f) {
            //BGM有効から無効に変わった場合はBGM停止
            stopBGM()
            return
        }
        //音量設定
        mediaPlayer?.setVolume(volume, volume)
    }

    /**
     * BGMの一時停止
     */
    fun pauseBGM() {
        if (mediaPlayer == null) {
            return
        }
        if (!mediaPlayer!!.isPlaying) {
            return
        }
        try {
            mediaPlayer!!.pause()
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * BGMの再開
     */
    fun resumeBGM() {
        if (mediaPlayer == null) {
            return
        }
        if (mediaPlayer!!.isPlaying) {
            return
        }
        try {
            mediaPlayer!!.start()
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * BGMの停止
     */
    fun stopBGM() {
        if (mediaPlayer == null) {
            return
        }
        mediaPlayer!!.setVolume(0f, 0f)
        pauseBGM()
        try {
            mediaPlayer!!.stop()
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
        mediaPlayer!!.reset()
        mediaPlayer!!.release()
        mediaPlayer = null
    }

    /**
     * サウンドプールの初期化
     */
    private fun initSoundPool() {
        soundPool?.release()
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            @Suppress("DEPRECATION")
            soundPool = SoundPool(Settings.Sound.POOL_SIZE, AudioManager.STREAM_MUSIC, 0)
        } else {
            audioAttributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            soundPool = SoundPool.Builder()
                    .setAudioAttributes(audioAttributes)
                    .setMaxStreams(Settings.Sound.POOL_SIZE)
                    .build()
        }
    }

    /**
     * 効果音の再生準備
     * @param resIds rawリソースIDの配列
     */
    fun loadSE(vararg resIds: Int) {
        //効果音の再生準備
        try {
            //インスタンスの初期化
            if (soundPool == null) {
                initSoundPool()
            }
            //サウンドのロード
            resIds.indices.forEach {
                val resId = resIds[it]
                sounds[resId] = soundPool!!.load(activity, resId, 1)
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * 効果音の再生
     * @param soundResId rawリソースID
     * @param volume 音量（割合）　※既定値：0.8f
     */
    fun playSE(soundResId: Int, volume: Float = 0.8f) {
        if (volume <= 0.0f) {
            return
        }
        val soundId = sounds[soundResId]
        if (soundId == null) {
            //サウンドロード完了時イベントの実装
            soundPool?.setOnLoadCompleteListener { soundPool: SoundPool, i: Int, _: Int ->
                //イベントの解除
                soundPool.setOnLoadCompleteListener(null)
                //サウンドの再生
                soundPool.play(i, volume, volume, 0, 0, 1f)
            }
            //サウンドのロードを実行
            loadSE(soundResId)
        } else {
            //サウンドの再生
            soundPool?.play(soundId, volume, volume, 0, 0, 1f)
        }
    }

    /**
     * 効果音の停止
     */
    fun stopSE() {
        soundPool?.release()
    }

    /**
     * BGMと効果音の停止
     */
    fun stopAll() {
        stopBGM()
        stopSE()
    }
}