package net.nikilek.toolkit.utility

import android.content.Context
import android.widget.ArrayAdapter

import net.nikilek.toolkit.R

/**
 * Created by nikilek on 2016/01/27.
 * Adapter操作用の汎用クラス
 */
object AdapterUtil {

    /**
     * 文字列Spinner用のアダプタを生成（文字サイズ：大、中央寄せ）
     * @param context コンテキスト
     * @param array スピナーに設定する文字の配列
     * @return 生成したアダプタ
     */
    fun createSpinnerAdapterOfLargeCenterText(context: Context, array: Array<String>): ArrayAdapter<CharSequence> {
        val adapter = ArrayAdapter<CharSequence>(context, R.layout.item_spinner_string_large_center, array)
        adapter.setDropDownViewResource(R.layout.item_spinner_singlechoice)
        return adapter
    }

    /**
     * 文字列Spinner用のアダプタを生成（文字サイズ：大、左寄せ）
     * @param context コンテキスト
     * @param array スピナーに設定する文字の配列
     * @return 生成したアダプタ
     */
    fun createSpinnerAdapterOfLargeLeftText(context: Context, array: Array<String>): ArrayAdapter<CharSequence> {
        val adapter = ArrayAdapter<CharSequence>(context, R.layout.item_spinner_string_large_left, array)
        adapter.setDropDownViewResource(R.layout.item_spinner_singlechoice)
        return adapter
    }

    /**
     * 文字列Spinner用のアダプタを生成（文字サイズ：中、中央寄せ）
     * @param context コンテキスト
     * @param array スピナーに設定する文字の配列
     * @return 生成したアダプタ
     */
    fun createSpinnerAdapterOfMiddleCenterText(context: Context, array: Array<String>): ArrayAdapter<CharSequence> {
        val adapter = ArrayAdapter<CharSequence>(context, R.layout.item_spinner_string_middle_center, array)
        adapter.setDropDownViewResource(R.layout.item_spinner_singlechoice)
        return adapter
    }

    /**
     * 文字列Spinner用のアダプタを生成（文字サイズ：中、左寄せ）
     * @param context コンテキスト
     * @param array スピナーに設定する文字の配列
     * @return 生成したアダプタ
     */
    fun createSpinnerAdapterOfMiddleLeftText(context: Context, array: Array<String>): ArrayAdapter<CharSequence> {
        val adapter = ArrayAdapter<CharSequence>(context, R.layout.item_spinner_string_middle_left, array)
        adapter.setDropDownViewResource(R.layout.item_spinner_singlechoice)
        return adapter
    }

    /**
     * 文字列Spinner用のアダプタを生成（文字サイズ：小、中央寄せ）
     * @param context コンテキスト
     * @param array スピナーに設定する文字の配列
     * @return 生成したアダプタ
     */
    fun createSpinnerAdapterOfSmallCenterText(context: Context, array: Array<String>): ArrayAdapter<CharSequence> {
        val adapter = ArrayAdapter<CharSequence>(context, R.layout.item_spinner_string_small_center, array)
        adapter.setDropDownViewResource(R.layout.item_spinner_singlechoice)
        return adapter
    }

    /**
     * 文字列Spinner用のアダプタを生成（文字サイズ：小、左寄せ）
     * @param context コンテキスト
     * @param array スピナーに設定する文字の配列
     * @return 生成したアダプタ
     */
    fun createSpinnerAdapterOfSmallLeftText(context: Context, array: Array<String>): ArrayAdapter<CharSequence> {
        val adapter = ArrayAdapter<CharSequence>(context, R.layout.item_spinner_string_small_left, array)
        adapter.setDropDownViewResource(R.layout.item_spinner_singlechoice)
        return adapter
    }

    /*
     * ↓互換性保持↓
     */

    /**
     * 文字列Spinner用のアダプタを生成（文字サイズ：中、中央寄せ）
     * @param context コンテキスト
     * @param array スピナーに設定する文字の配列
     * @return 生成したアダプタ
     */
    @Deprecated(message = "Use createSpinnerAdapterOfMiddleCenterText(context, array)",
            replaceWith = ReplaceWith("AdapterUtil.createSpinnerAdapterOfMiddleCenterText(context, array)"))
    fun createSpinnerAdapterOfMiddleText(context: Context, array: Array<String>): ArrayAdapter<CharSequence> {
        return createSpinnerAdapterOfMiddleCenterText(context, array)
    }

    /**
     * 文字列Spinner用のアダプタを生成（文字サイズ：小、中央寄せ）
     * @param context コンテキスト
     * @param array スピナーに設定する文字の配列
     * @return 生成したアダプタ
     */
    @Deprecated(message = "Use createSpinnerAdapterOfSmallCenterText(context, array)",
            replaceWith = ReplaceWith("AdapterUtil.createSpinnerAdapterOfSmallCenterText(context, array)"))
    fun createSpinnerAdapterOfSmallText(context: Context, array: Array<String>): ArrayAdapter<CharSequence> {
        return createSpinnerAdapterOfSmallCenterText(context, array)
    }
}
