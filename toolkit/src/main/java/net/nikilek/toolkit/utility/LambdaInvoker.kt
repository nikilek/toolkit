package net.nikilek.toolkit.utility

object LambdaInvoker {

    inline fun invokeUnit(lambda: () -> Unit) {
        lambda()
    }

    inline fun invoke(lambda: () -> Any): Any {
        return lambda()
    }

    inline fun invoke(arg1: Any, lambda: (Any) -> Any): Any {
        return lambda(arg1)
    }

    inline fun invoke(arg1: Any, arg2: Any, lambda: (Any, Any) -> Any): Any {
        return lambda(arg1, arg2)
    }

    inline fun invokeAsString(lambda: () -> String): String {
        return lambda()
    }

    inline fun invokeAsString(arg1: String, lambda: (String) -> String): String {
        return lambda(arg1)
    }

    inline fun invokeAsString(arg1: String, arg2: String, lambda: (String, String) -> String): String {
        return lambda(arg1, arg2)
    }

    inline fun invokeAsChar(lambda: () -> Char): Char {
        return lambda()
    }

    inline fun invokeAsChar(arg1: Char, lambda: (Char) -> Char): Char {
        return lambda(arg1)
    }

    inline fun invokeAsChar(arg1: Char, arg2: Char, lambda: (Char, Char) -> Char): Char {
        return lambda(arg1, arg2)
    }

    inline fun invokeAsBoolean(lambda: () -> Boolean): Boolean {
        return lambda()
    }

    inline fun invokeAsBoolean(arg1: Boolean, lambda: (Boolean) -> Boolean): Boolean {
        return lambda(arg1)
    }

    inline fun invokeAsBoolean(arg1: Boolean, arg2: Boolean, lambda: (Boolean, Boolean) -> Boolean): Boolean {
        return lambda(arg1, arg2)
    }

    inline fun invokeAsInt(lambda: () -> Int): Int {
        return lambda()
    }

    inline fun invokeAsInt(arg1: Int, lambda: (Int) -> Int): Int {
        return lambda(arg1)
    }

    inline fun invokeAsInt(arg1: Int, arg2: Int, lambda: (Int, Int) -> Int): Int {
        return lambda(arg1, arg2)
    }

    inline fun invokeAsLong(lambda: () -> Long): Long {
        return lambda()
    }

    inline fun invokeAsLong(arg1: Long, lambda: (Long) -> Long): Long {
        return lambda(arg1)
    }

    inline fun invokeAsLong(arg1: Long, arg2: Long, lambda: (Long, Long) -> Long): Long {
        return lambda(arg1, arg2)
    }

    inline fun invokeAsByte(lambda: () -> Byte): Byte {
        return lambda()
    }

    inline fun invokeAsByte(arg1: Byte, lambda: (Byte) -> Byte): Byte {
        return lambda(arg1)
    }

    inline fun invokeAsByte(arg1: Byte, arg2: Byte, lambda: (Byte, Byte) -> Byte): Byte {
        return lambda(arg1, arg2)
    }

    inline fun invokeAsShort(lambda: () -> Short): Short {
        return lambda()
    }

    inline fun invokeAsShort(arg1: Short, lambda: (Short) -> Short): Short {
        return lambda(arg1)
    }

    inline fun invokeAsShort(arg1: Short, arg2: Short, lambda: (Short, Short) -> Short): Short {
        return lambda(arg1, arg2)
    }

    inline fun invokeAsFloat(lambda: () -> Float): Float {
        return lambda()
    }

    inline fun invokeAsFloat(arg1: Float, lambda: (Float) -> Float): Float {
        return lambda(arg1)
    }

    inline fun invokeAsFloat(arg1: Float, arg2: Float, lambda: (Float, Float) -> Float): Float {
        return lambda(arg1, arg2)
    }

    inline fun invokeAsDouble(lambda: () -> Double): Double {
        return lambda()
    }

    inline fun invokeAsDouble(arg1: Double, lambda: (Double) -> Double): Double {
        return lambda(arg1)
    }

    inline fun invokeAsDouble(arg1: Double, arg2: Double, lambda: (Double, Double) -> Double): Double {
        return lambda(arg1, arg2)
    }
}