package net.nikilek.toolkit.utility

import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.os.Build
import android.os.Message
import android.provider.Settings
import android.content.ComponentName
import android.content.ContentProvider
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ProviderInfo
import android.net.Uri
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import net.nikilek.toolkit.AbstractActivity
import net.nikilek.toolkit.constant.CodeConst
import java.io.File

/**
 * Created by nikilek on 2015/11/01.
 * OS関連の処理を行う汎用クラス
 */
object AndroidUtil {

    /**
     * 権限チェックを行い、権限が無ければ権限リクエストも行う
     * @param activity 呼び出し元のActivity
     * @param manifestPermissions リクエストする権限（Manifest.permission.XXX）の配列
     * @param callback コールバック処理 { 全て権限有効化されたかのフラグ }
     * @return 権限チェック結果
     */
    fun requestPermission(activity: AbstractActivity, manifestPermissions: Array<String>, callback: ((Boolean) -> Unit)) {
        val requestNeeded = manifestPermissions.filter {
            ContextCompat.checkSelfPermission(activity, it) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.shouldShowRequestPermissionRationale(activity, it)
        }.toTypedArray()
        if (requestNeeded.isEmpty()) {
            //全て権限あり
            callback(true)
            return
        }
        //権限リクエスト実行
        activity.addRequestPermissionResultCallback(CodeConst.MSG.ToAbstractActivity.REQUEST_PERMISSION, fun(result: Boolean, _: Array<String>, _: IntArray) {
            callback(result)
        })
        ActivityCompat.requestPermissions(activity, requestNeeded, CodeConst.MSG.ToAbstractActivity.REQUEST_PERMISSION)
    }

    /**
     * 端末のBluetoothをONにする
     * ※AndroidManifestにパーミッション(android.permission.BLUETOOTH)が宣言済であること
     * @param activity
     * @param callback コールバック処理 { 有効化されたかのフラグ }
     */
    @SuppressLint("MissingPermission")
    fun enableBluetooth(activity: AbstractActivity, callback: (Boolean) -> Unit) {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (bluetoothAdapter == null) {
            callback(false)
            return
        }
        if (!bluetoothAdapter.isEnabled) {
            activity.addActivityResultCallback(CodeConst.MSG.ToAbstractActivity.REQUEST_BLUETOOTH_ON, fun(resultCode: Int, _: Intent?) {
                callback(resultCode == Activity.RESULT_OK)
            })
            activity.startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), CodeConst.MSG.ToAbstractActivity.REQUEST_BLUETOOTH_ON)
        }
    }

    /**
     * パッケージバージョンコード（API28以降の場合はマイナーバージョン）を取得
     * @param activity 呼び出し元のActivity
     * @return バージョンコード
     */
    fun getPkgVer(activity: Activity): Int {
        return try {
            val pkgMng = activity.packageManager.getPackageInfo(activity.packageName, 0)
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
                @Suppress("DEPRECATION")
                pkgMng.versionCode
            } else {
                pkgMng.longVersionCode.toInt()
            }
        } catch (e: PackageManager.NameNotFoundException) {
            -1
        }
    }

    /**
     * パッケージバージョンコードを取得
     * @param activity 呼び出し元のActivity
     * @return バージョンコード
     */
    fun getLongPkgVer(activity: Activity): Long {
        return try {
            val pkgMng = activity.packageManager.getPackageInfo(activity.packageName, 0)
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
                @Suppress("DEPRECATION")
                pkgMng.versionCode.toLong()
            } else {
                pkgMng.longVersionCode
            }
        } catch (e: PackageManager.NameNotFoundException) {
            -1
        }
    }

    /**
     * パッケージバージョン名を取得
     * @param activity 呼び出し元のActivity
     * @return バージョン名
     */
    fun getPkgVerName(activity: Activity): String {
        return try {
            activity.packageManager.getPackageInfo(activity.packageName, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            ""
        }

    }

    /**
     * パッケージの最終更新時刻を取得
     * @param activity 呼び出し元のActivity
     * @return 最終更新時刻（ミリ秒）
     */
    fun getLastUpdateTime(activity: Activity): Long {
        return try {
            activity.packageManager.getPackageInfo(activity.packageName, 0).lastUpdateTime
        } catch (e: PackageManager.NameNotFoundException) {
            -1
        }
    }

    /**
     * FileProviderのAuthorityを取得
     * @param activity 呼び出し元のActivity
     */
    fun getFileProviderAuthority(activity: Activity): String {
        val providerInfo = getProviderInfo(activity, FileProvider::class.java)
        return if (providerInfo == null) "" else providerInfo.authority
    }

    /**
     * ProviderInfoの取得
     * @param activity 呼び出し元のActivity
     * @param providerClass Providerクラス
     * @return ProviderInfo
     */
    fun getProviderInfo(activity: Activity, providerClass: Class<out ContentProvider>): ProviderInfo? {
        return try {
            activity.packageManager.getProviderInfo(ComponentName(activity, providerClass), PackageManager.GET_META_DATA)
        } catch (e: PackageManager.NameNotFoundException) {
            null
        }
    }

    /**
     * 時刻同期が有効かを取得
     * @param activity 呼び出し元のActivity
     * @return 時刻同期が有効か否か
     */
    fun enabledAutoAdjustClock(activity: Activity): Boolean {
        return try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                @Suppress("DEPRECATION")
                Settings.System.getInt(activity.contentResolver, Settings.System.AUTO_TIME) > 0
            } else {
                Settings.System.getInt(activity.contentResolver, Settings.Global.AUTO_TIME) > 0
            }
        } catch (e: Settings.SettingNotFoundException) {
            true
        }

    }

    /**
     * APKのインストールを行う
     * @param activity 呼び出し元のActivity
     * @param apkFile APKファイルへの参照
     * @return Intent送信の成否
     */
    fun installApk(activity: Activity, apkFile: File): Boolean {
        val intent: Intent
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val contentUri = FileProvider.getUriForFile(activity, getFileProviderAuthority(activity), apkFile)
            intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(contentUri, activity.contentResolver.getType(contentUri))
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
        } else {
            val contentUri = Uri.fromFile(apkFile)
            intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                intent.flags = Intent.FLAG_RECEIVER_FOREGROUND
            } else {
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
        }
        //パッケージインストール実行
        if (activity.packageManager.queryIntentActivities(intent, 0).size > 0) {
            activity.startActivity(intent)
            return true
        }
        return false
    }

    /**
     * メッセージを生成
     * @param what
     * @param arg1 既定値：-1
     * @param arg2 既定値：-1
     * @param obj 既定値：null
     * @return 生成したメッセージ
     */
    @JvmOverloads
    fun createMessage(what: Int, arg1: Int = -1, arg2: Int = -1, obj: Any? = null): Message {
        val msg = Message()
        msg.what = what
        msg.arg1 = arg1
        msg.arg2 = arg2
        msg.obj = obj
        return msg
    }

    /**
     * メッセージを生成（arg1, arg2 は -1 固定）
     * @param what
     * @param obj 既定値：null
     * @return 生成したメッセージ
     */
    fun createMessage(what: Int, obj: Any?): Message {
        val msg = Message()
        msg.what = what
        msg.arg1 = -1
        msg.arg2 = -1
        msg.obj = obj
        return msg
    }
}
