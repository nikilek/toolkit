package net.nikilek.toolkit.utility

import net.nikilek.toolkit.AbstractActivity
import net.nikilek.toolkit.R
import net.nikilek.toolkit.component.ProgressSubScreen
import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.extension.softUrlEncode
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

/**
 * Created by nikilek on 2018/06/17.
 * クライアントサイド向け通信クラス（HTTPダウンロードを行う）
 * ※ファイル取得用。受信データをファイルに書き出す。
 *
 * @param urlString 接続先URL
 * @param onFinishListener 終了時処理のリスナ
 */
class HttpFileDownloader(private val activity: AbstractActivity, private val urlString: String,
                         private val onFinishListener: OnFinishListener) {

    @Volatile
    private var processing = false //処理中フラグ
    @Volatile
    private var canceled = false //キャンセル済フラグ

    private var totalByte = 0 //ダウンロード対象のファイルの総バイト数
    private var readByte = 0 //ダウンロード済のバイト数

    /**
     * ダウンロード終了時イベントのリスナ
     */
    interface OnFinishListener {
        /**
         * 完了
         * @param file ダウンロードしたファイルへの参照
         */
        fun onComplete(file: java.io.File)

        /**
         * 中止
         * @param canceled 故意にキャンセルしたか否か
         */
        fun onRefuse(canceled: Boolean)
    }

    /**
     * ダウンロードの開始
     * @param showProgress プログレス表示するかのフラグ　※既定値：false
     * @param retryCount リトライ回数（初回含む）　※既定値：1
     */
    @JvmOverloads
    fun start(showProgress: Boolean = false, retryCount: Int = 1) {
        Thread(Runnable {
            //排他チェック
            while (processing && !Thread.currentThread().isInterrupted) {
                ThreadUtil.sleep(100)
                continue
            }
            processing = true //処理中フラグON
            //指定回数試行
            var con: HttpURLConnection? = null
            var file: File? = null
            for (i in 1..retryCount) {
                //初期化処理
                init(con)
                //指定URLに接続
                con = connect() ?: continue
                //データサイズの取得
                totalByte = con.contentLength
                if (totalByte == 0) {
                    continue
                }
                //ストリームからデータを読込
                file = readAll(con, showProgress)
                if (file == null) {
                    continue
                }
                //処理成功
                break
            }
            //処理完了
            finish(con, file)
        }).start()
    }

    /**
     * ダウンロードの中止
     */
    fun cancel() {
        canceled = true
    }

    /**
     * ダウンロードの進行状況を取得
     * @return 進行状況（％）
     */
    fun getProgress(): Int {
        return if (!processing || totalByte <= 0) 100 else (100L * readByte / totalByte).toInt()
    }

    /**
     * 初期化処理
     * @param con HTTPコネクション
     */
    private fun init(con: HttpURLConnection?) {
        //切断処理
        close(con)
        //変数の初期化
        totalByte = 0
        readByte = 0
        canceled = false
    }

    /**
     * 接続処理
     * @return 接続成功：HTTPコネクション、接続失敗：null
     */
    private fun connect(): HttpURLConnection? {
        return try {
            val url = URL(urlString.softUrlEncode())
            val urlConnection =
                    if (urlString.startsWith("https", true)) {
                        url.openConnection() as HttpsURLConnection
                    } else {
                        url.openConnection() as HttpURLConnection
                    }.apply {
                        readTimeout = Settings.HTTP.READ_TIMEOUT_DOWNLOAD
                        connectTimeout = Settings.HTTP.CONNECT_TIMEOUT
                        useCaches = false
                    }
            if (urlConnection.responseCode != HttpURLConnection.HTTP_OK) {
                //切断処理
                close(urlConnection)
                return null
            }
            urlConnection
        } catch (e: IOException) {
            LogUtil.debug(e)
            null
        }
    }

    /**
     * ストリームからデータを読込（メモリに保持せずファイルに書き出す）
     * @param con HTTPコネクション
     * @param showProgress プログレス表示するかのフラグ
     * @return 読み込んだデータ
     */
    private fun readAll(con: HttpURLConnection, showProgress: Boolean): java.io.File? {
        //出力先のファイルを取得（毎回同じファイルに出力するため、一度削除する）
        val file = File(activity.filesDir.path + File.separator + Settings.HTTP.PATH_TEMPORARY)
        file.parentFile!!.mkdirs()
        file.delete()
        //ストリームからデータを読み込んでファイルに書き出し
        return BufferedOutputStream(FileOutputStream(file)).use { bos ->
            //プログレス表示
            var progress: ProgressSubScreen? = null
            if (showProgress) {
                val ss = activity.showSubScreen(null, R.layout.subscreen_progress, R.id.content_root, null, false)
                progress = ss.createdView as ProgressSubScreen
                ThreadUtil.sleep(100)
            }
            try {
                val ips = con.inputStream
                val buffer = ByteArray(Settings.HTTP.READ_BUFFER_SIZE)
                while (!canceled) {
                    //読み込むバイト数の取得
                    var len = buffer.size //読み込むバイト数
                    //読込後、実際に読み込んだバイト数を格納
                    len = ips.read(buffer, 0, len)
                    if (len < 0) {
                        break
                    }
                    //受信データを格納
                    bos.write(buffer, 0, len)
                    //プログレス更新
                    readByte += len
                    progress?.setProgress(getProgress())
                }
                bos.flush()
                file
            } catch (e: IOException) {
                LogUtil.debug(e)
                null
            } finally {
                //プログレス非表示
                if (progress != null) {
                    activity.hideSubScreen(progress.subScreen, false)
                }
            }
        }
    }

    /**
     * 切断処理
     * @param con HTTPコネクション
     */
    private fun close(con: HttpURLConnection?) {
        try {
            con?.disconnect()
        } catch (e: IOException) {
            LogUtil.debug(e)
        }
    }

    /**
     * 一連のダウンロード処理の終了
     * @param con HTTPコネクション
     * @param file ダウンロードしたファイルへの参照（ダウンロード失敗時はnullを指定）
     */
    private fun finish(con: HttpURLConnection?, file: java.io.File?) {
        //切断処理
        close(con)
        //コールバック
        if (file != null) {
            //完了通知
            onFinishListener.onComplete(file)
        } else {
            //中止通知
            onFinishListener.onRefuse(canceled)
        }
        processing = false //処理中フラグOFF
    }
}