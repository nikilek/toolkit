package net.nikilek.toolkit.utility

import android.content.Context
import net.nikilek.toolkit.constant.Settings
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Created by nikilek on 2019/12/14.
 * アプリ内設定値を内部ストレージ上で管理する汎用クラス
 */
object AppPreference {

    private val lockFile = ReentrantLock()

    /**
     * プリファレンス値の保存
     * @param context
     * @param key プリファレンスキー
     * @param value プリファレンス値
     * @return 成否
     */
    fun putString(context: Context, key: String, value: String): Boolean {
        return putValue(context, key, value)
    }

    /**
     * プリファレンス値の保存
     * @param context
     * @param key プリファレンスキー
     * @param value プリファレンス値
     * @return 成否
     */
    fun putInt(context: Context, key: String, value: Int): Boolean {
        return putValue(context, key, value)
    }

    /**
     * プリファレンス値の保存
     * @param context
     * @param key プリファレンスキー
     * @param value プリファレンス値
     * @return 成否
     */
    fun putLong(context: Context, key: String, value: Long): Boolean {
        return putValue(context, key, value)
    }

    /**
     * プリファレンス値の保存
     * @param context
     * @param key プリファレンスキー
     * @param value プリファレンス値
     * @return 成否
     */
    fun putFloat(context: Context, key: String, value: Float): Boolean {
        return putValue(context, key, value)
    }

    /**
     * プリファレンス値の保存
     * @param context
     * @param key プリファレンスキー
     * @param value プリファレンス値
     * @return 成否
     */
    fun putBoolean(context: Context, key: String, value: Boolean): Boolean {
        return putValue(context, key, value)
    }

    /**
     * プリファレンス値の読込
     * @param context
     * @param key プリファレンスキー
     * @param defaultValue 既定値
     * @return 値
     */
    fun getString(context: Context, key: String, defaultValue: String = ""): String {
        return getValue(context, key) ?: defaultValue
    }

    /**
     * プリファレンス値の読込
     * @param context
     * @param key プリファレンスキー
     * @param defaultValue 既定値
     * @return 値
     */
    fun getInt(context: Context, key: String, defaultValue: Int = 0): Int {
        return getValue(context, key)?.toIntOrNull() ?: defaultValue
    }

    /**
     * プリファレンス値の読込
     * @param context
     * @param key プリファレンスキー
     * @param defaultValue 既定値
     * @return 値
     */
    fun getLong(context: Context, key: String, defaultValue: Long = 0L): Long {
        return getValue(context, key)?.toLongOrNull() ?: defaultValue
    }

    /**
     * プリファレンス値の読込
     * @param context
     * @param key プリファレンスキー
     * @param defaultValue 既定値
     * @return 値
     */
    fun getFloat(context: Context, key: String, defaultValue: Float = 0f): Float {
        return getValue(context, key)?.toFloatOrNull() ?: defaultValue
    }

    /**
     * プリファレンス値の読込
     * @param context
     * @param key プリファレンスキー
     * @param defaultValue 既定値
     * @return 値
     */
    fun getBoolean(context: Context, key: String, defaultValue: Boolean = false): Boolean {
        return getValue(context, key)?.toBoolean() ?: defaultValue
    }

    /**
     * プリファレンス値の削除
     * @param context
     * @param key プリファレンスキー
     * @return 成否
     */
    fun remove(context: Context, key: String): Boolean {
        lockFile.withLock {
            return editor(context, key).getFile()?.delete() ?: false
        }
    }

    /**
     * ファイル操作用オブジェクトの生成
     * @param context
     * @param key プリファレンスキー
     * @return 生成したファイル操作用オブジェクト
     */
    private fun editor(context: Context, key: String): FileIO {
        return FileIO(context, Settings.File.PATH_PREFERENCE_FOLDER + key, true)
    }

    /**
     * プリファレンス値の保存
     * @param context
     * @param key プリファレンスキー
     * @param value プリファレンス値
     * @return 成否
     */
    private fun putValue(context: Context, key: String, value: Any): Boolean {
        lockFile.withLock {
            return editor(context, key).writeString(value.toString(), false, Settings.File.BUFFER_SIZE)
        }
    }

    /**
     * プリファレンス値の読込
     * @param context
     * @param key プリファレンスキー
     * @return 成否
     */
    private fun getValue(context: Context, key: String): String? {
        lockFile.withLock {
            return editor(context, key).readString(Settings.File.BUFFER_SIZE)
        }
    }
}