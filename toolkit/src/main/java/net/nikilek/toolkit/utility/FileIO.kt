package net.nikilek.toolkit.utility

import javax.crypto.SealedObject

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment

import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.extension.toPrimitive
import java.io.*
import java.nio.charset.Charset
import java.util.regex.Pattern
import java.util.zip.ZipEntry
import java.util.zip.ZipFile
import java.util.zip.ZipInputStream

/**
 * Created by nikilek on 2016/01/27.
 * ファイル入出力処理を行う
 * フラグ{toSystem}により参照ディレクトリを切り分け：システム領域 or ユーザ領域（ユーザ領域における内部／外部はOS定義による）
 */
open class FileIO
/**
 * コンストラクタ
 * @param context  コンテキスト
 * @param filePath ファイルパス
 * @param toSystem true：システム領域参照、false：ユーザ領域参照
 */
(
        private val context: Context//コンテキスト
        , private var filePath: String? //ファイルパス
        , private var toSystem: Boolean //システム領域フラグ
) {
    private var file: File? = null //ファイル参照

    /**
     * ファイルパスの更新
     * @param filePath 新しいファイルパス
     */
    fun setFilePath(filePath: String?) {
        this.filePath = filePath
        if (filePath != null) {
            //内部／外部の自動判定（フルパス指定の場合）
            if (filePath.startsWith(context.filesDir.path + File.separator)) {
                toSystem = true
            } else if (filePath.startsWith(context.getExternalFilesDir(null)?.toString() + File.separator)) {
                toSystem = false
            }
        }
    }

    /**
     * ファイル存在フラグ
     * @return 存在有無
     */
    open val isExistFile: Boolean
        get() = if (this.file != null || getFile() != null) { //ファイル参照取得済み、または取得できた場合
            //存在チェックの結果を返却
            this.file!!.exists()
        } else false

    /**
     * ファイル参照を取得する
     * @return ファイル参照
     */
    fun getFile(): File? {
        try {
            when {
                this.filePath == null -> this.file = null
                this.filePath!!.startsWith(getEnvironmentDirectory()) -> this.file = File(this.filePath!!)
                else -> this.file = File(getEnvironmentDirectory() + this.filePath)
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
            this.file = null
        }
//        if (this.toSystem) {
//            //システム領域から取得
//            //this.file = context.getFileStreamPath(filePath)
//            this.file = File(getEnvironmentDirectory() + this.filePath)
//        } else {
//            //ユーザ領域から取得
//            try {
//                this.file = File(getEnvironmentDirectory() + this.filePath)
//            } catch (e: Exception) {
//                LogUtil.debug(e)
//                this.file = null
//            }
//        }
        return this.file
    }

    /**
     * 領域情報を取得
     * @return 領域のパス
     */
    fun getEnvironmentDirectory(): String {
        return if (this.toSystem) {
            context.filesDir.path + File.separator
        } else {
            context.getExternalFilesDir(null)?.toString() + File.separator
        }
    }

    /**
     * 入力ストリームの取得
     * @return 入力ストリーム
     */
    private val inputStream: InputStream?
        @Throws(Exception::class)
        get() {
            if (!isExistFile) {
                //ファイルが存在しない場合はリターン
                return null
            }
            return if (this.toSystem) { //システム領域参照
                //context.openFileInput(this.filePath)
                FileInputStream(this.file!!)
            } else { //ユーザ領域参照
                FileInputStream(this.file!!)
            }
        }

    /**
     * 出力ストリームの取得
     * @param append 追記か否かのフラグ
     * @return 出力ストリーム
     */
    @Throws(Exception::class)
    private fun getOutputStream(append: Boolean): OutputStream? {
        //ファイルの参照を取得できない場合はリターン
        if (this.file == null && getFile() == null) {
            return null
        }
        //親ディレクトリが存在しない場合は作成
        val parent = this.file!!.parentFile
        if (parent != null && !parent.exists()) {
            parent.mkdirs()
        }
        return if (this.toSystem) { //システム領域参照
            //var mode = Context.MODE_PRIVATE
            //if (append) {
            //    //追記モードに変更
            //    mode = Context.MODE_APPEND
            //}
            //context.openFileOutput(this.filePath, mode)
            FileOutputStream(this.file!!, append)
        } else { //ユーザ領域参照
            FileOutputStream(this.file!!, append)
        }
    }

    /**
     * オブジェクトをデシリアライズして読込
     * @param secret 複合化するか否か
     * @param key 複合化キー（複合化する場合のみ使用）　※既定値：null
     * @return 取得したオブジェクト
     */
    @JvmOverloads
    fun readObj(secret: Boolean, key: String? = null): Any? {
        var ips: InputStream? = null
        var ois: ObjectInputStream? = null
        try {
            // ファイル入力ストリームを取得
            ips = inputStream
            if (ips == null) {
                return null
            }
            // オブジェクト入力ストリームの取得
            ois = ObjectInputStream(ips)
            return if (secret) {
                // 暗号化されていれば、復号化して返す
                val so = ois.readObject() as SealedObject
                val cipherList = CryptoUtil(context).createDecryptCipher(key ?: Settings.Key.AES)
                try {
                    so.getObject(cipherList[0])
                } catch (ex: Exception) {
                    //最新の情報で複合できない場合は、バックアップした情報で複合
                    so.getObject(cipherList[1]) //バックアップでの複合失敗は例外をスローする
                }
            } else {
                // 暗号化されていなければ、そのまま返す
                ois.readObject()
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
            return null
        } finally {
            // クローズ処理
            closeStream(ois)
            closeStream(ips)
        }
    }

    /**
     * オブジェクトをシリアライズして出力
     * @param obj 出力するオブジェクト
     * @param append 追記か否か
     * @param secret 暗号化有無
     * @param key 暗号化キー（暗号化する場合のみ使用）　※既定値：null
     * @return 成否
     */
    @JvmOverloads
    fun writeObj(obj: Any, append: Boolean, secret: Boolean, key: String? = null): Boolean {
        var ops: OutputStream? = null
        var oos: ObjectOutputStream? = null
        try {
            // ファイル出ストリームを取得
            ops = getOutputStream(append)
            if (ops == null) {
                return false
            }
            // オブジェクト出力ストリームの取得
            oos = ObjectOutputStream(ops)
            if (secret) {
                // 暗号化して出力
                val so = SealedObject(
                        obj as Serializable,
                        CryptoUtil(context).createEncryptCipher(key ?: Settings.Key.AES)
                )
                oos.writeObject(so)
            } else {
                // 暗号化せずに出力
                oos.writeObject(obj)
            }
            return true
        } catch (e: Exception) {
            LogUtil.debug(e)
            return false
        } finally {
            // クローズ処理
            closeStream(oos)
            closeStream(ops)
        }
    }

    /**
     * ファイルの内容を文字列で読み込む（改行無しファイルにも対応）
     * @param bufferSize バッファサイズ
     * @param charset エンコード文字列　※既定値：定義クラスの定数値に従う
     * @return 読み込んだ文字列
     */
    @JvmOverloads
    fun readString(bufferSize: Int, charset: String = Settings.File.ENCODE): String? {
        var ips: InputStream? = null
        try {
            //入力ストリームの取得
            ips = inputStream
            if (ips == null) {
                return null
            }
            //ファイルデータ読込
            val buf = ByteArray(bufferSize) //バッファ
            var readSize = 0 //ストリームから読み込んだバイト数
            val sb = StringBuilder() //読み込んだ文字列を格納
            while ({ readSize = ips.read(buf, 0, buf.size); readSize }() >= 0) {
                //読み込んだバイトデータを文字列として格納
                sb.append(String(buf, 0, readSize, Charset.forName(charset)))
            }
            return sb.toString()
        } catch (e: Exception) {
            LogUtil.debug(e)
            return null
        } finally {
            //クローズ処理
            closeStream(ips)
        }
    }

    /**
     * 文字列をファイルに書き込む
     * @param str 書き込む文字列
     * @param append 上書きか否か
     * @param bufferSize バッファサイズ
     * @param charset エンコード文字列　※既定値：定義クラスの定数値に従う
     * @return 成否
     */
    @JvmOverloads
    fun writeString(
            str: String, append: Boolean, bufferSize: Int,
            charset: String = Settings.File.ENCODE
    ): Boolean {
        var ops: OutputStream? = null
        var bos: BufferedOutputStream? = null
        try {
            //出力ストリームの取得
            ops = getOutputStream(append)
            //ストリームのバッファサイズを指定
            bos = BufferedOutputStream(ops!!, bufferSize)
            //書き込むデータをバイト配列で取得
            val buf = str.toByteArray(Charset.forName(charset))
            // 書込み処理
            bos.write(buf, 0, buf.size)
            return true
        } catch (e: Exception) {
            LogUtil.debug(e)
            return false
        } finally {
            //クローズ処理
            if (bos != null) {
                try {
                    bos.flush()
                } catch (e: Exception) {
                    LogUtil.debug(e)
                }
            }
            closeStream(bos)
            closeStream(ops)
        }
    }

    /**
     * テキストファイルの内容を文字列で取得（改行付きファイルを想定）
     * @param charset エンコード文字列　※既定値：定義クラスの定数値に従う
     * @param lineSplitter 行間を区切る文字　※既定値：改行コード
     * @return 取得した文字列
     */
    fun readText(charset: String, lineSplitter: String): String {
        if (inputStream == null) {
            return ""
        }
        return readText(inputStream!!, charset, lineSplitter)
    }

    /**
     * テキストファイルの内容をリスト（行×列）で取得（改行付きファイルを想定）
     * ※分割文字を指定した場合：ダブルクォーテーション内の分割文字は無視。分割後の先頭要素が空文字の場合はnullを返す
     * @param delimiterOfWord 分割文字（分割しない場合は空白を指定）
     * @param charset エンコード文字列　※既定値：定義クラスの定数値に従う
     * @return 取得した文字列のリスト
     */
    fun readTextAsList(delimiterOfWord: String, charset: String): List<List<String>> {
        if (inputStream == null) {
            return listOf()
        }
        return readTextAsList(inputStream!!, delimiterOfWord, charset)
    }

    /**
     * テキストファイルの内容をオブジェクトのリストで取得（改行付きファイルを想定）
     * ※分割文字を指定した場合：ダブルクォーテーション内の分割文字は無視。分割後の先頭要素が空文字の場合はnullを返す
     * ※テキストファイルの１行目が、オブジェクトのフィールド名と完全一致する文字列であること
     * ※テキストファイルの２行目以降を、行ごとに、オブジェクトのフィールド値として取得
     * ※対象の各フィールドは、プリミティブ型にのみ対応
     * @param retDataType 読み込むオブジェクトの型
     * @param delimiterOfWord 分割文字（分割しない場合は空白を指定）
     * @param charset エンコード文字列　※既定値：定義クラスの定数値に従う
     * @return 取得した文字列のリスト
     */
    fun <T> readTextAsObjectList(retDataType: Class<T>, delimiterOfWord: String, charset: String): List<T> {
        if (inputStream == null) {
            return listOf()
        }
        return readTextAsObjectList(retDataType, inputStream!!, delimiterOfWord, charset)
    }

    /**
     * ファイル読み込む
     * @return 読み込んだファイル内容
     */
    fun readFile(bufferSize: Int): ByteArray? {
        ByteArrayOutputStream().use { bos ->
            var ips: InputStream? = null
            return try {
                //入力ストリームの取得
                ips = inputStream
                //ファイルの読込
                val buffer = ByteArray(bufferSize)
                while (true) {
                    //読み込むバイト数の取得
                    var len = buffer.size //読み込むバイト数
                    //読込後、実際に読み込んだバイト数を格納
                    len = ips!!.read(buffer, 0, len)
                    if (len < 0) {
                        break
                    }
                    //読み込んだデータを格納
                    bos.write(buffer, 0, len)
                }
                bos.toByteArray()
            } catch (e: Exception) {
                LogUtil.debug(e)
                null
            } finally {
                //クローズ処理
                closeStream(ips)
            }
        }
    }

    /**
     * ファイルを書き出す
     * @param byteArray ファイル内容
     * @param bufferSize バッファサイズ
     * @return 成否
     */
    fun writeFile(byteArray: ByteArray, bufferSize: Int): Boolean {
        var ops: OutputStream? = null
        var bos: BufferedOutputStream? = null
        return try {
            //出力ストリームの取得
            ops = getOutputStream(false)
            //ストリームのバッファサイズを指定
            bos = BufferedOutputStream(ops!!, bufferSize)
            // 書込み処理
            bos.write(byteArray, 0, byteArray.size)
            true
        } catch (e: Exception) {
            LogUtil.debug(e)
            false
        } finally {
            //クローズ処理
            if (bos != null) {
                try {
                    bos.flush()
                } catch (e: Exception) {
                    LogUtil.debug(e)
                }
            }
            closeStream(bos)
            closeStream(ops)
        }
    }

    /**
     * zipファイルの展開
     * ※対象のzipファイルがあるディレクトリ配下に、zipファイル名のディレクトリを作成して展開
     * @param bufferSize バッファサイズ
     * @return 展開したファイルのリスト（ファイル名のリスト）
     */
    fun extractZip(bufferSize: Int): List<String> {
        val entryList = mutableListOf<String>() //戻り値：展開したファイルのリスト（ファイル名）
        if (!isExistFile) {
            return entryList
        }
        try {
            //展開先ディレクトリの作成
            val target = File(file!!.parent!! + File.separator + file!!.nameWithoutExtension)
            if (!target.exists()) {
                target.mkdirs()
            }
            //ZIPファイルの取得
            val zipFile = ZipFile(file, ZipFile.OPEN_READ)
            //ストリームの取得
            val zipInputStream = ZipInputStream(BufferedInputStream(file!!.inputStream()))
            //Zipファイル内の各ファイルについて処理
            var entry: ZipEntry? = zipInputStream.nextEntry
            while (entry != null) {
                //入力ストリームの取得
                val ips = zipFile.getInputStream(entry)
                //出力先の取得
                val outFile = File(target.path + File.separator + entry.name)
                //出力ストリームの取得
                val fos = FileOutputStream(outFile)
                //データコピー
                val buffer = ByteArray(bufferSize)
                var len = ips.read(buffer)
                while (len > 0) {
                    fos.write(buffer, 0, len)
                    len = ips.read(buffer)
                }
                //クローズ処理
                closeStream(fos)
                closeStream(ips)
                zipInputStream.closeEntry()
                //エントリリストの更新
                entryList.add(entry.name)
                //次のエントリを取得
                entry = zipInputStream.nextEntry
            }
            zipInputStream.close()
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
        return entryList
    }

    /**
     * 指定パスにコピー
     * @param distFilePath コピー先のファイルパス
     * @param distToSystem コピー先の領域（true：システム領域、false：ユーザ領域）
     * @return 成否
     */
    fun copyToFile(distFilePath: String, distToSystem: Boolean): Boolean {
        //コピー先のファイルを生成
        val distFile = FileIO(context, distFilePath, distToSystem).getFile() ?: return false
        distFile.parentFile!!.mkdirs() //親ディレクトリの生成
        distFile.delete() //既存ファイルを削除
        try {
            distFile.createNewFile() //空のファイルを生成
        } catch (e: IOException) {
            return false
        }
        //ファイルコピー
        val ips = inputStream ?: return false
        val ops = distFile.outputStream()
        ips.copyTo(ops)
        closeStream(ops)
        closeStream(ips)
        return true
    }

    /**
     * 指定パスからコピー
     * @param sourceFilePath コピー元のファイルパス
     * @param sourceToSystem コピー元の領域（true：システム領域、false：ユーザ領域）
     * @return 成否
     */
    fun copyFromFile(sourceFilePath: String, sourceToSystem: Boolean): Boolean {
        //既存ファイルを削除
        val distFile = getFile() ?: return false
        distFile.parentFile!!.mkdirs() //親ディレクトリの生成
        distFile.delete() //既存ファイルを削除
        try {
            distFile.createNewFile() //空のファイルを生成
        } catch (e: IOException) {
            return false
        }
        //ファイルコピー
        val ips = FileIO(context, sourceFilePath, sourceToSystem).inputStream ?: return false
        val ops = distFile.outputStream()
        ips.copyTo(ops)
        closeStream(ops)
        closeStream(ips)
        return true
    }

    /**
     * 指定ストリームにコピー
     * @param disOutputStream コピー先の出力ストリーム
     * @return 成否
     */
    fun copyToFile(disOutputStream: OutputStream): Boolean {
        var ret = false
        val ips = inputStream
        if (ips != null) {
            ips.copyTo(disOutputStream)
            ret = true
        }
        closeStream(disOutputStream)
        closeStream(ips)
        return ret
    }

    /**
     * 指定ストリームからコピー
     * @param sourceInputStream コピー元の入力ストリーム
     * @return 成否
     */
    fun copyFromFile(sourceInputStream: InputStream): Boolean {
        var ret = false
        val ops = getOutputStream(false)
        if (ops != null) {
            sourceInputStream.copyTo(ops)
            ret = true
        }
        closeStream(ops)
        closeStream(sourceInputStream)
        return ret
    }

    companion object {

        /**
         * BOMのトリム
         * @param str 対象の文字列
         */
        fun trimBOM(str: String?): String? {
            return str?.replaceFirst(Pattern.quote("\uFEFF").toRegex(), "")
        }

        /**
         * ストリームのクローズ（例外抑止）
         * @param stream 対象のストリーム
         */
        fun closeStream(stream: Closeable?) {
            try {
                stream?.close()
            } catch (e: Exception) {
                LogUtil.debug(e)
            }
        }

        /**
         * テキストファイルの内容を文字列で取得（改行付きファイルを想定）
         * @param ips 入力ストリーム（オープン状態であること）
         * @param charset エンコード文字列　※既定値：定義クラスの定数値に従う
         * @param lineSplitter 行間を区切る文字　※既定値：改行コード
         * @return 取得した文字列
         */
        fun readText(ips: InputStream, charset: String, lineSplitter: String): String {
            var ret = ""
            var bfr: BufferedReader? = null
            try {
                bfr = BufferedReader(InputStreamReader(ips, charset))
                //１行ごとに読込
                var line: String? = trimBOM(bfr.readLine()) //読み込んだ１行
                while (line != null) {
                    //読み込んだ文字列を格納
                    ret += line.plus(lineSplitter)
                    //次の１行を読込
                    line = bfr.readLine()
                }
                return ret
            } catch (e: Exception) {
                LogUtil.debug(e)
                return ret
            } finally {
                closeStream(bfr)
                closeStream(ips)
            }
        }

        /**
         * テキストファイルの内容をリスト（行×列）で取得（改行付きファイルを想定）
         * ※分割文字を指定した場合：ダブルクォーテーション内の分割文字は無視。分割後の先頭要素が空文字の場合はnullを返す
         * @param ips 入力ストリーム（オープン状態であること）
         * @param delimiterOfWord 分割文字（分割しない場合は空白を指定）
         * @param charset エンコード文字列　※既定値：定義クラスの定数値に従う
         * @return 取得した文字列のリスト
         */
        fun readTextAsList(ips: InputStream, delimiterOfWord: String, charset: String): List<List<String>> {
            var bfr: BufferedReader? = null
            try {
                bfr = BufferedReader(InputStreamReader(ips, charset))
                //１行ごとに読込
                val list = mutableListOf<List<String>>() //最終結果のリスト
                var line: String? = trimBOM(bfr.readLine()) //読み込んだ１行
                while (line != null) {
                    if (delimiterOfWord.isNotEmpty()) {
                        //行内を分割した結果のリストを取得
                        val tmp = split(line, delimiterOfWord)
                        if (tmp != null) {
                            //無効行でなければ最終結果のリストに追加
                            list.add(tmp)
                        }
                    } else {
                        //そのまま追加
                        list.add(listOf(line))
                    }
                    //次の１行を読込
                    line = bfr.readLine()
                }
                return list
            } catch (e: Exception) {
                LogUtil.debug(e)
                return listOf()
            } finally {
                closeStream(bfr)
                closeStream(ips)
            }
        }

        /**
         * テキストファイルの内容をオブジェクトのリストで取得（改行付きファイルを想定）
         * ※分割文字を指定した場合：ダブルクォーテーション内の分割文字は無視。分割後の先頭要素が空文字の場合はnullを返す
         * ※テキストファイルの１行目が、オブジェクトのフィールド名と完全一致する文字列であること
         * ※テキストファイルの２行目以降を、行ごとに、オブジェクトのフィールド値として取得
         * ※対象の各フィールドは、プリミティブ型にのみ対応
         * @param retDataType 読み込むオブジェクトの型
         * @param ips 入力ストリーム（オープン状態であること）
         * @param delimiterOfWord 分割文字（分割しない場合は空白を指定）
         * @param charset エンコード文字列　※既定値：定義クラスの定数値に従う
         * @return 取得した文字列のリスト
         */
        fun <T> readTextAsObjectList(
                retDataType: Class<T>,
                ips: InputStream,
                delimiterOfWord: String,
                charset: String
        ): List<T> {
            var bfr: BufferedReader? = null
            try {
                bfr = BufferedReader(InputStreamReader(ips, charset))
                //１行ごとに読込
                var first = true //１行目（ヘッダ）フラグ
                val fieldName = mutableListOf<String>() //フィールド名のリスト
                val list = mutableListOf<T>() //最終結果のリスト
                var line: String? = trimBOM(bfr.readLine()) //読み込んだ１行
                while (line != null) {
                    //行内を分割した結果のリストを取得
                    val tmp = split(line, delimiterOfWord)
                    //無効行でなければ、各フィールドについて処理
                    if (tmp != null) {
                        val obj = retDataType.newInstance() //※１行目読込時は不要だけどあまり気にしない
                        tmp.indices.forEach { col ->
                            val str = tmp[col]
                            if (first) {
                                //１行目（ヘッダ）の場合はフィールド名として取得
                                fieldName.add(str)
                            } else {
                                //２行目以降の場合は、フィールド値として取得
                                val field = retDataType.getDeclaredField(fieldName[col])
                                field.isAccessible = true
                                //値をフィールドの型に対応して変換
                                val value = str.toPrimitive(field.type)
                                //返却するオブジェクトに設定
                                field.set(obj, value)
                            }
                        }
                        if (first && fieldName.isNotEmpty()) {
                            first = false //１行目（ヘッダ）フラグOFF
                        } else {
                            list.add(obj) //読み込んだオブジェクトを格納
                        }
                    }
                    //次の１行を読込
                    line = bfr.readLine()
                }
                return list
            } catch (e: Exception) {
                LogUtil.debug(e)
                return listOf()
            } finally {
                closeStream(bfr)
                closeStream(ips)
            }
        }

        /**
         * 文字列を指定文字で分割　※ダブルクォーテーション内の分割文字は無視。分割後の先頭要素が空文字の場合はnullを返す
         * @param target 対象の文字列
         * @param delimiterOfWord 分割文字（ダブルクォーテーションを含む文字列（CSV,TSVを想定）の場合、分割文字は1文字であること）
         * @return 取得した文字列のリスト　※無効行の場合はnull
         */
        fun split(target: String, delimiterOfWord: String): List<String>? {
            //ダブルクォーテーションを含まない場合は単純に分割
            if (!target.contains("\"")) {
                val list = target.split(Pattern.quote(delimiterOfWord).toRegex())
                return if (list[0].isEmpty()) null else list
            }
            //１文字ずつ処理
            val list = mutableListOf<String>() //区切り後の文字列のリスト
            var tmp = "" //区切り途中の文字列を一時的に格納
            var close = true //ダブルクォーテーションの外側かのフラグ（囲みの開閉を表す）
            for (i in 0 until target.length) {
                //読み込んだ１文字について処理
                when (val s = target.substring(i, i + 1)) {
                    "\"" -> { //ダブルクォーテーションの場合
                        close = !close //開閉フラグ反転
                    }
                    delimiterOfWord -> { //区切り文字の場合
                        //ダブルクォーテーション内であれば区切り文字として扱わない
                        if (!close) {
                            tmp += s //途中経過に格納
                        }
                        //ダブルクォーテーション外であればこの時点で文字列を区切る
                        else {
                            list.add(tmp) //結果のリストに格納
                            tmp = "" //途中経過を初期化
                            //先頭要素が空文字の場合はnullを返す
                            if (list[0].isEmpty()) {
                                return null
                            }
                        }
                    }
                    else -> tmp += s //途中経過に格納
                }
                //最後の１文字の場合
                if (i == target.length - 1) {
                    //リストに未格納の文字列を格納
                    list.add(tmp)
                    //先頭要素が空文字の場合はnullを返す
                    if (list[0].isEmpty()) {
                        return null
                    }
                }
            }
            //結果のリストを返す
            return list
        }
    }
}