package net.nikilek.toolkit.utility

import android.annotation.SuppressLint
import net.nikilek.toolkit.extension.ems
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.regex.Pattern

/**
 * Created by nikilek on 2016/01/27.
 * 値のチェックを行う汎用クラス
 */
object CheckUtil {

    /**
     * 数値チェック
     * @param input 入力値
     * @return 数値か否か
     */
    fun isNumber(input: String?): Boolean {
        //NULLチェック
        if (input == null) {
            return false
        }
        //トリミング
        val str = input.trim { it <= ' ' }
        val chars = str.toCharArray()
        val len = str.length
        //サイズ０ならリターン
        if (len <= 0) {
            return false
        }
        //先頭が「+」か「-」であれば読み飛ばす
        var start = 0
        if (chars[0] == '-' || chars[0] == '+') {
            start = 1
        }
        //探索
        var hasPoint = false    //'.' の検出
        var hasNum = false      //数字の検出
        for (i in start until len) {
            if (chars[i] in '0'..'9') {
                hasNum = true
            } else if (chars[i] == '.') {
                if (hasPoint) {         //２つ以上 '.' がある
                    return false
                } else {
                    hasPoint = true    //１つ目の '.'
                }
            } else {
                return false    //数字以外
            }
        }
        //数値であれば
        return hasNum
    }

    /**
     * 数値チェック（範囲チェック付き）
     * @param input 入力値
     * @param min 最小値
     * @param max 最大値
     * @return 数値か否か
     */
    fun isNumber(input: String?, min: Double, max: Double): Boolean {
        if (!isNumber(input)) {
            return false
        }
        val num = input!!.toDouble()
        if (num < min || max < num) {
            return false
        }
        return true
    }

    /**
     * 数値チェック（範囲チェック付き）
     * @param input 入力値
     * @param min 最小値
     * @param max 最大値
     * @return 数値か否か
     */
    fun isNumber(input: String?, min: Long, max: Long): Boolean {
        return isNumber(input, min.toDouble(), max.toDouble())
    }

    /**
     * 数値チェック（範囲チェック付き）
     * @param input 入力値
     * @param min 最小値
     * @param max 最大値
     * @return 数値か否か
     */
    fun isNumber(input: String?, min: Int, max: Int): Boolean {
        return isNumber(input, min.toDouble(), max.toDouble())
    }

    /**
     * 日付のフォーマットチェック
     * @param input 入力値
     * @param format 期待する日付フォーマット
     * @return 期待する日付フォーマットか否か
     */
    @SuppressLint("SimpleDateFormat")
    fun isDate(input: String?, format: String): Boolean {
        if (input.isNullOrBlank()) {
            return false
        }
        // 指定フォーマットの日付の作成
        val sdf = SimpleDateFormat(format, DateFormatSymbols())
        // 文字列を日付変換
        return try {
            val d = sdf.parse(input)!!
            // 入力値と出力値が異なる場合は、フォーマットどおりの日付でない
            input == sdf.format(d)
        } catch (e: Exception) {
            LogUtil.debug(e)
            false
        }

    }

    /**
     * IPアドレスフォーマットチェック(***.***.***.***)
     * @param input 入力値
     * @return IPアドレス形式か否か
     */
    fun isIpAddress(input: String?): Boolean {
        if (input.isNullOrBlank()) {
            return false
        }
        val arr = input.split(Pattern.quote("."))
        if (arr.size != 4) {
            return false
        }
        arr.forEach {
            if (!isNumber(it, 0, 255)) {
                return false
            }
        }
        return true
    }

    /**
     * 文字列の表示幅（全角：2、半角：1の合計）を取得
     * @param str 対象の文字列
     * @return 表示幅（全角：2、半角：1の合計）
     */
    fun getEms(str: String): Int {
        return str.ems()
    }

    /**
     * 配列同士の比較
     * ・null同士は「同じ」
     * ・要素数が異なれば「同じでない」
     * ・全ての要素が同値であれば「同じ」
     * @param array1 比較対象の配列
     * @param array2 比較対象の配列
     * @return ２つの配列が同じであるか
     */
    fun equals(array1: IntArray?, array2: IntArray?): Boolean {
        if (array1 == null && array2 == null) {
            return true
        } else if (array1 !is IntArray || array2 !is IntArray) {
            return false
        }
        if (array1.size != array2.size) {
            return false
        }
        return array1.indices.none { array1[it] != array2[it] }
    }
}