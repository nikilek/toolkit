package net.nikilek.toolkit.utility

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonMappingException
import java.net.Inet4Address
import java.net.InetSocketAddress
import java.net.Socket
import java.nio.ByteBuffer
import java.nio.charset.Charset

import com.fasterxml.jackson.databind.ObjectMapper

import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.extension.toIntOrDefault
import java.io.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Created by nikilek on 2018/02/17.
 * クライアントサイド向け通信クラス（JsonデータでTCP通信を行う）
 *
 * 【同期通信の場合】
 * ・送信：sendをコール（内部処理：send > (未接続の場合 connect) > trySend > read > readAll > (自動クローズの場合 disConnect)）
 *
 * 【非同期通信の場合】
 * ・送信：sendAsyncをコール（内部処理：sendAsync > send > (未接続の場合 connect) > trySend > read > readAll > onReceive）
 * ・受信：listenをコール（内部処理：listen > (永続待機) > read > (未接続の場合 connect) > readAll > onReceive）※disConnectで待機終了
 *
 * @param targetPortNo 接続先ポート番号　0～65535以外の場合設定情報から取得　※既定値：-1
 * @param targetAddress 接続先アドレス（ドメイン名 or IPアドレス） nullの場合は設定情報から取得　※既定値：null
 */
class JsonConnector(private var targetPortNo: Int = -1, private var targetAddress: String? = null) {

    private var socket: Socket? = null //通信ソケット

    private val charset = Charset.forName(Settings.TCP.CHARSET)

    init {
        //接続先ポートの取得
        if (targetPortNo < 0 || 65535 < targetPortNo) {
            //引数で未指定の場合は既定のポートを使用
            targetPortNo = Settings.TCP.PORT_PRIMARY
        }
        //接続先アドレスの取得
        if (targetAddress == null) {
            //引数で未指定の場合は既定のアドレスを使用
            if (Settings.TCP.DOMAIN_SERVER.isNotBlank()) {
                //ドメイン名での定義がある場合は、優先して使用
                targetAddress = Settings.TCP.DOMAIN_SERVER
            } else if (CheckUtil.isIpAddress(Settings.TCP.IP_SERVER)) {
                //IPアドレスの定義がある場合は使用
                targetAddress = Settings.TCP.IP_SERVER
            }
        }
    }

    /*
     * ↓同期通信用↓
     */

    /**
     * 【同期通信用】オブジェクトのTCP送信処理
     * @param autoClose 自動クローズフラグ　trueの場合は送信後に切断を行う
     * @param sndData 送信データ
     * @param rcvDataType サーバから返却されるデータの型　※nullの場合は応答を待たない
     * @param lock 送信処理の直列化用ロックオブジェクト　※未指定可
     * @return サーバから受信したデータ
     */
    @JvmOverloads
    fun <T> send(autoClose: Boolean, sndData: Serializable, rcvDataType: Class<T>?, lock: ReentrantLock = ReentrantLock()): T? {
        lock.withLock {
            try {
                //送信バッファの取得
                val buf = ObjectMapper().writeValueAsBytes(sndData)
                //STXの取得
                val stx = if (Settings.TCP.STX != null) byteArrayOf(Settings.TCP.STX!!) else "".toByteArray(charset)
                //データ長の情報を取得
                @Suppress("ConstantConditionIf")
                val len =
                        (if (Settings.TCP.BYTE_LENGTH_OF_DECLARER_DATA_SIZE > 0) {
                            String.format("%1$0" + Settings.TCP.BYTE_LENGTH_OF_DECLARER_DATA_SIZE + "d", buf.size)
                        } else {
                            ""
                        }).toByteArray(charset)
                //送信データの先頭にSTXとデータ長の情報を付加
                val byteBuffer = ByteBuffer.allocate(stx.size + len.size + buf.size)
                byteBuffer.put(stx)
                byteBuffer.put(len)
                byteBuffer.put(buf)
                //送信処理
                return trySend(byteBuffer.array(), rcvDataType)
            } catch (e: JsonProcessingException) {
                LogUtil.debug(e)
                return null
            } finally {
                // 切断処理
                if (autoClose) {
                    disConnect()
                }
            }
        }
    }

    /*
     * ↓非同期通信用↓
     */

    private var receiveThread: Thread? = null //非同期受信待機スレッド

    /**
     * 非同期受信処理のコールバック用リスナ
     */
    interface OnReceiveListener {
        /**
         * 受信時処理
         * @param receiveData 受信データ
         */
        fun onReceive(receiveData: Any?)

        /**
         * コネクション異常時処理
         */
        fun onConnectionBroken()
    }

    fun <T> listen(rcvDataType: Class<T>, onReceiveListener: OnReceiveListener, autoReConnect: Boolean = true) {
        //重複起動チェック
        if (receiveThread != null) {
            return
        }
        //受信スレッドの起動
        receiveThread = Thread(Runnable {
            while (!Thread.currentThread().isInterrupted) {
                try {
                    //受信待機
                    val ret = read(rcvDataType)
                    //受信できた場合、呼び出し元に通知
                    if (ret != null) {
                        onReceiveListener.onReceive(ret)
                    }
                    //一定時間待機
                    if (!ThreadUtil.sleep(10)) break
                } catch (e: IOException) {
                    LogUtil.debug(e)
                    //呼び出し元に通知
                    onReceiveListener.onConnectionBroken()
                    //自動再接続チェック
                    if (autoReConnect) {
                        //再接続して処理継続
                        connect()
                        continue
                    }
                    break
                }
            }
            // 切断処理
            closeSocket()
        }).apply {
            isDaemon = true
            start()
        }
    }

    /**
     * 【非同期通信用】オブジェクトのTCP送信処理（※応答を待つ場合は多重定義関数を参照）
     * @param sndData 送信データ
     * @param lock 送信処理の直列化用ロックオブジェクト　※未指定可
     */
    @JvmOverloads
    fun sendAsync(sndData: Serializable, lock: ReentrantLock = ReentrantLock()) {
        sendAsync<Any>(sndData, null, null, lock)
    }

    /**
     * 【非同期通信用】オブジェクトのTCP送信処理
     * @param sndData 送信データ
     * @param rcvDataType サーバから返却されるデータの型　※nullの場合は応答を待たない
     * @param onReceiveListener 応答データ受信時のコールバック処理　※nullの場合は無効（rcvDataTypeがnullの場合も同様）
     * @param lock 送信処理の直列化用ロックオブジェクト　※未指定可
     */
    @JvmOverloads
    fun <T> sendAsync(sndData: Serializable, rcvDataType: Class<T>?, onReceiveListener: OnReceiveListener?, lock: ReentrantLock = ReentrantLock()) {
        //送信スレッドの起動
        Thread(Runnable {
            //送信実行
            val ret = send(false, sndData, rcvDataType, lock)
            //呼び出し元に応答を返す
            if (rcvDataType != null && onReceiveListener != null) {
                onReceiveListener.onReceive(ret)
            }
        }).apply {
            isDaemon = true
            start()
        }
    }

    /*
     * ↓同期／非同期で共通↓
     */

    /**
     * ソケットを生成して接続状態にする（既に接続済の場合は、一度切断して再接続）
     * @return 接続成否
     */
    fun connect(): Boolean {
        return try {
            //ソケットのクローズ
            closeSocket()
            //接続先ソケットアドレスの取得
            val targetSocketAddress = if (CheckUtil.isIpAddress(targetAddress)) {
                //接続先アドレスがIPアドレス形式で指定されている場合
                InetSocketAddress(targetAddress, targetPortNo)
            } else {
                //接続先アドレスがドメイン名で指定されている場合（または、nullの場合＝例外発生）
                InetSocketAddress(Inet4Address.getByName(targetAddress), targetPortNo)
            }
            //ソケットの接続
            socket = Socket()
            socket!!.connect(targetSocketAddress, Settings.TCP.CONNECT_TIMEOUT)
            //プロパティの設定
            socket!!.tcpNoDelay = false
            socket!!.soTimeout = Settings.TCP.READ_TIMEOUT
            true
        } catch (e: IOException) {
            LogUtil.debug(e)
            closeSocket()
            false
        }
    }

    /**
     * ソケットを切断状態にする
     * @return 切断成否
     */
    fun disConnect(): Boolean {
        receiveThread?.interrupt() //非同期受信待機終了
        receiveThread = null
        closeSocket()
        return true
    }

    /*
     * ↓内部処理用↓
     */

    /**
     * ソケットを確実にクローズする
     */
    private fun closeSocket() {
        while (socket != null) {
            try {
                socket?.close()
                socket = null
            } catch (e: IOException) {
                LogUtil.debug(e)
                continue
            }
        }
    }

    /**
     * 送信の試行
     * @param buf 送信バッファ
     * @param rcvDataType サーバから返却されるデータの型　※nullの場合は応答を待たない
     * @return 受信したデータ
     */
    private fun <T> trySend(buf: ByteArray, rcvDataType: Class<T>?): T? {
        for (i in 1..Settings.TCP.RETRY_COUNT) {
            if (i >= 2) {
                //2回目以降は再接続前に一定時間待機
                if (!ThreadUtil.sleep(Settings.TCP.RETRY_INTERVAL)) break
            }
            //ソケットの取得
            if (socket == null || socket!!.isOutputShutdown) {
                if (!connect()) continue
            }
            //送信実行
            try {
                val pw = PrintWriter(socket!!.getOutputStream())
                pw.print(String(buf, charset))
                pw.flush()
            } catch (e: IOException) {
                LogUtil.debug(e)
                closeSocket() //切断
                continue
            }
            //応答を待たない場合は、送信完了時点で処理終了
            if (rcvDataType == null) {
                //応答を待たない場合
                return null
            }
            //応答の受信
            val ret = read(rcvDataType)
            if (ret != null) {
                //応答を受信できた場合、受信データを返却
                return ret
            }
        }
        return null
    }

    /**
     * オブジェクトのTCP受信処理
     * @param rcvDataType 受信データの型
     * @return 受信したデータ
     */
    private fun <T> read(rcvDataType: Class<T>): T? {
        //タイムアウト判定用オブジェクトの生成
        val waitLimit = Settings.TCP.READ_TIMEOUT * Settings.TCP.RETRY_COUNT.toLong() //待機時間の上限
        var checkTimeout = CheckTimeout(System.currentTimeMillis(), waitLimit)
        // ソケットの取得
        if (socket == null || socket!!.isInputShutdown) {
            if (!connect()) throw IOException()
        }
        val ips = socket!!.getInputStream() //入力ストリーム
        //ストリームの待機（STX取得）
        while (!Thread.currentThread().isInterrupted) {
            //タイムアウト判定
            if (checkTimeout.timedOut()) {
                return null
            }
            if (ips.available() <= 0) {
                if (!ThreadUtil.sleep(10)) return null //一瞬待つ
                continue
            }
            //STXの指定がある場合はチェック
            if (Settings.TCP.STX != null) {
                val stx = ByteArray(1)
                ips.read(stx, 0, stx.size)
                //不正なSTXの場合
                if (stx[0] != Settings.TCP.STX) {
                    continue
                }
            }
            break
        }
        //タイムアウト判定オブジェクトの初期化
        checkTimeout = CheckTimeout(System.currentTimeMillis(), waitLimit)
        //ストリームの待機（データサイズ取得）
        while (ips.available() < Settings.TCP.BYTE_LENGTH_OF_DECLARER_DATA_SIZE
                && !Thread.currentThread().isInterrupted) {
            //タイムアウト判定
            if (checkTimeout.timedOut()) {
                return null
            }
        }
        //データサイズの取得
        var totalSize = -1
        @Suppress("ConstantConditionIf")
        if (Settings.TCP.BYTE_LENGTH_OF_DECLARER_DATA_SIZE > 0) {
            val size = ByteArray(Settings.TCP.BYTE_LENGTH_OF_DECLARER_DATA_SIZE)
            ips.read(size, 0, size.size)
            //データサイズを取得
            totalSize = String(size).toIntOrDefault(-1)
        }
        //データの読込
        val buf = if (totalSize < 0) {
            readAll(ips, checkTimeout)
        } else {
            readAll(ips, totalSize, checkTimeout)
        }
        return try {
            ObjectMapper().readValue(buf, rcvDataType)
        } catch (e: IOException) {
            LogUtil.debug(e)
            null
        } catch (e: JsonParseException) {
            LogUtil.debug(e)
            null
        } catch (e: JsonMappingException) {
            LogUtil.debug(e)
            null
        }
    }

    /**
     * ストリームからデータを受信
     * InputStream#availableが0でない限り読み込む
     * @param ips 入力ストリーム
     * @param checkTimeout タイムアウト判定クラス
     * @return 受信したデータ
     */
    private fun readAll(ips: InputStream, checkTimeout: CheckTimeout): ByteArray {
        return ByteArrayOutputStream().use { bos ->
            val buffer = ByteArray(Settings.TCP.READ_BUFFER_SIZE)
            while (ips.available() > 0) {
                //タイムアウト判定
                if (checkTimeout.timedOut()) {
                    return bos.toByteArray()
                }
                //読み込むバイト数の取得
                var len = buffer.size //読み込むバイト数
                //読込後、実際に読み込んだバイト数を格納
                len = ips.read(buffer, 0, len)
                if (len < 0) {
                    continue
                }
                //受信データを格納
                bos.write(buffer, 0, len)
            }
            bos.toByteArray()
        }
    }

    /**
     * ストリームからデータを受信
     * 指定サイズに達するまで読む込む
     * @param ips 入力ストリーム
     * @param totalSize 受信するデータの合計サイズ
     * @param checkTimeout タイムアウト判定クラス
     * @return 受信したデータ
     */
    private fun readAll(ips: InputStream, totalSize: Int, checkTimeout: CheckTimeout): ByteArray {
        return ByteArrayOutputStream().use { bos ->
            val buffer = ByteArray(Settings.TCP.READ_BUFFER_SIZE)
            var rest = totalSize //まだ読み込んでいないbyte数
            while (rest > 0) {
                //タイムアウト判定
                if (checkTimeout.timedOut()) {
                    return bos.toByteArray()
                }
                //読み込むバイト数の取得
                var len = buffer.size //読み込むバイト数
                if (len > rest) {
                    //残りのバイト数を超えている場合は調整する
                    len = rest
                }
                //読込後、実際に読み込んだバイト数を格納
                len = ips.read(buffer, 0, len)
                if (len < 0) {
                    continue
                }
                //受信データを格納
                bos.write(buffer, 0, len)
                //残りのbyte数を更新
                rest -= len
            }
            bos.toByteArray()
        }
    }

    /**
     * タイムアウト判定クラス
     * @param startTime 経過時間を算出するための基準時刻
     * @param waitLimit 経過時間の上限（タイムアウト判定の境界値）
     */
    internal class CheckTimeout(private val startTime: Long, private val waitLimit: Long) {
        /**
         * タイムアウト判定
         * @return タイムアウトしたか否か
         */
        fun timedOut(): Boolean {
            return System.currentTimeMillis() - startTime > waitLimit
        }
    }
}
