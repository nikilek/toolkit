package net.nikilek.toolkit.utility

import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.extension.softUrlEncode
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

/**
 * Created by nikilek on 2018/06/17.
 * クライアントサイド向け通信クラス（HTTPダウンロードを行う）
 * ※コンテンツ取得用。オンメモリーで処理する。
 *
 * @param urlString 接続先URL
 * @param onFinishListener 終了時処理のリスナ
 */
class HttpContentDownloader(private val urlString: String, private val onFinishListener: OnFinishListener) {

    @Volatile
    private var processing = false //処理中フラグ
    @Volatile
    private var canceled = false //キャンセル済フラグ

    private var totalByte = 0 //ダウンロード対象のファイルの総バイト数
    private var readByte = 0 //ダウンロード済のバイト数

    /**
     * ダウンロード終了時イベントのリスナ
     */
    interface OnFinishListener {
        /**
         * 完了
         * @param byteArray ダウンロードしたファイルの内容
         */
        fun onComplete(byteArray: ByteArray)

        /**
         * 中止
         * @param canceled 故意にキャンセルしたか否か
         */
        fun onRefuse(canceled: Boolean)
    }

    /**
     * ダウンロードの開始
     * @param retryCount リトライ回数（初回含む）　※既定値：1
     */
    @JvmOverloads
    fun start(retryCount: Int = 1) {
        Thread(Runnable {
            //排他チェック
            while (processing && !Thread.currentThread().isInterrupted) {
                ThreadUtil.sleep(100)
                continue
            }
            processing = true //処理中フラグON
            //指定回数試行
            var con: HttpURLConnection? = null
            var byteArray: ByteArray? = null
            for (i in 1..retryCount) {
                //初期化処理
                init(con)
                //指定URLに接続
                con = connect() ?: continue
                //データサイズの取得
                totalByte = con.contentLength
                if (totalByte == 0) {
                    continue
                }
                //ストリームからデータを読込
                byteArray = readAll(con)
                if (byteArray.size < totalByte) {
                    continue
                }
                //処理成功
                break
            }
            //処理完了
            finish(con, byteArray)
        }).start()
    }

    /**
     * ダウンロードの中止
     */
    fun cancel() {
        canceled = true
    }

    /**
     * ダウンロードの進行状況を取得
     * @return 進行状況（％）
     */
    fun getProgress(): Int {
        return if (!processing || totalByte <= 0) 100 else (100L * readByte / totalByte).toInt()
    }

    /**
     * 初期化処理
     * @param con HTTPコネクション
     */
    private fun init(con: HttpURLConnection?) {
        //切断
        close(con)
        //変数の初期化
        totalByte = 0
        readByte = 0
        canceled = false
    }

    /**
     * 接続処理
     * @return 接続成功：HTTPコネクション、接続失敗：null
     */
    private fun connect(): HttpURLConnection? {
        return try {
            val url = URL(urlString.softUrlEncode())
            val urlConnection =
                    if (urlString.startsWith("https", true)) {
                        url.openConnection() as HttpsURLConnection
                    } else {
                        url.openConnection() as HttpURLConnection
                    }.apply {
                        readTimeout = Settings.HTTP.READ_TIMEOUT_DOWNLOAD
                        connectTimeout = Settings.HTTP.CONNECT_TIMEOUT
                        useCaches = false
                    }
            if (urlConnection.responseCode != HttpURLConnection.HTTP_OK) {
                //切断処理
                close(urlConnection)
                return null
            }
            urlConnection
        } catch (e: IOException) {
            LogUtil.debug(e)
            null
        }
    }

    /**
     * ストリームからデータを読込
     * @param con HTTPコネクション
     * @return 読み込んだデータ
     */
    private fun readAll(con: HttpURLConnection): ByteArray {
        return ByteArrayOutputStream().use { bos ->
            try {
                val ips = con.inputStream
                val buffer = ByteArray(Settings.HTTP.READ_BUFFER_SIZE)
                while (!canceled) {
                    //読み込むバイト数の取得
                    var len = buffer.size //読み込むバイト数
                    //読込後、実際に読み込んだバイト数を格納
                    len = ips.read(buffer, 0, len)
                    if (len < 0) {
                        break
                    }
                    //受信データを格納
                    bos.write(buffer, 0, len)
                    //プログレス更新
                    readByte += len
                }
                bos.flush()
            } catch (e: IOException) {
                LogUtil.debug(e)
            }
            bos.toByteArray()
        }
    }

    /**
     * 切断処理
     * @param con HTTPコネクション
     */
    private fun close(con: HttpURLConnection?) {
        try {
            con?.disconnect()
        } catch (e: IOException) {
            LogUtil.debug(e)
        }
    }

    /**
     * 一連のダウンロード処理の終了
     * @param con HTTPコネクション
     * @param byteArray ダウンロードしたファイルの内容（ダウンロード失敗時はnullを指定）
     */
    private fun finish(con: HttpURLConnection?, byteArray: ByteArray?) {
        //切断処理
        close(con)
        //コールバック
        if (byteArray != null) {
            //完了通知
            onFinishListener.onComplete(byteArray)
        } else {
            //中止通知
            onFinishListener.onRefuse(canceled)
        }
        processing = false //処理中フラグOFF
    }
}