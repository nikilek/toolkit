package net.nikilek.toolkit.utility

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.extension.softUrlEncode
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.Serializable
import java.net.URL
import java.util.concurrent.locks.ReentrantLock
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.nio.charset.Charset
import javax.net.ssl.HttpsURLConnection
import kotlin.concurrent.withLock

/**
 * Created by nikilek on 2018/07/29.
 * クライアントサイド向け通信クラス（JsonデータでHTTP通信を行う）
 * ※送信の度に再接続（コネクションの再利用無し）
 */
object JsonConnectorHttp {

    /*
     * ↓同期通信↓
     */

    /**
     * 送信処理（同期通信）
     * @param urlString 接続先URL
     * @param requestData 送信データ
     * @param responseDataType 応答データの型
     * @param lock 処理の直列化用ロックオブジェクト　※未指定可
     * @param retryCount リトライ回数（初回含む）　※既定値：1
     */
    @Suppress("UNCHECKED_CAST")
    @JvmOverloads
    fun <T> send(urlString: String, requestData: Serializable, responseDataType: Class<T>?,
                 lock: ReentrantLock = ReentrantLock(), retryCount: Int = 1): T? {
        lock.withLock {
            var con: HttpURLConnection? = null
            var responseData: Any? = null
            for (i in 1..retryCount) {
                //切断処理
                close(con)
                if (i >= 2) {
                    //2回目以降は再接続前に一定時間待機
                    if (!ThreadUtil.sleep(100)) break
                }
                //指定URLに接続
                con = connect(urlString) ?: continue
                //ストリームにデータを書出
                if (!write(con, requestData)) {
                    continue
                }
                //ストリームからデータを読込
                responseData = read(con, responseDataType)
                if (responseData == null) {
                    continue
                }
                //処理成功
                break
            }
            //切断処理
            close(con)
            //処理完了
            return responseData as T?
        }
    }

    /*
     * ↓非同期通信↓
     */

    /**
     * 通信終了時イベントのリスナ
     */
    interface OnFinishListener {
        /**
         * 完了
         * @param responseData 結果データ
         */
        fun onComplete(responseData: Any?)
    }

    /**
     * 送信処理（非同期通信）
     * @param urlString 接続先URL
     * @param requestData 送信データ
     * @param responseDataType 応答データの型
     * @param onFinishListener 通信終了時イベントのリスナ
     * @param lock 処理の直列化用ロックオブジェクト　※未指定可
     * @param retryCount リトライ回数（初回含む）　※既定値：1
     */
    fun <T> sendAsync(urlString: String, requestData: Serializable, responseDataType: Class<T>?,
                      onFinishListener: JsonConnectorHttp.OnFinishListener,
                      lock: ReentrantLock = ReentrantLock(), retryCount: Int = 1) {
        Thread(Runnable {
            lock.withLock {
                val responseData = send(urlString, requestData, responseDataType, lock, retryCount)
                onFinishListener.onComplete(responseData)
            }
        }).start()
    }

    /*
     * ↓共通処理↓
     */

    /**
     * 接続処理
     * @param urlString 接続先URL
     * @return HTTPコネクション（接続失敗時はnull）
     */
    private fun connect(urlString: String): HttpURLConnection? {
        return try {
            val url = URL(urlString.softUrlEncode())
            if (urlString.startsWith("https", true)) {
                url.openConnection() as HttpsURLConnection
            } else {
                url.openConnection() as HttpURLConnection
            }.apply {
                readTimeout = Settings.HTTP.READ_TIMEOUT
                connectTimeout = Settings.HTTP.CONNECT_TIMEOUT
                doOutput = true
                doInput = true
                useCaches = false
            }
        } catch (e: IOException) {
            LogUtil.debug(e)
            null
        }
    }

    /**
     * ストリームにデータを書出
     * @param con HTTPコネクション
     * @param requestData 送信データ
     * @return 成否（true：レスポンスコード200、false：それ以外）
     */
    private fun write(con: HttpURLConnection, requestData: Serializable): Boolean {
        return try {
            //送信データの取得
            val data = ObjectMapper().writeValueAsString(requestData)
            val length = data.toByteArray(Charset.forName("UTF-8")).size //UTF-8固定（JSON標準）
            //プロパティの設定
            con.requestMethod = "POST"
            con.setRequestProperty("Content-Type", "application/JSON; charset=UTF-8") //UTF-8固定（JSON標準）
            con.setRequestProperty("Content-Length", length.toString())
            //送信処理
            val osw = OutputStreamWriter(con.outputStream)
            osw.write(data)
            osw.flush()
            con.responseCode == HttpsURLConnection.HTTP_OK
        } catch (e: IOException) {
            LogUtil.debug(e)
            false
        }
    }

    /**
     * ストリームからデータを読込
     * @param con HTTPコネクション
     * @param responseDataType 応答データの型
     * @return 読み込んだデータ
     */
    private fun <T> read(con: HttpURLConnection, responseDataType: Class<T>?): T? {
        val byteArray = readAll(con)
        return try {
            ObjectMapper().readValue(byteArray, responseDataType)
        } catch (e: IOException) {
            LogUtil.debug(e)
            null
        } catch (e: JsonParseException) {
            LogUtil.debug(e)
            null
        } catch (e: JsonMappingException) {
            LogUtil.debug(e)
            null
        }
    }

    /**
     * ストリームからデータを読込
     * @param con HTTPコネクション
     * @return 読み込んだデータ
     */
    private fun readAll(con: HttpURLConnection): ByteArray {
        return ByteArrayOutputStream().use { bos ->
            try {
                val ips = con.inputStream
                val buffer = ByteArray(Settings.HTTP.READ_BUFFER_SIZE)
                while (true) {
                    //読み込むバイト数の取得
                    var len = buffer.size //読み込むバイト数
                    //読込後、実際に読み込んだバイト数を格納
                    len = ips.read(buffer, 0, len)
                    if (len < 0) {
                        break
                    }
                    //受信データを格納
                    bos.write(buffer, 0, len)
                }
            } catch (e: IOException) {
                LogUtil.debug(e)
            }
            bos.toByteArray()
        }
    }

    /**
     * 切断処理
     * @param con HTTPコネクション
     */
    private fun close(con: HttpURLConnection?) {
        try {
            con?.disconnect()
        } catch (e: IOException) {
            LogUtil.debug(e)
        }
    }
}