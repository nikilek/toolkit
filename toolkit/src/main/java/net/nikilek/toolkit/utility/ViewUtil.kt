package net.nikilek.toolkit.utility

import android.app.Activity
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import kotlin.math.roundToInt

/**
 * Created by nikilek on 2017/10/16.
 * 画面表示関連の処理を行う汎用クラス
 */
class ViewUtil {

    private var activity: Activity //呼び出し元のActivity
    private var res: Resources //activity.resources

    /**
     * コンストラクタ（パッケージ依存）
     * @param activity 呼び出し元のActivity
     */
    constructor(activity: Activity) {
        this.activity = activity
        this.res = activity.resources
    }

    /* 互換性維持 */
    @Suppress("UNUSED_PARAMETER")
    @Deprecated(message = "Use ViewUtil(activity)",
            replaceWith = ReplaceWith("ViewUtil(activity)"))
    constructor(activity: Activity, drawable: Any) {
        this.activity = activity
        this.res = activity.resources
    }

    /**
     * drawableリソースを取得
     * @param resId 対象のリソースID
     * @param reqWidth 取得するdrawableの幅（リサイズ用）
     * @param reqHeight 取得するdrawableの高さ（リサイズ用）
     * @return 取得したdrawableリソース
     */
    fun getDrawable(resId: Int, reqWidth: Int, reqHeight: Int): Drawable {
        return getDrawable(res, resId, reqWidth, reqHeight)
    }

    /**
     * drawableリソースを取得
     * @param res activity.resources
     * @param resId 対象のリソースID
     * @param reqWidth 取得するdrawableの幅（リサイズ用）
     * @param reqHeight 取得するdrawableの高さ（リサイズ用）
     * @return 取得したdrawableリソース
     */
    fun getDrawable(res: Resources?, resId: Int, reqWidth: Int, reqHeight: Int): Drawable {
        return BitmapDrawable(res, getBitmap(res, resId, reqWidth, reqHeight))
    }

    /**
     * Bitmapオブジェクトを取得
     * @param resId 対象のリソースID
     * @return Bitmapオブジェクト
     */
    fun getBitmap(resId: Int): Bitmap? {
        return getBitmap(res, resId)
    }

    /**
     * Bitmapオブジェクトを取得
     * @param res activity.resources
     * @param resId 対象のリソースID
     * @return Bitmapオブジェクト
     */
    fun getBitmap(res: Resources?, resId: Int): Bitmap? {
        //Bitmapを生成して返す
        return BitmapFactory.decodeResource(res, resId)
    }

    /**
     * Bitmapオブジェクトを取得（リサイズ機能付き）
     * @param resId 対象のリソースID
     * @param reqWidth 取得するBitmapの幅（リサイズ用）
     * @param reqHeight 取得するBitmapの高さ（リサイズ用）
     * @return Bitmapオブジェクト
     */
    fun getBitmap(resId: Int, reqWidth: Int, reqHeight: Int): Bitmap? {
        return getBitmap(res, resId, reqWidth, reqHeight)
    }

    /**
     * Bitmapオブジェクトを取得（リサイズ機能付き）
     * @param res activity.resources
     * @param resId 対象のリソースID
     * @param reqWidth 取得するBitmapの幅（リサイズ用）
     * @param reqHeight 取得するBitmapの高さ（リサイズ用）
     * @return Bitmapオブジェクト
     */
    fun getBitmap(res: Resources?, resId: Int, reqWidth: Int, reqHeight: Int): Bitmap? {
        try {
            //サイズ取得用に画像をデコード
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true //メモリ上には展開しない
            BitmapFactory.decodeResource(res, resId, options)
            //指定サイズにするには何倍にすればいいかを算出
            val rawHeight = options.outHeight //元画像の幅
            val rawWidth = options.outWidth //元画像の高さ
            var inSampleSize = 1.0f //倍率
            if (reqHeight > 0 && rawWidth > rawHeight) {
                //高さで合わせる
                inSampleSize = rawHeight.toFloat() / reqHeight.toFloat()
            } else if (reqWidth > 0 && rawHeight > reqHeight) {
                //幅で合わせる
                inSampleSize = rawWidth.toFloat() / reqWidth.toFloat()
            } else if (reqHeight in 1 until rawHeight) {
                //高さで合わせる
                inSampleSize = rawHeight.toFloat() / reqHeight.toFloat()
            } else if (reqWidth in 1 until rawWidth) {
                //幅で合わせる
                inSampleSize = rawWidth.toFloat() / reqWidth.toFloat()
            }
            //算出した倍率を用いて画像をメモリ上にデコード
            options.inJustDecodeBounds = false //メモリ上に展開する
            options.inSampleSize = inSampleSize.roundToInt()
            //Bitmapを生成して返す
            return BitmapFactory.decodeResource(res, resId, options)
        } catch (e: Exception) {
            return null
        }

    }

    /**
     * ファイル名を元にdrawableリソースを取得
     * @param fileName 対象のファイル名
     * @param reqWidth 取得するdrawableの幅（リサイズ用）
     * @param reqHeight 取得するdrawableの高さ（リサイズ用）
     * @return 取得したdrawableリソース
     */
    fun getDrawable(fileName: String, reqWidth: Int, reqHeight: Int): Drawable {
        return getDrawable(res, fileName, reqWidth, reqHeight)
    }

    /**
     * ファイル名を元にdrawableリソースを取得
     * @param res activity.resources
     * @param fileName 対象のファイル名
     * @param reqWidth 取得するdrawableの幅（リサイズ用）
     * @param reqHeight 取得するdrawableの高さ（リサイズ用）
     * @return 取得したdrawableリソース
     */
    fun getDrawable(res: Resources?, fileName: String, reqWidth: Int, reqHeight: Int): Drawable {
        return BitmapDrawable(res, getBitmap(res, fileName, reqWidth, reqHeight))
    }

    /**
     * ファイル名を元にBitmapオブジェクトを取得
     * @param fileName 対象のファイル名
     * @return Bitmapオブジェクト
     */
    fun getBitmap(fileName: String): Bitmap? {
        return getBitmap(res, fileName)
    }

    /**
     * ファイル名を元にBitmapオブジェクトを取得
     * @param res activity.resources
     * @param fileName 対象のファイル名
     * @return Bitmapオブジェクト
     */
    fun getBitmap(res: Resources?, fileName: String): Bitmap? {
        //リソースIDの取得
        val resId = this.getImgResId(fileName)
        //Bitmapを生成して返す
        return BitmapFactory.decodeResource(res, resId)
    }

    /**
     * ファイル名を元にBitmapオブジェクトを取得（リサイズ機能付き）
     * @param fileName 対象のファイル名
     * @param reqWidth 取得するBitmapの幅（リサイズ用）
     * @param reqHeight 取得するBitmapの高さ（リサイズ用）
     * @return Bitmapオブジェクト
     */
    fun getBitmap(fileName: String, reqWidth: Int, reqHeight: Int): Bitmap? {
        return getBitmap(res, fileName, reqWidth, reqHeight)
    }

    /**
     * ファイル名を元にBitmapオブジェクトを取得（リサイズ機能付き）
     * @param res activity.resources
     * @param fileName 対象のファイル名
     * @param reqWidth 取得するBitmapの幅（リサイズ用）
     * @param reqHeight 取得するBitmapの高さ（リサイズ用）
     * @return Bitmapオブジェクト
     */
    fun getBitmap(res: Resources?, fileName: String, reqWidth: Int, reqHeight: Int): Bitmap? {
        try {
            //リソースIDの取得
            val resId = getImgResId(fileName)
            //Bitmapを生成して返す
            return getBitmap(res, resId, reqWidth, reqHeight)
        } catch (e: Exception) {
            return null
        }
    }

    /**
     * ファイル名を元にリソースIDを取得
     * @param fileName 対象のファイル名
     * @return リソースID
     */
    fun getImgResId(fileName: String): Int {
        return getImgResId(res, fileName)
    }

    /**
     * ファイル名を元にリソースIDを取得
     * @param res activity.resources
     * @param fileName 対象のファイル名
     * @return リソースID
     */
    fun getImgResId(res: Resources, fileName: String): Int {
        return try {
            var tmpFileName = fileName
            if (tmpFileName.isBlank()) {
                tmpFileName = "blank"
            }
            res.getIdentifier(tmpFileName, "drawable", activity.packageName)
        } catch (e: Exception) {
            0
        }
    }
}