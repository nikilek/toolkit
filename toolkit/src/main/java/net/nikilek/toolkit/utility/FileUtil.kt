package net.nikilek.toolkit.utility

import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import net.nikilek.toolkit.constant.Settings

import java.io.File
import java.io.InputStream
import java.io.OutputStream

/**
 * Created by nikilek on 2016/01/27.
 * ファイルごとの入出力処理を行う
 * フラグ{toSystem}により参照ディレクトリを切り分け：システム領域 or ユーザ領域（ユーザ領域における内部／外部はOS定義による）
 */
class FileUtil
/**
 * コンストラクタ
 * @param context コンテキスト
 * @param filePath ファイルパス
 * @param toSystem true：システム領域参照、false：ユーザ領域参照
 */
(context: Context, filePath: String, toSystem: Boolean) : FileIO(context, filePath, toSystem) {

    /**
     * ファイル存在チェック
     * @return 存在有無
     */
    override val isExistFile: Boolean
        get() = super.isExistFile

    /**
     * ファイルの最終更新日時を取得
     * @return 最終更新日時（long値）
     */
    fun checkLastModified(): Long {
        //ファイル情報取得
        val f = super.getFile() ?: return -1
        //最終更新日時を取得して返す
        return f.lastModified()
    }

    /*
     * コピー／移動／リネーム
     */

    /**
     * 指定パスにコピー
     * @param distFilePath コピー先のファイルパス
     * @param distToSystem コピー先の領域（true：システム領域、false：ユーザ領域）
     * @return 成否
     */
    fun copyTo(distFilePath: String, distToSystem: Boolean): Boolean {
        return super.copyToFile(distFilePath, distToSystem)
    }

    /**
     * 指定パスからコピー
     * @param sourceFilePath コピー元のファイルパス
     * @param sourceToSystem コピー元の領域（true：システム領域、false：ユーザ領域）
     * @return 成否
     */
    fun copyFrom(sourceFilePath: String, sourceToSystem: Boolean): Boolean {
        return super.copyFromFile(sourceFilePath, sourceToSystem)
    }

    /**
     * 指定URIにコピー
     * @param context コンテキスト
     * @param disUri コピー先のURI
     * @return 成否
     */
    fun copyTo(context: Context, disUri: Uri): Boolean {
        val distOutputStream = context.contentResolver.openOutputStream(disUri) ?: return false
        return copyTo(distOutputStream)
    }

    /**
     * 指定URIからコピー
     * @param context コンテキスト
     * @param sourceUri コピー元のURI
     * @return 成否
     */
    fun copyFrom(context: Context, sourceUri: Uri): Boolean {
        val sourceInputStream = context.contentResolver.openInputStream(sourceUri) ?: return false
        return copyFrom(sourceInputStream)
    }

    /**
     * 指定ストリームにコピー
     * @param disOutputStream コピー先の出力ストリーム
     * @return 成否
     */
    fun copyTo(disOutputStream: OutputStream): Boolean {
        return super.copyToFile(disOutputStream)
    }

    /**
     * 指定ストリームからコピー
     * @param sourceInputStream コピー元の入力ストリーム
     * @return 成否
     */
    fun copyFrom(sourceInputStream: InputStream): Boolean {
        return super.copyFromFile(sourceInputStream)
    }

    /**
     * ファイル名変更
     * @param newName 新しいファイル名（ディレクト名含まず）
     * @param autoAddExtension 新しいファイル名の末尾に、自動でリネーム前と同じ拡張子を付加するかのフラグ　※既定値：false
     * @return 成否
     */
    @JvmOverloads
    fun rename(newName: String, autoAddExtension: Boolean = false): Boolean {
        if (!isExistFile) {
            return false
        }
        return try {
            val target: File = super.getFile()!! //リネーム対象のファイル
            val directory: String = target.parent!! + File.separator //現在のディレクトリ
            val extension: String = if (autoAddExtension) ("." + target.extension) else "" //付加する拡張子
            val newPath: String = directory + newName + extension //リネーム後のファイルパス
            return rename(newPath, autoAddEnvironmentDirectory = false, autoAddExtension = false)
        } catch (e: Exception) {
            LogUtil.debug(e)
            false
        }
    }

    /**
     * ファイル名変更
     * @param newPath 新しいファイルパス（ディレクト名を含む場合は、autoAddEnvironmentDirectoryの値をfalseにすること）
     * @param autoAddEnvironmentDirectory 領域パスを自動で付加するかのフラグ
     * @param autoAddExtension 新しいファイル名の末尾に、自動でリネーム前と同じ拡張子を付加するかのフラグ
     * @return 成否
     */
    fun rename(newPath: String, autoAddEnvironmentDirectory: Boolean, autoAddExtension: Boolean): Boolean {
        if (!isExistFile) {
            return false
        }
        return try {
            val target: File = super.getFile()!! //リネーム対象のファイル
            val directory: String = if (autoAddEnvironmentDirectory) getEnvironmentDirectory() else "" //現在のディレクトリ
            val extension: String = if (autoAddExtension) ("." + target.extension) else "" //付加する拡張子
            val newFullPath = directory + newPath + extension
            val newFile = File(newFullPath)
            if (!newFile.parentFile!!.exists()) {
                newFile.parentFile!!.mkdirs()
            }
            val result = target.renameTo(newFile)
            if (result) {
                //保持しているファイル情報を更新
                setFilePath(newFullPath)
                getFile()
            }
            result
        } catch (e: Exception) {
            LogUtil.debug(e)
            false
        }
    }

    /*
     * ファイル入出力
     */

    /**
     * オブジェクト読込
     * @param secret 暗号化されたオブジェクトか（複合化が必要か）のフラグ　※既定値：true
     * @param key 複合化キー（複合化する場合のみ使用）　※既定値：null
     * @return 読み込んだオブジェクト
     */
    @JvmOverloads
    fun <T> load(secret: Boolean = true, key: String? = null): T? {
        //読込処理
        val retObj = readObj(secret, key)
        //結果のオブジェクトを所定の型に変換してリターン
        return try {
            @Suppress("UNCHECKED_CAST")
            retObj as T?
        } catch (e: Exception) {
            LogUtil.debug(e)
            null
        }
    }

    /**
     * オブジェクト書出
     * @param data 書き込むセーブデータ
     * @param secret 暗号化するかのフラグ　※既定値：true
     * @param key 暗号化キー（暗号化する場合のみ使用）　※既定値：null
     * @return 成否
     */
    @JvmOverloads
    fun <T> save(data: T?, secret: Boolean = true, key: String? = null): Boolean {
        data ?: return false
        //書込み処理を行い、結果をリターン
        return writeObj(data, false, secret, key)
    }

    /**
     * JSON読込
     * @param retDataType 読み込むオブジェクトの型
     * @param charset エンコード文字列　※既定値：UTF-8
     * @return 読み込んだオブジェクト
     */
    @JvmOverloads
    fun <T> readJSON(retDataType: Class<T>, charset: String = "UTF-8"): T? {
        //読込処理
        val json = readString(Settings.File.BUFFER_SIZE, charset)
        //結果のオブジェクトを所定の型に変換してリターン
        return try {
            return ObjectMapper().readValue(json, retDataType)
        } catch (e: Exception) {
            LogUtil.debug(e)
            null
        }
    }

    /**
     * JSON書出
     * @param data 書き込むオブジェクト
     * @param charset エンコード文字列　※既定値：UTF-8
     * @return 成否
     */
    @JvmOverloads
    fun <T> writeJSON(data: T?, charset: String = "UTF-8"): Boolean {
        data ?: return false
        //JSON文字列の取得
        val json: String
        try {
            json = ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).writeValueAsString(data)
        } catch (e: Exception) {
            LogUtil.debug(e)
            return false
        }
        //書込み処理を行い、結果をリターン
        return writeString(json, false, Settings.File.BUFFER_SIZE, charset)
    }

    /*
     * テキスト操作
     */

    /**
     * テキストファイルの内容を文字列で取得（改行付きファイルを想定）
     * @param lineSplitter 行間を区切る文字　※既定値：改行コード
     * @return 取得した文字列
     */
    fun readText(lineSplitter: String = Settings.String.SEPARATOR): String {
        return readText(Settings.File.ENCODE, lineSplitter)
    }

    /**
     * テキストファイルの内容をリスト（行×列）で取得（改行付きファイルを想定）
     * ※分割文字を指定した場合：ダブルクォーテーション内の分割文字は無視。分割後の先頭要素が空文字の場合はnullを返す
     * @param delimiterOfWord 分割文字（分割しない場合は空白を指定）
     * @return 取得した文字列のリスト
     */
    fun readTextAsList(delimiterOfWord: String): List<List<String>> {
        return readTextAsList(delimiterOfWord, Settings.File.ENCODE)
    }

    /**
     * テキストファイルの内容をオブジェクトのリストで取得（改行付きファイルを想定）
     * ※分割文字を指定した場合：ダブルクォーテーション内の分割文字は無視。分割後の先頭要素が空文字の場合はnullを返す
     * ※テキストファイルの１行目が、オブジェクトのフィールド名と完全一致する文字列であること
     * ※テキストファイルの２行目以降を、行ごとに、オブジェクトのフィールド値として取得
     * ※対象の各フィールドは、プリミティブ型にのみ対応
     * @param retDataType 読み込むオブジェクトの型
     * @param delimiterOfWord 分割文字（分割しない場合は空白を指定）
     * @return 取得した文字列のリスト
     */
    fun <T> readTextAsObjectList(retDataType: Class<T>, delimiterOfWord: String): List<T> {
        return readTextAsObjectList(retDataType, delimiterOfWord, Settings.File.ENCODE)
    }

    /**
     * CSVファイルの内容をリスト（行×列）で取得（改行付きファイルを想定）
     * ※分割文字を指定した場合：ダブルクォーテーション内の分割文字は無視。分割後の先頭要素が空文字の場合はnullを返す
     * @return 取得した文字列のリスト
     */
    fun readCsvAsList(): List<List<String>> {
        return readTextAsList(",", Settings.File.ENCODE)
    }

    /**
     * CSVファイルの内容をオブジェクトのリストで取得（改行付きファイルを想定）
     * ※分割文字を指定した場合：ダブルクォーテーション内の分割文字は無視。分割後の先頭要素が空文字の場合はnullを返す
     * ※テキストファイルの１行目が、オブジェクトのフィールド名と完全一致する文字列であること
     * ※テキストファイルの２行目以降を、行ごとに、オブジェクトのフィールド値として取得
     * ※対象の各フィールドは、プリミティブ型にのみ対応
     * @param retDataType 読み込むオブジェクトの型
     * @return 取得した文字列のリスト
     */
    fun <T> readCsvAsObjectList(retDataType: Class<T>): List<T> {
        return readTextAsObjectList(retDataType, ",", Settings.File.ENCODE)
    }

    /**
     * TSVファイルの内容をリスト（行×列）で取得（改行付きファイルを想定）
     * ※分割文字を指定した場合：ダブルクォーテーション内の分割文字は無視。分割後の先頭要素が空文字の場合はnullを返す
     * @return 取得した文字列のリスト
     */
    fun readTsvAsList(): List<List<String>> {
        return readTextAsList("\t", Settings.File.ENCODE)
    }

    /**
     * TSVファイルの内容をオブジェクトのリストで取得（改行付きファイルを想定）
     * ※分割文字を指定した場合：ダブルクォーテーション内の分割文字は無視。分割後の先頭要素が空文字の場合はnullを返す
     * ※テキストファイルの１行目が、オブジェクトのフィールド名と完全一致する文字列であること
     * ※テキストファイルの２行目以降を、行ごとに、オブジェクトのフィールド値として取得
     * ※対象の各フィールドは、プリミティブ型にのみ対応
     * @param retDataType 読み込むオブジェクトの型
     * @return 取得した文字列のリスト
     */
    fun <T> readTsvAsObjectList(retDataType: Class<T>): List<T> {
        return readTextAsObjectList(retDataType, "\t", Settings.File.ENCODE)
    }

    /*
     * ZIP操作
     */

    /**
     * zipファイルの展開
     * ※対象のzipファイルがあるディレクトリ配下に、zipファイル名のディレクトリを作成して展開
     * @return 展開したファイルのリスト（ファイル名のリスト）
     */
    fun extractZip(): List<String> {
        return extractZip(Settings.File.BUFFER_SIZE)
    }
}