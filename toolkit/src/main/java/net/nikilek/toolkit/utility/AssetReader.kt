package net.nikilek.toolkit.utility

import android.content.Context
import net.nikilek.toolkit.constant.Settings
import java.io.InputStream

/**
 * Created by nikilek on 2016/01/27.
 * Assetファイルの読込を行う汎用クラス
 */
open class AssetReader(private val context: Context) {

    /**
     * テキストファイルの内容を文字列で取得
     * @param filePath アプリケーションルートからの相対パス
     * @param charset エンコード文字列
     * @param lineSplitter 行間を区切る文字　※既定値：改行コード
     * @return 取得した文字列
     */
    @JvmOverloads
    fun readRawText(filePath: String, charset: String = Settings.File.ENCODE,
                 lineSplitter: String = Settings.String.SEPARATOR): String {
        val ips = context.assets.open(filePath)
        return FileIO.readText(ips, charset, lineSplitter)
    }

    /*
     * ↓文字列のリストを取得↓
     */

    /**
     * テキストファイルの内容をリスト（行×列）で取得
     * @param filePath アプリケーションルートからの相対パス
     * @param delimiterOfWord 分割文字（分割しない場合は空白を指定）
     * @param charset エンコード文字列
     * @return 取得した文字列のリスト
     */
    fun readText(filePath: String, delimiterOfWord: String,
                 charset: String = Settings.File.ENCODE): List<List<String>> {
        return FileIO.readTextAsList(getInputStream(filePath), delimiterOfWord, charset)
    }

    /**
     * CSVファイルの内容をリスト（行×列）で取得
     * @param filePath アプリケーションルートからの相対パス
     * @param charset エンコード文字列
     * @return 取得した文字列のリスト
     */
    fun readCsv(filePath: String, charset: String = Settings.File.ENCODE): List<List<String>> {
        return FileIO.readTextAsList(getInputStream(filePath), ",", charset)
    }

    /**
     * TSVファイルの内容をリスト（行×列）で取得
     * @param filePath アプリケーションルートからの相対パス
     * @param charset エンコード文字列
     * @return 取得した文字列のリスト
     */
    fun readTsv(filePath: String, charset: String = Settings.File.ENCODE): List<List<String>> {
        return FileIO.readTextAsList(getInputStream(filePath), "\t", charset)
    }

    /*
     * ↓オブジェクトのリストを取得↓
     */

    /**
     * テキストファイルの内容をリスト（行×列）で取得
     * @param retDataType 読み込むオブジェクトの型
     * @param filePath アプリケーションルートからの相対パス
     * @param delimiterOfWord 分割文字（分割しない場合は空白を指定）
     * @param charset エンコード文字列
     * @return 取得した文字列のリスト
     */
    fun <T> readTextAsObjectList(retDataType: Class<T>, filePath: String, delimiterOfWord: String,
                                 charset: String = Settings.File.ENCODE): List<T> {
        return FileIO.readTextAsObjectList(retDataType, getInputStream(filePath), delimiterOfWord, charset)
    }

    /**
     * CSVファイルの内容をリスト（行×列）で取得
     * @param retDataType 読み込むオブジェクトの型
     * @param filePath アプリケーションルートからの相対パス
     * @param charset エンコード文字列
     * @return 取得した文字列のリスト
     */
    fun <T> readCsvAsObjectList(retDataType: Class<T>, filePath: String,
                                charset: String = Settings.File.ENCODE): List<T> {
        return FileIO.readTextAsObjectList(retDataType, getInputStream(filePath), ",", charset)
    }

    /**
     * TSVファイルの内容をリスト（行×列）で取得
     * @param retDataType 読み込むオブジェクトの型
     * @param filePath アプリケーションルートからの相対パス
     * @param charset エンコード文字列
     * @return 取得した文字列のリスト
     */
    fun <T> readTsvAsObjectList(retDataType: Class<T>, filePath: String,
                                charset: String = Settings.File.ENCODE): List<T> {
        return FileIO.readTextAsObjectList(retDataType, getInputStream(filePath), "\t", charset)
    }

    /**
     * 入力ストリームの取得
     * @param filePath アプリケーションルートからの相対パス
     */
    private fun getInputStream(filePath: String): InputStream {
        return context.assets.open(filePath)
    }
}