package net.nikilek.toolkit.utility

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Job

/**
 * Created by nikilek on 2015/11/01.
 * スレッド関連の処理を行う汎用クラス
 */
object ThreadUtil {

    /**
     * スレッドスリープを実行
     * @param during スリープ時間（ミリ秒）
     * @param skip trueの場合、duringの指定によらず0秒で復帰　既定値：false
     * @return 割込があればfalse
     */
    @JvmOverloads
    fun sleep(during: Long, skip: Boolean = false): Boolean {
        return try {
            Thread.sleep(if (skip) 0 else during)
            true
        } catch (e: InterruptedException) {
            false
        }
    }

    /**
     * スレッドスリープを実行
     * @param during スリープ時間（ミリ秒）
     * @param skip trueの場合、duringの指定によらず0秒で復帰　既定値：false
     */
    @JvmOverloads
    suspend fun delay(during: Long, skip: Boolean = false) {
        kotlinx.coroutines.delay(if (skip) 0 else during)
    }

    /**
     * ジョブをキャンセルする
     * @param job ジョブ
     * @return キャンセルを実行したかのフラグ
     */
    fun cancel(job: Job?): Boolean {
        return try {
            job?.cancel()
            true
        } catch (e: CancellationException) {
            false
        }
    }
}