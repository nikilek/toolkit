package net.nikilek.toolkit.utility

import android.app.Notification
import android.app.NotificationManager
import android.app.NotificationChannel
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import androidx.annotation.DrawableRes
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import net.nikilek.toolkit.constant.Settings

/**
 * Created by nikilek on 2018/07/01.
 * 通知の表示を行う汎用クラス
 *
 * ※LED点灯、サウンド再生などのオプションが必要な場合は、下記のいずれかの方法で表示を行う
 * 　(方法1) createBuilder() → 呼び出し元でビルダーに設定を追加 → show(引数：設定を追加したビルダー)
 * 　(方法2) 呼び出し元でビルダーを生成 → show(引数：生成したビルダー)
 *
 * ※上記以外は、直接show(引数：ビルダーに渡す各パラメタ)をコール
 *
 */
object NotificationUtil {

    /**
     * 通知の表示
     * @param context コンテキスト
     * @param icon アイコンのリリースID
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param autoCancel タップで消去するかのフラグ
     * @param vibrate バイブレーション有無（パターンは定数定義に応じる）　※既定値：false
     * @param contentIntent タップ時に渡すインテント　※既定値：null
     * @param notificationId 通知ID　※既定値：定数定義値
     */
    @JvmOverloads
    fun show(context: Context, @DrawableRes icon: Int, title: String, message: String, autoCancel: Boolean,
             vibrate: Boolean = false, contentIntent: PendingIntent? = null, notificationId: Int = Settings.Notification.ID) {
        show(context, createBuilder(context, icon, title, message, autoCancel, vibrate, contentIntent), notificationId)
    }

    /**
     * 通知の表示
     * @param context コンテキスト
     * @param builder 通知ビルダー
     * @param notificationId 通知ID　※既定値：定数定義値
     */
    @JvmOverloads
    fun show(context: Context, builder: NotificationCompat.Builder, notificationId: Int = Settings.Notification.ID) {
        show(context, builder.build(), notificationId)
    }

    /**
     * 通知の表示
     * @param context コンテキスト
     * @param notification 通知オブジェクト
     * @param notificationId 通知ID　※既定値：定数定義値
     */
    @JvmOverloads
    fun show(context: Context, notification: Notification, notificationId: Int = Settings.Notification.ID) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        //チャンネルの作成
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel(notificationManager)
        }
        //通知の実行
        notificationManager.notify(notificationId, notification)
    }

    /**
     * 通知ビルダーの取得
     * @param context コンテキスト
     * @param icon アイコンのリリースID
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param autoCancel タップで消去するかのフラグ
     * @param vibrate バイブレーション有無（パターンは定数定義に応じる）　※既定値：false
     * @param contentIntent タップ時に渡すインテント　※既定値：null
     * @return 通知ビルダー
     */
    @JvmOverloads
    fun createBuilder(
            context: Context, @DrawableRes icon: Int, title: String, message: String, autoCancel: Boolean,
            vibrate: Boolean = false, contentIntent: PendingIntent? = null): NotificationCompat.Builder {
        //ビルダーの取得と設定
        val builder = NotificationCompat.Builder(context, Settings.Notification.CHANNEL_ID)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(autoCancel)
                .setStyle(NotificationCompat.BigTextStyle().bigText(message))
        //バイブレーションの設定
        if (vibrate) {
            builder.setVibrate(Settings.Notification.VIBRATION_PATTERN)
        }
        //インテントの設定
        if (contentIntent != null) {
            builder.setContentIntent(contentIntent)
        }
        return builder
    }

    /**
     * チャンネル作成
     * @param notificationManager 通知マネージャ
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel(notificationManager: NotificationManager) {
        val channel = NotificationChannel(
                Settings.Notification.CHANNEL_ID,
                Settings.Notification.CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT)
        channel.importance = NotificationManager.IMPORTANCE_LOW
        notificationManager.createNotificationChannel(channel)
    }
}