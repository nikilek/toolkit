package net.nikilek.toolkit.utility

import android.content.Context
import android.util.Log
import net.nikilek.toolkit.constant.Settings

/**
 * Created by nikilek on 2017/10/16.
 * ログ出力を行う汎用クラス
 */
object LogUtil {

    /**
     * タグの生成
     */
    fun createTag(classReference: Any): String {
        return classReference.javaClass.simpleName.take(23)
    }

    /**
     * デバッグログの出力（コンソール）
     * @param classReference クラスの参照（例：this）
     * @param log 出力する文字列　※既定値：空文字
     */
    fun debug(classReference: Any?, log: String) {
        @Suppress("ConstantConditionIf")
        //if(!BuildConfig.DEBUG) { //BuildConfigに従う場合
        if (!Settings.Mode.DEBUG) { //定数定義に従う場合
            return
        }
        val tag = if (classReference == null) "net.nikilek.toolkit" else createTag(classReference)
        val str = "[" + getCurrentTimeString() + "]" + log
        Log.d(tag, str)
    }

    /**
     * デバッグログの出力（コンソール）
     * @param e エラー情報
     */
    fun debug(classReference: Any?, e: Throwable) {
        debug(classReference, Log.getStackTraceString(e))
    }

    /**
     * デバッグログの出力（コンソール）
     * @param log 出力する文字列　※既定値：空文字
     */
    @JvmOverloads
    fun debug(log: String = "") {
        debug(null, log)
    }

    /**
     * デバッグログの出力（コンソール）
     * @param e エラー情報
     */
    fun debug(e: Throwable) {
        debug(null, Log.getStackTraceString(e))
    }

    /**
     * エラーログの出力（ファイル出力）
     * @param e エラー情報
     * @param toSystem システム領域に出力するか　※既定値：true
     */
    @JvmOverloads
    fun outputErrorLog(context: Context, e: Throwable, toSystem: Boolean = true) {
        //出力文字列の取得
        val sb = StringBuilder()
        sb.setLength(0)
        sb.append("[" + getCurrentTimeString() + "]")
        sb.append(Settings.String.SEPARATOR)
        sb.append(Log.getStackTraceString(e))
        sb.append(Settings.String.SEPARATOR)
        //ファイル出力
        val util = FileUtil(context, Settings.File.PATH_ERROR_LOG, toSystem)
        util.writeString(sb.toString(), false, Settings.File.BUFFER_SIZE)
    }

    /**
     * 現在時刻の文字列を取得
     */
    fun getCurrentTimeString(): String {
        val str = java.sql.Timestamp(System.currentTimeMillis()).toString()
        return str.padEnd(23, '0')
    }
}
