package net.nikilek.toolkit.utility

import java.util.ArrayList

/**
 * Created by nikilek on 2017/10/16.
 * コレクションの操作をする汎用クラス
 */
object CollectionUtil {

    /**
     * 指定配列の各要素同士の組み合わせを、リストで取得する（同一要素同士の組み合わせを含まない）
     * 【例】array={1,2,3}, combinationSize=2の場合：{1,2}, {1,3}, {2,3}
     * @param array 元の配列
     * @param combinationSize 各ペアの要素数
     * @return 組み合わせのリスト
     */
    fun getCombinationList(array: IntArray, combinationSize: Int): List<IntArray> {
        val resultList = arrayListOf<ArrayList<Int>>()
        getCombinationList(resultList, ArrayList(), array.toList(), 0, combinationSize - 1, 0)
        return resultList.indices.map { resultList[it].toIntArray() }
    }

    private fun getCombinationList(resultList: ArrayList<ArrayList<Int>>, combinationList: ArrayList<Int>, //
                                   array: List<Int>, nest: Int, depth: Int, startI: Int) {
        var tmpCombinationList = combinationList
        val tempList = ArrayList(tmpCombinationList)
        for (i in startI until array.size - depth + startI) {
            if (i >= array.size) {
                continue
            }
            tmpCombinationList = ArrayList(tempList)
            tmpCombinationList.add(array[i])
            if (nest < depth) {
                getCombinationList(resultList, tmpCombinationList, array, nest + 1, depth, i + 1)
            } else if (nest >= depth) {
                resultList.add(tmpCombinationList)
            }
        }
    }

    /**
     * 指定配列の各要素同士の組み合わせを、リストで取得する（同一要素同士の組み合わせを含む）
     * 【例】array={1,2,3}, combinationSize=2の場合：{1,1}, {1,2}, {1,3}, {2,2}, {2,3}, {3,3}
     * @param array 元の配列
     * @param combinationSize 各ペアの要素数
     * @return 組み合わせのリスト
     */
    fun getCombinationListDuplication(array: IntArray, combinationSize: Int): List<IntArray> {
        val resultList = arrayListOf<ArrayList<Int>>()
        getCombinationListDuplication(resultList, ArrayList(), array.toList(), 0, combinationSize - 1, 0)
        return resultList.indices.map { resultList[it].toIntArray() }
    }

    private fun getCombinationListDuplication(resultList: ArrayList<ArrayList<Int>>, combinationList: ArrayList<Int>,
                                              array: List<Int>, nest: Int, depth: Int, startI: Int) {
        var tmpCombinationList = combinationList
        val tempList = ArrayList(tmpCombinationList)
        for (i in startI until array.size + startI) {
            if (i >= array.size) {
                continue
            }
            tmpCombinationList = ArrayList(tempList)
            tmpCombinationList.add(array[i])
            if (nest < depth) {
                getCombinationListDuplication(resultList, tmpCombinationList, array, nest + 1, depth, i)
            } else if (nest >= depth) {
                resultList.add(tmpCombinationList)
            }
        }
    }
}