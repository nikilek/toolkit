package net.nikilek.toolkit.utility

import android.content.Context

import net.nikilek.toolkit.R

/**
 * Created by nikilek on 2016/01/27.
 * Toast関連の汎用クラス
 */
object ToastUtil {

    /**
     * サーバエラー時のトーストを表示する
     * @param context コンテキスト
     */
    fun showServerError(context: Context) {
        Toaster.show(context, context.getString(R.string.toast_server_error_title), context.getString(R.string.toast_server_error_message), true)
    }

    /**
     * セーブデータ保存失敗時のトーストを表示
     * @param context コンテキスト
     */
    fun showSaveError(context: Context) {
        Toaster.show(context, context.getString(R.string.toast_save_error_title), context.getString(R.string.toast_save_error_message), true)
    }

    /**
     * セーブデータ読込失敗時のトーストを表示
     * @param context コンテキスト
     */
    fun showLoadError(context: Context) {
        Toaster.show(context, context.getString(R.string.toast_load_error_title), context.getString(R.string.toast_load_error_message), true)
    }

    /**
     * セーブデータ読込失敗時（バックアップ読込）のトーストを表示
     * @param context コンテキスト
     */
    fun showLoadBackup(context: Context) {
        Toaster.show(context, context.getString(R.string.toast_load_error_title), context.getString(R.string.toast_load_error_message_read_backup), true)
    }

    /**
     * データ参照エラー時のトーストを表示する
     * @param context コンテキスト
     */
    fun showDataError(context: Context) {
        Toaster.show(context, context.getString(R.string.toast_data_error_title), context.getString(R.string.toast_data_error_message), true)
    }

    /**
     * 機能未実装の通知用トーストを表示する
     * @param context コンテキスト
     */
    fun showOutOfOrder(context: Context) {
        Toaster.show(context, context.getString(R.string.toast_out_of_order_title), context.getString(R.string.toast_out_of_order_message), true)
    }
}
