package net.nikilek.toolkit.utility

import android.content.Context
import android.util.Base64
import java.nio.charset.Charset
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

/**
 * Created by nikilek on 2016/01/27.
 * 暗号化／複合化を行うクラス
 * @param context コンテキスト
 */
class CryptoUtil(private val context: Context) {

    companion object {
        private const val TYPE = "AES" //暗号化方式
        private const val MODE = "$TYPE/CBC/PKCS5Padding" //パディングモード
        private const val ALGORITHM = "PBKDF2WithHmacSHA1" //アルゴリズム
        private const val KEY_LENGTH = 128 //暗号化強度
        private const val IV_LENGTH = 16 //IVサイズ
        private const val SALT_LENGTH = KEY_LENGTH / 8 //saltサイズ
    }

    /**
     * 暗号化処理
     * @param sKey キー文字列
     * @param plain 平文のバイナリ
     * @return 暗号化済のバイナリ
     * @throws Exception
     */
    @Throws(Exception::class)
    fun encrypt(sKey: String, plain: ByteArray): ByteArray {
        val cipher = createEncryptCipher(sKey)
        return cipher.doFinal(plain)
    }

    /**
     * 複合化処理
     * @param sKey キー文字列
     * @param encrypted 暗号化済のバイナリ
     * @param emptyParam saltとivに固定値を用いるかのフラグ　※既定値：false
     * @return 複合化済のバイナリ
     * @throws Exception
     */
    @Throws(Exception::class)
    fun decrypt(sKey: String, encrypted: ByteArray, emptyParam: Boolean = false): ByteArray {
        val cipherList = createDecryptCipher(sKey, emptyParam)
        return try {
            cipherList[0].doFinal(encrypted)
        } catch (ex: Exception) {
            //最新の情報で複合できない場合は、バックアップした情報で複合
            cipherList[1].doFinal(encrypted) //バックアップでの複合失敗は例外をスローする
        }
    }

    /**
     * 指定文字列を暗号化済みのBase64エンコード文字列に変換する
     * @param sKey キー文字列
     * @param plain 平文
     * @return 暗号化およびBase64エンコード済の文字列
     * @throws Exception
     */
    @Throws(Exception::class)
    fun encrypt(sKey: String, plain: String): String {
        val encrypted = encrypt(sKey, plain.toByteArray())
        return Base64.encodeToString(encrypted, Base64.DEFAULT)
    }

    /**
     * Base64エンコードされた暗号化済み文字列をデコードおよび複合化する
     * @param sKey キー文字列
     * @param encrypted Base64エンコードされた暗号化済み文字列
     * @param emptyParam saltとivに固定値を用いるかのフラグ　※既定値：false
     * @return デコードおよび複合化済の文字列
     * @throws Exception
     */
    @Throws(Exception::class)
    fun decrypt(sKey: String, encrypted: String, charsetName: String, emptyParam: Boolean = false): String {
        val enc = Base64.decode(encrypted.toByteArray(), Base64.DEFAULT)
        val result = decrypt(sKey, enc, emptyParam)
        return String(result, Charset.forName(charsetName))
    }

    /**
     * 暗号化用Cipherオブジェクト生成（※SealedObjectのコンストラクタ等で使用）
     * @param sKey 暗号化キー
     * @param emptyParam saltとivに固定値を用いるかのフラグ　※既定値：false
     * @return 暗号化用Cipherオブジェクト
     * @throws Exception
     */
    @JvmOverloads
    @Throws(Exception::class)
    fun createEncryptCipher(sKey: String, emptyParam: Boolean = false): Cipher {
        //暗号化キーの種を生成
        val salt = ByteArray(SALT_LENGTH)
        var iv = ByteArray(IV_LENGTH)
        if (!emptyParam) {
            //ランダム生成
            val random = SecureRandom()
            random.nextBytes(salt)
            iv = random.generateSeed(IV_LENGTH)
            //生成した salt, IV を保存（復号時に使用）
            AppPreference.putString(context, "salt_old", AppPreference.getString(context, "salt"))
            AppPreference.putString(context, "iv_old", AppPreference.getString(context, "iv"))
            AppPreference.putString(context, "salt", Base64.encodeToString(salt, Base64.DEFAULT))
            AppPreference.putString(context, "iv", Base64.encodeToString(iv, Base64.DEFAULT))
        }
        //暗号化キーの取得
        val key = createKey(sKey, salt)
        val param = createParam(iv)
        //暗号化クラスのインスタンスを生成
        val cipher = Cipher.getInstance(MODE)
        cipher.init(Cipher.ENCRYPT_MODE, key, param)
        return cipher
    }

    /**
     * 復号化用Cipherオブジェクト生成（※SealedObject#getObject等で使用）
     * @param sKey 複合化キー
     * @param emptyParam saltとivに固定値を用いるかのフラグ　※既定値：false
     * @return 復号化用Cipherオブジェクトのリスト（0：最新、1：バックアップ）
     * @throws Exception
     */
    @JvmOverloads
    @Throws(Exception::class)
    fun createDecryptCipher(sKey: String, emptyParam: Boolean = false): List<Cipher> {
        //暗号化時に保存した salt, IV を取得
        var salt = Base64.decode(AppPreference.getString(context, "salt", ""), Base64.DEFAULT)
        var iv = Base64.decode(AppPreference.getString(context, "iv", ""), Base64.DEFAULT)
        var saltOld = Base64.decode(AppPreference.getString(context, "salt_old", ""), Base64.DEFAULT)
        var ivOld = Base64.decode(AppPreference.getString(context, "iv_old", ""), Base64.DEFAULT)
        //固定値を使用、または保存した値が取得できなかった場合は、空の値を割り当てる
        if (emptyParam || salt.size < SALT_LENGTH) salt = ByteArray(SALT_LENGTH)
        if (emptyParam || iv.size < IV_LENGTH) iv = ByteArray(IV_LENGTH)
        if (emptyParam || saltOld.size < SALT_LENGTH) saltOld = ByteArray(SALT_LENGTH)
        if (emptyParam || ivOld.size < IV_LENGTH) ivOld = ByteArray(IV_LENGTH)
        //復号化用Cipherオブジェクト生成して返却
        val cipher = createDecryptCipher(sKey, salt, iv)
        val cipherOld = createDecryptCipher(sKey, saltOld, ivOld)
        return listOf(cipher, cipherOld)
    }

    /**
     * 復号化用Cipherオブジェクト生成（※SealedObject#getObject等で使用）
     * @param sKey 複合化キー
     * @param salt 付加データ
     * @param iv 初期化ベクトル
     * @return 復号化用Cipherオブジェクト
     * @throws Exception
     */
    @Throws(Exception::class)
    fun createDecryptCipher(sKey: String, salt: ByteArray, iv: ByteArray): Cipher {
        //復号化キーの取得
        val key = createKey(sKey, salt)
        val param = createParam(iv)
        //複合化クラスのインスタンスを生成
        val cipher = Cipher.getInstance(MODE)
        cipher.init(Cipher.DECRYPT_MODE, key, param)
        return cipher
    }

    /**
     * 暗号化／複合化キーを生成する
     * @param sKey キー文字列
     * @param salt 付加データ
     * @return 暗号化／復号化キー
     */
    private fun createKey(sKey: String, salt: ByteArray): SecretKeySpec {
        val keySpec = PBEKeySpec(sKey.toCharArray(), salt, 1000, KEY_LENGTH)
        val keyFactory = SecretKeyFactory.getInstance(ALGORITHM)
        val keyBytes = keyFactory.generateSecret(keySpec).encoded
        keySpec.clearPassword()
        return SecretKeySpec(keyBytes, TYPE)
    }

    /**
     * 暗号化／復号化パラメタを生成する
     * @param iv 初期化ベクトル
     * @return 暗号化／復号化パラメタ
     */
    private fun createParam(iv: ByteArray): IvParameterSpec {
        return IvParameterSpec(iv)
    }
}