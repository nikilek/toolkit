package net.nikilek.toolkit.utility

import android.app.Activity
import android.graphics.Point
import android.graphics.Rect
import android.util.DisplayMetrics
import android.view.View

import java.util.HashMap

/**
 * Created by nikilek on 2016/01/27.
 * 数値計算を行う汎用クラス
 */
object CalcUtil {

    /**
     * 整数の除算
     * @param divided 割られる数
     * @param divideBy 割る数
     * @return 結果（小数点以下切り捨て）
     */
    fun divide(divided: Int, divideBy: Int): Int {
        return if (divideBy == 0) {
            divided
        } else divided / divideBy
    }

    /**
     * 配列内の整数値の合計を算出する
     * @param array 対象の配列
     * @return 合計値
     */
    fun sum(array: IntArray): Int {
        return array.indices.sumBy { array[it] }
    }

    /**
     * 配列内の数値に(-1)を掛けた数値の配列を取得
     * @param array 対象の配列
     * @return 結果の配列
     */
    operator fun minus(array: IntArray): IntArray {
        val ret = IntArray(array.size)
        for (i in array.indices) {
            ret[i] = array[i] * -1
        }
        return ret
    }

    /**
     * 配列の各要素同士の引き算（array1.size >= array2.size であること）
     * @param array1 引かれる側の配列
     * @param array2 引く側の配列
     * @return 結果の配列
     */
    fun sub(array1: IntArray, array2: IntArray): IntArray {
        return sum(array1, array2, true)
    }

    /**
     * 配列の各要素同士の足し算（array1.size >= array2.size であること）
     * @param array1 足される側の配列
     * @param array2 足す側の配列
     * @param minus array2の正負を反転させる（引き算する）か否か　※既定値：false
     * @return 結果の配列
     */
    fun sum(array1: IntArray, array2: IntArray, minus: Boolean = false): IntArray {
        val ret = IntArray(array1.size)
        val sign = if (minus) -1 else 1
        array1.indices
                .takeWhile { it < array2.size }
                .forEach { ret[it] = array1[it] + array2[it] * sign }
        return ret
    }

    /**
     * 配列内の、指定値より大きいorより小さい数値の数をカウントする
     * @param array 対象の配列
     * @param threshold しきい値（しきい値と同値はカウント対象外）
     * @param lessThan true：「より小さい」、false：「より大きい」　※既定値：false
     * @return 条件を満たす要素の数
     */
    fun count(array: IntArray, threshold: Int, lessThan: Boolean = false): Int {
        return if (lessThan) {
            array.count { it < threshold }
        } else {
            array.count { it > threshold }
        }
    }

    /**
     * 乱数（自然数）を生成
     * @param range 数値の範囲（ゼロ起点。range=3の場合：0, 1, 2）
     * @return 生成した乱数
     */
    fun getRandom(range: Int): Int {
        return (Math.random() * range).toInt()
    }

    /**
     * px値を元にdip値を算出
     * @param activity 呼び出し元のActivity
     * @param px px値
     * @return dip値
     */
    fun convertPxToDip(activity: Activity, px: Int): Int {
        //画面サイズの取得
        val windowManager = activity.windowManager
        val display = windowManager.defaultDisplay
        val displayMetrics = DisplayMetrics()
        display.getMetrics(displayMetrics)
        //int m_widthPixels = displayMetrics.widthPixels;
        //int m_heightPixels = displayMetrics.heightPixels;
        val mDensity = displayMetrics.density
        return (px * mDensity).toInt()
    }

    /**
     * 正方形内の点を90度単位で回転させた座標を算出する
     * @param targetX 対象の正方形のx座標
     * @param targetY 対象の正方形のy座標
     * @param targetWidth 正方形の幅
     * @param targetHeight 正方形の高さ
     * @param radian 回転する角度
     * @return 回転後の座標
     */
    fun rotatePoint(targetX: Int, targetY: Int, targetWidth: Int, targetHeight: Int, radian: Int): Point {
        var x = targetX
        var y = targetY
        //元の点が正方形を４分割したうちのどこに位置するかを算出
        var position = 0 // ４分割の左上=0,右上=1,右下=2,左下=3
        if (x > targetWidth / 2) {
            position = if (y > targetHeight / 2) 2 else 1
        } else if (y > targetHeight / 2) {
            position = 3
        }
        //回転
        if (radian == 90) {
            if (position % 2 == 0) {
                x = targetWidth - x
            } else {
                y = targetHeight - y
            }
        } else if (radian == 180) {
            x = targetWidth - x
            y = targetHeight - y
        } else if (radian == 270) {
            if (position % 2 == 0) {
                y = targetHeight - y
            } else {
                x = targetWidth - x
            }
        }
        //新しい座標をPointとして取得して返却
        return Point(x, y)
    }

    /**
     * ビューの四辺の座標および中心座標をマップで取得する
     * マップのキーは下記
     * ・x：X座標（左側）
     * ・x0：X座標（左側）
     * ・x50：X座標（中央）
     * ・x100：X座標（右側）
     * ・y：Y座標（左側）
     * ・y0：Y座標（左側）
     * ・y50：Y座標（中央）
     * ・y100：Y座標（右側）
     * ・w：幅
     * ・h：高さ
     * @param v 対象のビュー（相対位置の場合、contextがActivityであること）
     * @param relative true：相対位置（ステータスバーを含まない座標）、false：絶対位置（ステータスバーを含む座標）※既定値：false
     * @param onWindow true：ウィンドウ上の位置、false：スクリーン上の位置　※既定値：false
     * @return 各座標を格納したマップ
     */
    @JvmOverloads
    fun getPointOfView(v: View?, relative: Boolean = false, onWindow: Boolean = false): Map<String, Int>? {
        if (v == null) {
            return null
        }
        //左上座標の取得
        val pos = IntArray(2)
        if (onWindow) {
            v.getLocationInWindow(pos)
        } else {
            v.getLocationOnScreen(pos)
        }
        if (relative) {
            //ステータスバーの高さを引く
            val rect = Rect()
            (v.context as? Activity)?.apply {
                window.decorView.getWindowVisibleDisplayFrame(rect)
            }
            pos[1] -= rect.top
        }
        //座標情報のマップを生成して返却
        return createPointMap(pos[0], pos[1], v.width, v.height)
    }

    /**
     * 左上座標と幅・高さの情報を元に、各種座標情報のマップを生成する
     * @param x0 左上x座標
     * @param y0 左上y座標
     * @param w 幅
     * @param h 高さ
     * @return 座標情報のマップ
     */
    private fun createPointMap(x0: Int, y0: Int, w: Int, h: Int): Map<String, Int> {
        val x100 = x0 + w
        val y100 = y0 + h
        val x50 = x0 + w / 2
        val y50 = y0 + h / 2
        //マップに詰めて返す
        val map = HashMap<String, Int>()
        map["x0"] = x0
        map["x50"] = x50
        map["x100"] = x100
        map["y0"] = y0
        map["y50"] = y50
        map["y100"] = y100
        map["x"] = x0
        map["y"] = y0
        map["w"] = w
        map["h"] = h
        return map
    }
}