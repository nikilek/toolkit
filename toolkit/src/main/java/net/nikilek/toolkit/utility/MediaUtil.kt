package net.nikilek.toolkit.utility

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import net.nikilek.toolkit.constant.Settings

import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.net.URLConnection

/**
 * Created by nikilek on 2020/06/07.
 * メディアファイルの入出力処理を行う
 * ※未テスト
 */
class MediaUtil
/**
 * コンストラクタ
 * @param context コンテキスト
 */
(private val context: Context) {

    /**
     * 画像の保存
     * @param fileName ファイル名
     * @param byteArray ファイルバイナリ
     * @param bufferSize バッファサイズ
     */
    fun saveImage(fileName: String, byteArray: ByteArray, bufferSize: Int): Boolean {
        val uri = getContentUri(fileName) ?: return false
        val fileIO = FileIO(context, uri.path, false)
        return fileIO.writeFile(byteArray, bufferSize)
    }

    /**
     * 画像の読込
     * @param fileName ファイル名
     * @param bufferSize バッファサイズ
     */
    fun loadImage(fileName: String, bufferSize: Int): ByteArray? {
        val uri = getContentUri(fileName) ?: return null
        val fileIO = FileIO(context, uri.path, false)
        return fileIO.readFile(bufferSize)
    }

    /**
     * 指定ファイルのUriを取得
     */
    fun getContentUri(fileName: String): Uri? {
        val mimeType = URLConnection.guessContentTypeFromName(fileName)
        val values = ContentValues().apply {
            put(MediaStore.Images.Media.DISPLAY_NAME, fileName)
            put(MediaStore.Images.Media.MIME_TYPE, mimeType)
        }
        @Suppress("DEPRECATION") val collection =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
                else Uri.fromFile(Environment.getExternalStorageDirectory())
        return context.contentResolver.insert(collection, values)
    }

    /**
     * 指定ファイルをギャラリーに反映させる（v10未満のみ）
     * @param file 対象ファイル
     */
    fun scanFile(file: File) {
        try {
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> {
                    //v10以上の場合は何もしない
                }
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT -> {
                    //v4.4以上の場合はスキャン
                    @Suppress("DEPRECATION") val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
                    val contentUri = Uri.fromFile(file)
                    mediaScanIntent.data = contentUri
                    this.context.sendBroadcast(mediaScanIntent)
                }
                else -> {
                    //v4.4未満の場合はマウント
                    this.context.sendBroadcast(
                            Intent(
                                    Intent.ACTION_MEDIA_MOUNTED,
                                    Uri.parse("file://" + context.externalCacheDir.toString())
                            )
                    )
                }
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }
}