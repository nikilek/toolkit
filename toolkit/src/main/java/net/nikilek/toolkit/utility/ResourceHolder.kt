package net.nikilek.toolkit.utility

import android.content.Context
import android.graphics.Typeface
import net.nikilek.toolkit.constant.Settings

/**
 * Created by nikilek on 2017/10/16.
 * リソースの参照を保持するクラス
 */
class ResourceHolder {

    object Typefaces {
        private var typefaceDefault: Typeface? = null //デフォルト
        private var typefaceBold: Typeface? = null //太字

        /**
         * システム定義のタイプフェースを取得する（標準文字用）
         * @param context
         * @return 取得したタイプフェース
         */
        fun getDefault(context: Context): Typeface {
            if (typefaceDefault == null) {
                typefaceDefault = Typeface.createFromAsset(context.assets, Settings.File.PATH_FONT_FILE)
            }
            return typefaceDefault!!
        }

        /**
         * システム定義のタイプフェースを取得する（太字用）
         * @param context
         * @return 取得したタイプフェース
         */
        fun getBold(context: Context): Typeface {
            if (typefaceBold == null) {
                typefaceBold = Typeface.createFromAsset(context.assets, Settings.File.PATH_FONT_FILE_BOLD)
            }
            return typefaceBold!!
        }
    }
}