package net.nikilek.toolkit.utility

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import net.nikilek.toolkit.AbstractActivity
import net.nikilek.toolkit.constant.CodeConst
import java.io.FileInputStream
import java.io.FileOutputStream
import java.lang.Exception
import java.net.URLConnection

/**
 * Created by nikilek on 2020/06/07.
 * キュメントファイルの入出力処理を行う
 * ※同一インスタンスで複数の処理を同時実行しないこと（非同期コールバックが返るまで、次の実行はNG）
 */
object DocumentUtil {

    /**
     * バイナリデータ読込
     * @param activity
     * @param mimeType MIMEタイプ
     */
    class BinaryReader(private val activity: AbstractActivity, private val mimeType: String? = null) {
        private lateinit var onOpenDocument: ((ByteArray?) -> Unit)

        /**
         * ドキュメントの読込
         * @param callback 処理完了時のコールバック
         */
        @RequiresApi(Build.VERSION_CODES.KITKAT)
        fun read(callback: ((ByteArray?) -> Unit)) {
            this.onOpenDocument = callback
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = mimeType
            }
            activity.addActivityResultCallback(CodeConst.MSG.ToAbstractActivity.REQUEST_OPEN_DOCUMENT, this::openDocumentCallback)
            activity.startActivityForResult(intent, CodeConst.MSG.ToAbstractActivity.REQUEST_OPEN_DOCUMENT)
        }

        /**
         * ドキュメントオープン要求のコールバック
         * @param intent
         */
        private fun openDocumentCallback(resultCode: Int, intent: Intent?) {
            if (resultCode != Activity.RESULT_OK || intent == null) {
                onOpenDocument.invoke(null)
                return
            }
            try {
                var byteArray: ByteArray? = null
                val uri = intent.data as Uri
                activity.contentResolver.openFileDescriptor(uri, "r")?.use { pfd ->
                    FileInputStream(pfd.fileDescriptor).use { fos ->
                        byteArray = fos.readBytes()
                    }
                }
                onOpenDocument.invoke(byteArray)
            } catch (e: Exception) {
                LogUtil.debug(e)
                onOpenDocument.invoke(null)
            }
        }
    }

    /**
     * オブジェクト読込
     * @param activity
     * @param retDataType 読み込むオブジェクトの型
     */
    class ObjectReader<T>(private val activity: AbstractActivity, private val retDataType: Class<T>) {
        private lateinit var onOpenDocument: ((T?) -> Unit)

        /**
         * ドキュメントの読込
         * @param mimeType MIMEタイプ
         * @param callback 処理完了時のコールバック
         */
        @RequiresApi(Build.VERSION_CODES.KITKAT)
        fun read(callback: ((T?) -> Unit)) {
            this.onOpenDocument = callback
            val mimeType = URLConnection.guessContentTypeFromName("_.json")
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = mimeType
            }
            activity.addActivityResultCallback(CodeConst.MSG.ToAbstractActivity.REQUEST_OPEN_DOCUMENT, this::openDocumentCallback)
            activity.startActivityForResult(intent, CodeConst.MSG.ToAbstractActivity.REQUEST_OPEN_DOCUMENT)
        }

        /**
         * ドキュメントオープン要求のコールバック
         * @param intent
         */
        private fun openDocumentCallback(resultCode: Int, intent: Intent?) {
            if (resultCode != Activity.RESULT_OK || intent == null) {
                onOpenDocument.invoke(null)
                return
            }
            try {
                var byteArray: ByteArray? = null
                val uri = intent.data as Uri
                activity.contentResolver.openFileDescriptor(uri, "r")?.use { pfd ->
                    FileInputStream(pfd.fileDescriptor).use { fos ->
                        byteArray = fos.readBytes()
                    }
                }
                val obj = ObjectMapper().readValue(byteArray, retDataType)
                onOpenDocument.invoke(obj)
            } catch (e: Exception) {
                LogUtil.debug(e)
                onOpenDocument.invoke(null)
            }
        }
    }

    /**
     * 書き込み
     */
    class Writer(private val activity: AbstractActivity) {
        private lateinit var writeData: ByteArray
        private var onWriteDocument: ((Boolean) -> Unit)? = null

        /**
         * ドキュメントの保存
         * @param fileName ファイル名
         * @param data データオブジェクト
         * @param callback 処理完了時のコールバック
         */
        @RequiresApi(Build.VERSION_CODES.KITKAT)
        fun <T> writeJson(fileName: String, data: T, callback: ((Boolean) -> Unit)? = null) {
            val byteArray = ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).writeValueAsBytes(data)
            writeBinary(fileName, byteArray, callback)
        }

        /**
         * ドキュメントの保存
         * @param fileName ファイル名
         * @param byteArray ファイルバイナリ
         * @param callback 処理完了時のコールバック
         */
        @RequiresApi(Build.VERSION_CODES.KITKAT)
        fun writeBinary(fileName: String, byteArray: ByteArray, callback: ((Boolean) -> Unit)? = null) {
            this.writeData = byteArray
            this.onWriteDocument = callback
            val mimeType = URLConnection.guessContentTypeFromName(fileName)
            val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = mimeType
                putExtra(Intent.EXTRA_TITLE, fileName)
            }
            activity.addActivityResultCallback(CodeConst.MSG.ToAbstractActivity.REQUEST_CREATE_DOCUMENT, this::createDocumentCallback)
            activity.startActivityForResult(intent, CodeConst.MSG.ToAbstractActivity.REQUEST_CREATE_DOCUMENT)
        }

        /**
         * ドキュメント作成要求のコールバック
         * @param intent
         */
        private fun createDocumentCallback(resultCode: Int, intent: Intent?) {
            if (resultCode != Activity.RESULT_OK || intent == null) {
                onWriteDocument?.invoke(false)
                return
            }
            try {
                val uri = intent.data as Uri
                activity.contentResolver.openFileDescriptor(uri, "w")?.use { pfd ->
                    FileOutputStream(pfd.fileDescriptor).use { fos ->
                        fos.write(writeData)
                    }
                }
                onWriteDocument?.invoke(true)
            } catch (e: Exception) {
                LogUtil.debug(e)
                onWriteDocument?.invoke(false)
            }
        }
    }
}