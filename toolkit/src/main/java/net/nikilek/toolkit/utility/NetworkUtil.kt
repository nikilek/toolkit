package net.nikilek.toolkit.utility

import java.net.InetAddress
import java.net.NetworkInterface

object NetworkUtil {

    /**
     * 端末のIPアドレスを取得
     * @return IPアドレス
     */
    fun getPrimaryIpAddress(): String {
        return getPrimaryHostInfo().first
    }

    /**
     * 端末のホスト名を取得
     * @return ホスト名
     */
    fun getPrimaryHostName(): String {
        return getPrimaryHostInfo().second
    }

    /**
     * 端末のIPアドレス・ホスト名のペアを取得
     * @return IPアドレス・ホスト名のペア
     */
    fun getPrimaryHostInfo(): Pair<String, String> {
        return getHostInfo(true)[0]
    }

    /**
     * 端末のIPアドレス・ホスト名のペアを取得
     * @param firstOne true：最初の１つが見つかった時点で探索を終了、false：複数の情報を返却
     * @return IPアドレス・ホスト名のペアのリスト
     */
    fun getHostInfo(firstOne: Boolean = false): List<Pair<String, String>> {
        try {
            val list = mutableListOf<Pair<String, String>>()
            //システムから取得
            val localHost = InetAddress.getLocalHost()
            val localIpAddress = localHost.hostAddress
            val localHostName = localHost.hostName
            if (!localHost.isLoopbackAddress
                    && !localHostName.equals("localhost", ignoreCase = true)
                    && localIpAddress.indexOf(':') == -1) {
                list.add(Pair(localIpAddress, localHostName))
            }
            if (firstOne && list.size > 0) {
                //単一探索の場合かつ、システムからの取得に成功した場合は終了
                return list
            }
            //ネットワークアダプタから取得
            NetworkInterface.getNetworkInterfaces().iterator().forEach { netIf ->
                netIf.inetAddresses.iterator().forEach { adr ->
                    val ipAddress = adr.hostAddress
                    val hotName = adr.hostName
                    if (!adr.isLoopbackAddress
                            && !hotName.equals("localhost", ignoreCase = true)
                            && ipAddress.indexOf(':') == -1) {
                        list.add(Pair(ipAddress, hotName))
                        if (firstOne) {
                            //単一探索の場合は終了
                            return list
                        }
                    }
                }
            }
            if (list.size == 0) {
                //見つからなかった場合は、空白を返却
                list.add(Pair("", ""))
            }
            return list
        } catch (e: Exception) {
            LogUtil.debug(e)
            return listOf(Pair("", ""))
        }
    }
}