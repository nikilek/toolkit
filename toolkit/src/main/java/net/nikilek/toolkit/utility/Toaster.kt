package net.nikilek.toolkit.utility

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast

import net.nikilek.toolkit.R
import net.nikilek.toolkit.extension.isIn
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Created by nikilek on 2016/01/27.
 * Toast表示用クラス
 */
object Toaster {
    private var toastTop: Toast? = null //画面上部に表示したトースト
    private var toastBottom: Toast? = null //画面下部に表示したトースト
    private var toastCenter: Toast? = null //画面中央に表示したトースト

    private val lock = ReentrantLock() //表示処理排他用ロックオブジェクト

    /*
     * ↓既定のレイアウトを使用して表示↓
     */

    /**
     * トーストの表示（画面中央）
     * @param context コンテキスト
     * @param title タイトル文字列
     * @param isError エラーか否か　※既定値：false
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun show(context: Context, title: String, isError: Boolean = false, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.CENTER, title, "", isError, duration)
    }

    /**
     * トーストの表示（画面中央）
     * @param context コンテキスト
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param isError エラーか否か　※既定値：false
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun show(context: Context, title: String, message: String, isError: Boolean = false, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.CENTER, title, message, isError, duration)
    }

    /**
     * トーストの表示（画面上部）
     * @param context コンテキスト
     * @param title タイトル文字列
     * @param isError エラーか否か　※既定値：false
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun showTop(context: Context, title: String, isError: Boolean = false, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.TOP, title, "", isError, duration)
    }

    /**
     * トーストの表示（画面上部）
     * @param context コンテキスト
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param isError エラーか否か　※既定値：false
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun showTop(context: Context, title: String, message: String, isError: Boolean = false, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.TOP, title, message, isError, duration)
    }

    /**
     * トーストの表示（画面下部）
     * @param context コンテキスト
     * @param title タイトル文字列
     * @param isError エラーか否か　※既定値：false
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun showBottom(context: Context, title: String, isError: Boolean = false, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.BOTTOM, title, "", isError, duration)
    }

    /**
     * トーストの表示（画面下部）
     * @param context コンテキスト
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param isError エラーか否か　※既定値：false
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun showBottom(context: Context, title: String, message: String, isError: Boolean = false, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.BOTTOM, title, message, isError, duration)
    }

    /*
     * ↓レイアウトを指定して表示↓
     */

    /**
     * トーストの表示（画面中央）
     * @param context コンテキスト
     * @param layoutId レイアウトのリソースID　※レイアウト内に、IDが「R.id.title」「R.id.message」であるビューが存在すること
     * @param title タイトル文字列
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun show(context: Context, layoutId: Int, title: String, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.CENTER, title, "", false, duration, layoutId)
    }

    /**
     * トーストの表示（画面中央）
     * @param context コンテキスト
     * @param layoutId レイアウトのリソースID　※レイアウト内に、IDが「R.id.title」「R.id.message」であるビューが存在すること
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun show(context: Context, layoutId: Int, title: String, message: String, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.CENTER, title, message, false, duration, layoutId)
    }

    /**
     * トーストの表示（画面上部）
     * @param context コンテキスト
     * @param layoutId レイアウトのリソースID　※レイアウト内に、IDが「R.id.title」「R.id.message」であるビューが存在すること
     * @param title タイトル文字列
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun showTop(context: Context, layoutId: Int, title: String, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.TOP, title, "", false, duration, layoutId)
    }

    /**
     * トーストの表示（画面上部）
     * @param context コンテキスト
     * @param layoutId レイアウトのリソースID　※レイアウト内に、IDが「R.id.title」「R.id.message」であるビューが存在すること
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun showTop(context: Context, layoutId: Int, title: String, message: String, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.TOP, title, message, false, duration, layoutId)
    }

    /**
     * トーストの表示（画面下部）
     * @param context コンテキスト
     * @param layoutId レイアウトのリソースID　※レイアウト内に、IDが「R.id.title」「R.id.message」であるビューが存在すること
     * @param title タイトル文字列
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun showBottom(context: Context, layoutId: Int, title: String, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.BOTTOM, title, "", false, duration, layoutId)
    }

    /**
     * トーストの表示（画面下部）
     * @param context コンテキスト
     * @param layoutId レイアウトのリソースID　※レイアウト内に、IDが「R.id.title」「R.id.message」であるビューが存在すること
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）　※既定値：Toast.LENGTH_SHORT
     */
    @JvmOverloads
    fun showBottom(context: Context, layoutId: Int, title: String, message: String, duration: Int = Toast.LENGTH_SHORT) {
        show(context, Gravity.BOTTOM, title, message, false, duration, layoutId)
    }

    /**
     * トーストの表示
     * @param context コンテキスト
     * @param gravity 表示位置（android.view.Gravity 参照）
     * @param title タイトル文字列
     * @param message メッセージ文字列
     * @param isError エラーか否か
     * @param duration 表示時間（Toast.LENGTH_SHORT or Toast.LENGTH_LONG）
     */
    fun show(context: Context, gravity: Int, title: String, message: String, isError: Boolean, duration: Int, layoutId: Int = 0) {
        lock.withLock {
            //ビューの取得
            val v = when {
                layoutId != 0 -> View.inflate(context, layoutId, null)
                isError -> View.inflate(context, R.layout.partial_toast_error, null)
                else -> View.inflate(context, R.layout.partial_toast_normal, null)
            }
            //メッセージのセット
            v.findViewById<TextView>(R.id.title).text = title
            v.findViewById<TextView>(R.id.message).text = message

            //表示の切替
            if (message.isEmpty()) {
                v.findViewById<View>(R.id.message).visibility = View.GONE
            }
            //表示処理
            val handler = Handler(Looper.getMainLooper())
            handler.post {
                //生成
                val toast = Toast(context)
                toast.view = v
                toast.duration = duration
                toast.setGravity(
                        gravity,
                        0,
                        if (gravity.isIn(Gravity.TOP, Gravity.BOTTOM))
                            context.resources.getDimension(R.dimen.layout_48).toInt()
                        else
                            0
                )
                //表示位置が同じトーストがあれば上書きする
                when (gravity) {
                    Gravity.TOP -> {
                        toastTop?.cancel()
                        toastTop = toast
                    }
                    Gravity.BOTTOM -> {
                        toastBottom?.cancel()
                        toastBottom = toast
                    }
                    else -> {
                        toastCenter?.cancel()
                        toastCenter = toast
                    }
                }
                //表示
                toast.show()
            }
        }
    }

    /**
     * トーストの非表示（画面中央）
     */
    fun cancel() {
        cancel(Gravity.CENTER)
    }

    /**
     * トーストの非表示（画面上部）
     */
    fun cancelTop() {
        cancel(Gravity.TOP)
    }

    /**
     * トーストの非表示（画面下部）
     */
    fun cancelBottom() {
        cancel(Gravity.BOTTOM)
    }

    /**
     * トーストの非表示
     * @param gravity 表示位置（CENTER／TOP／BOTTOM　※android.view.Gravity 参照）
     */
    fun cancel(gravity: Int) {
        when (gravity) {
            Gravity.TOP -> {
                toastTop?.cancel()
            }
            Gravity.BOTTOM -> {
                toastBottom?.cancel()
            }
            else -> {
                toastCenter?.cancel()
            }
        }
    }
}
