package net.nikilek.toolkit.utility

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Paint
import android.os.Build
import android.view.View
import android.view.animation.CycleInterpolator
import net.nikilek.toolkit.R

import net.nikilek.toolkit.constant.AnimationConst
import net.nikilek.toolkit.extension.valid
import net.nikilek.toolkit.model.AnimationData
import net.nikilek.toolkit.model.EffectData
import java.util.ArrayList

/**
 * Created by nikilek on 2015/07/29.
 * アニメーション関連の処理を行う汎用クラス
 */
@SuppressLint("ObsoleteSdkInt")
object AnimationUtil {
    /**
     * アニメーションリストを生成する（単一のアニメーション用）
     * @param activity 呼び出し元のActivity
     * @param type 画面効果ID　※AnimationConst参照
     * @param view 対象のビュー
     * @param during アニメーション時間
     * @param wait アニメーション開始までの待機時間
     * @return 生成したアニメーションリスト
     */
    fun createEffect(activity: Activity, type: Int, view: View, during: Long, wait: Long): List<Animator> {
        val fixedDuring = during.valid(0, Int.MAX_VALUE.toLong()) //※duringにInt.MAX_VALUEよりも大きな値を設定するとエラーになる事象への対応
        val list = ArrayList<Animator>() //アニメーションクラスのインスタンス
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return list
        }
        //移動幅の取得
        val span = activity.resources.getDimension(R.dimen.margin_8)

        //アニメーションの種類ごとに処理
        when (type) {
            AnimationConst.EFFECT_APPEAL -> { // 揺らす
                //上下移動（１回繰り返し）
                list.add(ObjectAnimator.ofFloat(view, "translationY", 0f, span).apply { interpolator = CycleInterpolator(1f) })
            }

            AnimationConst.EFFECT_SHAKE -> { // 揺らす
                //左右移動（２回繰り返し）
                list.add(ObjectAnimator.ofFloat(view, "translationX", 0f, span).apply { interpolator = CycleInterpolator(2f) })
            }

            AnimationConst.EFFECT_FADE_IN -> { // フェードイン
                list.add(ObjectAnimator.ofFloat(view, "alpha", 0f, 1f))
            }

            AnimationConst.EFFECT_FADE_OUT -> { // フェードアウト
                list.add(ObjectAnimator.ofFloat(view, "alpha", 1f, 0f))
            }
            AnimationConst.EFFECT_FLASH -> { // 点滅
                list.add(ObjectAnimator.ofFloat(view, "alpha", 1f, 0.1f))
                list[list.size - 1].interpolator = CycleInterpolator(fixedDuring.toFloat() / 1500)
            }
            AnimationConst.EFFECT_SMALL_TO_MIDDLE -> { // 小→中
                list.add(ObjectAnimator.ofFloat(view, "scaleX", 0.5f, 1.0f))
                list.add(ObjectAnimator.ofFloat(view, "scaleY", 0.5f, 1.0f))
            }
            AnimationConst.EFFECT_BIG_TO_MIDDLE -> { // 大→中
                list.add(ObjectAnimator.ofFloat(view, "scaleX", 2.0f, 1.0f))
                list.add(ObjectAnimator.ofFloat(view, "scaleY", 2.0f, 1.0f))
            }
            AnimationConst.EFFECT_MIDDLE_TO_BIG -> { // 中→大
                list.add(ObjectAnimator.ofFloat(view, "scaleX", 1.0f, 2.0f))
                list.add(ObjectAnimator.ofFloat(view, "scaleY", 1.0f, 2.0f))
            }
            AnimationConst.EFFECT_MIDDLE_TO_SMALL -> { // 中→小
                list.add(ObjectAnimator.ofFloat(view, "scaleX", 1.0f, 0.5f))
                list.add(ObjectAnimator.ofFloat(view, "scaleY", 1.0f, 0.5f))
            }
            AnimationConst.EFFECT_ROTATE -> { // 回転
                list.add(ObjectAnimator.ofFloat(view, "rotation", 0f, 360f))
            }
            AnimationConst.EFFECT_ROTATE_SHAKE -> { // 回転（左右繰り返し）
                list.add(ObjectAnimator.ofFloat(view, "rotation", 0f, 5f))
                list[list.size - 1].interpolator = CycleInterpolator(fixedDuring.toFloat() / 10000)
            }
        }
        //指定時間かけてアニメーション
        for (animator in list) {
            animator.duration = fixedDuring
            animator.startDelay = wait
        }
        return list
    }

    /**
     * アニメーションリストを生成する
     * @param activity 呼び出し元のActivity
     * @param effect 画面効果オブジェクト
     * @return 生成したアニメーションリスト
     */
    fun createEffect(activity: Activity, effect: EffectData): List<Animator> {
        val type = effect.type
        val view = effect.view
        val during = effect.during
        val wait = effect.wait
        return createEffect(activity, type, view, during, wait)
    }

    /**
     * 画面効果を適用する
     * @param activity 呼び出し元のActivity
     * @param type 画面効果ID　※AnimationConst参照
     * @param view 対象のビュー
     * @param during アニメーション時間
     * @param wait 開始までの待機時間
     */
    fun execEffect(activity: Activity, type: Int, view: View?, during: Long, wait: Long) {
        //NULLチェック
        if (view == null) {
            return
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return
        }
        //アニメーションの実行
        val set = AnimatorSet()
        set.playTogether(createEffect(activity, type, view, during, wait))
        view.post {
            view.clearAnimation()
            try {
                set.start()
            } catch (e: Exception) {
                LogUtil.debug(e)
            }
        }
    }

    /**
     * 画面効果を適用する
     * @param activity 呼び出し元のActivity
     * @param effect 画面効果オブジェクト
     */
    fun execEffect(activity: Activity, effect: EffectData) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return
        }
        //アニメーション対象の確定
        val v = effect.view
        //アニメーションの実行
        val set = AnimatorSet()
        set.playTogether(createEffect(activity, effect))
        v.post {
            v.clearAnimation()
            try {
                set.start()
            } catch (e: Exception) {
                LogUtil.debug(e)
            }
        }
    }

    /**
     * 画面効果を適用する
     * @param activity 呼び出し元のActivity
     * @param type 画面効果ID　※AnimationConst参照
     * @param views 対象のビューの配列
     * @param during アニメーション時間
     * @param wait 開始までの待機時間
     */
    fun execEffects(activity: Activity, type: Int, views: Array<View>, during: Long, wait: Long) {
        val effects = mutableListOf<EffectData>()
        views.forEach { view ->
            effects.add(EffectData(type, view, during, wait))
        }
        execEffects(activity, effects.toTypedArray())
    }

    /**
     * 画面効果を適用する
     * @param activity 呼び出し元のActivity
     * @param effects 画面効果オブジェクトの配列
     */
    fun execEffects(activity: Activity, effects: Array<EffectData>) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return
        }
        val list = ArrayList<Animator>()
        var parent: View? = null
        //アニメーションオブジェクトごとに処理
        for (i in effects.indices) {
            //アニメーションリストの追加
            list.addAll(createEffect(activity, effects[i]))
            //ビューの取得
            if (effects.size == 1) {
                //単体対象の場合、そのビューに対してPOST
                parent = effects[i].view
            } else if (parent == null) {
                //複数対象の場合、ルートビューに対してPOST
                parent = effects[i].view.rootView
            }
        }
        //アニメーション対象の確定
        val v = parent
        //アニメーションの実行
        v?.post {
            v.clearAnimation()
            val set = AnimatorSet()
            set.playTogether(list)
            try {
                set.start()
            } catch (e: Exception) {
                LogUtil.debug(e)
            }
        }
    }

    /**
     * 画面効果を停止する
     * @param view 対象のビュー
     * @param resetProperties 位置やサイズなどの値を全て元に戻すかのフラグ　※既定値：false
     */
    @JvmOverloads
    fun clearEffect(view: View, resetProperties: Boolean = false) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return
        }
        view.post {
            view.clearAnimation()
            if (resetProperties) {
                try {
                    ObjectAnimator.ofFloat(view, "rotation", 0.0f, 0.0f).start()
                    ObjectAnimator.ofFloat(view, "rotationX", 0.0f, 0.0f).start()
                    ObjectAnimator.ofFloat(view, "rotationY", 0.0f, 0.0f).start()
                    ObjectAnimator.ofFloat(view, "translationX", 0.0f, 0.0f).start()
                    ObjectAnimator.ofFloat(view, "translationY", 0.0f, 0.0f).start()
                    ObjectAnimator.ofFloat(view, "scaleX", 1.0f, 1.0f).start()
                    ObjectAnimator.ofFloat(view, "scaleY", 1.0f, 1.0f).start()
                    //ObjectAnimator.ofFloat(view, "pivotX", 0.5f, 0.5f).start()
                    //ObjectAnimator.ofFloat(view, "pivotY", 0.5f, 0.5f).start()
                    ObjectAnimator.ofFloat(view, "alpha", 1.0f, 1.0f).start()
                    //ObjectAnimator.ofFloat(view, "x", 0.0f, 0.0f).start()
                    //ObjectAnimator.ofFloat(view, "y", 0.0f, 0.0f).start()
                } catch (e: Exception) {
                    LogUtil.debug(e)
                }
            }
        }
    }

    /**
     * アニメーションデータの生成（※１枚絵用）
     * @param imageResId 画像のリソースID
     * @param width 画像の幅
     * @param height 画像の高さ
     * @return 生成したアニメーションデータ
     */
    fun createAnimationObj(imageResId: String, width: Int, height: Int): AnimationData {
        val type = 1 // タイプ
        val loopNum = 4 // 繰り返し回数
        // アニメーションオブジェクトを生成して返す
        return AnimationData(type, imageResId, 0f, 0f, width, height, 0, 0, loopNum, 0, Paint())
    }
}