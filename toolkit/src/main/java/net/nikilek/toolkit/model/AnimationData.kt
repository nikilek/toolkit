package net.nikilek.toolkit.model

import android.graphics.Paint

/**
 * Created by nikilek on 2017/10/16.
 * キャンバスに描画するアニメーションの情報を保持
 * ※紙芝居に使用する画像名の命名規則：{プレフィックス}{任意文字列}{２桁の連番}
 * 　↑プレフィックス＝Settings.String.PREFIX_ANIMATION_FILE
 * 　↑連番の開始番号は任意。最初にAnimationDataに設定したファイル名(連番込)を元に次の番号を探索する。
 * @param type タイプ(0=画像、1=アイコン、2=文字列)
 * @param value 値（画像のファイル名または表示する文字列）
 * @param positionX 表示位置X
 * @param positionY 表示位置Y
 * @param imgWidth 画像幅（type=0,1の場合に使用）
 * @param imgHeight 画像高さ（type=0,1の場合に使用）
 * @param fontSize フォントサイズ（type=2の場合に使用）
 * @param nowCount 繰り返し回数（現在）
 * @param maxCount 繰り返し回数（最大）
 * @param special 特殊効果（※実装にて挙動を決定）
 * @param paint 描画に用いるペイントオブジェクト
 */
data class AnimationData(
        var type: Int = 0, //タイプ(0=画像、1=アイコン、2=文字列)
        var value: String = "", //値（画像のファイル名または表示する文字列）
        var positionX: Float = 0f, //表示位置X
        var positionY: Float = 0f, //表示位置Y
        var imgWidth: Int = 0, //画像幅
        var imgHeight: Int = 0, //画像高さ
        var fontSize: Int = 0, //フォントサイズ
        var nowCount: Int = 0, //繰り返し回数（現在）
        var maxCount: Int = 0, //繰り返し回数（最大）
        var special: Int = 0, //特殊効果（※実装にて挙動を決定）
        var paint: Paint = Paint().apply { isFilterBitmap = true } //描画に用いるペイントオブジェクト
) {
    companion object {
        /* タイプ */
        const val TYPE_IMAGE = 0 //画像
        const val TYPE_ICON = 1 //アイコン
        const val TYPE_TEXT = 2 //文字列
    }
}