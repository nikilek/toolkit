package net.nikilek.toolkit.model

import android.view.View

/**
 * Created by nikilek on 2017/10/16.
 * @param type エフェクトの種類　※AnimationConst参照
 * @param view エフェクトを適用する対象のビュー
 * @param during 動作時間（ミリ秒）
 * @param wait 動作を開始するまでの待機時間（ミリ秒）
 */
data class EffectData(var type: Int = 0, var view: View, var during: Long = 0, var wait: Long = 0)
