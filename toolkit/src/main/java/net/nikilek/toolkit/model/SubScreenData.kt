package net.nikilek.toolkit.model

import android.view.View
import net.nikilek.toolkit.AbstractSubScreen
import net.nikilek.toolkit.constant.AnimationConst

/**
 * Created by nikilek on 2017/10/16.
 * AbstractSubScreen系の情報を保持
 * @param parentScreen 親スクリーン（当該サブスクリーンの親要素となるスクリーンの情報）
 * @param layoutId レイアウトID（当該サブスクリーンのレイアウトのリソースID）
 * @param windowAnimationTarget ウィンドウアニメーションの対象とするビューのID（0：ウィンドウ全体、-1：アニメーション無し）
 * @param animationPivot ウィンドウアニメーションの中心（AnimationConst.PIVOT_xxx）
 * @param createdView 生成したビューの参照（レイアウトIDを元に生成したビュー）
 * @param createdTime 生成時刻（ビューを生成した時刻）
 * @param enableBack バックキー有効／無効（バックキーで当該サブスクリーンを閉じられるか）
 * @param childSubScreens 配下のサブスクリーンの一覧（parentScreenと当該SubScreenの両方に紐付く）
 */
data class SubScreenData(
        val parentScreen: ScreenData, //親スクリーン
        val layoutId: Int = 0, //レイアウトID
        val windowAnimationTarget: Int = 0, //ウィンドウアニメーションの対象とするビューのID
        val animationPivot: Int = AnimationConst.PIVOT_CENTER, //ウィンドウアニメーションの中心
        val createdView: View, //生成したビューの参照
        val createdTime: Long = 0L, //生成時刻
        val enableBack: Boolean = true, //バックキー有効／無効
        val childSubScreens: MutableList<SubScreenData> = mutableListOf() //配下のサブスクリーン一覧
) {
    /**
     * サブスクリーンの参照を取得
     */
    @Suppress("UNCHECKED_CAST")
    fun <T : AbstractSubScreen> mSubScreen(): T {
        return createdView as T
    }

    /**
     * 配下のサブスクリーンを再帰的に取得
     * ※childSubScreensは当該SubScreenの直接の配下のみであり、
     * ※本メソッドでは、配下のまた配下まで、全てを取得する
     */
    fun getAllChildSubScreen(): MutableList<SubScreenData> {
        val list = mutableListOf<SubScreenData>()
        this.childSubScreens.forEach { ss ->
            list.add(ss)
            list.addAll(ss.getAllChildSubScreen())
        }
        return list
    }
}