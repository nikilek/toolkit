package net.nikilek.toolkit.model

import net.nikilek.toolkit.interfaces.ISaveData

/**
 * Created by nikilek on 2017/10/16.
 *
 * 空のセーブデータ
 *
 * ※このクラスは、セーブデータの読込失敗時にのみ使用します
 * ※アプリで実際に使用するものは、{net.nikilek.toolkit._sample.SaveData}を参考に、別途作成してください
 */
data class EmptySaveData(
        var id: Long = 0 //ID
) : ISaveData {

    /**
     * プロパティ
     */
    override val isEmpty: Boolean
        get() = true

    /**
     * メソッド
     */
    override fun valid() {
        //何もしない
    }

    override fun clone(): ISaveData {
        return this.copy()
    }
}
