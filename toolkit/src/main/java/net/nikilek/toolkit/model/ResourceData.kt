package net.nikilek.toolkit.model

/**
 * Created by nikilek on 2017/10/16.
 * リソースIDとファイル名のペアを保持
 * @param resId リソースID
 * @param resFileName ファイル名
 */
data class ResourceData(var resId : Int = 0, var resFileName : String = "")