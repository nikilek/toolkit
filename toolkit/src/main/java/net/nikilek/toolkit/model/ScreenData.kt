package net.nikilek.toolkit.model

import android.view.View
import net.nikilek.toolkit.AbstractScreen

/**
 * Created by nikilek on 2017/10/16.
 * AbstractScreen系の情報を保持
 * @param screenId 当該データの識別子（呼び出し時に採番）
 * @param layoutId レイアウトID（当該スクリーンのレイアウトのリソースID）
 * @param createdView 生成したビューの参照（レイアウトIDを元に生成したビュー）
 * @param createdTime 生成時刻（ビューを生成した時刻）
 * @param enableBack バックキー有効／無効（バックキーで当該スクリーンを閉じられるか）
 * @param subScreens サブスクリーン一覧（当該スクリーンの子となるサブスクリーンのリスト）
 */
data class ScreenData(
        val screenId: Int = 0, //当該データの識別子
        val layoutId: Int = 0, //レイアウトID
        val createdView: View, //生成したビューの参照
        val createdTime: Long = 0L, //生成時刻
        val enableBack: Boolean = true, //バックキー有効／無効
        val subScreens: MutableList<SubScreenData> = mutableListOf() //サブスクリーン一覧
) {
    /**
     * スクリーンの参照を取得
     */
    @Suppress("UNCHECKED_CAST")
    fun <T : AbstractScreen> mScreen(): T {
        return createdView as T
    }
}