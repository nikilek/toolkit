package net.nikilek.toolkit.model

/**
 * Created by nikilek on 2017/10/16.
 * リストに表示する情報を保持
 * @param name 表示用文字列
 * @param value 内部に保持する値
 * @param option オプション（呼び出し元で任意に定義）
 */
data class ListData(
    var name: String = "", //表示用文字列
    var value: Int = 0, //内部に保持する値
    var option: Any? = null //オプション（呼び出し元で任意に定義）
)