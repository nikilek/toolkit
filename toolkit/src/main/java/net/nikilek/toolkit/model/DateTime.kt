package net.nikilek.toolkit.model

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

/**
 * Created by nikilek on 2016/01/25.
 * Timestampの拡張クラス
 * ・日本時間専用（国際化未対応）
 * ・最新の日付ライブラリには未対応
 */
class DateTime : java.sql.Timestamp {

    private var cal: Calendar? = null //メモ：必ずプロパティ参照する。あと、これをいじったらsetTime()もして同期を取る。

    val timeMillis: Long //ミリ秒
        get() = time

    val calendar: Calendar //カレンダークラス
        get() {
            if (cal !is Calendar) {
                cal = Calendar.getInstance()
            }
            cal!!.timeInMillis = this.timeMillis
            return cal!!
        }

    /**
     * コンストラクタ
     * @param millis 時刻（ミリ秒）
     */
    constructor(millis: Long) : super(millis)

    /**
     * コンストラクタ
     * @param date 時刻（java.util.Date）
     */
    constructor(date: java.util.Date) : super(date.time)

    /**
     * コンストラクタ
     * @param date 時刻（java.sql.Date）
     */
    constructor(date: java.sql.Date) : super(date.time)

    /**
     * コンストラクタ
     * @param time 時刻（java.sql.Time）
     */
    constructor(time: java.sql.Time) : super(time.time)

    /**
     * コンストラクタ
     * @param timestamp 時刻（java.sql.Timestamp）
     */
    constructor(timestamp: java.sql.Timestamp) : super(timestamp.time)

    @Throws(ParseException::class)
    constructor(date: String, format: String) : super(SimpleDateFormat(format, Locale(LANGUAGE, COUNTRY)).parse(date)!!.time)

    /**
     * java.util.Dateへの変換
     * @return 変換後の値
     */
    fun toUtilDate(): java.util.Date {
        return java.util.Date(time)
    }

    /**
     * java.sql.Dateへの変換
     * @return 変換後の値
     */
    fun toSqlDate(): java.sql.Date {
        val cal = Calendar.getInstance()
        cal.timeInMillis = time
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        return java.sql.Date(cal.timeInMillis)
    }

    /**
     * java.sql.Timeへの変換
     * @return 変換後の値
     */
    fun toSqlTime(): java.sql.Time {
        val cal = Calendar.getInstance()
        cal.timeInMillis = time
        cal.set(Calendar.YEAR, 1970)
        cal.set(Calendar.MONTH, Calendar.JANUARY)
        cal.set(Calendar.DATE, 1)
        return java.sql.Time(cal.timeInMillis)
    }

    /**
     * java.sql.Timestampへの変換
     * @return 変換後の値
     */
    fun toSqlTimestamp(): java.sql.Timestamp {
        return java.sql.Timestamp(time)
    }

    /**
     * 文字列への変換
     * @param format フォーマット
     * @return 日付文字列
     */
    fun toString(format: String): String {
        return SimpleDateFormat(format, Locale(LANGUAGE, COUNTRY)).format(this)
    }

    /*
     * 下位精度切り捨ての例：(1999/01/02 01:00:00 - 1999/01/01 23:30:00) ⇒ 0日、1時間、150分　★こっち
     * 下位精度切り上げの例：(1999/01/02 01:00:00 - 1999/01/01 23:30:00) ⇒ 1日、2時間、150分
     */
    /**
     * 指定日時からの経過時間（ミリ秒単位）を取得（下位精度：切り捨て）
     * @param dateTime 比較対象の日時
     * @return 経過ミリ秒数
     */
    fun millisFrom(dateTime: DateTime): Long {
        return this.timeMillis - dateTime.timeMillis
    }

    /**
     * 指定日時からの経過時間（秒単位）を取得（下位精度：切り捨て）
     * @param dateTime 比較対象の日時
     * @return 経過秒数
     */
    fun secondsFrom(dateTime: DateTime): Int {
        return millisFrom(dateTime).toInt() / UNIT_SECONDS
    }

    /**
     * 指定日時からの経過時間（分単位）を取得（下位精度：切り捨て）
     * @param dateTime 比較対象の日時
     * @return 経過分数
     */
    fun minutesFrom(dateTime: DateTime): Int {
        return millisFrom(dateTime).toInt() / UNIT_MINUTES
    }

    /**
     * 指定日時からの経過時間（時単位）を取得（下位精度：切り捨て）
     * @param dateTime 比較対象の日時
     * @return 経過時数
     */
    fun hoursFrom(dateTime: DateTime): Int {
        return millisFrom(dateTime).toInt() / UNIT_HOURS
    }

    /**
     * 指定日時からの経過時間（日単位）を取得（下位精度：切り捨て）
     * @param dateTime 比較対象の日時
     * @return 経過日数
     */
    fun daysFrom(dateTime: DateTime): Int {
        return millisFrom(dateTime).toInt() / UNIT_DAYS
    }

    /**
     * 指定日時からの経過時間（月単位）を取得（下位精度：切り捨て）
     * @param dateTime 比較対象の日時
     * @return 経過月数
     */
    fun monthsFrom(dateTime: DateTime): Int {
        val cal1 = Calendar.getInstance()
        cal1.timeInMillis = this.timeMillis
        val cal2 = Calendar.getInstance()
        cal2.timeInMillis = dateTime.timeMillis
        var count = 0
        if (cal1.before(cal2)) {
            while (cal1.before(cal2)) {
                cal1.add(Calendar.MONTH, 1)
                count--
            }
        } else {
            count--
            while (!cal1.before(cal2)) {
                cal1.add(Calendar.MONTH, -1)
                count++
            }
        }
        return count
    }

    /**
     * 指定日時からの経過時間（年単位）を取得（下位精度：切り捨て）
     * @param dateTime 比較対象の日時
     * @return 経過年数
     */
    fun yearsFrom(dateTime: DateTime): Int {
        return monthsFrom(dateTime) / 12
    }

    /*
     * 下位精度切り捨ての例：(1999/01/02 01:00:00 - 1999/01/01 23:30:00) ⇒ 0日、1時間、150分
     * 下位精度切り上げの例：(1999/01/02 01:00:00 - 1999/01/01 23:30:00) ⇒ 1日、2時間、150分　★こっち
     */
    /**
     * 指定日時からの経過時間（ミリ秒単位）を取得（下位精度：切り上げ）
     * @param dateTime 比較対象の日時
     * @return 経過ミリ秒数
     */
    fun totalMillisFrom(dateTime: DateTime): Long {
        return this.timeMillis - dateTime.timeMillis
    }

    /**
     * 指定日時からの経過時間（秒単位）を取得（下位精度：切り上げ）
     * @param dateTime 比較対象の日時
     * @return 経過秒数
     */
    fun totalSecondsFrom(dateTime: DateTime): Long {
        val to = floorMillis(this.timeMillis)
        val fm = floorMillis(dateTime.timeMillis)
        return (to - fm) / UNIT_SECONDS
    }

    /**
     * 指定日時からの経過時間（分単位）を取得（下位精度：切り上げ）
     * @param dateTime 比較対象の日時
     * @return 経過分数
     */
    fun totalMinutesFrom(dateTime: DateTime): Int {
        val to = floorSeconds(this.timeMillis)
        val fm = floorSeconds(dateTime.timeMillis)
        return ((to - fm) / UNIT_MINUTES).toInt()
    }

    /**
     * 指定日時からの経過時間（時単位）を取得（下位精度：切り上げ）
     * @param dateTime 比較対象の日時
     * @return 経過時数
     */
    fun totalHoursFrom(dateTime: DateTime): Int {
        val to = floorMinutes(this.timeMillis)
        val fm = floorMinutes(dateTime.timeMillis)
        return ((to - fm) / UNIT_HOURS).toInt()
    }

    /**
     * 指定日時からの経過時間（日単位）を取得（下位精度：切り上げ）
     * @param dateTime 比較対象の日時
     * @return 経過日数
     */
    fun totalDaysFrom(dateTime: DateTime): Int {
        val to = floorHours(this.timeMillis)
        val fm = floorHours(dateTime.timeMillis)
        return ((to - fm) / UNIT_DAYS).toInt()
    }

    /**
     * 指定日時からの経過時間（月単位）を取得（下位精度：切り上げ）
     * @param dateTime 比較対象の日時
     * @return 経過月数
     */
    fun totalMonthsFrom(dateTime: DateTime): Int {
        val cal1 = Calendar.getInstance()
        cal1.timeInMillis = floorHours(this.timeMillis)
        cal1.set(Calendar.DATE, 1)
        val cal2 = Calendar.getInstance()
        cal2.timeInMillis = floorHours(dateTime.timeMillis)
        cal2.set(Calendar.DATE, 1)
        var count = 0
        if (cal1.before(cal2)) {
            while (cal1.before(cal2)) {
                cal1.add(Calendar.MONTH, 1)
                count--
            }
        } else {
            count--
            while (!cal1.before(cal2)) {
                cal1.add(Calendar.MONTH, -1)
                count++
            }
        }
        return count
    }

    /**
     * 指定日時からの経過時間（年単位）を取得
     * @param dateTime 比較対象の日時
     * @return 経過年数
     */
    fun totalYearsFrom(dateTime: DateTime): Int {
        val cal1 = Calendar.getInstance()
        cal1.timeInMillis = floorHours(this.timeMillis)
        cal1.set(Calendar.DATE, 1)
        cal1.set(Calendar.MONTH, 1)
        val cal2 = Calendar.getInstance()
        cal2.timeInMillis = floorHours(dateTime.timeMillis)
        cal2.set(Calendar.DATE, 1)
        cal2.set(Calendar.MONTH, 1)
        var count = 0
        if (cal1.before(cal2)) {
            while (cal1.before(cal2)) {
                cal1.add(Calendar.YEAR, 1)
                count--
            }
        } else {
            count--
            while (!cal1.before(cal2)) {
                cal1.add(Calendar.YEAR, -1)
                count++
            }
        }
        return count
    }

    /**
     * 年の取得
     * @return 年
     */
    fun year(): Int {
        return this.calendar.get(Calendar.YEAR)
    }

    /**
     * 月の取得
     * @return 月
     */
    fun month(): Int {
        return this.calendar.get(Calendar.MONTH) + 1
    }

    /**
     * 日の取得
     * @return 日
     */
    fun day(): Int {
        return this.calendar.get(Calendar.DATE)
    }

    /**
     * 時の取得
     * @return 時
     */
    fun hour(): Int {
        return this.calendar.get(Calendar.HOUR_OF_DAY)
    }

    /**
     * 分の取得
     * @return 分
     */
    fun minute(): Int {
        return this.calendar.get(Calendar.MINUTE)
    }

    /**
     * 秒の取得
     * @return 秒
     */
    fun second(): Int {
        return this.calendar.get(Calendar.SECOND)
    }

    /**
     * ミリ秒の取得
     * @return ミリ秒
     */
    fun millisecond(): Int {
        return this.calendar.get(Calendar.MILLISECOND)
    }

    /**
     * 週の取得
     * @return 週
     */
    fun week(): Int {
        return this.calendar.get(Calendar.DAY_OF_WEEK) - 1
    }

    /**
     * 日（年起点）の取得
     * @return 日（年起点）
     */
    fun dayOfYear(): Int {
        return this.calendar.get(Calendar.DAY_OF_YEAR)
    }

    /**
     * 日（月起点）の取得
     * @return 日（月起点）
     */
    fun dayOfMonth(): Int {
        return this.calendar.get(Calendar.DAY_OF_MONTH)
    }

    /**
     * 日（週起点）の取得
     * @return 日（週起点）
     */
    fun dayOfWeek(): Int {
        return this.calendar.get(Calendar.DAY_OF_WEEK)
    }

    /**
     * 曜日の取得
     * @return 曜日
     */
    fun dayOfWeekStr(): String {
        return if (LANGUAGE === "ja") {
            arrayOf("日", "月", "火", "水", "木", "金", "土")[dayOfWeek() - 1]
        } else ""
    }

    /**
     * 年の設定
     * @param year 新しい年
     */
    fun year(year: Int) {
        val cal = this.calendar
        cal.set(Calendar.YEAR, year)
        this.time = cal.timeInMillis
    }

    /**
     * 月の設定
     * @param month 新しい月
     */
    fun month(month: Int) {
        val cal = this.calendar
        cal.set(Calendar.MONTH, month - 1)
        this.time = cal.timeInMillis
    }

    /**
     * 日の設定
     * @param day 新しい日
     */
    fun day(day: Int) {
        val cal = this.calendar
        cal.set(Calendar.DATE, day)
        this.time = cal.timeInMillis
    }

    /**
     * 時の設定
     * @param hour 新しい時
     */
    fun hour(hour: Int) {
        val cal = this.calendar
        cal.set(Calendar.HOUR_OF_DAY, hour)
        this.time = cal.timeInMillis
    }

    /**
     * 分の設定
     * @param minute 新しい分
     */
    fun minute(minute: Int) {
        val cal = this.calendar
        cal.set(Calendar.MINUTE, minute)
        this.time = cal.timeInMillis
    }

    /**
     * 秒の設定
     * @param second 新しい秒
     */
    fun second(second: Int) {
        val cal = this.calendar
        cal.set(Calendar.SECOND, second)
        this.time = cal.timeInMillis
    }

    /**
     * ミリ秒の設定
     * @param millisecond 新しいミリ秒
     */
    fun millisecond(millisecond: Int) {
        val cal = this.calendar
        cal.set(Calendar.MILLISECOND, millisecond)
        this.time = cal.timeInMillis
    }

    /**
     * 年の加算
     * @param d 加算値
     */
    fun addYear(d: Int) {
        val cal = this.calendar
        cal.add(Calendar.YEAR, d)
        this.time = cal.timeInMillis
    }

    /**
     * 月の加算
     * @param d 加算値
     */
    fun addMonth(d: Int) {
        val cal = this.calendar
        cal.add(Calendar.MONTH, d)
        this.time = cal.timeInMillis
    }

    /**
     * 日の加算
     * @param d 加算値
     */
    fun addDay(d: Int) {
        val cal = this.calendar
        cal.add(Calendar.DATE, d)
        this.time = cal.timeInMillis
    }

    /**
     * 時の加算
     * @param d 加算値
     */
    fun addHour(d: Int) {
        val cal = this.calendar
        cal.add(Calendar.HOUR_OF_DAY, d)
        this.time = cal.timeInMillis
    }

    /**
     * 分の加算
     * @param d 加算値
     */
    fun addMinute(d: Int) {
        val cal = this.calendar
        cal.add(Calendar.MINUTE, d)
        this.time = cal.timeInMillis
    }

    /**
     * 秒の加算
     * @param d 加算値
     */
    fun addSecond(d: Int) {
        val cal = this.calendar
        cal.add(Calendar.SECOND, d)
        this.time = cal.timeInMillis
    }

    /**
     * ミリ秒の加算
     * @param d 加算値
     */
    fun addMillisecond(d: Int) {
        val cal = this.calendar
        cal.add(Calendar.MILLISECOND, d)
        this.time = cal.timeInMillis
    }

    companion object {

        const val LANGUAGE = "ja" //言語コード
        const val COUNTRY = "JP" //地域コード

        const val UNIT_SECONDS = 1000 //秒の単位（ミリ秒基準）
        const val UNIT_MINUTES = UNIT_SECONDS * 60 //分の単位（ミリ秒基準）
        const val UNIT_HOURS = UNIT_MINUTES * 60 //時の単位（ミリ秒基準）
        const val UNIT_DAYS = UNIT_HOURS * 24 //日の単位（ミリ秒基準）

        /**
         * ミリ秒以下を切り捨てた時刻を取得
         * @param millis 操作対象の時刻
        　　*/
        fun floorMillis(millis: Long): Long {
            val dt = DateTime(millis)
            dt.millisecond(0)
            return dt.timeMillis
        }

        /**
         * 秒以下を切り捨てた時刻を取得
         * @param millis 操作対象の時刻
         */
        fun floorSeconds(millis: Long): Long {
            val dt = DateTime(millis)
            dt.millisecond(0)
            dt.second(0)
            return dt.timeMillis
        }

        /**
         * 分以下を切り捨てた時刻を取得
         * @param millis 操作対象の時刻
         */
        fun floorMinutes(millis: Long): Long {
            val dt = DateTime(millis)
            dt.millisecond(0)
            dt.second(0)
            dt.minute(0)
            return dt.timeMillis
        }

        /**
         * 時以下を切り捨てた時刻を取得
         * @param millis 操作対象の時刻
         */
        fun floorHours(millis: Long): Long {
            val dt = DateTime(millis)
            dt.millisecond(0)
            dt.second(0)
            dt.minute(0)
            dt.hour(0)
            return dt.timeMillis
        }
    }
}
