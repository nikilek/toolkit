package net.nikilek.toolkit

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.*
import java.util.ArrayList

import android.view.SurfaceHolder
import android.view.SurfaceView
import android.graphics.Bitmap.Config
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

import net.nikilek.toolkit.utility.ViewUtil
import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.model.AnimationData
import net.nikilek.toolkit.utility.LogUtil
import net.nikilek.toolkit.utility.ThreadUtil
import java.lang.ref.WeakReference
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Created by nikilek on 2016/01/27.
 * カスタムビュー
 * 描画情報オブジェクトで描画内容を制御
 */
@SuppressLint("ViewConstructor")
open class MySurfaceView : SurfaceView, SurfaceHolder.Callback, Runnable {
    private var activity: Activity //呼び出し元のアクティビティ
    private var svHolder: SurfaceHolder //サーフェイスホルダー
    private var viewUtil: ViewUtil //描画処理用汎用クラス
    private var thread: Thread? = null //描画スレッド
    private var isAttached: Boolean = false //アタッチ済みかのフラグ
    /* 描画情報 */
    private var screenWidth: Float = 0.toFloat() //画面の幅
    private var screenHeight: Float = 0.toFloat() //画面の幅
    private val animationList = mutableListOf<AnimationData>() //描画情報オブジェクトのリスト
    private val listLock = ReentrantLock() //リスト追加／削除ロックオブジェクト
    private var mBitmap: Bitmap? = null // ダブルバッファ用ビットマップ

    /**
     * 描画情報のリストを取得する
     * @return
     */
    val animationObject: List<AnimationData>
        get() {
            listLock.withLock {
                return animationList
            }
        }

    /**
     * コンストラクタ
     * @param activity 呼び出し元のアクティビティ
     * @param sv 対象のビュー（オーバーレイ表示する場合に指定）※既定値：null
     * @param viewUtil 描画処理用汎用クラス（R.drawable()を適切に設定する必要あり）
     */
    @Suppress("ConvertSecondaryConstructorToPrimary")
    constructor(activity: Activity, sv: SurfaceView? = null, viewUtil: ViewUtil) : super(activity) {
        //パラメータの取得
        this.activity = activity
        if (sv != null) {
            svHolder = sv.holder
            sv.setZOrderOnTop(true) //最前面にする
        } else {
            svHolder = holder
        }
        this.viewUtil = viewUtil
        // 半透明を設定
        svHolder.setFormat(PixelFormat.TRANSLUCENT)
        // コールバック登録
        GlobalScope.launch {
            svHolder.addCallback(WeakReference(this@MySurfaceView).get())
        }
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        //幅・高さを取得
        screenWidth = width.toFloat()
        screenHeight = height.toFloat()
        // オフスクリーン用のBitmapを生成する
        mBitmap?.recycle()
        mBitmap = Bitmap.createBitmap(screenWidth.toInt(), screenHeight.toInt(), Config.ARGB_8888)
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        //アタッチ済みフラグON
        isAttached = true
        //スレッドの開始
        thread = Thread(this)
        thread?.isDaemon = true
        thread?.start()
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        //アタッチ済みフラグOFF
        isAttached = false
        //スレッドの終了を待機
        while (thread != null && thread!!.isAlive);
        //Bitmapを解放
        mBitmap?.recycle()
    }

    /**
     * 描画情報リストの追加
     * @param animationObject 描画情報を保持するオブジェクト
     * @return 追加後のリストのサイズ
     */
    fun addAnimationList(animationObject: AnimationData): Int {
        listLock.withLock {
            animationList.add(animationObject)
            return animationList.size
        }
    }

    /**
     * 描画情報リストから指定インデックスの要素を削除
     * @param position 削除対象の位置（負の値の場合、全削除）
     * @return 削除後のリストのサイズ
     */
    fun removeAnimationList(position: Int): Int {
        listLock.withLock {
            if (position < 0) {
                animationList.clear()
            } else if (position < animationList.size) {
                animationList.removeAt(position)
            }
            return animationList.size
        }
    }

    /**
     * 描画情報リストの定期更新　※描画期間を過ぎた要素は削除する
     * ※紙芝居に使用する画像名の命名規則：{プレフィックス}{任意文字列}{２桁の連番}
     * 　↑プレフィックス＝Settings.String.PREFIX_ANIMATION_FILE
     * 　↑連番の開始番号は任意。最初にAnimationDataに設定したファイル名(連番込)を元に次の番号を探索する。
     */
    private fun updateAnimationList() {
        listLock.withLock {
            val delList = ArrayList<AnimationData>() // 削除対象のリスト
            //リストの各オブジェクトについて処理
            for (obj in animationList) {
                //繰り返し回数の更新
                obj.nowCount = obj.nowCount + 1
                //繰り返し回数の上限判定
                if (obj.nowCount >= obj.maxCount) {
                    //削除対象に追加
                    delList.add(obj)
                    continue
                }
                //画像ファイル名の更新（複数画像で紙芝居をする場合に使用）
                if (obj.type == AnimationData.TYPE_IMAGE || obj.type == AnimationData.TYPE_ICON) {
                    val oldFileName = obj.value // 現在のファイル名
                    val nowCount = obj.nowCount // 現在の繰り返し回数
                    //連番の更新
                    val lenPrefix = Settings.String.PREFIX_ANIMATION_FILE.length //プレフィックスの文字長
                    if (oldFileName.length > (lenPrefix + 1)
                            && oldFileName.substring(0, lenPrefix) == Settings.String.PREFIX_ANIMATION_FILE) {
                        val rawFileName = oldFileName.substring(0, oldFileName.length - 2) // 連番部分を除いたファイル名
                        val newFileName = if (nowCount + 1 < 10) {
                            rawFileName + "0" + (nowCount + 1)
                        } else {
                            rawFileName + (nowCount + 1)
                        }
                        obj.value = newFileName //連番を更新したファイル名を設定
                    }
                }
            }
            // 削除対象のオブジェクトをすべて削除する
            animationList.removeAll(delList)
        }
    }

    override fun run() {
        val startTime = System.currentTimeMillis()
        var frameCnt: Long = 0
        while (isAttached) {
            //フレーム数の更新
            frameCnt++
            if (System.currentTimeMillis() - startTime > frameCnt * Settings.Interval.FRAME_SKIP_ANIMATION) {
                //フレームスキップ
                if (!ThreadUtil.sleep(Settings.Interval.FRAME_SKIP_ANIMATION / 2)) {
                    break
                }
                continue
            }
            //描画実行
            draw()
            //描画リストの更新
            updateAnimationList()
            //次のフレームまで待機
            val wait = frameCnt * Settings.Interval.FRAME_SKIP_ANIMATION - (System.currentTimeMillis() - startTime) //スリープ時間の取得
            if (wait > 0 && !ThreadUtil.sleep(wait)) {
                break
            }
        }
    }

    /**
     * 描画の実行
     */
    @Synchronized
    private fun draw() {
        var canvas: Canvas? = null
        val tmpAnimationList = animationList.toList() //アニメーションをコピーして使用
        try {
            //オフスクリーンバッファに描画
            if (mBitmap != null) {
                val offScreen = Canvas(mBitmap!!)
                offScreen.drawColor(0, PorterDuff.Mode.CLEAR) //一度クリア
                //各描画情報について、描画を実行
                for (obj in tmpAnimationList) {
                    drawBmp(offScreen, obj)
                }
                //キャンバスに描画
                canvas = svHolder.lockCanvas()
                canvas?.drawColor(0, PorterDuff.Mode.CLEAR) //一度クリア
                canvas?.drawBitmap(mBitmap!!, 0f, 0f, null) //オフスクリーンバッファからコピー
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        } finally {
            // キャンバスの解放
            if (canvas != null) {
                svHolder.unlockCanvasAndPost(canvas)
            }
        }
    }

    /**
     * 描画情報に従って描画を実行
     * @param canvas 描画先
     * @param data 描画情報
     */
    private fun drawBmp(canvas: Canvas, data: AnimationData) {
        if (data.type == AnimationData.TYPE_IMAGE || data.type == AnimationData.TYPE_ICON) {
            //画像の描画
            val bmp = viewUtil.getBitmap(data.value, data.imgWidth, data.imgHeight)
                    ?: return //画像の取得
            val src = Rect(0, 0, bmp.width, bmp.height) //元画像の領域
            //リサイズ画像の領域を取得
            val dst = Rect(0, 0, data.imgWidth, data.imgHeight)
            dst.offset(data.positionX.toInt(), data.positionY.toInt())
            //画像をリサイズしてCanvasに描画
            canvas.drawBitmap(bmp, src, dst, data.paint)
        } else {
            //文字列の描画
            data.paint.textSize = data.fontSize.toFloat() //文字サイズの設定
            canvas.drawText(data.value, data.positionX, data.positionY, data.paint)
        }
    }
}