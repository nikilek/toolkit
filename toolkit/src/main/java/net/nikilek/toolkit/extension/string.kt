package net.nikilek.toolkit.extension

import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.utility.CheckUtil
import java.net.URLEncoder
import java.util.regex.Pattern

/**
 * Created by nikilek on 2018/02/03.
 * String用の拡張関数
 */

/**
 * 文字列の結合
 * @param delimiter 文字列と文字列の間に付与する文字列
 * @param args 結合する文字列の配列
 * @return 結合後の文字列
 */
fun String.join(delimiter: String, vararg args: String): String {
    var ret = this
    args.forEach {
        ret += delimiter.plus(it)
    }
    return ret
}

/**
 * 文字列の結合（空白の文字列は無視する）
 * @param delimiter 文字列と文字列の間に付与する文字列
 * @param args 結合する文字列の配列
 * @return 結合後の文字列
 */
fun String.joinNotEmpty(delimiter: String, vararg args: String): String {
    var ret = this
    args.forEach {
        if (it.isEmpty()) {
            return@forEach
        }
        if (ret.isEmpty()) {
            ret = it
        } else {
            ret += delimiter.plus(it)
        }
    }
    return ret
}

/**
 * 改行付き加算
 * @param line 加算する文字列
 * @param lineSeparator 改行文字　※既定値：Settings.String.SEPARATOR
 * @return 加算後の文字列
 */
fun String.addNewLine(line: String, lineSeparator: String = Settings.String.SEPARATOR): String {
    return if (this.isEmpty()) {
        line
    } else {
        this.plus(lineSeparator).plus(line)
    }
}

/**
 * この文字列が、別のある文字列の集合の中に含まれるかを判定
 * @param args 文字列の集合
 * @return 集合に含まれるか否か
 */
fun String.isIn(vararg args: String): Boolean {
    return args.contains(this)
}

/**
 * 指定されたプリミティブ型（配列を除く）に変換する
 * ※Charには対応、CharArrayには非対応
 * @param cls プリミティブ型のJavaクラス
 * @return 変換後のオブジェクト
 */
fun String.toPrimitive(cls: Class<*>): Any {
    @Suppress("IMPLICIT_CAST_TO_ANY")
    return when (cls.simpleName) {
        "long" -> this.toLong()
        "int" -> this.toInt()
        "short" -> this.toShort()
        "byte" -> this.toByte()
        "double" -> this.toDouble()
        "float" -> this.toFloat()
        "boolean" -> this.toBoolean()
        "char" -> if (this.isEmpty()) '\u0020' else this[0]
        else -> this
    }
}

/**
 * Intに変換
 * @param defaultValue 変換失敗時に返却する値
 * @return 変換後の値またはデフォルト値
 */
fun String.toIntOrDefault(defaultValue: Int): Int {
    if (CheckUtil.isNumber(this)) {
        return try {
            java.lang.Double.parseDouble(this).toInt()
        } catch (e: Exception) {
            defaultValue
        }
    }
    return defaultValue
}

/**
 * Booleanに変換　※ "1"=true, "0"=false と見なす
 * @return 変換後の値
 */
fun String.toBooleanByNumber(): Boolean {
    return when {
        "1" == this -> true
        "0" == this -> false
        else -> java.lang.Boolean.parseBoolean(this)
    }
}

/**
 * 文字列に含まれる半角数字を全角数字に変換する
 * @return 変換後文字列
 */
fun String.halfWidthNumberToFullWidthNumber(): String {
    val sb = StringBuffer(this)
    for (i in 0 until this.length) {
        val c = this[i]
        if (c in '0'..'9') {
            sb.setCharAt(i, (c - '0' + '０'.toInt()).toChar())
        }
    }
    return sb.toString()
}

/**
 * 文字列の表示幅を取得
 * @return 文字列の表示幅
 */
fun String.ems(): Int {
    var ems = 0
    this.asSequence().forEach {
        ems += it.ems()
    }
    return ems
}

/**
 * 文字列を指定の表示幅（全角＝２、半角＝１の合計）にする（指定幅以降は削除）
 * @return 変換後文字列
 */
fun String.substringByEms(maxEms: Int): String {
    var ret = ""
    var tmpEms = 0
    for (i in 0 until this.length) {
        val c = this[i]
        tmpEms += c.ems()
        if (tmpEms <= maxEms) {
            ret += this.substring(i, i + 1)
        }
    }
    return ret
}

/**
 * 文字列にマルチバイト文字を含むかを判定する
 * @return true：マルチバイト文字を含む、false：含まない
 */
fun String.containsMultiByte(): Boolean {
    this.asSequence().forEach {
        if (it.ems() > 1) {
            return true
        }
    }
    return false
}

/**
 * 文字列の置換（”非”正規表現置換）
 * @return 変換後文字列
 */
fun String.replaceAll(target: String, replacement: String): String {
    return this.replace(Pattern.quote(target).toRegex(), replacement)
}

/**
 * 空白文字のみの場合に、指定文字に置換（元の文字列の長さ分、指定文字を設定）
 * @param replacement 禁止文字の置換先の文字列（例：「*」「?」）
 * @return 変換後文字列
 */
fun String.replaceAllIfBlank(replacement: String): String {
    var ret = this
    if (this.isNotEmpty() && this.isBlank()) {
        ret = ""
        repeat(this.length) { ret += replacement }
    }
    return ret
}

/**
 * 禁止文字を指定文字に置換する
 * ◇既定で許可（JIS X 0208 準拠）
 * ・半角英数字
 * ・全角かな
 * ・全角カナ
 * ・JIS第1水準漢字
 * ・JIS第2水準漢字
 * ◇引数で可否を指定
 * ・全角英数字
 * ・半角カナ
 * ・記号（JIS 1区）
 * ・記号（JIS 2区）
 * ・ギリシャ文字（JIS 6区）
 * ・キリル文字（JIS 7区）
 * ・罫線（JIS 8区）
 * @param replacement 禁止文字の置換先の文字列（例：「*」「?」）
 * @param permitHalfWidthKANA 半角カナを許可するかのフラグ
 * @param permitHalfWidthSymbol 半角記号（ASCII準拠）を許可するかのフラグ
 * @param permitFullWidthNumericAndAlphabet 全角英数字を許可するかのフラグ
 * @param permitFullWidthSymbol1 記号（JIS 1区）を許可するかのフラグ
 * @param permitFullWidthSymbol2 記号（JIS 2区）を許可するかのフラグ
 * @param permitFullWidthGreek ギリシャ文字（JIS 6区）を許可するかのフラグ
 * @param permitFullWidthCyrillic キリル文字（JIS 7区）を許可するかのフラグ
 * @param permitFullWidthBorder 罫線（JIS 8区）を許可するかのフラグ
 * @param permitChars 上記以外で許可したい文字の配列（無い場合はnull）　※既定値：null
 * @return 変換後文字列
 */
fun String.replaceAllNGChar(replacement: String,
                            permitHalfWidthKANA: Boolean, permitHalfWidthSymbol: Boolean,
                            permitFullWidthNumericAndAlphabet: Boolean,
                            permitFullWidthSymbol1: Boolean, permitFullWidthSymbol2: Boolean,
                            permitFullWidthGreek: Boolean, permitFullWidthCyrillic: Boolean,
                            permitFullWidthBorder: Boolean,
                            permitChars: Array<String>? = null): String? {
    var ret = this
    for (i in 0 until ret.length) {
        val target = ret.substring(i, i + 1)
        //空白文字・改行文字チェック
        if (target.isIn(" ", "　", "\n", "\r\n")) {
            continue
        }
        //例外チェック
        if (permitChars != null && permitChars.contains(target)) {
            continue
        }
        //パターンチェック（全角英数字、全角かな・全角カナ・JIS第1水準漢字・JIS第2水準漢字）
        if (Pattern.matches("^[0-9a-zA-zぁ-んァ-ヶ一-龠]*$", target)) {
            continue
        }
        //パターンチェック（半角カナ）
        if (permitHalfWidthKANA && Pattern.matches("^[ｱ-ﾝ]*$", target)) {
            continue
        }
        //パターンチェック（半角記号）
        if (permitHalfWidthSymbol && Pattern.matches("^[!-/:-@\\[-`{-~]*$", target)) {
            continue
        }
        //パターンチェック（全角英数字）
        if (permitFullWidthNumericAndAlphabet && Pattern.matches("^[０-９ａ-ｚＡ-Ｚ]*$", target)) {
            continue
        }
        //パターンチェック（JIS記号 1区）
        if (permitFullWidthSymbol1 && Pattern.matches("^[、。，．・：；？！゛゜´｀¨＾￣＿ヽヾゝゞ〃仝々〆〇ー―‐／＼～∥｜…‥‘’“”（）〔〕［］｛｝〈〉《》「」『』【】＋－±×÷＝≠＜＞≦≧∞∴♂♀°′″℃￥＄￠￡％＃＆＊＠§☆★○●◎◇]*$", target)) {
            continue
        }
        //パターンチェック（JIS記号 2区）
        if (permitFullWidthSymbol2 && Pattern.matches("^[◆□■△▲▽▼※〒→←↑↓〓∈∋⊆⊇⊂⊃∪∩∧∨￢⇒⇔∀∃∠⊥⌒∂∇≡≒≪≫√∽∝∵∫∬Å‰♯♭♪†‡¶◯]*$", target)) {
            continue
        }
        //パターンチェック（JIS記号 6区）
        if (permitFullWidthGreek && Pattern.matches("^[ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρστυφχψω]*$", target)) {
            continue
        }
        //パターンチェック（JIS記号 7区）
        if (permitFullWidthCyrillic && Pattern.matches("^[АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя]*$", target)) {
            continue
        }
        //パターンチェック（JIS記号 8区）
        if (permitFullWidthBorder && Pattern.matches("^[─│┌┐┘└├┬┤┴┼━┃┏┓┛┗┣┳┫┻╋┠┯┨┷┿┝┰┥┸╂]*$", target)) {
            continue
        }
        //置換処理
        ret = ret.replaceAll(target, replacement)
    }
    return ret
}

/**
 * 指定文字で囲まれた部分を抽出（ xxx'aaa'xxx'bbb'xxx でシンボルが ' の場合、 aaa を抽出）
 * @param symbol 区切り文字
 * @param index 抽出した複数文字列のうち、先頭から何番目に出現したものを返却するか　既定値：0
 * @return 抽出した文字列
 */
fun String.pick(symbol: String, index: Int = 0): String {
    val list = this.pickList(symbol, index + 1)
    if (list.size < index + 1) {
        return ""
    }
    return list[index]
}

/**
 * 指定文字で囲まれた部分をリストで抽出（ xxx'aaa'xxx'bbb'xxx でシンボルが ' の場合、 {aaa, bbb} を抽出）
 * @param symbol 区切り文字
 * @param maxCount 抽出する数の上限
 * @return 抽出した文字列のリスト
 */
fun String.pickList(symbol: String, maxCount: Int = Int.MAX_VALUE): List<String> {
    val ret = mutableListOf<String>()
    var idx = 0
    var cnt = 0
    while (idx < this.length && cnt < maxCount) {
        val st = this.indexOf(symbol, idx)
        if (st == -1) {
            break
        }
        val ed = this.indexOf(symbol, st + symbol.length)
        if (ed == -1) {
            break
        }
        ret.add(this.substring(st + symbol.length, ed))
        idx = ed + symbol.length
        cnt++
    }
    return ret
}

/**
 * パラメータの入力
 * ※元々の文字列にある「%1, %2, %3, ...」を引数の文字列で置き換える
 * @return 変換後文字列
 */
fun String.putParams(vararg args: Any): String {
    var ret = this
    for (i in 1..args.size) {
        ret = ret.replaceAll("%$i", args[i - 1].toString())
    }
    return ret
}

/**
 * 先頭の文字を小文字にする
 * @return 変換後文字列
 */
fun String.toBeginWithLowerCase(): String {
    return when (this.length) {
        0 -> ""
        1 -> this.toLowerCase()
        else -> this[0].toLowerCase() + this.substring(1)
    }
}

/**
 * 先頭の文字を大文字にする
 * @return 変換後文字列
 */
fun String.toBeginWithUpperCase(): String {
    return when (this.length) {
        0 -> ""
        1 -> this.toUpperCase()
        else -> this[0].toUpperCase() + this.substring(1)
    }
}

/**
 * キャメルケースにする
 * @return 変換後文字列
 */
fun String.toCamelCase(): String {
    return this.split('_').joinToString("") {
        it.toBeginWithUpperCase()
    }.toBeginWithLowerCase()
}

/**
 * パスカルケースにする
 * @return 変換後文字列
 */
fun String.toPascalCase(): String {
    return this.split('_').joinToString("") {
        it.toBeginWithUpperCase()
    }
}

/**
 * スネークケースにする
 * @return 変換後文字列
 */
fun String.toSnakeCase(): String {
    var ret = ""
    var first = true
    this.toBeginWithUpperCase().forEach {
        ret += if (it.isUpperCase()) {
            if (first) {
                first = false
                it.toLowerCase()
            } else {
                "_" + it.toLowerCase()
            }
        } else {
            it
        }
    }
    return ret
}

/**
 * 全角文字と半角スペースのみURLエンコードする
 * @param enc エンコード名
 * @return URLエンコード後の文字列
 */
@JvmOverloads
fun String.softUrlEncode(enc: String = "utf-8"): String {
    var ret = ""
    this.asSequence().forEach {
        ret += if (it.ems() == 1) {
            it.toString()
        } else {
            URLEncoder.encode(it.toString(), enc)
        }
    }
    return ret
}

/**
 * プレフィックスを追加する
 * @param prefix 追加するプレフィックス
 * @return サフィックスを追加した文字列
 */
fun String.addPrefix(prefix: String): String {
    if (this.startsWith(prefix, false)) {
        return this
    }
    return prefix.plus(this)
}

/**
 * サフィックスを追加する
 * @param suffix 追加するサフィックス
 * @return サフィックスを追加した文字列
 */
fun String.addSuffix(suffix: String): String {
    if (this.endsWith(suffix, false)) {
        return this
    }
    return this.plus(suffix)
}