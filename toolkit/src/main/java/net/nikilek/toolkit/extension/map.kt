package net.nikilek.toolkit.extension

/**
 * Created by nikilek on 2017/10/16.
 * Map用の拡張関数
 */

/**
 * 数値の加算
 * @param key 対象要素のキー
 * @param value 加算する値
 * @param max 最大値（加算結果が最大値を上回る場合は、最大値となるように修正する）
 */
fun <T> MutableMap<T, Int>.addValue(key: T, value: Int, max: Int = Int.MAX_VALUE) {
    this[key] = this.getOrElse(key) { 0 } + value
    if (this[key]!! > max) {
        this[key] = max
    }
}

/**
 * 数値の加算
 * @param key 対象要素のキー
 * @param value 加算する値
 * @param max 最大値（加算結果が最大値を上回る場合は、最大値となるように修正する）
 */
fun <T> MutableMap<T, Long>.addValue(key: T, value: Long, max: Long = Long.MAX_VALUE) {
    this[key] = this.getOrElse(key) { 0 } + value
    if (this[key]!! > max) {
        this[key] = max
    }
}

/**
 * リストに追加
 * @param key 対象要素のキー
 * @param item 追加する値
 * @param index 追加先のインデックス（末尾へ追加時は-1）※既定値：-1
 */
@JvmOverloads
fun <T1, T2> MutableMap<T1, MutableList<T2>>.addList(key: T1, item: T2, index: Int = -1) {
    if (this.containsKey(key)) {
        val list = this[key]!!
        val idx = if (index < 0 || list.size < index) list.size else index
        this[key]!!.add(idx, item)
    } else {
        this[key] = mutableListOf(item)
    }
}

/**
 * DBアクセス用：取得結果のうち指定キーに該当する値を取得（文字列）
 * @param key キー
 * @return 取得した値
 */
fun Map<String, Any?>.getString(key: String): String {
    return this[key] as String? ?: ""
}

/**
 * DBアクセス用：取得結果のうち指定キーに該当する値を取得（数値）
 * @param key キー
 * @return 取得した値
 */
fun Map<String, Any?>.getInt(key: String): Int {
    return this[key] as Int? ?: 0
}

/**
 * DBアクセス用：取得結果のうち指定キーに該当する値を取得（数値）
 * @param key キー
 * @return 取得した値
 */
fun Map<String, Any?>.getLong(key: String): Long {
    return this[key] as Long? ?: 0
}

/**
 * DBアクセス用：取得結果のうち指定キーに該当する値を取得（日付文字列）
 * @param key キー
 * @return 取得した値
 */
fun Map<String, Any?>.getDateTimeString(key: String): String {
    //パフォーマンス重視のため、日付クラスは使わずに処理
    return (this[key] as String? ?: "").replaceAll("-", "/").replaceAll("T", " ")
}

/**
 * DBアクセス用：取得結果のうち指定キーに該当する値を取得（文字列のリスト）
 * @param key キー
 * @return 取得した値
 */
fun Map<String, Any?>.getStringList(key: String): List<String> {
    if (this[key] == null) {
        return listOf()
    }
    return (this[key] as MutableList<*>).map { it.toString() }
}