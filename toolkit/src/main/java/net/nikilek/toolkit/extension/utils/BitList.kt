package net.nikilek.toolkit.extension.utils

import net.nikilek.toolkit.extension.length
import net.nikilek.toolkit.extension.pow

/**
 * Created by nikilek on 2019/07/13.
 * ビット位置のリストを扱うための拡張関数
 */

/**
 * 2進数表記のInt値を、値が「1」である桁番号のリストに変換
 * ※桁番号は1の位から開始（MSB→ 0000000000 ←LSB）
 * ※「List<Int>.toBinaryDigits」と相互変換
 * @param startFromZero true：桁番号を0から開始（0～9）、false：桁番号を1から開始（1～10）
 * @return 桁番号のリスト
 */
@JvmOverloads
fun Int.toOnBitList(startFromZero: Boolean = true): MutableList<Int> {
    val ret = mutableListOf<Int>()
    for (i in 1..this.length()) {
        val remainder = this % 10.pow(i)
        val divide = if (i == 0) 1 else 10.pow(i - 1)
        if (remainder / divide == 1) {
            ret.add(i - if (startFromZero) 1 else 0)
        }
    }
    return ret
}

/**
 * 桁番号のリストを、対象の桁が「1」である2進数表記のInt値に変換
 * ※桁番号は1の位から開始（MSB→ 0000000000 ←LSB）
 * ※「Int.toOnBitList」と相互変換
 * @param startFromZero true：桁番号を0から開始（0～9）、false：桁番号を1から開始（1～10）
 * @param 2進数表記の数値
 */
@JvmOverloads
fun List<Int>.toBinaryDigits(startFromZero: Boolean = true): Int {
    var ret = 0
    this.distinct().forEach {
        val position = it - if (startFromZero) 0 else 1
        ret += 10.pow(position)
    }
    return ret
}