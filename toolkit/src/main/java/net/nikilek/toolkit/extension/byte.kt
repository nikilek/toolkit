package net.nikilek.toolkit.extension

/**
 * 符号なしのIntに変換（-128～127 ⇒ 0～255）
 * @return 符号なしのInt
 */
fun Byte.toUnsignedInt(): Int {
    return this.toInt() and 0xFF
}