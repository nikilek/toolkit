package net.nikilek.toolkit.extension

/**
 * Created by nikilek on 2017/10/16.
 * List用の拡張関数
 */

/**
 * 指定されたアイテムがリストに含まれるかを判定（リストが空の場合は判定を行わない）
 * ※「contains」or「list isEmpty」
 * @param items 指定アイテム
 * @return true：含まれるorリストが空、false：含まれない
 */
fun <T> MutableList<T>.conform(vararg items: T): Boolean {
    if (items.isEmpty()) {
        return false
    }
    if (this.isEmpty()) {
        return true
    }
    items.forEach { item ->
        if (this.contains(item)) {
            return true
        }
    }
    return false
}

/**
 * 指定されたアイテムがリストに含まれないかを判定（リストが空の場合は判定を行わない）
 * ※「not contains」or「list isEmpty」
 * @param items 指定アイテム
 * @return true：含まれない、false：含むorリストが空
 */
fun <T> MutableList<T>.notConform(vararg items: T): Boolean {
    return !this.conform(*items)
}

/**
 * 値の再設定
 * @param list 新しいリスト
 */
fun <T> MutableList<T>.reset(list: List<T>) {
    this.clear()
    this.addAll(list)
}

/**
 * 値の削除
 * @param elements 指定要素の配列
 */
fun <T> MutableList<T>.except(vararg elements: T) {
    this.reset(this.minus(elements))
}

/**
 * 値の削除
 * @param elementList 指定要素のリスト
 */
fun <T> MutableList<T>.except(elementList: List<T>) {
    this.reset(this.minus(elementList))
}

/**
 * マップ（リスト内の要素と、その要素の出現回数）に変換
 * @return Map<T, Count as Integer>
 */
fun <T> List<T>.toCountMap(): Map<T, Int> {
    val map = HashMap<T, Int>()
    for (item in this) {
        var cnt = 0
        if (map.containsKey(item)) {
            cnt = map[item]!!
        }
        map[item] = cnt + 1
    }
    return map
}