package net.nikilek.toolkit.extension.utils

import net.nikilek.toolkit.extension.pow

/**
 * Created by nikilek on 2019/07/13.
 * 基数変換関連の拡張関数
 */

/**
 * 履歴番号を「英字n桁＋数字m桁」の文字列に変換する（英字はO, I を除外）
 * @param numericLength 数字部分の桁数
 * @param separator 変換後文字列の、英字部分と数字部分の間に設定する区切り文字
 * @return 変換後の「英字n桁＋数字m桁」文字列
 */
fun Int.toDispHistoryNo(numericLength: Int, separator: String = ""): String {
    val d = (10).pow(numericLength)
    val upper = this.div(d)
    val lower = this.rem(d)
    val upperStr = upper.toAlphabet24()
    val lowerStr = lower.toString().padStart(numericLength, '0')
    return "$upperStr$separator$lowerStr"
}

/**
 * 履歴番号の「英字n桁＋数字m桁」文字列を数値に変換する（英字はO, I を除外）
 * @param numericLength 数字部分の桁数
 * @param separator 英字部分と数字部分の間に設定されている区切り文字
 * @return 変換後の数値
 */
fun String.toRawHistoryNo(numericLength: Int, separator: String = ""): Int {
    val sepSt = this.length - numericLength - separator.length
    val sepEd = this.length - numericLength
    val str = this.removeRange(sepSt, sepEd)
    val upperStr = str.substring(0, str.length - numericLength)
    val lowerStr = str.substring(upperStr.length)
    val upper = upperStr.toNumeric24()
    val lower = lowerStr.toInt()
    return upper.times((10).pow(numericLength)) + lower
}

/**
 * 数値⇒24進数変換（O, I を除外した英字）
 * @param minLength 変換後文字列の最小桁数　※既定値：1
 * @return 変換後の24進数文字列（元の数値が負の場合は空白文字）
 */
fun Int.toAlphabet24(minLength: Int = 1): String {
    if (this < 0) {
        return ""
    }
    val dictionary = arrayOf(
            "A", "B", "C", "D", "E", "F", "G", "H", "J", "K",
            "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z")
    val base = dictionary.size
    val list = mutableListOf<String>()
    var count = 0
    while (true) {
        val target = this.div(base.pow(count))
        if (count != 0 && target == 0) {
            break
        }
        val rem = target.rem(base)
        val value = dictionary[rem]
        list.add(0, value)
        count++
    }
    return list.joinToString("").padStart(minLength, dictionary[0][0])
}

/**
 * 24進数⇒数値変換（O, I を除外した英字）
 * @return 変換後の数値
 */
fun String.toNumeric24(): Int {
    if (this.isBlank()) {
        return -1
    }
    val dictionary = arrayOf(
            "A", "B", "C", "D", "E", "F", "G", "H", "J", "K",
            "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z")
    val base = dictionary.size
    var result = 0
    for (i in 0 until this.length) {
        val target = this[i].toString()
        val value = dictionary.indexOf(target)
        val place = this.length - i
        result += value * base.pow(place - 1)
    }
    return result
}

/**
 * 数値⇒32進数変換（0, 1, O, I を除外した英数字）
 * @param minLength 変換後文字列の最小桁数
 * @return 変換後の32進数文字列（元の数値が負の場合は空白文字）
 */
fun Int.toAlphabet32(minLength: Int = 1): String {
    if (this < 0) {
        return ""
    }
    val dictionary = arrayOf(
            "A", "B", "C", "D", "E", "F", "G", "H", "J", "K",
            "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z", "2", "3", "4", "5", "6", "7", "8", "9")
    val base = dictionary.size
    val list = mutableListOf<String>()
    var count = 0
    while (true) {
        val target = this.div(base.pow(count))
        if (count != 0 && target == 0) {
            break
        }
        val rem = target.rem(base)
        val value = dictionary[rem]
        list.add(0, value)
        count++
    }
    return list.joinToString("").padStart(minLength, dictionary[0][0])
}

/**
 * 32進数⇒数値変換（0, 1, O, I を除外した英数字）
 * @return 変換後の数値
 */
fun String.toNumeric32(): Int {
    if (this.isBlank()) {
        return -1
    }
    val dictionary = arrayOf(
            "A", "B", "C", "D", "E", "F", "G", "H", "J", "K",
            "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z", "2", "3", "4", "5", "6", "7", "8", "9")
    val base = dictionary.size
    var result = 0
    for (i in 0 until this.length) {
        val target = this[i].toString()
        val value = dictionary.indexOf(target)
        val place = this.length - i
        result += value * base.pow(place - 1)
    }
    return result
}