package net.nikilek.toolkit.extension

/**
 * byte配列⇒16進数文字列
 * @return 16進数文字列（大文字）
 */
fun ByteArray.toHexString(): String {
    val sb = StringBuffer("")
    for (i in 0 until this.size) {
        var hex = Integer.toHexString(this[i].toInt())
        if (hex.length == 1) {
            sb.append("0")
        } else if (hex.length > 2) {
            hex = hex.substring(hex.length - 2)
        }
        sb.append(hex)
    }
    return sb.toString().toUpperCase()
}