package net.nikilek.toolkit.extension

import android.view.View

/**
 * Created by nikilek on 2019/01/01.
 * android.view.View用の拡張関数
 */

/**
 * setVisibility(View.VISIBLE) or setVisibility(View.INVISIBLE | View.GONE)
 * @param visible true：View.VISIBLE, false：View.INVISIBLE | View.GONE　※既定値：View.INVISIBLE
 */
@JvmOverloads
fun View.setVisible(visible: Boolean, invisibleStyle: Int = View.INVISIBLE) {
    this.visibility = if (visible) View.VISIBLE else invisibleStyle
}

/**
 * setVisibility(View.INVISIBLE) or setVisibility(View.VISIBLE)
 * @param invisible true：View.INVISIBLE, false：View.VISIBLE
 */
fun View.setInvisible(invisible: Boolean) {
    this.visibility = if (invisible) View.INVISIBLE else View.VISIBLE
}

/**
 * setVisibility(View.GONE) or setVisibility(View.VISIBLE)
 * @param gone true：View.GONE, false：View.VISIBLE
 */
fun View.setGone(gone: Boolean) {
    this.visibility = if (gone) View.GONE else View.VISIBLE
}

/**
 * setClickable and setFocusable
 * @param isActive isClickable and isFocusable
 */
fun View.setActive(isActive: Boolean) {
    this.isClickable = isActive
    this.isFocusable = isActive
}

/**
 * レイアウトIDをもとにビューを探索（階層ごとに絞り込んでいく場合に使用）
 * ※findViewByIdを複数回実行
 * ※ダウンキャストにも成功する場合があるため、厳密な型チェックは行わない
 * @param layoutId レイアウトID（上の階層から順に複数指定）
 * @return 探索結果のビューの参照（末端まで探索できなかった場合はnullを返却）
 */
fun <T : View> View.findViewByIds(vararg layoutId: Int): T? {
    var ret: View = this
    layoutId.forEach {
        ret = ret.findViewById(it) ?: return null
    }
    @Suppress("UNCHECKED_CAST")
    return ret as? T
}