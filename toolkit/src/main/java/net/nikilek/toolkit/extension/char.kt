package net.nikilek.toolkit.extension

/**
 * 文の幅（全角＝２、半角＝１）を取得
 * @return 文字幅
 */
fun Char.ems(): Int {
    return if (this <= '\u007e' || // 英数字
            this == '\u00a5' || // \記号
            this == '\u203e' || // ~記号
            this in '\uff61'..'\uff9f') { // 半角カナ
        1
    } else {
        2
    }
}