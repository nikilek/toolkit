package net.nikilek.toolkit.extension

import kotlin.math.pow

/**
 * Created by nikilek on 2018/02/16.
 * Int用の拡張関数
 */

/**
 * この数値が、指定された数値の集合の中に含まれるかを判定
 * @param args 数値の集合
 * @return 集合に含まれるか否か
 */
fun Int.isIn(vararg args: Int): Boolean {
    return args.contains(this)
}

/**
 * この数値が、指定された数値の範囲に含まれるかを判定
 * @param min 最小値
 * @param max 最大値
 * @param equals true：等号あり、false：等号なし　※既定値：true
 * @return 範囲に含まれるか否か
 */
@JvmOverloads
fun Int.isInRange(min: Int, max: Int, equals: Boolean = true): Boolean {
    return if (equals) {
        (this in min..max)
    } else {
        (this in (min + 1) until max)
    }
}

/**
 * 数値を全角文字に変換
 * @return 変換後文字列
 */
fun Int.toFullWidthString(): String {
    val str = this.toString()
    val sb = StringBuffer(str)
    for (i in 0 until str.length) {
        val c = str[i]
        if (c in '0'..'9') {
            sb.setCharAt(i, (c - '0' + '０'.toInt()).toChar())
        }
    }
    return sb.toString()
}

/**
 * 絶対値の取得
 * @return 変換後の数値
 */
fun Int.absolute(): Int {
    return if (this < 0) {
        this * -1
    } else this
}

/**
 * 符号の反転
 * @return 変換後の数値
 */
fun Int.inverse(): Int {
    return this * (-1)
}

/**
 * 0と1の反転　※0でも1でも無い場合は変換無し
 * @return 変換後の数値
 */
fun Int.reverse(): Int {
    return when (this) {
        0 -> 1
        1 -> 0
        else -> this
    }
}

/**
 * べき乗の取得
 * @param power 乗数
 * @return 結果の値
 */
fun Int.pow(power: Int): Int {
    return this.toDouble().pow(power).toInt()
}

/**
 * 0～9の値を、位に合った値（例；10, 50, 300）に変換
 * @param place 1～10　※10桁目は最大値2である事に注意
 * @return 結果の値
 */
fun Int.scale(place: Int): Int {
    val scale = (10).pow(place)
    return ((this.toLong() * scale) / 10).toInt()
}

/**
 * 指定された位の値を取得（例：12345 の 2桁目 は 4）
 * @param place 1～10
 * @return 対象の桁の数値（0～9）
 */
fun Int.pick(place: Int): Int {
    return (this % (10).pow(place)) / (10).pow(place - 1)
}

/**
 * 桁数の取得
 * @return 桁数
 */
fun Int.length(): Int {
    return this.toString().length
}

/**
 * 上限を超えているかを判定し、範囲内の値を返却する
 * @param max 上限値
 * @return 上限を超えている場合は上限値、それ以外は元の値
 */
fun Int.validMax(max: Int): Int {
    return if (this > max) {
        max
    } else {
        this
    }
}

/**
 * 下限を超えているかを判定し、範囲内の値を返却する
 * @param min 下限値
 * @return 下限を超えている場合は下限値、それ以外は元の値
 */
fun Int.validMin(min: Int): Int {
    return if (this < min) {
        min
    } else {
        this
    }
}

/**
 * 上限・下限を超えているかを判定し、範囲内の値を返却する
 * @param min 下限値
 * @param max 上限値
 * @return 上限または下限を超えている場合はその境界値、それ以外は元の値
 */
fun Int.valid(min: Int, max: Int): Int {
    val ret = this.validMin(min)
    return ret.validMax(max)
}

/**
 * 上限・下限を超えない範囲で値を増減する
 * @param value 増減値
 * @param min 下限値　※既定値：Int.MIN_VALUE
 * @param max 上限値　※既定値：Int.MAX_VALUE
 * @return 増減後の値
 */
@JvmOverloads
fun Int.validAdd(value: Int, min: Int = Int.MIN_VALUE, max: Int = Int.MAX_VALUE): Int {
    return when {
        this.toLong() + value > max -> max
        this.toLong() + value < min -> min
        else -> this + value
    }
}