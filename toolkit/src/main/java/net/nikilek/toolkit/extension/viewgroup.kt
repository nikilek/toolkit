package net.nikilek.toolkit.extension

import android.view.View
import android.view.ViewGroup

/**
 * Created by nikilek on 2018/09/15.
 * android.view.ViewGroup用の拡張関数
 */

/**
 * 当該ビューの子ビューについて、先頭から指定数は表示、それ以外は非表示にする
 * @param count 表示する子ビューの数
 * @param inVisibleValue 非表示を表す値（View.INVISIBLE または View.GONE）※既定値：View.INVISIBLE
 */
@JvmOverloads
fun ViewGroup.setVisibleChildCount(count: Int, inVisibleValue: Int = View.INVISIBLE) {
    for (i in 0 until this.childCount) {
        val child = this.getChildAt(i)
        child.visibility = if (i < count) {
            View.VISIBLE
        } else {
            inVisibleValue
        }
    }
}