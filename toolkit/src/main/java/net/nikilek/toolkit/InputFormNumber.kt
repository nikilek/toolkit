package net.nikilek.toolkit

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Typeface
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.widget.TextView

import net.nikilek.toolkit.component.InputNumberDialog
import net.nikilek.toolkit.constant.CodeConst
import net.nikilek.toolkit.constant.Settings
import net.nikilek.toolkit.utility.LogUtil
import net.nikilek.toolkit.utility.ResourceHolder
import net.nikilek.toolkit.utility.Toaster

/**
 * Created by nikilek on 2018/02/23.
 * 別窓で数値入力ができるテキストボックス
 * ※現時点では自然数限定
 *
 * 【独自XML属性】※「res/values/attrs.xml」参照
 * ・minValue：入力可能な数値の下限（未指定時は下限チェックなし）
 * ・maxValue：入力可能な数値の上限（未指定時は上限チェックなし）
 * ・maxLength：入力可能な最大桁数（未指定時は上限チェックなし。maxValue指定時は無視される）
 */
open class InputFormNumber : TextView {

    /**
     * テキスト変更時時イベントのリスナ
     */
    interface OnTextChangedListener {
        /**
         * @param text 新たなテキスト
         */
        fun onTextChanged(text: String)
    }

    private var propMinValue: Int = -1 //最小値（無効値：-1）
    private var propMaxValue: Int = -1 //最大値（無効値：-1）
    private var propMaxLength: Int = 0 //最大文字長（無効値：0）
    private var title: String = "" //入力ダイアログのタイトル
    private var textStyle: Int = Typeface.NORMAL //文字の表示スタイル
    private var dlg: InputNumberDialog? = null
    var onTextChangedListener: InputFormNumber.OnTextChangedListener? = null //テキスト変更時イベントのリスナ

    /**
     * 文字列の取得
     */
    val textStr: String
        get() = text.toString()

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context)

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        getAttrs(attrs)
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        getAttrs(attrs)
    }

    /**
     * 属性の取得
     * @param attrs 属性セット
     */
    private fun getAttrs(attrs: AttributeSet) {
        //オリジナル属性の取得
        val attrsOriginal = context.obtainStyledAttributes(attrs, R.styleable.InputFormNumber)
        propMinValue = attrsOriginal.getInt(R.styleable.InputFormNumber_minValue, -1)
        propMaxValue = attrsOriginal.getInt(R.styleable.InputFormNumber_maxValue, -1)
        propMaxLength = attrsOriginal.getInt(R.styleable.InputFormNumber_maxLength, 0)
        attrsOriginal.recycle()
        //基本属性の取得
        textStyle = attrs.getAttributeIntValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "textStyle", textStyle)
        if (context is Context) {
            val resIdEmpty = R.string.dummy_empty
            this.title = context.getString(attrs.getAttributeResourceValue(CodeConst.NameSpace.ANDROID_SCHEMAS, "hint", resIdEmpty))
        }
    }

    public override fun onFinishInflate() {
        super.onFinishInflate()
        if (this.isInEditMode) {
            return
        }
        if (this.transformationMethod !is PasswordTransformationMethod) { //パスワード入力時は標準のフォントを使用する
            //フォントの設定
            setFont(context)
        }
        //クリック時イベントの設定
        attachClickEvent()
    }

    /**
     * テキスト変更時イベントの設定
     * ※プロパティアクセスと同じ
     */
    fun attachOnTextChangedEvent(onTextChangedListener: InputFormNumber.OnTextChangedListener) {
        this.onTextChangedListener = onTextChangedListener
    }

    /**
     * クリックイベントの設定
     */
    private fun attachClickEvent() {
        this.setOnClickListener {
            if(!checkClick()) return@setOnClickListener //連打防止
            val currentText = textStr
            val onFinishListener = object : InputNumberDialog.OnFinishListener {
                override fun onFinish(text: String, textChanged: Boolean, canceled: Boolean) {
                    if (canceled) {
                        return
                    }
                    //入力されたテキストを反映
                    this@InputFormNumber.text = text
                    //呼び出し元へ通知
                    if (textChanged) {
                        onTextChangedListener?.onTextChanged(textStr)
                    }
                }
            }
            //数値入力用ダイアログを表示
            dlg = if (propMaxValue >= 0) {
                InputNumberDialog(context as AbstractActivity, onFinishListener, title,
                        currentText, propMinValue, propMaxValue)
            } else {
                InputNumberDialog(context as AbstractActivity, onFinishListener, title,
                        currentText, propMaxLength)
            }
            dlg?.show()
        }
        this.setOnLongClickListener {
            //入力されているテキストをクリップボードにコピー
            val cm = context.getSystemService(Context.CLIPBOARD_SERVICE)
            if (cm is ClipboardManager) {
                cm.setPrimaryClip(ClipData.newPlainText("", this@InputFormNumber.text))
                Toaster.showBottom(context, context.getString(R.string.toast_clipboard_copy))
            }
            return@setOnLongClickListener true
        }
    }

    /**
     * 連打チェック
     */
    private var lastClickTime: Long = 0 //最後にボタンを押した時刻（連打防止チェック用）
    private fun checkClick(): Boolean {
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastClickTime < Settings.Interval.PERMIT_CLICK) {
            return false
        }
        lastClickTime = currentTime
        return true
    }

    /**
     * フォントを設定
     * @param context コンテキスト
     */
    private fun setFont(context: Context?) {
        try {
            //システム定義のフォントを設定
            if (textStyle == Typeface.BOLD) {
                this.typeface = ResourceHolder.Typefaces.getBold(context!!)
            } else {
                this.typeface = ResourceHolder.Typefaces.getDefault(context!!)
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * タイトルを設定
     * @param title タイトル文字列
     */
    fun setTitle(title: String){
        this.title = title
    }

    /**
     * 入力可能範囲の設定
     * @param minValue 最小値
     * @param maxValue 最大値
     */
    fun setRange(minValue: Int, maxValue: Int) {
        //パラメタの格納
        propMinValue = minValue
        propMaxValue = maxValue
        //ダイアログに反映
        if (dlg != null && dlg!!.isShowing && context is Activity) {
            (context as Activity).runOnUiThread {
                dlg?.setRange(minValue, maxValue)
            }
        }
    }
}
