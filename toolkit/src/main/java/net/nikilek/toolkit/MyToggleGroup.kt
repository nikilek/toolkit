package net.nikilek.toolkit

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout

/**
 * Created by nikilek on 2019/06/22.
 * カスタムビュー
 * ※配下のトグルボタンに択一選択の性質を付与
 */
open class MyToggleGroup : LinearLayout {

    //配下のいずれかのボタンのチェック状態が変更された場合の通知用
    var onCheckedChangeListener: OnCheckedChangeListener? = null

    //インフレート済かのフラグ
    private var finishInflate = false

    /**
     * チェック状態変更時イベントのリスナ
     */
    interface OnCheckedChangeListener {
        /**
         * @param checkedIndex チェックされているボタンのインデックス
         */
        fun onCheckedChange(checkedIndex: Int)
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context)

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    public override fun onFinishInflate() {
        super.onFinishInflate()
        //if (this.isInEditMode) {
        //    return
        //}
        //配下のトグルボタンに、チェック状態変更時イベントを設定
        for (i in (this.childCount - 1) downTo 0) {
            val child = this.getChildAt(i)
            //トグルボタン以外の追加は許可しない
            if (child !is MyToggleButton) {
                removeView(child)
                return
            }
            //チェック状態変更時イベントの設定
            attachCheckedChangeEvent(child)
        }
        //チェックされているボタンが無い場合は、先頭のボタンをチェック状態にする
        if (this.childCount > 0 && getCheckedIndex() < 0) {
            (this.getChildAt(0) as MyToggleButton).isChecked = true
        }
        //インフレート済みフラグON
        finishInflate = true
    }

    override fun onViewAdded(child: View?) {
        super.onViewAdded(child)
        //インフレート済みでなければリターン
        if (!finishInflate) {
            return
        }
        //トグルボタン以外の追加は許可しない
        if (child !is MyToggleButton) {
            removeView(child)
            return
        }
        //チェックされているボタンが無い場合は、先頭のボタンをチェック状態にする
        if (this.childCount > 0 && getCheckedIndex() < 0) {
            (this.getChildAt(0) as MyToggleButton).isChecked = true
        }
        //チェック状態変更時イベントの設定
        attachCheckedChangeEvent(child)
    }

    /**
     * 指定インデックスのボタンをチェック状態にする
     * @param buttonIndex 対象のボタンのインデックス
     */
    fun checkButtonByIndex(buttonIndex: Int) {
        if (0 <= buttonIndex && buttonIndex < this.childCount) {
            (this.getChildAt(buttonIndex) as MyToggleButton).isChecked = true
        }
    }

    /**
     * 指定IDのボタンをチェック状態にする
     * @param viewId 対象のボタンのビューID
     */
    fun checkButtonByViewId(viewId: Int) {
        for (i in 0 until this.childCount) {
            val child = this.getChildAt(i) as MyToggleButton
            if (child.id == viewId) {
                child.isChecked = true
                return
            }
        }
    }

    /**
     * チェックされているボタンのインデックスを取得
     * @return チェックされているボタンのインデックス
     */
    fun getCheckedIndex(): Int {
        for (i in 0 until this.childCount) {
            val child = this.getChildAt(i) as MyToggleButton
            if (child.isChecked) {
                return i
            }
        }
        return -1
    }

    /**
     * チェックされているボタンのIDを取得
     * @return チェックされているボタンのインデックス
     */
    fun getCheckedViewId(): Int {
        return when (val checkedIndex = getCheckedIndex()) {
            -1 -> -1
            else -> getChildAt(checkedIndex).id
        }
    }

    /**
     * チェック状態変更時イベントを設定
     * @param toggleButton 対象のトグルボタン
     */
    private fun attachCheckedChangeEvent(toggleButton: MyToggleButton) {
        toggleButton.setOnCheckedChangeListener { buttonView, isChecked ->
            //いったん配下のすべてのボタンのイベントを解除（ループ回避）
            for (i in 0 until this@MyToggleGroup.childCount) {
                val child = this@MyToggleGroup.getChildAt(i) as MyToggleButton
                child.setOnCheckedChangeListener(null)
            }
            //変更後の状態に応じて処理
            if (!isChecked) {
                //OFFになった場合はONに戻す（択一を前提とするため）
                buttonView.isChecked = true
            } else {
                //ONになった場合は、他のボタンを全てOFFにする
                for (i in 0 until this@MyToggleGroup.childCount) {
                    val child = this@MyToggleGroup.getChildAt(i) as MyToggleButton
                    if (child != buttonView) {
                        child.isChecked = false
                    }
                }
                //上位へ通知
                val checkedIndex = getCheckedIndex()
                onCheckedChangeListener?.onCheckedChange(checkedIndex)
            }
            //配下のすべてのボタンにイベントを再付与する
            for (i in 0 until this@MyToggleGroup.childCount) {
                val child = this@MyToggleGroup.getChildAt(i) as MyToggleButton
                attachCheckedChangeEvent(child)
            }
        }
    }
}
