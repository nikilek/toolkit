package net.nikilek.toolkit

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.BaseAdapter
import android.widget.ListView

/**
 * Created by nikilek on 2016/01/27.
 * カスタムビュー
 */
@SuppressLint("NewApi")
open class MyListView : ListView {

    /**
     * コンストラクタ
     * @param context コンテキスト
     */
    constructor(context: Context) : super(context) {
        prepare()
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        prepare()
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param attrs アトリビュートセット
     * @param defStyle デフォルトスタイル
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        prepare()
    }

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param adapter リスト表示用アダプタ
     */
    constructor(context: Context, adapter: BaseAdapter) : super(context) {
        prepare()
        this.adapter = adapter
        this.post { adapter.notifyDataSetChanged() }
    }

    /**
     * 初期設定
     */
    @SuppressLint("ObsoleteSdkInt")
    private fun prepare() {
        this.isScrollingCacheEnabled = false //スクロールキャッシュの無効化
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD_MR1) {
            //オーバースクロールの設定
            this.overScrollMode = View.OVER_SCROLL_IF_CONTENT_SCROLLS
        }
    }

    /**
     * リストアダプタのセット
     * @param adapter リストアダプタ
     */
    fun setListAdapter(adapter: BaseAdapter) {
        this.adapter = adapter
        this.post { adapter.notifyDataSetChanged() }
    }

    override fun overScrollBy(deltaX: Int, deltaY: Int, scrollX: Int, scrollY: Int, scrollRangeX: Int, scrollRangeY: Int, maxOverScrollX: Int, maxOverScrollY: Int, isTouchEvent: Boolean): Boolean {
        //オーバースクロール時の挙動を設定
        return false
        //return super.overScrollBy(0, deltaY, 0, scrollY, 0, scrollRangeY, 0, 50, isTouchEvent);
    }

    /**
     * 指定位置までスクロール
     * @param position 位置
     */
    fun scrollTo(position: Int) {
        this.post { scrollTo(position) }
    }

    /**
     * 指定位置までスクロール（スムーズなスクロール）
     * @param position 位置
     */
    fun smoothScrollTo(position: Int) {
        this.post { smoothScrollToPosition(position) }
    }
}
