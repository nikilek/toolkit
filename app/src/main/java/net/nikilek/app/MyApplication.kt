package net.nikilek.app

import net.nikilek.toolkit.AbstractApplication
import net.nikilek.toolkit.constant.Settings

/**
 * Created by nikilek on 2017/10/16.
 * Applicationの実装サンプル
 */
class MyApplication : AbstractApplication() {
    override fun onCreate() {
        super.onCreate()
        //定数値の設定
        //※既定値から変更したい値をここで設定する
        Settings.Mode.DEBUG = true
    }
}