package net.nikilek.app.screen

/**
 * Created by nikilek on 2018/03/21.
 * トップ画面の実装サンプル
 */
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.widget.Button
import androidx.core.content.FileProvider
import kotlinx.coroutines.*
import net.nikilek.app.R
import net.nikilek.toolkit.AbstractScreen
import net.nikilek.toolkit.AnimationTextView
import net.nikilek.toolkit.InputFormNumberInline
import net.nikilek.toolkit.utility.FileUtil
import net.nikilek.toolkit.utility.HttpFileDownloader
import net.nikilek.toolkit.utility.LogUtil
import java.io.File

class TopScreen(context: Context, attrs: AttributeSet) : AbstractScreen(context, attrs) {

    override fun onAttach() {
        //非同期サンプル
        val job = GlobalScope.launch(Dispatchers.Main) {
            delay(5000)
            //メッセージのアニメーションを開始
            findViewById<AnimationTextView>(R.id.txt_notice).animateText()
        }

        //数値入力部品の初期化
        val inputFormNumberInline = findViewById<InputFormNumberInline>(R.id.input_number_inline)
        inputFormNumberInline.targetLayout = findViewById(R.id.area_dial_pad)

        //ダウンロードボタンのイベントを設定
        findViewById<Button>(R.id.btn_download).setOnClickListener {
            try {
                //WEBサーバからzipファイルをダウンロード
                //※ここで使用するWEBサーバは、各自でご準備ください
                HttpFileDownloader(activity, "http://localhost/download/test.zip", object : HttpFileDownloader.OnFinishListener {
                    override fun onComplete(file: File) {
                        //Do Nothing.
                    }

                    override fun onRefuse(canceled: Boolean) {
                        //Do Nothing.
                    }
                }).start()
            } catch (e: Exception) {
                LogUtil.debug(e)
            }
        }

        //zip解凍ボタンのイベントを設定
        findViewById<Button>(R.id.btn_zip).setOnClickListener {
            //内部ストレージにあるzipファイルを解凍
            //※ここで使用するzipファイルは、各自でご準備ください
            try {
                val util = FileUtil(activity, "tmp/test.zip", true)
                util.extractZip()
            } catch (e: Exception) {
                LogUtil.debug(e)
            }
        }

        //ダウンロードボタンのイベントを設定
        findViewById<Button>(R.id.btn_apk).setOnClickListener {
            try {
                //共有フォルダにあるapkファイルをインストール
                //※共有フォルダの設定については、AndroidManifest.xmlを参照してください
                //※ここで使用するapkファイルは、各自でご準備ください
                val file = FileUtil(activity, "shared/test.apk", true).getFile()!!
                shareFile(activity, file)
            } catch (e: Exception) {
                LogUtil.debug(e)
            }
        }
    }

    override fun onActive() {
    }

    override fun onInActive() {
    }

    override fun onDetach() {
    }

    override fun draw() {
    }

    /**
     * 共有フォルダ内のファイルを他アプリに送信
     * @param activity 呼び出し元のActivity
     * @param file 送信するファイル　※FileProviderで共有されたフォルダ配下のファイルであること
     * @param resultCode Intent送信の結果コード
     */
    @JvmOverloads
    fun shareFile(activity: Activity, file: File, resultCode: Int = 0) {
        val contentUri = FileProvider.getUriForFile(activity, "${activity.packageName}.fileprovider", file)
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(contentUri, activity.contentResolver.getType(contentUri))
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
        activity.startActivityForResult(intent, resultCode)
    }
}