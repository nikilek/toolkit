package net.nikilek.app

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Message
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import net.nikilek.app.R
import net.nikilek.app.model.SaveData
import net.nikilek.toolkit.AbstractActivity
import net.nikilek.toolkit.MyUncaughtExceptionHandler
import net.nikilek.toolkit.constant.CodeConst
import net.nikilek.toolkit.interfaces.ISaveData
import net.nikilek.toolkit.utility.LogUtil

/**
 * Created by nikilek on 2018/02/11.
 * MainActivityの実装サンプル
 */
class MainActivity : AbstractActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(net.nikilek.toolkit.R.layout.activity_root)
        Thread.setDefaultUncaughtExceptionHandler(MyUncaughtExceptionHandler(applicationContext))
        //セーブデータが無い場合
        //※(1)セーブデータが無い場合は、{net.nikilek.toolkit.EmptySaveData}が返却される
        //※(2)セーブデータがある場合は、実際にセーブしたデータの型（ここでは{net.nikilek.app.model.SaveData}）が返却される
        //※上記２パターンに対応するために、インターフェースを通して空チェックを行う
        if (mSaveData<ISaveData>().isEmpty) {
            //(1)セーブデータ作成画面の表示 or (2)セーブデータを作成してメニュー画面表示
            //(1)セーブデータ作成画面の表示
            //showScreen(R.layout.{セーブデータ作成画面})
            //(2)セーブデータを作成してメニュー画面表示
            val savedata = SaveData() //セーブデータの生成
            savedata.fileCreatedTime = 1 //特定の値を設定する事で、isEmpty=false（空でないデータ）となる
            save(savedata) //作成したデータをセーブ
            //メニュー画面の表示
            showScreen(R.layout.screen_top, null)
            //LogUtil.debug("セーブデータ無し")
        } else {
            //メニュー画面の表示
            showScreen(R.layout.screen_top, null)
            //LogUtil.debug("セーブデータあり")
        }
        //データベースのロード
        loadDatabase()
    }

    override fun onNewIntent(intent: Intent) {
        //通知を消す
        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.cancel(net.nikilek.toolkit.R.string.dummy_string) //※通知表示時に指定したキー
        //IntentをActivityに設定
        setIntent(intent)
        super.onNewIntent(intent)
        //ActivityのIntent付加データを消費
        val activatedFromNotify = this.intent.getBooleanExtra(CodeConst.Intent.EXTRA_MAIN_FROM_NOTIFY, false)
        this.intent.removeExtra(CodeConst.Intent.EXTRA_MAIN_FROM_NOTIFY)
        //通知からの起動の場合の処理
        if (activatedFromNotify) {
            //do something
        }
    }

    override fun onResume() {
        super.onResume()
        //再描画
        draw()
    }

    override fun handleMessage(msg: Message) {
        try {
            when (msg.what) {
                CodeConst.MSG.ToIndividualActivity.DRAW -> { //再描画依頼
                    draw()
                }
                else -> super.handleMessage(msg) //親クラスへ委譲
            }
        } catch (e: Exception) {
            LogUtil.debug(e)
        }
    }

    /**
     * 画面の描画
     */
    @Synchronized
    private fun draw() {
        //do something
    }

    /**
     * データベースのロード
     */
    private fun loadDatabase() {
        GlobalScope.launch {
            //ローディングを表示
            showLoading(false)
            //各種データのロード
            //[例] AssetからCSVをロード
            //val database = AssetReaderDatabase(this).readSampleDatabase()
            //ローディングを閉じる
            hideLoading()
        }
    }
}