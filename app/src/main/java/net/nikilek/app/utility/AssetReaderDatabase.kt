package net.nikilek.app.utility

import android.content.Context

import net.nikilek.toolkit.utility.AssetReader
import java.util.*

/**
 * Created by nikilek on 2018/02/11.
 * Assetファイル読込の実装サンプル
 */
class AssetReaderDatabase(context: Context) : AssetReader(context) {

    //sample.csvの読込
    fun readSampleDatabase(): SortedMap<Int, Any> { //実際の使用時は、Anyを独自の data class等に置き換えてください
        val sampleDataList = readCsv("sample.csv")
        val ret = sortedMapOf<Int, Any>()
        for (data in sampleDataList) {
            val key = data[0].toInt()
            val value = Any() //実際の使用時は、Anyを独自の data class等に置き換えてください
            //valueの設定
            //value.name = data[1]
            //value.param = data[2]
            //...
            ret[key] = value
        }
        return ret
    }
}
