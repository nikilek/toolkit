package net.nikilek.app.model

import net.nikilek.toolkit.interfaces.ISaveData

/**
 * Created by nikilek on 2017/10/16.
 * セーブデータの実装サンプル
 */
data class SaveData(
        /** システムデータ  */
        var id: Long = 0, //ID（オンライン接続時に採番）
        var fileCreatedTime: Long = 0, //ファイル作成時刻
        var lastPlayTime: Long = 0, // 前回プレイ時刻
        var lastNotifyTime: Long = 0, //最後に通知を受け取った時刻
        var lastBonusTime: Long = 0, //最後にログインボーナスを受け取った時刻
        var lastBackupTime: Long = 0, //最後にセーブデータをサーバに送信した時刻
        var lastRestoreTime: Long = 0, //最後にセーブデータをサーバから受信した時刻
        val revisionCount: MutableMap<Int, Int> = mutableMapOf(), //補填回数
        var hintShowCount: Int = 0, //メイン画面でのポップアップヒント表示回数（カウントアップし、同じものは２度表示しないようにする）
        var battleResult: Byte = 0, // 戦闘結果（0→なし、1→勝ち、2→負け、3→引分）
        var currentStory: String = "" // 現在のストーリー（3桁のファイルID＋3桁の場面ID）
        /** プレイデータ  */
        //add members...
) : ISaveData {

    /**
     * プロパティ
     */
    override val isEmpty: Boolean
        get() = fileCreatedTime == 0L

    /**
     * メソッド
     */
    override fun valid() {
        //数値の上下限判定など
    }

    override fun clone(): ISaveData {
        return this.copy(revisionCount = revisionCount.toMutableMap())
    }
}
