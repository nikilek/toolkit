ゆるゆるツールキット v1.0

[概要]
ToolKitは、Androidアプリ開発を
効率化するためのライブラリ群です

[配布場所]
Bitbucket
https://bitbucket.org/nikilek/toolkit

[使い方ガイド] ※準備中
ゆるゆるっとアプリ開発
https://nikilek.net/toolkit

[ソースコードのライセンス]
Apache License, Version 2.0

[フォントファイルのライセンス]
SIL OPEN FONT LICENSE Version 1.1
※サードパーティ製のフォントを再配布しています
※下記ディレクトリ配下の「README.txt」をご参照ください
・toolkit\src\main\assets\font\kazesawa

[画像ファイルのライセンス]
パブリックドメイン

[備考]
ライセンスの詳細は各「LICENSE.txt」をご参照ください
要約しますと、ライセンスの明示があれば、
商用利用・埋め込み・同梱とも可能となります
※解釈に誤りがありましたらご指摘願います

[連絡先]
nikilek
E-mail：rollingpanda.k@gmail.com
Twitter：https://twitter.com/nikilekroid
